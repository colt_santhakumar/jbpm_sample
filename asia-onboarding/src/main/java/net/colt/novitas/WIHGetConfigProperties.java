package net.colt.novitas;

import java.util.HashMap;
import java.util.Map;

import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.HandlerUtils;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;

/**
 * @author DRao This is a WIH file created by Dharrma Rao.
 * 
 */
public class WIHGetConfigProperties extends BaseWorkItemHandler {

	private static Logger logger = LoggerFactory.getLogger(WIHGetConfigProperties.class);

	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {

		try {
			String logUniqueId = String.valueOf(workItem.getParameter("log_unique_id"));
			logger = getLogger(workItem, logUniqueId);
			EnvironmentProperties prop = HandlerUtils.getEnvironmentProperties(workItem);
            logger.info("Retrived config service properties are :: >>"+prop);
            prop.setLogUniqueId(logUniqueId);
			Map<String, Object> retval = new HashMap<String, Object>();
					
			retval.put("Result", prop);
			manager.completeWorkItem(workItem.getId(), retval);

		} catch (Exception e) {
			logger.error("WIHGetConfigProperties -> Failed", e);
				HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(
						HandlerErrorCode.FAILED_TO_GET_CONFIG_PROPERTIES));
			
		}
	}
	

}