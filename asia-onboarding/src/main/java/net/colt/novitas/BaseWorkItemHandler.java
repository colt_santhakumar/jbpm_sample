package net.colt.novitas;

import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;

public abstract class BaseWorkItemHandler implements WorkItemHandler {
	
	private static final Logger logger = LoggerFactory.getLogger(BaseWorkItemHandler.class);
	
	protected static final String INPROGRESS = "INPROGRESS";
	protected static final String DCA_PORT = "COLT On Demand DCA Port";
	
	protected static final String DCA_PORT_DELETE_SIEBEL_SIGNAL = "delete-dca-port-siebel-signal";
	protected static final String DCA_PORT_CREATE_SIEBEL_SIGNAL = "create-dca-port-siebel-signal";
	
	protected Logger getLogger(WorkItem workItem, String logUniqueId){
		return LoggerFactory.getLogger(logUniqueId+"|ProcessId-"+ workItem.getProcessInstanceId()+"|"+workItem.getName());
	}
	
	protected EnvironmentProperties getEnvProperties(WorkItem workItem){
		return (EnvironmentProperties) workItem.getParameter("env_properties");
	}
	
	protected Integer getCustomerId(WorkItem workItem) {
		Object obj = workItem.getParameter("customer_id");
		Integer requestId = null;
		if (obj instanceof String) {
			requestId = Integer.valueOf(String.valueOf(obj));
		}
		else {
			requestId = (Integer) obj;
		}
		logger.info("Customer Id : " + requestId);

		return requestId;
	}
	

}
