package net.colt.novitas;

import java.util.HashMap;
import java.util.Map;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.HandlerUtils;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;
import com.colt.novitas.siebel.assests.client.SiebelAssetsClient;
import com.colt.novitas.siebel.assets.request.Contact;
import com.colt.novitas.siebel.assets.request.SiebelAssestRequest;

/**
 * @author DRao This is a WIH file created by Dharrma Rao.
 * 
 */
public class WIHSiebelAssestMgmt extends BaseWorkItemHandler {

	private static Logger logger = LoggerFactory.getLogger(WIHSiebelAssestMgmt.class);

	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {

		try {
			
			
			EnvironmentProperties prop = getEnvProperties(workItem);
			logger = getLogger(workItem, prop.getLogUniqueId());
			String[] contact1 = (String[]) workItem.getParameter("contact_1"); 
			String[] contact2 = (String[]) workItem.getParameter("contact_2");
			Integer customerId = (Integer) workItem.getParameter("customer_id");
			String novitasId = (String) workItem.getParameter("novitas_service_id");	
			String ocn  = (String) workItem.getParameter("ocn");
			String productLine  = (String) workItem.getParameter("product_line");
			String localName  = (String) workItem.getParameter("local_name");
			String coltRegion  = (String) workItem.getParameter("colt_region");
			logger.info("Contact-1 info --->"+contact1);
			logger.info("Contact-2 info--->"+contact2);
			SiebelAssestRequest request = new SiebelAssestRequest();
			Map<String, String> attibutesMap = new HashMap<String, String>();
			request.setAttributesMap(attibutesMap);
			attibutesMap.put("ContractedRegion",coltRegion);	
			attibutesMap.put("A00_AccountJPN", localName);
			attibutesMap.put("A99_ProductLine",productLine);
			//request.setServiceId(String.valueOf(customerId));
			request.setServiceId(novitasId);
			request.setOcn(ocn);
			//request.setNovitasServiceId(novitasId);
			request.setStatus("Active");
            request.setSlaId(" ");
			request.setUrlCallback(prop.getCallBackUrl());
			
			request.setProductName("Novitas Umbrella Service");
			request.setResourceId(novitasId);
			List<Contact> contactDetails = new ArrayList<Contact>(); 
			addContactObject(contact1, contactDetails);
			addContactObject(contact2, contactDetails);
			request.setContactDetails(contactDetails);
			String transactionId  = new SiebelAssetsClient(prop.getSiebelUrl(),prop.getSiebelUsername(),
					                                    prop.getSiebelPassword()).receiveAssetMgmtRequest(request);

			Map<String, Object> retval = new HashMap<String, Object>();
			if (StringUtils.isBlank(transactionId)) {
				logger.info("Failed to Notify Siebel Assests Management");
				throw new NovitasHandlerException();
			}
			
			retval.put("Result", transactionId);
			manager.completeWorkItem(workItem.getId(), retval);

		} catch (Exception e) {
			logger.error("WIHSiebelAssestMgmt -> Failed", e);
				HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(
						HandlerErrorCode.NOTIFY_ONBOARDING_SIEBEL_SYSTEM));
			
		}
	}
	
	private void addContactObject(String[] contactArray,List<Contact> contacts){
		
		if(null != contactArray ){
			logger.info("Converting Array Contact info to Object --->"+contactArray);
			Contact contact = new Contact();
			logger.info("Contact array size --->"+contactArray.length);
			if(contactArray.length > 6){
			  contact.setTitle(contactArray[0]);
			  contact.setFirstName(contactArray[1]);
			  contact.setLastName(contactArray[2]);
			  contact.setMobilePhone(contactArray[3]);
			  contact.setLandlinePhone(contactArray[4]);
			  contact.setEmail(contactArray[5]);
			  contact.setPreferredLanguage(contactArray[6]);
			  logger.info("Converted Array Contact info to Object----->"+contact);
			  contacts.add(contact);
			}
		 }
	}

}