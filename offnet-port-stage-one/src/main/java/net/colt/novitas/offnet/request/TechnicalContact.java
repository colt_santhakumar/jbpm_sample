package net.colt.novitas.offnet.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TechnicalContact implements Serializable {

	private static final long serialVersionUID = -1941239526943014645L;

	@JsonProperty("name")
	private String name;

	@JsonProperty("phone")
	private String phone;

	@JsonProperty("email")
	private String email;

	@JsonProperty("company")
	private String company;

	public TechnicalContact() {
	}

	public TechnicalContact(String name, String phone, String email, String company) {
		super();
		this.name = name;
		this.phone = phone;
		this.email = email;
		this.company = company;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	@Override
	public String toString() {
		return "TechnicalContact [name=" + name + ", phone=" + phone + ", email=" + email + ", company=" + company
				+ "]";
	}

}
