package net.colt.novitas;


import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DeleteIPAccessRequest implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty("ocn")
	private String ocn; 

	@JsonProperty("action")
	private String action;
	
	
	@JsonProperty("connection_id") 
	private String connectionId;


	@Override
	public String toString() {
		return "DeleteIPAccessRequest [ocn=" + ocn + ", action=" + action + ", connectionId=" + connectionId + "]";
	}


	public String getOcn() {
		return ocn;
	}


	public void setOcn(String ocn) {
		this.ocn = ocn;
	}


	public String getAction() {
		return action;
	}


	public void setAction(String action) {
		this.action = action;
	}


	public String getConnectionId() {
		return connectionId;
	}


	public void setConnectionId(String connectionId) {
		this.connectionId = connectionId;
	}
	
	
	
	
}
