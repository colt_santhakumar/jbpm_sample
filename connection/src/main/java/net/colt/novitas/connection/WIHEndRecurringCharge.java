package net.colt.novitas.connection;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.httpclient.NoHttpResponseException;
import org.apache.commons.lang.StringUtils;
import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.HandlerUtils;
import com.colt.novitas.billing.client.BillingAPIClient;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;

public class WIHEndRecurringCharge extends BaseWorkItemHandler {
	
	private static Logger logger = LoggerFactory.getLogger(WIHEndRecurringCharge.class);
	
	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}
	

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
		EnvironmentProperties prop = getEnvProperties(workItem);
		logger = getLogger(workItem, prop.getLogUniqueId());
		try {
			String connectionType = (String) workItem.getParameter("connection_type");
			Object chargeResponse = new BillingAPIClient(
					prop.getBillingAPIOneOffUrl(),
					prop.getBillingAPIRecurrUrl(), 
					prop.getBillingAPIUsername(),
					prop.getBillingAPIPassword()).endRecurringCharges(connectionType, getServiceConnectionId(workItem));
			
			logger.info("Recurring charge ended. " + chargeResponse);
			
			if (StringUtils.isBlank(String.valueOf(chargeResponse))) {
				throw new NoHttpResponseException();
			}
			
			Map<String,Object> retval = new HashMap<String,Object>();
			manager.completeWorkItem(workItem.getId(), retval);
			
		} catch (Exception e) {
			logger.error("WIHEndRecurringCharge --> Failed", e);
			HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(HandlerErrorCode.MC_MODIFY_RECURRING_CHARGE));
		}
	}
	
	protected String getServiceConnectionId(WorkItem workItem) {
		Object obj = workItem.getParameter("service_connection_id");
		return String.valueOf(obj);
	}

}
