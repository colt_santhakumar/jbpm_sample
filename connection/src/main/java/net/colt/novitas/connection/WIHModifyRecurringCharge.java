package net.colt.novitas.connection;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.httpclient.NoHttpResponseException;
import org.apache.commons.lang.StringUtils;
import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.HandlerUtils;
import com.colt.novitas.billing.client.BillingAPIClient;
import com.colt.novitas.billing.client.request.ModifyRecurringChargeRequest;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;
import com.colt.novitas.response.ConnectionResponse;
import com.colt.novitas.response.GetConnectionRequestResponse;

public class WIHModifyRecurringCharge extends BaseWorkItemHandler {
	
	private static Logger logger = LoggerFactory.getLogger(WIHModifyRecurringCharge.class);
	
	private WorkItem workItem;
	
	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}
	

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
		
		try {
			this.workItem = workItem; 
			EnvironmentProperties prop = getEnvProperties(workItem);
			logger = getLogger(workItem, prop.getLogUniqueId());
			GetConnectionRequestResponse connRequest = getConnectionRequst(workItem);
			String connectionType = (String) workItem.getParameter("connection_type");
			ModifyRecurringChargeRequest recurringCharge = new ModifyRecurringChargeRequest();
			recurringCharge.setAmount(connRequest.getRentalCharge());
			recurringCharge.setCurrency(connRequest.getRentalCurrency());
			recurringCharge.setFrequency(connRequest.getRentalUnit());
			recurringCharge.setRequestId(connRequest.getId());
			addCommitmentDetails(recurringCharge,connRequest);
			logger.info("Modify Recurring object is ..."+recurringCharge.toString());
			Map<String,Object> retval = new HashMap<String,Object>();
			
			if (recurringCharge.getAmount() != null && recurringCharge.getAmount() > 0) {
				logger.info("Charge is non-zero. Updating...");
				
				Object chargeResponse = new BillingAPIClient(
						prop.getBillingAPIOneOffUrl(),
						prop.getBillingAPIRecurrUrl(), 
						prop.getBillingAPIUsername(),
						prop.getBillingAPIPassword())
						.modifyRecurringCharges(connectionType, connRequest.getConnectionId(), recurringCharge);
				
				logger.info(String.valueOf(chargeResponse));
				
				if (StringUtils.isBlank(String.valueOf(chargeResponse))) {
					throw new NoHttpResponseException();
				}
				retval.put("Result", chargeResponse);
				
			}
			else {
				logger.info("Charge is zero. Skipping...");
			}
			
			manager.completeWorkItem(workItem.getId(), retval);
			
		} catch (Exception e) {
			logger.error("WIHModifyRecurringCharge -> Failed.", e);
			HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(HandlerErrorCode.MC_MODIFY_RECURRING_CHARGE));
		}
	}
	
	private GetConnectionRequestResponse getConnectionRequst(WorkItem workItem) {
		return (GetConnectionRequestResponse) workItem.getParameter("request_obj");
	}
	
	private ConnectionResponse getCustomerConnection(WorkItem workItem) {
		return (ConnectionResponse) workItem.getParameter("customer_connection_obj");
	}
	
	private void addCommitmentDetails(ModifyRecurringChargeRequest recurringReq,GetConnectionRequestResponse connectionReq){
		 
		Integer newCommitmentPeriod = connectionReq.getCommitmentPeriod();
		String existCommitmentExpiry = getCustomerConnection(workItem).getCommitmentExpiryDate();
		
		if(null != newCommitmentPeriod && newCommitmentPeriod > 0) {
			
			if (null!= connectionReq.getCoterminusOption() && connectionReq.getCoterminusOption() && net.colt.novitas.workitems.BaseWorkItemHandler.isCommitmentDateValid(existCommitmentExpiry)) {
				recurringReq.setCommitmentExpiryDate(existCommitmentExpiry);				
			} else {
				recurringReq.setCommitmentExpiryDate(net.colt.novitas.workitems.BaseWorkItemHandler.getCommitmentExpiryDate(connectionReq.getRequestedAt(),newCommitmentPeriod,connectionReq.getRentalUnit()));
			}
			recurringReq.setCommitmentType("COMMIT");
		}		
	}
	

}
