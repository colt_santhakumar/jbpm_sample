package net.colt.novitas.connection;

import java.util.HashMap;
import java.util.Map;

import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.client.RequestAPIClient;
import com.colt.novitas.client.ServiceAPIClient;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;

public class WIHCreateConnectionErrorHandler extends BaseWorkItemHandler {
	
	private WorkItem workItem;
	private EnvironmentProperties prop;
	
	private static Logger logger = LoggerFactory.getLogger(WIHCreateConnectionErrorHandler.class);

	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {

		 Integer requestId = null;
		 Map<String,Object> retval = new HashMap<String,Object>();
		 this.prop = getEnvProperties(workItem);
		 logger = getLogger(workItem, prop.getLogUniqueId());
		try {
			this.workItem = workItem;
			requestId = getRequestId();
			logger.info("Error handling started for create connection request. ID : " + requestId);
			
			//Get error details
			HandlerErrorCode errorCode = null;
			Exception exception = getWorkflowError();
			if (exception instanceof NovitasHandlerException) {
				errorCode = ((NovitasHandlerException) exception).getErrorCode();
				logger.info("Error code : " + errorCode);
			}

			if (errorCode == null) {
				logger.info("NovitasHandlerException thrown without error code. Unable to process further.");
				errorCode = HandlerErrorCode.GENERAL_ERROR_CODE;
			}

			if (errorCode != null) {
				if (errorCode.isFailRequest()) {
				    failRequest(errorCode);
			    }else if (errorCode.isPaymentError()) {
					logger.info("Failed to create or cancel charges. Code : " + errorCode + " request : " + requestId);
					if(HandlerErrorCode.CONNECTION_SMARTS_CALL_BACK_STATUS.equals(errorCode)){
						retval.put("is_smarts_callback_failed", true);
					}else{
						retval.put("payment_failed", "true");
					}
				}
				
				//compensate(errorCode);
				retval.put("payment_error_code", errorCode.getErrorId());
				retval.put("payment_error_description", errorCode.getDescription());
			}
			/*else {
				failRequest(null);
			}*/
		} catch (Exception e) {
			logger.info("Error handling failed for create connection request. ID : " + requestId,e.getMessage());
			manager.abortWorkItem(workItem.getId());
		}
		logger.info("Error handling completed for request : " + requestId);
		manager.completeWorkItem(workItem.getId(), retval);
	}
	
	protected void compensate(HandlerErrorCode errorCode) {
		logger.info("Calling compensation with error code : " + errorCode);
	}
	
	protected void failRequest(HandlerErrorCode errorCode) {

		/*Integer requestId = null;

		try {
			
			requestId = getRequestId();
            String connectionId = (String) workItem.getParameter("connection_id");
			RequestAPIClient client = getRequestClient();
			UpdateRequestStatusRequest requestObject = new UpdateRequestStatusRequest();
			requestObject.setStatus("FAILED");
            if(StringUtils.isNotBlank(connectionId))requestObject.setServiceId(connectionId);
			if (errorCode != null) {
				requestObject.setStatusCode(errorCode.getErrorId());
			}
			client.updatePortorConnectionRequest(requestId, requestObject);
		} catch (Exception e) {
			logger.error("Failed to update request to FAILED state. Request Id : " + requestId);
		}*/

	}
	
	
	private Exception getWorkflowError() {
		Object obj = workItem.getParameter("error_object");
		if (obj instanceof Exception) {
			return (Exception) obj;
		}
		return null;
	}
	
	protected Integer getRequestId() {
		Object obj = workItem.getParameter("request_id");
		Integer requestId = null;
		if (obj instanceof String) {
			requestId = Integer.valueOf(String.valueOf(obj));
		}
		else {
			requestId = (Integer) obj;
		}
		logger.info("Request Id : " + requestId);

		return requestId;
	}
	

	protected RequestAPIClient getRequestClient() {

		return new RequestAPIClient(
				prop.getRequestAPIUrl(),
				prop.getRequestAPIUsername(),
				prop.getRequestAPIPassword()
				);
	}

	protected ServiceAPIClient getServiceClient() {

		return new ServiceAPIClient(prop.getServiceAPIBaseUrl(), 
                prop.getServiceAPIUsername(), 
                prop.getServiceAPIPassword());
	}

}
