package net.colt.novitas.connection;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.HandlerUtils;
import com.colt.novitas.client.ServiceAPIClient;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;
import com.colt.novitas.request.RequestVLANIdRange;
import com.colt.novitas.request.UpdateConnRequest;
import com.colt.novitas.response.ConnectionResponse;
import com.colt.novitas.response.GetConnectionRequestResponse;
import com.colt.novitas.response.ServiceVLANIdRange;

public class WIHUpdateCustomerConnection extends BaseWorkItemHandler {

	private static Logger logger = LoggerFactory.getLogger(WIHUpdateCustomerConnection.class);

	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}


	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {

		String status = null;
		Boolean isVlanModifyFlag = false;

		try {
			EnvironmentProperties prop = getEnvProperties(workItem);
			logger = getLogger(workItem, prop.getLogUniqueId());
			String connectionId =  getConnectionId(workItem);
			status = getStatus(workItem);
			isVlanModifyFlag = getIsVlanModifyFlag(workItem);
			ConnectionResponse customerConnection = (ConnectionResponse)workItem.getParameter("customer_conn_obj");

			UpdateConnRequest connReq = new UpdateConnRequest();
			connReq.setStatus(status);
			//update resource id if provided
			String ncTechServId = getNcTechServiceId(workItem);
			String circuitId = String.valueOf(workItem.getParameter("circuit_reference"));

			if (ncTechServId != null) {
				connReq.setNcTechServiceId(ncTechServId);
			}

			if (circuitId != null) {
				connReq.setResourceId(circuitId);
			}

			//for charges
			GetConnectionRequestResponse connectionRequest = getPortRequst(workItem);
			if ( fullUpdate(workItem) && connectionRequest != null) {
				connReq.setBandwidth(connectionRequest.getBandwidth());
				connReq.setRentalCharge(connectionRequest.getRentalCharge());
				connReq.setRentalCurrency(connectionRequest.getRentalCurrency());
				connReq.setRentalUnit(connectionRequest.getRentalUnit());
			}

			logger.info("isVlanModifyFlag: " + isVlanModifyFlag);
			if ("ACTIVE".equals(status) && null != isVlanModifyFlag && !isVlanModifyFlag) {
				if (null != connectionRequest.getRentalCharge()) {
					logger.info("Set base rental: " + connectionRequest.getRentalCharge());
					connReq.setBaseRental(connectionRequest.getRentalCharge());
				}
				if (null != connectionRequest.getBandwidth()) {
					logger.info("Set base bandwidth : " + connectionRequest.getBandwidth());
					connReq.setBaseBandwidth(connectionRequest.getBandwidth());
				}
			}

			if ("ACTIVE".equals(status) && null != connectionRequest.getCommitmentPeriod() && connectionRequest.getCommitmentPeriod() > 0) {
				connReq.setCommitmentPeriod(connectionRequest.getCommitmentPeriod());

				String commitmentExpiryStartDate = connectionRequest.getRequestedAt();

				if (connectionRequest.getCoterminusOption() != null && connectionRequest.getCoterminusOption() 
						&& net.colt.novitas.workitems.BaseWorkItemHandler.isCommitmentDateValid(customerConnection.getCommitmentExpiryDate())) {
					//will maintain existing commitment expiry date
					commitmentExpiryStartDate = customerConnection.getCommitmentExpiryDate();
					connReq.setCommitmentExpiryDate(commitmentExpiryStartDate);

				} else {
					connReq.setCommitmentExpiryDate(net.colt.novitas.workitems.BaseWorkItemHandler.getCommitmentExpiryDate(commitmentExpiryStartDate, connectionRequest.getCommitmentPeriod(), connectionRequest.getRentalUnit()));
				}
			}

			//reset the VLAN properties
			if ("MODIFYING".equals(status)) {
				//VLAN properties
				connReq.setFromVlanMapping(connectionRequest.getFromVlanMapping());
				connReq.setToVlanMapping(connectionRequest.getToVlanMapping());
				connReq.setFromVlanType(connectionRequest.getFromVlanType());
				connReq.setToVlanType(connectionRequest.getToVlanType());
				connReq.setFromPortVLANIdRange(getServiceVLANIds(connectionRequest.getFromPortVLANIdRange()));
				connReq.setToPortVLANIdRange(getServiceVLANIds(connectionRequest.getToPortVLANIdRange()));
			}

			Object result = new ServiceAPIClient(prop.getServiceAPIBaseUrl(),
					prop.getServiceAPIUsername(),
					prop.getServiceAPIPassword()).updateConnection(connectionId, connReq );

			logger.info("Results of update service conn : "  + result);
			if (result != null && "FAILED".equals(String.valueOf(result))) {
				throw new NovitasHandlerException();
			}

			Map<String,Object> retval = new HashMap<String,Object>();
			manager.completeWorkItem(workItem.getId(), retval);

		} catch (Exception e) {
			logger.error("WIHUpdateCustomerConnection - >  failed ", e);
			if ("ACTIVE".equals(status)) {
				HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(HandlerErrorCode.CC_UPDATE_SERVICE_ACTIVE_CONN_ID));
			}
			else if ("MODIFYING".equals(status)) {
				HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(HandlerErrorCode.MC_UPDATE_SERVICE_MODIFYING));
			}
			else if ("DECOMMISSIONING".equals(status)) {
				HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(HandlerErrorCode.DC_UPDATE_SERVICE_DECOMMISSIONING));
			}
			else if ("DECOMMISSIONED".equals(status)) {
				HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(HandlerErrorCode.DC_UPDATE_SERVICE_DECOMMISSIONED));
			}
		}
	}

	protected String getStatus(WorkItem workItem) {
		return (String) workItem.getParameter("status");
	}

	protected String getConnectionId(WorkItem workItem) {
		return (String) workItem.getParameter("connection_id");
	}

	protected String getNcTechServiceId(WorkItem workItem) {
		return (String) workItem.getParameter("nc_tech_service_id");
	}

	protected Boolean getIsVlanModifyFlag(WorkItem workItem) {
		return (Boolean) workItem.getParameter("is_vlan_modify");
	}

	//Use for update charges
	protected GetConnectionRequestResponse getPortRequst(WorkItem workItem) {
		return (GetConnectionRequestResponse) workItem.getParameter("request_obj");
	}

	protected boolean fullUpdate(WorkItem workItem) {
		Object obj = workItem.getParameter("full_update");
		if (obj != null && obj instanceof String) {
			return "true".equalsIgnoreCase(String.valueOf(obj));
		}
		return false;
	}

	protected List<ServiceVLANIdRange> getServiceVLANIds(List<RequestVLANIdRange> requestRanges) {

		List<ServiceVLANIdRange> serviceRanges = new ArrayList<ServiceVLANIdRange>();
		if (requestRanges != null && ! requestRanges.isEmpty()) {
			for (RequestVLANIdRange requestVLANIdRange : requestRanges) {
				serviceRanges.add(new ServiceVLANIdRange(requestVLANIdRange.getFromIdRange(), requestVLANIdRange.getToIdRange()));
			}
		}

		return serviceRanges;

	}


	private static String getCommitmentExpiryDate(String startDate,Integer commitmentPeriod,String frequency){

		DateFormat fdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
		fdf.setTimeZone(TimeZone.getTimeZone("UTC"));
		Calendar c = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		if (startDate != null) {

			try {
				c.setTime(fdf.parse(startDate));
			} catch (ParseException e) {

				e.printStackTrace();
			}
			c.add(Calendar.MONTH, commitmentPeriod);
			if ("MONTHLY".equals(frequency)) {
				c.set(Calendar.HOUR_OF_DAY,
						c.getActualMaximum(Calendar.HOUR_OF_DAY));
			}
			c.set(Calendar.MINUTE, c.getActualMaximum(Calendar.MINUTE));
			c.set(Calendar.SECOND, c.getActualMaximum(Calendar.SECOND));

			return fdf.format(c.getTime());

		} else {
			return "";
		}
	}
}
