package net.colt.novitas.connection;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Random;

import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.request.RequestVLANIdRange;
import com.colt.novitas.response.ServiceVLANIdRange;

public abstract class BaseWorkItemHandler implements WorkItemHandler {
	
	private static Logger logger = LoggerFactory.getLogger(BaseWorkItemHandler.class);
	
	protected static final String INPROGRESS = "INPROGRESS";
	protected static final String DCA_PORT = "COLT On Demand DCA Port";
	
	protected static final String DCA_PORT_DELETE_SIEBEL_SIGNAL = "delete-dca-port-siebel-signal";
	protected static final String DCA_PORT_CREATE_SIEBEL_SIGNAL = "create-dca-port-siebel-signal";
	
    protected static String LOG_UNIQUE_ID = null;
	
	protected Logger getLogger(WorkItem workItem, String uniqueLogId){
		if(null == LOG_UNIQUE_ID) {
			LOG_UNIQUE_ID = uniqueLogId;
			if(null != uniqueLogId) {
				MDC.put("unique_log_id", "J" + uniqueLogId.substring(1));
			} else {
				Random rand = new Random(); 
				MDC.put("unique_log_id", "J"+String.valueOf(rand.nextInt(1000)));
			}
		}
		logger = LoggerFactory.getLogger(uniqueLogId+"|ProcessId-"+ workItem.getProcessInstanceId()+"|"+workItem.getName());
		return logger;
	}
	
	protected EnvironmentProperties getEnvProperties(WorkItem workItem){
		return (EnvironmentProperties) workItem.getParameter("env_properties");
	}
	
	protected Integer getRequestId(WorkItem workItem) {
		Object obj = workItem.getParameter("request_id");
		Integer requestId = null;
		if (obj instanceof String) {
			requestId = Integer.valueOf(String.valueOf(obj));
		}
		else {
			requestId = (Integer) obj;
		}
		logger.info("Request Id : " + requestId);

		return requestId;
	}
	
	 protected String getJbpmUrl(String url){
		try {
			String host = InetAddress.getLocalHost().getHostName();
			url = url.replaceAll("%s", host);
		} catch (UnknownHostException e) {
			logger.error(e.getMessage());
		}
		return url;
	}
	
	 
	 public static String getStringFromList(List<RequestVLANIdRange> requestVLANIdRanges) {

			if (requestVLANIdRanges == null || requestVLANIdRanges.isEmpty()) {
				return null;
			}

			StringBuilder sb = new StringBuilder();
			for (RequestVLANIdRange requestVLANIdRange : requestVLANIdRanges) {
				if (sb.length() > 0) {
					sb.append(",");
				}
				

				if (null != requestVLANIdRange.getFromIdRange() && null != requestVLANIdRange.getToIdRange()
					&& !requestVLANIdRange.getFromIdRange().equals(requestVLANIdRange.getToIdRange())) {
					
					sb.append("[").append(requestVLANIdRange.getFromIdRange()).append("-").append(requestVLANIdRange.getToIdRange()).append("]");
				}else if (null != requestVLANIdRange.getFromIdRange())
					sb.append(requestVLANIdRange.getFromIdRange());

			}
			logger.info("Converted VLANs--> " + sb.toString());
			return sb.toString();
		}
	 
	 public static String getStringFromServiceList(List<ServiceVLANIdRange> requestVLANIdRanges) {

			if (requestVLANIdRanges == null || requestVLANIdRanges.isEmpty()) {
				return null;
			}

			StringBuilder sb = new StringBuilder();
			for (ServiceVLANIdRange requestVLANIdRange : requestVLANIdRanges) {
				if (sb.length() > 0) {
					sb.append(",");
				}
				

				if (null != requestVLANIdRange.getFromIdRange() && null != requestVLANIdRange.getToIdRange()
					&& !requestVLANIdRange.getFromIdRange().equals(requestVLANIdRange.getToIdRange())) {
					
					sb.append("[").append(requestVLANIdRange.getFromIdRange()).append("-").append(requestVLANIdRange.getToIdRange()).append("]");
				}else if (null != requestVLANIdRange.getFromIdRange())
					sb.append(requestVLANIdRange.getFromIdRange());

			}
			logger.info("Converted VLANs--> " + sb.toString());
			return sb.toString();
		}
}
