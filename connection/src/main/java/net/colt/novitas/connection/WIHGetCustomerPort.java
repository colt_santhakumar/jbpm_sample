package net.colt.novitas.connection;

import java.util.HashMap;
import java.util.Map;

import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.client.ServiceAPIClient;
import com.colt.novitas.response.CustomerDedicatedPortResponse;
import com.colt.novitas.response.GetConnectionRequestResponse;

public class WIHGetCustomerPort extends BaseWorkItemHandler {
	
	private static Logger logger = LoggerFactory.getLogger(WIHGetCustomerPort.class);
	
	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
		EnvironmentProperties prop = getEnvProperties(workItem);
		logger = getLogger(workItem, prop.getLogUniqueId());
		try {
			GetConnectionRequestResponse connectionRequest = getPortRequst(workItem);
			String portId = null;
			String portDirection = getPortDirection(workItem);
			if ("to".equalsIgnoreCase(portDirection) ) {
				portId = connectionRequest.getToPort();
			}
			else {
				portId = connectionRequest.getFromPort();
			}
			
			Object result = new ServiceAPIClient(prop.getServiceAPIBaseUrl(), 
								                    prop.getServiceAPIUsername(), 
								                    prop.getServiceAPIPassword()).getPort(portId); 
			
			if (result instanceof CustomerDedicatedPortResponse) {
				logger.info("Get customer port call successful :" + result);
			}
			else {
				logger.info("Get customer port call failed :" + result);
			}
			
			Map<String,Object> retval = new HashMap<String,Object>();
			retval.put("Result", result);
			manager.completeWorkItem(workItem.getId(), retval);
			
		} catch (Exception e) {
			logger.error("WIHGetCustomerPort -> Fail : ", e);
			manager.abortWorkItem(workItem.getId());
		}
	}
	
	protected GetConnectionRequestResponse getPortRequst(WorkItem workItem) {
		return (GetConnectionRequestResponse) workItem.getParameter("request_obj");
	}
	
	protected String getPortDirection(WorkItem workItem) {
		return (String) workItem.getParameter("port_direction");
	}

}
