package net.colt.novitas.connection.util;

import org.kie.api.runtime.process.WorkItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.client.ServiceAPIClient;
import com.colt.novitas.response.ConnectionResponse;

public class ConnectionUtil {
	
	private static Logger logger = LoggerFactory.getLogger(ConnectionUtil.class);
	
	public static String getSlaIdForConnection(String connectionId,WorkItem workItem){
		
		EnvironmentProperties prop = (EnvironmentProperties) workItem.getParameter("env_properties");
		logger = LoggerFactory.getLogger(prop.getLogUniqueId()+"|ProcessId-"+ workItem.getProcessInstanceId()+"|"+workItem.getName());
		
		ConnectionResponse response = new ServiceAPIClient(prop.getServiceAPIBaseUrl(), 
                                                           prop.getServiceAPIUsername(), 
                                                           prop.getServiceAPIPassword()).getConnection(connectionId );
		
		
		if(null != response && null !=response.getSlaId()){
			logger.info("Create Connection SLAID :" + response.getSlaId());
			return response.getSlaId();
		}
		return null;
		
	}

}
