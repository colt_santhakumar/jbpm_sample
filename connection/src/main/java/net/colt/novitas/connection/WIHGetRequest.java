package net.colt.novitas.connection;

import java.util.HashMap;
import java.util.Map;

import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.HandlerUtils;
import com.colt.novitas.WorkflowType;
import com.colt.novitas.client.RequestAPIClient;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;
import com.colt.novitas.response.GetConnectionRequestResponse;

public class WIHGetRequest extends BaseWorkItemHandler {
	
	private static Logger logger = LoggerFactory.getLogger(WIHGetRequest.class);
	
	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}
	
	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
		EnvironmentProperties prop = getEnvProperties(workItem);
		logger = getLogger(workItem, prop.getLogUniqueId());
		try {
			Integer requestId = getRequestId(workItem);
			
			Object requestObj = new RequestAPIClient(
					prop.getRequestAPIUrl(),
					prop.getRequestAPIUsername(),
					prop.getRequestAPIPassword()
					).getPortorConnectionRequest(requestId);
			
			logger.info("WIHGetRequest -> Response: " + requestObj.toString());
			
			if (requestObj instanceof GetConnectionRequestResponse) {
				GetConnectionRequestResponse connReq = (GetConnectionRequestResponse) requestObj;
				
				Map<String,Object> retval = new HashMap<String,Object>();
				retval.put("Result", connReq);
				retval.put("service_from_port_id", connReq.getFromPort());
				retval.put("service_to_port_id", connReq.getToPort());
				retval.put("request_bandwidth", connReq.getBandwidth());
				retval.put("service_connection_id", connReq.getConnectionId());
				retval.put("novitas_service_id", connReq.getServiceId());
				retval.put("ocn", connReq.getOcn());
				retval.put("customer_name", connReq.getCustomerName());
				retval.put("portal_user_id", connReq.getPortalUserId());
				manager.completeWorkItem(workItem.getId(), retval);
				
			}
			else {
				throw new Exception("Invalid connection request.");
			}
			
		} catch (Exception e) {
			logger.error("WIHGetRequest -> Error: ", e);
			HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(getErrorCode(workItem)));
		}
		
	}
	
	protected HandlerErrorCode getErrorCode(WorkItem workItem) {
		WorkflowType type = HandlerUtils.getWorkflowType(workItem);
		if (type == WorkflowType.MODIFY_CONNECTION) {
			return HandlerErrorCode.MC_INVALID_REQUEST;
		} 
		else if (type == WorkflowType.DELETE_CONNECTION) {
			return HandlerErrorCode.DC_INVALID_REQUEST;
		}
		return HandlerErrorCode.CC_INVALID_REQUEST;
	}
	
	protected Integer getRequestId(WorkItem workItem) {
		Object obj = workItem.getParameter("request_id");
		Integer requestId = null;
		if (obj instanceof String) {
			requestId = Integer.valueOf(String.valueOf(obj));
		}
		else {
			requestId = (Integer) obj;
		}
		logger.info("Request Id : " + requestId);

		return requestId;
	}

}
