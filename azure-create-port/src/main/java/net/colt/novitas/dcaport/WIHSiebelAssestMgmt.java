package net.colt.novitas.dcaport;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.HandlerUtils;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;
import com.colt.novitas.response.CloudPortRequestResponse;
import com.colt.novitas.siebel.assests.client.SiebelAssetsClient;
import com.colt.novitas.siebel.assets.request.SiebelAssestRequest;
import com.colt.novitas.siebel.assets.request.SlaIdType;
import net.colt.novitas.workitems.response.LocationResponse;

/**
 * @author DRao This is a WIH file created by Dharrma Rao.
 * 
 */
public class WIHSiebelAssestMgmt extends BaseWorkItemHandler {

	private static Logger logger = LoggerFactory.getLogger(WIHSiebelAssestMgmt.class);

	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
		EnvironmentProperties prop = getEnvProperties(workItem);
		logger = getLogger(workItem, prop.getLogUniqueId());
		try {

			CloudPortRequestResponse portRequestResponse = getPortRequest(workItem);
			String servicePortId = getNovitasPortServiceId(workItem);
			String status = getStatus(workItem);			
			String circuitReference = String.valueOf(workItem.getParameter("circuit_reference"));
			String bandwidth = getBandwidth(workItem);
			SiebelAssestRequest request = new SiebelAssestRequest();
			request.setPamFlag("Y");
			request.setSlaId(SlaIdType.SLA_01.value());
			request.setServiceId(servicePortId);
			request.setServiceName(portRequestResponse.getPortName());
			request.setOcn(portRequestResponse.getOcn());
			request.setNovitasServiceId(portRequestResponse.getServiceId());
			request.setStatus(status);
			String localName = String.valueOf(workItem.getParameter("local_name"));
			String nms = String.valueOf(workItem.getParameter("nms"));
			String localBuildingName = String.valueOf(workItem.getParameter("local_building_name"));
			String buildingName = String.valueOf(workItem.getParameter("building_name"));
			request.setProductName("COLT On Demand DCA Port");			
			request.setResourceId(circuitReference);
			
			Map<String, String> attibutesMap = new HashMap<String, String>();
			attibutesMap.put("bandwidth", bandwidth);
			attibutesMap.put("technology", portRequestResponse.getProductName());
			attibutesMap.put("csp_name", "Microsoft Azure");
			attibutesMap.put("resource_id", circuitReference);

			if("Active".equals(status)){

				LocationResponse locationResponse = getLocationResponse(workItem);
				attibutesMap.put("location", locationResponse.getSiteName());
				attibutesMap.put("address", locationResponse.getAddress());

				attibutesMap.put("rental_charge",portRequestResponse.getRentalCharge().toString());
				attibutesMap.put("rental_unit", portRequestResponse.getRentalUnit());
				attibutesMap.put("rental_currency",portRequestResponse.getRentalCurrency());

				if("PO".equals(nms) || "ASIA".equals(nms)){
					attibutesMap.put("A00_AccountJPN", localName);
					attibutesMap.put("A03_A-BuildingName", buildingName);
					attibutesMap.put("A04_B-BuildingName", buildingName);
					attibutesMap.put("A06_B-AccountName", localName);
					attibutesMap.put("A16_A03_A-BuildingNameJPN", localBuildingName);
					attibutesMap.put("A17_B-BuildingNameJPN", localBuildingName);
					attibutesMap.put("A18_ServiceJPN", "オンデマンド");
					attibutesMap.put("A05_A-AccountName", portRequestResponse.getCustomerName());
					attibutesMap.put("Service_Type", request.getProductName());
					attibutesMap.put("A08_STTEnable", "No");
					attibutesMap.put("A09_BillingDate", getFirstOfFollowingMonth());
					attibutesMap.put("A14_A-AccountNameJPN", localName);
			   }	
			}else{
				attibutesMap.put("decommissioning_charge", portRequestResponse.getDecommissioningCharge().toString());
				attibutesMap.put("decommissioning_currency", portRequestResponse.getDecommissioningCurrency());
				attibutesMap.put("decommissioned_on", new Date().toString());
				attibutesMap.put("A19_RequestedDisconnectDate", portRequestResponse.getRequestedAt());
			}

			attibutesMap.put("commitment_expiry", portRequestResponse.getCommitmentPeriod().toString());
			
			attibutesMap.put("created_date", portRequestResponse.getRequestedAt());
			request.setAttributesMap(attibutesMap);
			
			
			request.setUrlCallback(prop.getCallBackUrl());
			
			String transactionId  = new SiebelAssetsClient(prop.getSiebelUrl(),prop.getSiebelUsername(),
					                                    prop.getSiebelPassword()).receiveAssetMgmtRequest(request);

			Map<String, Object> retval = new HashMap<String, Object>();
			if (StringUtils.isBlank(transactionId)) {
				logger.info("Failed to Update Siebel Assests Management");
				throw new NovitasHandlerException();
			}
			
			retval.put("Result", transactionId);
			manager.completeWorkItem(workItem.getId(), retval);

		} catch (Exception e) {
			logger.error("WIHSiebelAssestMgmt -> Failed", e);
				HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(
						HandlerErrorCode.CREATE_OR_UPDATE_SIEBEL_ASSETS_MGMT));
			
		}
	}
	
	private String getFirstOfFollowingMonth(){
		Calendar calendar = Calendar.getInstance();         
		calendar.add(Calendar.MONTH, 1);
		calendar.set(Calendar.DATE, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
		Date nextMonthFirstDay = calendar.getTime();
		return nextMonthFirstDay.toString();
	}
	

}
