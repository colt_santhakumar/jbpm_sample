package net.colt.novitas.dcaport;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.billing.client.BillingAPIClient;
import com.colt.novitas.billing.client.request.OneOffChargeRequest;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;
import com.colt.novitas.response.CloudPortRequestResponse;

public class WIHInstallationCharge extends BaseWorkItemHandler {
	
	private static Logger logger = LoggerFactory.getLogger(WIHInstallationCharge.class);
	
	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}
	

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
		
		EnvironmentProperties prop = getEnvProperties(workItem);
		logger = getLogger(workItem, prop.getLogUniqueId());
		try {
			
			Integer requestId = getRequestId(workItem);
			CloudPortRequestResponse portRequest = getPortRequest(workItem);
			String dedicatedPort = getNovitasPortServiceId(workItem);
			
			OneOffChargeRequest oneoff = new OneOffChargeRequest();
			oneoff.setRequestId(requestId);
			oneoff.setServiceId(portRequest.getServiceId());
			oneoff.setServiceInstanceId(dedicatedPort);
			oneoff.setServiceInstanceType("DCAPORT");
			oneoff.setBcn(portRequest.getBcn());
			oneoff.setChargeType("INSTALLATION");
			oneoff.setDescription("Installation for port " + portRequest.getPortName());
			oneoff.setCurrency(portRequest.getInstallationCurrency());
			oneoff.setAmount(portRequest.getInstallationCharge());	
		    oneoff.setServiceInstanceName(portRequest.getPortName());
		    
			Object chargeResponse = new BillingAPIClient(
					prop.getBillingAPIOneOffUrl(),
					prop.getBillingAPIRecurrUrl(), 
					prop.getBillingAPIUsername(),
					prop.getBillingAPIPassword()).createOneOffCharges(oneoff);
			
			logger.info(String.valueOf(chargeResponse));
			
			if (StringUtils.isBlank(String.valueOf(chargeResponse))) {
				logger.info("Failed to create Installation charges");
				throw new NovitasHandlerException();
			}
			
			Map<String,Object> retval = new HashMap<String,Object>();
			retval.put("Result", chargeResponse);
			manager.completeWorkItem(workItem.getId(), retval);
			
		} catch (Exception e) {
			logger.error("WIHInstallationCharge -> Failed", e);
			throw new NovitasHandlerException(HandlerErrorCode.CP_PORT_INSTALL_CHARGE);
		}
	}
}
