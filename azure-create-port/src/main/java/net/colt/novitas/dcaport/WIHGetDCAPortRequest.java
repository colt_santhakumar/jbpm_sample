package net.colt.novitas.dcaport;

import java.util.HashMap;
import java.util.Map;

import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.HandlerUtils;
import com.colt.novitas.client.RequestAPIClient;
import com.colt.novitas.exception.NovitasHandlerException;
import com.colt.novitas.response.CloudPortRequestResponse;


public class WIHGetDCAPortRequest extends BaseWorkItemHandler {
	
	private static  Logger logger = LoggerFactory.getLogger(WIHGetDCAPortRequest.class);
	
	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}
	
	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
		Integer requestId = getRequestId(workItem);
		
		try {
			CloudPortRequestResponse cloudPortResponse  = null;
			EnvironmentProperties prop = getEnvProperties(workItem);
			logger = getLogger(workItem, prop.getLogUniqueId());
			Object cloudPortResponseObj = new RequestAPIClient(
											prop.getRequestAPIUrl(),
											prop.getRequestAPIUsername(),
											prop.getRequestAPIPassword()
											).getCloudPortOrConnectionRequest(requestId);
			
			logger.info("Response: " + cloudPortResponseObj.toString());
			
			if (cloudPortResponseObj instanceof CloudPortRequestResponse) {
				cloudPortResponse = (CloudPortRequestResponse) cloudPortResponseObj;
			}
			else {
				logger.info("Invalid port request id : " + requestId);
				throw new NovitasHandlerException();
			}
			
			Map<String,Object> retval = new HashMap<String,Object>();
			retval.put("Result", cloudPortResponse);
			
			if (cloudPortResponse.getPortId() != null) {
				retval.put("service_port_id", cloudPortResponse.getPortId());
			}
			retval.put("ocn", cloudPortResponse.getOcn());
			manager.completeWorkItem(workItem.getId(), retval);
			
		} catch (Exception e) {
			logger.error("WIHGetDCAPortRequest -> Failed", e);
			  HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(getErrorCode(workItem)));
		}
			
	}

}