package net.colt.novitas.dcaport;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.client.RequestAPIClient;
import com.colt.novitas.client.ServiceAPIClient;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;
import com.colt.novitas.request.UpdatePortRequest;
import com.colt.novitas.request.UpdateRequestStatusRequest;


public class WIHDCACreatePortErrorHandler extends BaseWorkItemHandler {
	
	private static Logger logger = LoggerFactory.getLogger(WIHDCACreatePortErrorHandler.class);

	private WorkItem workItem;
	private EnvironmentProperties prop;

	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
        
		EnvironmentProperties prop = getEnvProperties(workItem);
		logger = getLogger(workItem, prop.getLogUniqueId());
		
		try {
			logger.info("Starting error handling for creat port request......... ");

			this.workItem = workItem;
			this.prop = getEnvProperties(workItem);
			Integer requestId = getRequestId(workItem);

			logger.info("Starting error handling for request : " + requestId);

			Map<String,Object> retval = new HashMap<String,Object>();
			
			
			HandlerErrorCode errorCode = null;
			Exception exception = getWorkflowError(workItem);
			if (exception instanceof NovitasHandlerException) {
				errorCode = ((NovitasHandlerException) exception).getErrorCode();
				logger.info("Error code : " + errorCode);
			}

			if (errorCode == null) {
				logger.info("NovitasHandlerException thrown without error code. Unable to process further.");
				errorCode = HandlerErrorCode.GENERAL_ERROR_CODE;
			}

			logger.info("Failed to create or cancel charges. Code : " + errorCode + " request : " + requestId);
			if(errorCode.isPaymentError()){
			   retval.put("payment_failed", "true");
		    }
			retval.put("payment_error_code", errorCode.getErrorId());
			retval.put("payment_error_description", errorCode.getDescription());
			logger.info("Error handling completed for request : " + requestId);

			manager.completeWorkItem(workItem.getId(), retval);
			
			
		} catch (Exception e) {
			logger.error("Error handling for DCA creat port has been failed.", e);
			manager.abortWorkItem(workItem.getId());
		}

	}


	protected void failRequest(HandlerErrorCode errorCode) {

		Integer requestId = null;

		try {
			
			requestId = getRequestId(workItem);
            String portid = (String) workItem.getParameter("dedicated_port_id");
			RequestAPIClient client = getRequestClient();
			UpdateRequestStatusRequest requestObject = new UpdateRequestStatusRequest();
			
			requestObject.setStatus("FAILED");
            if(StringUtils.isNotBlank(portid))requestObject.setServiceId(portid);
			if (errorCode != null ) {
				if(errorCode.getErrorId() == 270) requestObject.setStatus("REJECTED");
				requestObject.setStatusCode(errorCode.getErrorId());
			}

			client.updatePortorConnectionRequest(requestId, requestObject);
		} catch (Exception e) {
			logger.error("Failed to update request to FAILED state. Request Id : " + requestId, e);
		}
	}


	protected void compensate(HandlerErrorCode errorCode) {
		
		//switch failed to compile on kie container
		if (errorCode == HandlerErrorCode.CP_UPDATE_SERVICE_PORT_DETAILS) {
			String resoucePortId = null;

			try {
				
				logger.info("Succesfully rollback to  AVAILABLE status failed for resource port : " + resoucePortId );
			} catch (Exception e) {
				logger.error("Rollback to  AVAILABLE status failed for resource port : " + resoucePortId, e );
			}
		}
		
		if (errorCode == HandlerErrorCode.CP_UPADTE_RESOURCE_PORT_ALLOCATED) {
			String servicePortId = null; 
			try {
				//servicePortId = getDedicatedPortId(workItem);
				UpdatePortRequest portReq = new UpdatePortRequest();
				portReq.setStatus("DECOMMISSIONED");

				logger.info("Rollbacking service port status to  DECOMMISSIONED. Port : " + servicePortId);
				getServiceClient().updatePort(servicePortId, portReq);
				logger.info("Successsfully rollbacked service port status to  DECOMMISSIONED. Port : " + servicePortId);

			} catch (Exception e) {
				logger.error("Rollback to  DECOMMISSIONED status failed for service port : " + servicePortId, e);
			}
		}

	}


	protected RequestAPIClient getRequestClient() {

		return new RequestAPIClient(
				prop.getRequestAPIUrl(),
				prop.getRequestAPIUsername(),
				prop.getRequestAPIPassword()
				);
	}

	protected ServiceAPIClient getServiceClient() {

		return new ServiceAPIClient(
				prop.getServiceAPIBaseUrl(),
				prop.getServiceAPIUsername(),
				prop.getServiceAPIPassword()
				);
	}

	


}
