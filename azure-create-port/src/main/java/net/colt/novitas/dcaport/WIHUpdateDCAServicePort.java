package net.colt.novitas.dcaport;

import java.util.HashMap;
import java.util.Map;

import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.HandlerUtils;
import com.colt.novitas.client.ServiceAPIClient;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;
import com.colt.novitas.request.UpdateCloudPortRequest;
import net.colt.novitas.workitems.response.LocationResponse;

public class WIHUpdateDCAServicePort extends BaseWorkItemHandler {
	
	private static Logger logger = LoggerFactory.getLogger(WIHUpdateDCAServicePort.class);
	
	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}
	

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
		EnvironmentProperties prop = getEnvProperties(workItem);
		logger = getLogger(workItem, prop.getLogUniqueId());
		try {

			String dedicatedPortId = getNovitasPortServiceId(workItem);
            LocationResponse location = getLocationResponse(workItem);
            String ncTechSrvcId_1 = String.valueOf(workItem.getParameter("nc_tech_service_id_1"));
            String ncTechSrvcId_2 = String.valueOf(workItem.getParameter("nc_tech_service_id_2"));
            String circuit1 = String.valueOf(workItem.getParameter("circuit_reference_1"));
            String circuit2 = String.valueOf(workItem.getParameter("circuit_reference_2"));
            UpdateCloudPortRequest req = new UpdateCloudPortRequest();
			req.setStatus(getStatus(workItem));
			req.setNcTechServiceId1(ncTechSrvcId_1);
			req.setNcTechServiceId2(ncTechSrvcId_2);
			
			req.setResourceId1(circuit1);
			req.setResourceId2(circuit2);
			
			if(null != location){
				req.setLocationBuildingName(location.getBuildingName());
				req.setLocationStreetName(location.getStreetName());
				req.setLocationCity(location.getCity());
	            req.setLocationCountry(location.getCountry());
	            req.setLatitude(location.getLatitude());
	            req.setLongitude(location.getLongitude());
	            req.setSiteFloor(location.getFloor());   
	            req.setSiteRoomName(location.getRoomName());
	            req.setLocationPremisesNumber(location.getPremisesNumber());
	            req.setPostalZipCode(location.getPostalZipCode());
	            req.setLocationState(location.getState());
				req.setLocationId(location.getSiteId());
				req.setAddress(location.getAddress());
				req.setSiteType(location.getNovitasSiteType());
				req.setLocalBuildingName(location.getLocalBuildingName());
				req.setLocationCityCode(location.getCityCode());
	            req.setLocationCountryCode(location.getCountryCode());
			}
			
			Object response = new ServiceAPIClient(prop.getServiceAPIBaseUrl(), 
												   prop.getServiceAPIUsername(), 
												   prop.getServiceAPIPassword()).updateCloudPort(dedicatedPortId, req);
			
			if ("FAILED".equals(response)) {
				logger.info("Port status update failed. Port : " + dedicatedPortId);
				throw new NovitasHandlerException();
			}
			
			Map<String,Object> retval = new HashMap<String,Object>();
			manager.completeWorkItem(workItem.getId(), retval);
			
		} catch (Exception e) {
			logger.error("WIHUpdateServicePort -> Failed", e);
			if ("DECOMMISSIONED".equals(getStatus(workItem))) {
				HandlerUtils.throwHandlerExcetpion(
						new NovitasHandlerException(HandlerErrorCode.DP_UPDATE_SERVICE_PORT_DECOMMISSIONED));
			}else if ("DECOMMISSIONING".equals(getStatus(workItem))) {
				HandlerUtils.throwHandlerExcetpion(
						new NovitasHandlerException(HandlerErrorCode.DP_UPDATE_SERVICE_PORT_DECOMMISSIONING));
			}else if("ACTIVE".equals(getStatus(workItem))) {
				HandlerUtils.throwHandlerExcetpion(
						new NovitasHandlerException(HandlerErrorCode.DP_UPADTE_RESOURCE_PORT_AVAILABLE));
		    }
		}
	}
	
}
