package net.colt.novitas.dcaport;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.HandlerUtils;
import com.colt.novitas.client.RequestAPIClient;
import com.colt.novitas.exception.NovitasHandlerException;
import com.colt.novitas.request.UpdateRequestStatusRequest;
import com.colt.novitas.response.UpdateRequestStatusResponse;

public class WIHUpdateRequestStatus extends BaseWorkItemHandler {
	
	private static  Logger logger = LoggerFactory.getLogger(WIHUpdateRequestStatus.class);
	
	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}
	
	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
		
		EnvironmentProperties prop = getEnvProperties(workItem);
		logger = getLogger(workItem, prop.getLogUniqueId());
		
		try {
			Integer requestId = getRequestId(workItem);
			logger.info("WIHUpdateRequestStatus --> Request Id : " + requestId);
			
			String status = getRequestStatus(workItem);
			Integer statusCode = getStatusCode(workItem);
			String bw = getBandwidth(workItem);
			Integer bandwidth = Integer.valueOf(StringUtils.isNotBlank(bw) && StringUtils.isNumeric(bw) ? bw : "0");
			
			UpdateRequestStatusRequest requestObject = new UpdateRequestStatusRequest();
			requestObject.setStatus(status);
			
			String portId = getNovitasPortServiceId(workItem);
			
			if(StringUtils.isNotBlank(portId)){
			  requestObject.setServiceId(portId);
			}
			if (statusCode != null) {
				requestObject.setStatusCode(statusCode);
			}
			
			if(null != bandwidth && bandwidth > 0){
				  requestObject.setBandwidth(bandwidth);
			}
			
			if(INPROGRESS.equals(status)){
				addJbpmDetails(requestObject, workItem);
			}
			
			
			Object response = new RequestAPIClient(
					prop.getRequestAPIUrl(),
					prop.getRequestAPIUsername(),
					prop.getRequestAPIPassword()).updateCloudPortOrConnectionRequest(requestId, requestObject);
			
			logger.info("Return from update request object: " + response + " class " + response.getClass().getName());
			
			if (response instanceof UpdateRequestStatusResponse) {
				Integer statusId = ((UpdateRequestStatusResponse)response).getStatusId();
				logger.info("Return code from update request: " + statusId);
				if (statusId <= 0) {
					throw new NovitasHandlerException();
				}
				
			} else if (response instanceof String) {
				logger.info("response is string");
				if ("-1".equals((String)response) || "FAILED".equals((String)response)) {
					throw new NovitasHandlerException();
				}
			} else {
				logger.info("response is something else");
			}
			
			
			Map<String,Object> retval = new HashMap<String,Object>();
			//retval.put("Result", response);
			logger.info("Completed WIHUpdateRequestStatus work item handler: " );
			manager.completeWorkItem(workItem.getId(), retval);
		} catch (Exception e) {
			logger.error("WIHUpdateRequestStatus -> Failed.", e);
			HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(getErrorCode(workItem)));
		}
	}
	

}
