package net.colt.novitas.dcaport;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.HandlerUtils;
import com.colt.novitas.billing.client.BillingAPIClient;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;

public class WIHEndRecurringCharge extends BaseWorkItemHandler {
	
	private static Logger logger = LoggerFactory.getLogger(WIHEndRecurringCharge.class);
	
	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}
	

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
		
		EnvironmentProperties prop = getEnvProperties(workItem);
		logger = getLogger(workItem, prop.getLogUniqueId());
		
		try {
			
			
			Object chargeResponse = new BillingAPIClient(prop.getBillingAPIOneOffUrl(),
					prop.getBillingAPIRecurrUrl(), 
					prop.getBillingAPIUsername(),
					prop.getBillingAPIPassword()).endRecurringCharges("DCAPORT",getServicePortId(workItem));
			
			logger.info("Recurring charge ended.");
			
			if (StringUtils.isBlank(String.valueOf(chargeResponse))) {
				logger.info("Unable to cancel recurring charge");
				throw new NovitasHandlerException();
			}
			
			Map<String,Object> retval = new HashMap<String,Object>();
			manager.completeWorkItem(workItem.getId(), retval);
			
		} catch (Exception e) {
			logger.error("WIHEndRecurringCharge --> Failed" + e.getMessage(), e);
			HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(HandlerErrorCode.DP_PORT_CANCEL_RECURRING_CHARGE));
		}
	}
	
	private String getServicePortId(WorkItem workItem) {
		return String.valueOf(getNovitasPortServiceId(workItem));
	}

}
