package net.colt.novitas.dcaport;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.HandlerUtils;
import com.colt.novitas.client.CallBackAPIClient;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;
import com.colt.novitas.request.CallBack;

/**
 * @author DRao This is a WIH file created by Dharrma Rao.
 * 
 */
public class WIHUpdateCallBackStatus extends BaseWorkItemHandler {

	private static Logger logger = LoggerFactory.getLogger(WIHUpdateCallBackStatus.class);

	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
		String signalName = null;
		EnvironmentProperties prop = getEnvProperties(workItem);
		logger = getLogger(workItem, prop.getLogUniqueId());
		try {

			String[] signalData = getSignalData(workItem);
            signalName = getSignalName(workItem);
            
            CallBack callBack = new CallBack();
			callBack.setTransactionId(signalData[0]);
			callBack.setProcessInstanceId(workItem.getProcessInstanceId());
			callBack.setStatus(signalData[1]);
			callBack.setSignalName(signalName);
            callBack.setStatusDescription(signalData[2]);
            callBack.setDeploymentId(prop.getPortDeploymentId());
			
            CallBackAPIClient callBackAPIClient = new CallBackAPIClient(prop.getInboundRestApiUrl(),prop.getInboundRestApiUserName(),prop.getInboundRestApiPassword());

			callBackAPIClient.updateStatus(callBack);
 
			Map<String, Object> retval = new HashMap<String, Object>();
			
			if (StringUtils.isBlank(signalData[1]) || "ERROR".equals(signalData[1])) {
    				logger.info("Status in response either null not SUCCESS");
    				throw new NovitasHandlerException();
            }
			
			manager.completeWorkItem(workItem.getId(), retval);

		} catch (Exception e) {
			logger.error("WIHUpdateCallBackStatus -> Failed", e);
			if(StringUtils.isNotBlank(signalName)){
				if(DCA_PORT_DELETE_SIEBEL_SIGNAL.equals(signalName) || DCA_PORT_CREATE_SIEBEL_SIGNAL.equals(signalName)){
					HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(
							HandlerErrorCode.PORT_SIEBEL_ASSETS_CALL_BACK_STATUS));
				}		
			}else{
				HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(
						HandlerErrorCode.UPDATE_STATUS_CB_DETAILS));
			}
			
		}
	}

}