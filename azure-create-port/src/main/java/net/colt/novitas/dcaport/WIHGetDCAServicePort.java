package net.colt.novitas.dcaport;

import java.util.HashMap;
import java.util.Map;

import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.HandlerUtils;
import com.colt.novitas.client.ServiceAPIClient;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;
import com.colt.novitas.response.CloudPortResponse;

public class WIHGetDCAServicePort extends BaseWorkItemHandler {
	
	private static Logger logger = LoggerFactory.getLogger(WIHGetDCAServicePort.class);
	
	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
        
		EnvironmentProperties prop = getEnvProperties(workItem);
		logger = getLogger(workItem, prop.getLogUniqueId());
		try {
			
			String portId = getNovitasPortServiceId(workItem);
			
			Object result = new ServiceAPIClient(prop.getServiceAPIBaseUrl(), 
												 prop.getServiceAPIUsername(), 
												 prop.getServiceAPIPassword()).getCloudPort(portId); 
			
			logger.info("Get DCA customer port call successfull :" + result);
			
			Map<String,Object> retval = new HashMap<String,Object>();
			
			if (result instanceof CloudPortResponse) {
				CloudPortResponse response = (CloudPortResponse) result;
				retval.put("Result", response);
				retval.put("vlan_id", String.valueOf(response.getVlan()));
				retval.put("nms", response.getNms());
			}
			else {
				throw new Exception("Get customer port call failed :" + result);
			}
			
			manager.completeWorkItem(workItem.getId(), retval);
			
		} catch (Exception e) {
			logger.error("WIHGetServicePort -> Fail : ", e);
			HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(HandlerErrorCode.DP_NO_MATCHING_SERVICE_PORT));
		}
	}
	

}
