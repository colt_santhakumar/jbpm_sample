package net.colt.novitas.dcaport;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.HandlerUtils;
import com.colt.novitas.billing.client.BillingAPIClient;
import com.colt.novitas.billing.client.request.OneOffChargeRequest;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;
import com.colt.novitas.response.CloudPortRequestResponse;

public class WIHPenaltyCharge extends BaseWorkItemHandler {
	
	private static Logger logger = LoggerFactory.getLogger(WIHPenaltyCharge.class);
	
	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}
	

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
		EnvironmentProperties prop = getEnvProperties(workItem);
		logger = getLogger(workItem, prop.getLogUniqueId());
		try {
			
			Map<String,Object> retval = new HashMap<String,Object>();
			
			CloudPortRequestResponse portRequest = getPortRequest(workItem);
			
			if (portRequest.getPenalty() != null && portRequest.getPenalty() > 0) {
				OneOffChargeRequest oneoff = new OneOffChargeRequest();
				oneoff.setRequestId(portRequest.getId());
				oneoff.setServiceId(portRequest.getServiceId());
				oneoff.setServiceInstanceId(portRequest.getPortId());
				oneoff.setServiceInstanceType("DCAPORT");
				oneoff.setBcn(portRequest.getBcn());
				oneoff.setChargeType("PENALTY");
				oneoff.setDescription("Penalty for port " + portRequest.getPortName());
				oneoff.setCurrency(portRequest.getPenaltyCurrency());
				oneoff.setAmount(portRequest.getPenalty());
				oneoff.setServiceInstanceName(portRequest.getPortName());
				
				Object chargeResponse = new BillingAPIClient(prop.getBillingAPIOneOffUrl(),
						prop.getBillingAPIRecurrUrl(), 
						prop.getBillingAPIUsername(),
						prop.getBillingAPIPassword()).createOneOffCharges(oneoff);
				
				logger.info("WIHDecommissionCharge ->" + String.valueOf(chargeResponse));
				
				if (StringUtils.isBlank(String.valueOf(chargeResponse))) {
					logger.info("Unable to create penalty charge");
					throw new NovitasHandlerException();
				}
				
				retval.put("Result", chargeResponse);
			}
			
			manager.completeWorkItem(workItem.getId(), retval);
			
		} catch (Exception e) {
			logger.error("WIHPenaltyCharge -> Failed.", e);
			HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(HandlerErrorCode.DP_PORT_PENALTY_CHARGE));
		}
	}
		
	
}
