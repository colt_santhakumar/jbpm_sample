package net.colt.novitas.dcaport;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.HandlerUtils;
import com.colt.novitas.WorkflowType;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.request.UpdateRequestStatusRequest;
import net.colt.novitas.workitems.response.LocationResponse;
import com.colt.novitas.response.CloudPortRequestResponse;
import com.colt.novitas.response.AzureServiceDetails;

public abstract class BaseWorkItemHandler implements WorkItemHandler {
	
	private static Logger logger = LoggerFactory.getLogger(BaseWorkItemHandler.class);
	
	protected static final String INPROGRESS = "INPROGRESS";
	protected static final String DCA_PORT = "COLT On Demand DCA Port";
	
	protected static final String DCA_PORT_DELETE_SIEBEL_SIGNAL = "delete-dca-port-siebel-signal";
	protected static final String DCA_PORT_CREATE_SIEBEL_SIGNAL = "create-dca-port-siebel-signal";
	
    protected static String LOG_UNIQUE_ID = null;
	
	protected Logger getLogger(WorkItem workItem, String uniqueLogId){
		if(null == LOG_UNIQUE_ID) {
			LOG_UNIQUE_ID = uniqueLogId;
			MDC.put("unique_log_id","J"+uniqueLogId.substring(1));
		}
		logger = LoggerFactory.getLogger(uniqueLogId+"|ProcessId-"+ workItem.getProcessInstanceId()+"|"+workItem.getName());
		return logger;
	}

	
	protected EnvironmentProperties getEnvProperties(WorkItem workItem){
		return (EnvironmentProperties) workItem.getParameter("env_properties");
	}
	
	protected String getTransactionId(WorkItem workItem){
		return (String) workItem.getParameter("transactionId");
	}
	protected String getBandwidth(WorkItem workItem){
		return String.valueOf(workItem.getParameter("bandwidth"));
	}
	protected String getSignalName(WorkItem workItem){
		return (String) workItem.getParameter("signal_name");
	}
	
	protected LocationResponse getLocationResponse(WorkItem workItem){
		return (LocationResponse) workItem.getParameter("site_response_obj");
	}
	
	protected String getSiteId(WorkItem workItem){
		return (String) workItem.getParameter("site_id");
	}
	
	protected String[] getSignalData(WorkItem workItem){
		return (String[]) workItem.getParameter("received_signal_data");
	}
 	
	protected String getStatus(WorkItem workItem){
		return (String) workItem.getParameter("status");
	}
	
	protected Integer getRequestId(WorkItem workItem) {
		Object obj = workItem.getParameter("request_id");
		Integer requestId = null;
		if (obj instanceof String) {
			requestId = Integer.valueOf(String.valueOf(obj));
		}
		else {
			requestId = (Integer) obj;
		}
		logger.info("Request Id : " + requestId);

		return requestId;
	}
	
	protected String getRequestStatus(WorkItem workItem){
		return (String) workItem.getParameter("status");
	}
	
	protected Integer getStatusCode(WorkItem workItem) {
		return (Integer) workItem.getParameter("status_code");
	}
	
	protected Exception getWorkflowError(WorkItem workItem) {
		Object obj = workItem.getParameter("error_object");
		if (obj instanceof Exception) {
			return (Exception) obj;
		}
		return null;
	}
	
	protected void addJbpmDetails(UpdateRequestStatusRequest req,WorkItem workItem) {
		req.setProcessInstanceId(String.valueOf(workItem.getProcessInstanceId()));
		try {
			req.setHostAddress(InetAddress.getLocalHost().getHostName());
		} catch (UnknownHostException e) {

		}
	}
	
	protected HandlerErrorCode getErrorCode(WorkItem workItem) {
		WorkflowType type = HandlerUtils.getWorkflowType(workItem);
		if (type == WorkflowType.DELETE_PORT) {
			return HandlerErrorCode.DP_UPADTE_REQUEST_INPROGRESS;
		} 
		return HandlerErrorCode.CP_UPADTE_REQUEST_INPROGRESS;
	}
	
	protected CloudPortRequestResponse getPortRequest(WorkItem workItem) {
		return (CloudPortRequestResponse) workItem.getParameter("request_obj");
	}
	
	
	protected String getNovitasPortServiceId(WorkItem workItem){
		return (String) workItem.getParameter("service_port_id");
	}
	
	protected AzureServiceDetails getAzureServiceDetails(WorkItem workItem){
        return (AzureServiceDetails) workItem.getParameter("azure_service_details");		
	}
	
	protected String getAzureVlanId(WorkItem workItem){
		return (String) workItem.getParameter("vlan_id");
		
	}
	
	protected String getResourcePortId(WorkItem workItem){
		return (String) workItem.getParameter("resource_port_id");
	}
	
	 protected String getJbpmUrl(String url){
			try {
				String host = InetAddress.getLocalHost().getHostName();
				url = url.replaceAll("%s", host);
			} catch (UnknownHostException e) {
				logger.error(e.getMessage());
			}
			return url;
		}
	 
   protected String getNms(WorkItem workItem) {
			return (String) workItem.getParameter("nms");
   }

   
   protected LocationResponse getLocation(WorkItem workItem) {
		
		Object object = workItem.getParameter("location_obj");
		if (object instanceof LocationResponse) {
			return (LocationResponse)object;
		}
		return null;
		
	}

}
