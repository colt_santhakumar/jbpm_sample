package net.colt.novitas.dcaport;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.HandlerUtils;
import com.colt.novitas.client.ServiceAPIClient;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;
import com.colt.novitas.request.CreateCloudPortRequest;
import com.colt.novitas.response.AzureServiceDetails;
import com.colt.novitas.response.CloudPortRequestResponse;


public class WIHCreateDCAServicePort extends BaseWorkItemHandler {
	
	private static Logger logger = LoggerFactory.getLogger(WIHCreateDCAServicePort.class);
	
	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}
	

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
		
		EnvironmentProperties prop = getEnvProperties(workItem);
		logger = getLogger(workItem, prop.getLogUniqueId());
		
		try {
			
			CloudPortRequestResponse portRequest = getPortRequest(workItem);
			AzureServiceDetails azureServiceDetails = getAzureServiceDetails(workItem);
			
			CreateCloudPortRequest req = new CreateCloudPortRequest();
			
			req.setName(portRequest.getPortName());
			req.setLocation(portRequest.getLocationName());
			
			req.setRentalCharge(portRequest.getRentalCharge());
			req.setRentalCurrency(portRequest.getRentalCurrency());
			req.setRentalUnit(portRequest.getRentalUnit());
			
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.MONTH, portRequest.getCommitmentPeriod());
			req.setCommitmentExpiry(cal.getTime());
			req.setServiceId(portRequest.getServiceId());
			req.setDecommissioningCharge(portRequest.getDecommissioningCharge());
			req.setDecommissioningCurrency(portRequest.getDecommissioningCurrency());
			
			req.setOcn(portRequest.getOcn());
			req.setCustomerName(portRequest.getCustomerName());
			
			req.setCloudProvider(portRequest.getCloudProvider());
			req.setServiceKey(portRequest.getServiceKey());
			req.setVlan(Integer.valueOf(azureServiceDetails.getVlanId()));	
		    req.setPort1(azureServiceDetails.getPrimaryAzurePort());
		    req.setPort2(azureServiceDetails.getSecondaryAzurePort());
			req.setBandwidth(Integer.valueOf(StringUtils.isNotBlank(azureServiceDetails.getBandwidth()) ?  azureServiceDetails.getBandwidth() : "0") );
			
			
			logger.info("Create Service Cloud Port and request is  :: " + req);
			
			String response = new ServiceAPIClient(prop.getServiceAPIBaseUrl(), 
					                               prop.getServiceAPIUsername(), 
					                               prop.getServiceAPIPassword()).createCloudPort(req);
			
			logger.info("Create Service Cloud Port response :" + response);
			
			if (StringUtils.isBlank(response)) {
				throw new NovitasHandlerException();
			}
			
			
			Map<String,Object> retval = new HashMap<String,Object>();
			retval.put("Result", response);
			manager.completeWorkItem(workItem.getId(), retval);
			
		} catch (Exception e) {
			logger.error("WIHCreateDCAServicePort -> Failed", e);
			HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(HandlerErrorCode.CP_CREATE_SERVICE_PORT));
		}
	  }
	
}
