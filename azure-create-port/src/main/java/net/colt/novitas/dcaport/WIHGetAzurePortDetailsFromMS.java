package net.colt.novitas.dcaport;

import java.util.HashMap;
import java.util.Map;

import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.HandlerUtils;
import com.colt.novitas.client.AzureAPIClient;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;
import com.colt.novitas.response.AzureServiceDetails;
import com.colt.novitas.response.CloudPortRequestResponse;

public class WIHGetAzurePortDetailsFromMS extends BaseWorkItemHandler {
	
	private static Logger logger = LoggerFactory.getLogger(WIHGetAzurePortDetailsFromMS.class);
	
	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}
	

public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
		
	   EnvironmentProperties prop = getEnvProperties(workItem);
	   logger = getLogger(workItem, prop.getLogUniqueId());	
	   
	  try {
			
			CloudPortRequestResponse portRequest = getPortRequest(workItem);
			AzureServiceDetails azureServiceDetails =  new AzureAPIClient(prop.getAzureApiUrl(), 
								                        prop.getAzureJksPath(), 
								                        prop.getAzureJksPassword(),prop.getAzureSubscriptionId(),
								                        prop.getAzureApiVersionId(),prop.getAzureRequestTimeOut())
								                        .getExpressRouteCircuit(portRequest.getServiceKey());
			
			Map<String,Object> retval = new HashMap<String,Object>();
			if ( null == azureServiceDetails) {
				logger.info("No AzureService Details for the cloud provider :: "+portRequest.getCloudProvider());
				throw new NovitasHandlerException();
			}
			logger.info("Response from MS Azure for service key "+ portRequest.getServiceKey()+ " is :: "+azureServiceDetails);
			retval.put("azure_service_details",azureServiceDetails);
			retval.put("vlan_id", azureServiceDetails.getVlanId());
			retval.put("bandwidth", azureServiceDetails.getBandwidth());
			retval.put("primary_interface", azureServiceDetails.getPrimaryAzurePort());
			retval.put("secondary_interface", azureServiceDetails.getSecondaryAzurePort());
			manager.completeWorkItem(workItem.getId(), retval);
		} catch (Exception e) {
			logger.error("WIHGetAzurePortDetailsFromXNG -> Failed", e);
			HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(HandlerErrorCode.FAILED_TO_GET_AZURE_DETAILS));
		}
	}
		

}
