package net.colt.novitas.dcaconnection;

import java.util.HashMap;
import java.util.Map;

import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.HandlerUtils;
import com.colt.novitas.client.ServiceAPIClient;
import com.colt.novitas.exception.NovitasHandlerException;
import com.colt.novitas.response.CloudConnectionResponse;

public class WIHGetServiceConnection extends BaseWorkItemHandler {
	
	private static Logger logger = LoggerFactory.getLogger(WIHGetServiceConnection.class);
	
	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}
	

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
		EnvironmentProperties prop = getEnvProperties(workItem);
		logger = getLogger(workItem, prop.getLogUniqueId());
		try {
			String connctionId = getServiceConnectionId(workItem);
			
			 Object response = new ServiceAPIClient(prop.getServiceAPIBaseUrl(), 
						prop.getServiceAPIUsername(), 
						prop.getServiceAPIPassword()).getCloudConnection(connctionId);
			
			logger.info("Get service Connection :" + response);
			
			if (!(response instanceof CloudConnectionResponse)) {
				throw new NovitasHandlerException();
			}
			
			Map<String,Object> retval = new HashMap<String,Object>();
			retval.put("Result", response);
			retval.put("nc_tech_service_id_1", ((CloudConnectionResponse) response).getNcTechServiceId1());
			retval.put("nc_tech_service_id_2", ((CloudConnectionResponse) response).getNcTechServiceId2());
			retval.put("penalty_charge", ((CloudConnectionResponse) response).getPenaltyCharge());
			retval.put("circuit_reference_1", ((CloudConnectionResponse) response).getResourceId1());
			retval.put("circuit_reference_2", ((CloudConnectionResponse) response).getResourceId2());
			retval.put("base_bandwidth", ((CloudConnectionResponse) response).getBaseBandwidth());
			retval.put("base_rental", ((CloudConnectionResponse) response).getBaseRental());
			retval.put("current_service_bandwidth", ((CloudConnectionResponse) response).getBandwidth());
			
			
			manager.completeWorkItem(workItem.getId(), retval);
			
		} catch (Exception e) {
			logger.error("WIHGetServiceConnection -> Fail : ", e.getMessage());
			HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(getErrorCode(workItem)));
		}
	}
	

}
