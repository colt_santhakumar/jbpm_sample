package net.colt.novitas.dcaconnection;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.HandlerUtils;
import com.colt.novitas.billing.client.BillingAPIClient;
import com.colt.novitas.billing.client.request.RecurringChargeRequest;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;
import com.colt.novitas.response.CloudConnectionRequestResponse;

public class WIHRentCharge extends BaseWorkItemHandler {

	private static Logger logger = LoggerFactory.getLogger(WIHRentCharge.class);

	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
		EnvironmentProperties prop = getEnvProperties(workItem);
		logger = getLogger(workItem, prop.getLogUniqueId());
		try {

			CloudConnectionRequestResponse connRequest = getConnectionRequest(workItem);
			String dedicateConn = getServiceConnectionId(workItem);
			
			RecurringChargeRequest recurring = new RecurringChargeRequest();
			recurring.setRequestId(connRequest.getId());
			recurring.setServiceId(connRequest.getServiceId());
			recurring.setServiceInstanceId(dedicateConn);
			recurring.setServiceInstanceType("DCACONNECTION");
			recurring.setBcn(connRequest.getBcn());
			recurring.setChargeType("RENTAL");
			recurring.setDescription("Rental for connection : "+ connRequest.getConnectionName());
			recurring.setCurrency(connRequest.getRentalCurrency());
			recurring.setAmount(connRequest.getRentalCharge());
			recurring.setFrequency(connRequest.getRentalUnit());
			recurring.setServiceInstanceName(connRequest.getConnectionName());
			
			if (null != connRequest.getCommitmentPeriod()&& connRequest.getCommitmentPeriod() > 0) {
				recurring.setCommitmentType("COMMIT");
				recurring.setCommitmentExpiryDate(getCommitmentExpiryDate(
														connRequest.getRequestedAt(),
														connRequest.getCommitmentPeriod(),
														connRequest.getRentalUnit()));
			}
			
			Object chargeResponse = new BillingAPIClient(
					prop.getBillingAPIOneOffUrl(),
					prop.getBillingAPIRecurrUrl(),
					prop.getBillingAPIUsername(), prop.getBillingAPIPassword())
					.createRecurringCharges(recurring);

			logger.info(String.valueOf(chargeResponse));

			if (StringUtils.isBlank(String.valueOf(chargeResponse))) {
				throw new NovitasHandlerException();
			}

			Map<String, Object> retval = new HashMap<String, Object>();
			retval.put("Result", chargeResponse);
			manager.completeWorkItem(workItem.getId(), retval);

		} catch (Exception e) {
			logger.error("WIHRentCharge -> Failed", e);
			HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(
					HandlerErrorCode.CC_RECURRING_CHARGE));
		}
	}
	
}
