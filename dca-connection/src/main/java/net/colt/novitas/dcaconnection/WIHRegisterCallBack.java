package net.colt.novitas.dcaconnection;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.HandlerUtils;
import com.colt.novitas.client.CallBackAPIClient;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;
import com.colt.novitas.request.CallBack;

/**
 * @author DRao This is a WIH file created by Dharrma Rao.
 * 
 */
public class WIHRegisterCallBack extends BaseWorkItemHandler {

	private static Logger logger = LoggerFactory.getLogger(WIHRegisterCallBack.class);

	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
		EnvironmentProperties prop = getEnvProperties(workItem);
		logger = getLogger(workItem, prop.getLogUniqueId());
		try {
      
			String transactionId = getTransactionId(workItem);
			String signalName = getSignalName(workItem);
			
			CallBack callBack = new CallBack();
			callBack.setTransactionId(transactionId);
			callBack.setProcessInstanceId(workItem.getProcessInstanceId());
			callBack.setStatus("WAITING");
			callBack.setSignalName(signalName);
			callBack.setDeploymentId(prop.getDcaConnectionDeploymentId());
			callBack.setJbpmUrl(getJbpmUrl(prop.getJbpmUrl()));
			CallBackAPIClient callBackAPIClient = new CallBackAPIClient(prop.getInboundRestApiUrl(),prop.getInboundRestApiUserName(),prop.getInboundRestApiPassword());
			String savedId = callBackAPIClient.registerCallBack(callBack);

			if (StringUtils.isBlank(savedId)) {
				logger.info("Failed to Register callback details for transaction Id ::"+transactionId);
				throw new NovitasHandlerException();
			}
			logger.info("Registered callback details for transaction Id :: "+transactionId);
			Map<String, Object> retval = new HashMap<String, Object>();
			manager.completeWorkItem(workItem.getId(), retval);

		} catch (Exception e) {
			logger.error("WIHRegisterCallBack -> Failed", e);
				HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(
						HandlerErrorCode.RG_CB_DETAILS));
			
		}
	}

}