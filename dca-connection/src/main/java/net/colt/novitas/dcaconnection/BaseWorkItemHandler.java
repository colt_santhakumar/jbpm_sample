package net.colt.novitas.dcaconnection;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.HandlerUtils;
import com.colt.novitas.WorkflowType;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.request.ConnectionPortPair;
import com.colt.novitas.request.RequestVLANIdRange;
import com.colt.novitas.request.UpdateRequestStatusRequest;
import com.colt.novitas.response.AzureServiceDetails;
import com.colt.novitas.response.CloudConnectionRequestResponse;
import com.colt.novitas.response.CloudConnectionResponse;
import com.colt.novitas.response.CloudPortResponse;
import com.colt.novitas.response.CustomerDedicatedPortResponse;
import com.colt.novitas.response.ServiceVLANIdRange;

public abstract class BaseWorkItemHandler implements WorkItemHandler {

	private static Logger logger = LoggerFactory
			.getLogger(BaseWorkItemHandler.class);

	protected static final String INPROGRESS = "INPROGRESS";
	protected static final String PROVISIONING = "PROVISIONING";
	protected static final String PROVISIONED = "PROVISIONED";
	protected static final String DEPROVISION = "DEPROVISION";

	protected static final String DCA_CREATE_CONN_SIGNAL_1 = "create_connection_signal_1";
	protected static final String DCA_CREATE_CONN_SIGNAL_2 = "create_connection_signal_2";
	protected static final String DCA_CONN_CREATE_SIEBEL_SIGNAL_1 = "create_connection_siebel_signal_1";
	protected static final String DCA_CONN_CREATE_SIEBEL_SIGNAL_2 = "create_connection_siebel_signal_2";
	protected static final String DCA_CONN_MODIFY_SIEBEL_SIGNAL = "modify_dca_conn_siebel_signal";
	protected static final String DCA_CONN_DELETE_SIEBEL_SIGNAL = "delete_dca_conn_siebel_signal";

	protected static final String DCA_CONN_CREATE_SMART_SIGNAL_1 = "create_conn_smart_signal_1";
	protected static final String DCA_CONN_CREATE_SMART_SIGNAL_2 = "create_conn_smart_signal_1";
	protected static final String DCA_CONN_DELETE_SMART_SIGNAL_1 = "delete_dca_conn_smart_signal_1";
	protected static final String DCA_CONN_DELETE_SMART_SIGNAL_2 = "delete_dca_conn_smart_signal_2";
	protected static final String DCA_CONNECTION = "DCACONNECTION";

    protected static String LOG_UNIQUE_ID = null;
	
	protected Logger getLogger(WorkItem workItem, String uniqueLogId){
		if(null == LOG_UNIQUE_ID) {
			LOG_UNIQUE_ID = uniqueLogId;
			MDC.put("unique_log_id","J"+uniqueLogId.substring(1));
		}
		logger = LoggerFactory.getLogger(uniqueLogId+"|ProcessId-"+ workItem.getProcessInstanceId()+"|"+workItem.getName());
		return logger;
	}
	

	protected EnvironmentProperties getEnvProperties(WorkItem workItem) {
		return (EnvironmentProperties) workItem.getParameter("env_properties");
	}

	protected String getTransactionId(WorkItem workItem) {
		return (String) workItem.getParameter("transaction_id");
	}

	protected String getSignalName(WorkItem workItem) {
		return (String) workItem.getParameter("signal_name");
	}

	protected String[] getSignalData(WorkItem workItem) {
		return (String[]) workItem.getParameter("received_signal_data");
	}

	protected String getStatus(WorkItem workItem) {
		return (String) workItem.getParameter("status");
	}

	protected Integer getRequestId(WorkItem workItem) {
		Object obj = workItem.getParameter("request_id");
		Integer requestId = null;
		if (obj instanceof String) {
			requestId = Integer.valueOf(String.valueOf(obj));
		} else {
			requestId = (Integer) obj;
		}
		logger.info("Request Id : " + requestId);

		return requestId;
	}

	protected AzureServiceDetails getAzureServiceDetails(WorkItem workItem) {
		return (AzureServiceDetails) workItem
				.getParameter("azure_service_details");
	}

	protected String getRequestStatus(WorkItem workItem) {
		return (String) workItem.getParameter("status");
	}

	protected String getParentServiceId(WorkItem workItem) {
		return (String) workItem.getParameter("novitas_service_id");
	}

	protected String getNovitasPortServiceId(WorkItem workItem) {
		return (String) workItem.getParameter("service_port_id");
	}

	protected String getProcessDefId(WorkItem workItem) {
		return (String) workItem.getParameter("process_def_id");
	}

	protected Integer getStatusCode(WorkItem workItem) {
		return null != workItem.getParameter("status_code") ? Integer.valueOf(String.valueOf(workItem.getParameter("status_code"))) : null;
	}

	protected Exception getWorkflowError(WorkItem workItem) {
		Object obj = workItem.getParameter("error_object");
		if (obj instanceof Exception) {
			return (Exception) obj;
		}
		return null;
	}

	protected void addJbpmDetails(UpdateRequestStatusRequest req,
			WorkItem workItem) {
		req.setProcessInstanceId(String.valueOf(workItem.getProcessInstanceId()));
		try {
			req.setHostAddress(InetAddress.getLocalHost().getHostName());
		} catch (UnknownHostException e) {

		}
	}

	protected HandlerErrorCode getErrorCode(WorkItem workItem) {
		WorkflowType type = HandlerUtils.getWorkflowType(workItem);
		if (type == WorkflowType.DELETE_PORT) {
			return HandlerErrorCode.DP_UPADTE_REQUEST_INPROGRESS;
		}
		return HandlerErrorCode.CP_UPADTE_REQUEST_INPROGRESS;
	}

	protected CloudConnectionRequestResponse getConnectionRequest(
			WorkItem workItem) {
		return (CloudConnectionRequestResponse) workItem
				.getParameter("request_obj");
	}

	protected String getServiceConnectionId(WorkItem workItem) {
		return (String) workItem.getParameter("service_connection_id");
	}

	protected String getServicePortId(WorkItem workItem) {
		return (String) workItem.getParameter("service_port_id");
	}

	protected Object getPenaltyCharge(WorkItem workItem) {
		return workItem.getParameter("penalty_charge");
	}

	protected String getResourcePortId(WorkItem workItem) {
		return String.valueOf(workItem.getParameter("resource_port_id"));
	}

	protected String getFromResourcePortId(WorkItem workItem) {
		return String.valueOf(workItem.getParameter("from_resource_port_id"));
	}

	protected String getToResourcePortId(WorkItem workItem) {
		return String.valueOf(workItem.getParameter("to_resource_port_id"));
	}

	

	protected CloudPortResponse getFromDedicatedPort(WorkItem workItem) {
		return (CloudPortResponse) workItem.getParameter("from_customer_port");
	}

	protected CustomerDedicatedPortResponse getToDedicatedPort(WorkItem workItem) {
		return (CustomerDedicatedPortResponse) workItem
				.getParameter("to_customer_port");
	}

	protected String getNcTechServiceId1(WorkItem workItem) {
		return (String) workItem.getParameter("nc_tech_service_id_1");
	}

	protected String getNcTechServiceId2(WorkItem workItem) {
		return (String) workItem.getParameter("nc_tech_service_id_2");
	}

	public String getCommitmentExpiryDate(String startDate,
			Integer commitmentPeriod, String frequency) {

		DateFormat fdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
		fdf.setTimeZone(TimeZone.getTimeZone("UTC"));
		Calendar c = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		if (startDate != null) {

			try {
				c.setTime(fdf.parse(startDate));
			} catch (ParseException e) {
				
				e.printStackTrace();
			}
			c.add(Calendar.MONTH, commitmentPeriod);
			if ("MONTHLY".equals(frequency)) {
				c.set(Calendar.HOUR_OF_DAY,
						c.getActualMaximum(Calendar.HOUR_OF_DAY));
			}
			c.set(Calendar.MINUTE, c.getActualMaximum(Calendar.MINUTE));
			c.set(Calendar.SECOND, c.getActualMaximum(Calendar.SECOND));

			return fdf.format(c.getTime());

		} else {
			return "";
		}
	}

	protected String getEvcId(WorkItem workItem) {
		return String.valueOf(workItem.getParameter("evc_id"));
	}

	protected String getTargetAction(WorkItem workItem) {
		return (String) workItem.getParameter("target_action");
	}

	protected Integer getReqBandwidth(WorkItem workItem) {
		return getIntValue(workItem.getParameter("request_bandwidth"));
	}

	protected Integer getFromPortBandwidth(WorkItem workItem) {
		return getIntValue(workItem.getParameter("from_port_bandwidth"));
	}

	protected Integer getToPortBandwidth(WorkItem workItem) {
		return getIntValue(workItem.getParameter("to_port_bandwidth"));
	}

	protected String getResourceConnectionId(WorkItem workItem) {
		return (String) workItem.getParameter("resource_connection");
	}

	

	protected ConnectionPortPair getConnectionPortPair(WorkItem workItem) {

		return (ConnectionPortPair) workItem
				.getParameter("connection_port_pair");
	}

	protected List<ServiceVLANIdRange> getServiceVLANIds(
			List<RequestVLANIdRange> requestRanges) {

		List<ServiceVLANIdRange> serviceRanges = new ArrayList<ServiceVLANIdRange>();
		if (requestRanges != null && !requestRanges.isEmpty()) {
			for (RequestVLANIdRange requestVLANIdRange : requestRanges) {
				serviceRanges.add(new ServiceVLANIdRange(requestVLANIdRange
						.getFromIdRange(), requestVLANIdRange.getToIdRange()));
			}
		}
		return serviceRanges;
	}

	
	protected String getFromResPortName(WorkItem workItem) {
		return (String) workItem.getParameter("from_resource_port_name");
	}

	protected String getToResPortName(WorkItem workItem) {
		return (String) workItem.getParameter("to_resource_port_name");
	}

	protected String getChargeType(WorkItem workItem) {
		return (String) workItem.getParameter("charge_type");
	}

	protected String getOperationType(WorkItem workItem) {
		return (String) workItem.getParameter("operation_type");
	}

	protected String getPortEnd(WorkItem workItem) {
		return (String) workItem.getParameter("port_end");
	}

	protected String getVlanId(WorkItem workItem) {
		return (String) workItem.getParameter("vlan_id");
	}

	protected String getServiceKey(WorkItem workItem) {
		return (String) workItem.getParameter("service_key");
	}

	protected String getCSPName(WorkItem workItem) {
		return (String) workItem.getParameter("csp_name");
	}

	protected CloudConnectionResponse getCustomerConnection(WorkItem workItem) {
		return (CloudConnectionResponse) workItem
				.getParameter("customer_connection_obj");

	}

	private Integer getIntValue(Object val) {
		if (val instanceof Integer) {
			logger.info("Integer value : " + val);
			return (Integer) val;
		}

		logger.info("String value : " + Integer.valueOf(String.valueOf(val)));
		return Integer.valueOf(String.valueOf(val));
	}

	
	protected String getPortExpiryDate(Integer commitmentPeriod) {

		DateFormat fdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
		fdf.setTimeZone(TimeZone.getTimeZone("UTC"));
		Calendar c = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		c.add(Calendar.DATE, commitmentPeriod);

		return fdf.format(c.getTime());

	}

	protected String getJbpmUrl() {
		String url = "http://%s:8080/business-central";
		try {
			String host = InetAddress.getLocalHost().getHostName();
			url = url.replaceAll("%s", host);
		} catch (UnknownHostException e) {
			logger.error(e.getMessage());
		}
		return url;
	}

	protected Integer getBlueplanetWorkflowId(WorkItem workItem) {
		return getIntValue(workItem.getParameter("blueplanet_workflow_id"));
	}

	protected String getDeploymentId(WorkItem workItem) {
		return (String) workItem.getParameter("deployment_id");
	}

	// support comma separated
	protected String[] getVarName(WorkItem workItem) {
		String vars = (String) workItem.getParameter("var_name");
		if (vars == null || "".equals(vars)) {
			return null;
		}
		return vars.split(",");
	}

	protected Integer iterations(WorkItem workItem) {
		Object obj = workItem.getParameter("iterations");
		if (obj != null) {
			if (obj instanceof Integer) {
				return (Integer) obj;
			}
			return Integer.parseInt((String) obj);
		}

		return 0;
	}

	protected String getJbpmUrl(String url) {
		try {
			String host = InetAddress.getLocalHost().getHostName();
			url = url.replaceAll("%s", host);
		} catch (UnknownHostException e) {
			logger.error(e.getMessage());
		}
		return url;
	}


	public static String getStringFromList(List<RequestVLANIdRange> requestVLANIdRanges) {

		if (requestVLANIdRanges == null || requestVLANIdRanges.isEmpty()) {
			return null;
		}

		StringBuilder sb = new StringBuilder();
		for (RequestVLANIdRange requestVLANIdRange : requestVLANIdRanges) {
			if (sb.length() > 0) {
				sb.append(",");
			}
			

			if (null != requestVLANIdRange.getFromIdRange() && null != requestVLANIdRange.getToIdRange()
				&& !requestVLANIdRange.getFromIdRange().equals(requestVLANIdRange.getToIdRange())) {
				
				sb.append("[").append(requestVLANIdRange.getFromIdRange()).append("-").append(requestVLANIdRange.getToIdRange()).append("]");
			}else if (null != requestVLANIdRange.getFromIdRange())
				sb.append(requestVLANIdRange.getFromIdRange());

		}
		logger.info("Converted VLANs--> " + sb.toString());
		return sb.toString();
	}
	
	 public static String getStringFromServiceList(List<ServiceVLANIdRange> requestVLANIdRanges) {

			if (requestVLANIdRanges == null || requestVLANIdRanges.isEmpty()) {
				return null;
			}

			StringBuilder sb = new StringBuilder();
			for (ServiceVLANIdRange requestVLANIdRange : requestVLANIdRanges) {
				if (sb.length() > 0) {
					sb.append(",");
				}
				

				if (null != requestVLANIdRange.getFromIdRange() && null != requestVLANIdRange.getToIdRange()
					&& !requestVLANIdRange.getFromIdRange().equals(requestVLANIdRange.getToIdRange())) {
					
					sb.append("[").append(requestVLANIdRange.getFromIdRange()).append("-").append(requestVLANIdRange.getToIdRange()).append("]");
				}else if (null != requestVLANIdRange.getFromIdRange())
					sb.append(requestVLANIdRange.getFromIdRange());

			}
			logger.info("Converted VLANs--> " + sb.toString());
			return sb.toString();
		}
}
