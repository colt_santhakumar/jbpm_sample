package net.colt.novitas.dcaconnection;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.httpclient.NoHttpResponseException;
import org.apache.commons.lang.StringUtils;
import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.HandlerUtils;
import com.colt.novitas.billing.client.BillingAPIClient;
import com.colt.novitas.billing.client.request.ModifyRecurringChargeRequest;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;
import com.colt.novitas.response.CloudConnectionRequestResponse;

public class WIHModifyRecurringCharge extends BaseWorkItemHandler {

	private static Logger logger = LoggerFactory.getLogger(WIHModifyRecurringCharge.class);

	private WorkItem workItem;

	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
		EnvironmentProperties prop = getEnvProperties(workItem);
		logger = getLogger(workItem, prop.getLogUniqueId());
		try {
			this.workItem = workItem;
			CloudConnectionRequestResponse connRequest = getConnectionRequest(workItem);

			ModifyRecurringChargeRequest recurringCharge = new ModifyRecurringChargeRequest();
			recurringCharge.setAmount(connRequest.getRentalCharge());
			recurringCharge.setCurrency(connRequest.getRentalCurrency());
			recurringCharge.setFrequency(connRequest.getRentalUnit());
			recurringCharge.setRequestId(connRequest.getId());
			addCommitmentDetails(recurringCharge, connRequest);
			logger.info("Modify Recurring object is ..." + recurringCharge.toString());
			Map<String, Object> retval = new HashMap<String, Object>();

			if (recurringCharge.getAmount() != null && recurringCharge.getAmount() > 0) {
				logger.info("Charge is non-zero. Updating...");

				Object chargeResponse = new BillingAPIClient(prop.getBillingAPIOneOffUrl(),
						prop.getBillingAPIRecurrUrl(), prop.getBillingAPIUsername(), prop.getBillingAPIPassword())
								.modifyRecurringCharges("DCACONNECTION", connRequest.getConnectionId(),
										recurringCharge);

				logger.info(String.valueOf(chargeResponse));

				if (StringUtils.isBlank(String.valueOf(chargeResponse))) {
					throw new NoHttpResponseException();
				}
				retval.put("Result", chargeResponse);

			} else {
				logger.info("Charge is zero. Skipping...");
			}

			manager.completeWorkItem(workItem.getId(), retval);

		} catch (Exception e) {
			logger.error("WIHModifyRecurringCharge -> Failed.", e);
			HandlerUtils
					.throwHandlerExcetpion(new NovitasHandlerException(HandlerErrorCode.MC_MODIFY_RECURRING_CHARGE));
		}
	}

	private void addCommitmentDetails(ModifyRecurringChargeRequest recurringReq,
			CloudConnectionRequestResponse connectionReq) {

		Integer newCommitmentPeriod = connectionReq.getCommitmentPeriod();
		String existCommitmentExpiry = getCustomerConnection(workItem).getCommitmentExpiryDate();

		if (null != newCommitmentPeriod && newCommitmentPeriod > 0) {

			if (null != connectionReq.getCoterminusOption() && connectionReq.getCoterminusOption()
					&& net.colt.novitas.workitems.BaseWorkItemHandler.isCommitmentDateValid(existCommitmentExpiry)) {

				recurringReq.setCommitmentExpiryDate(existCommitmentExpiry);
			} else {
				recurringReq.setCommitmentExpiryDate(getCommitmentExpiryDate(connectionReq.getRequestedAt(),
						newCommitmentPeriod, connectionReq.getRentalUnit()));
			}
			recurringReq.setCommitmentType("COMMIT");
		}
	}

}
