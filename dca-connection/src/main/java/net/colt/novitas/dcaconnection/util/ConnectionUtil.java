package net.colt.novitas.dcaconnection.util;

import org.kie.api.runtime.process.WorkItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.client.ServiceAPIClient;
import com.colt.novitas.response.CloudConnectionResponse;

public class ConnectionUtil {
	
	private static final Logger logger = LoggerFactory.getLogger(ConnectionUtil.class);
	
	public static String getSlaIdForConnection(String connectionId,WorkItem workItem){
		
		EnvironmentProperties prop = (EnvironmentProperties) workItem.getParameter("env_properties");
		
		CloudConnectionResponse response = new ServiceAPIClient(
										prop.getServiceAPIBaseUrl(), 
										prop.getServiceAPIUsername(), 
										prop.getServiceAPIPassword()).getCloudConnection(connectionId );
		
		
		if(null != response && null !=response.getSlaId()){
			logger.info("Create DCA Connection SLAID :" + response.getSlaId());
			return response.getSlaId();
		}
		return null;
		
	}

}
