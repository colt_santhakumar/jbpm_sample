package net.colt.novitas.dcaconnection;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.httpclient.NoHttpResponseException;
import org.apache.commons.lang.StringUtils;
import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.HandlerUtils;
import com.colt.novitas.billing.client.BillingAPIClient;
import com.colt.novitas.billing.client.request.OneOffChargeRequest;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;
import com.colt.novitas.response.CloudConnectionRequestResponse;

public class WIHInstallationCharge extends BaseWorkItemHandler {
	
	private static Logger logger = LoggerFactory.getLogger(WIHInstallationCharge.class);
	
	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}
	

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
		EnvironmentProperties prop = getEnvProperties(workItem);
		logger = getLogger(workItem, prop.getLogUniqueId());
		
		try {
			
			CloudConnectionRequestResponse connRequest = getConnectionRequest(workItem);
			String dedicatedConnectionId = getServiceConnectionId(workItem);
			String chargeType = getChargeType(workItem);
			Object penaltyCharge =  getPenaltyCharge(workItem);
			
			OneOffChargeRequest oneoff = new OneOffChargeRequest();
			oneoff.setRequestId(connRequest.getId());
			oneoff.setServiceId(connRequest.getServiceId());
			oneoff.setServiceInstanceId(dedicatedConnectionId);
			oneoff.setServiceInstanceType("DCACONNECTION");
			oneoff.setBcn(connRequest.getBcn());
			oneoff.setChargeType(chargeType);
			oneoff.setDescription( chargeType +" for connection " + connRequest.getConnectionName());
			oneoff.setCurrency(connRequest.getInstallationCurrency());
			oneoff.setServiceInstanceName(connRequest.getConnectionName());
			
			if("PENALTY".equalsIgnoreCase(chargeType)){
				oneoff.setAmount(null !=penaltyCharge ? (Float)penaltyCharge : null);
			}else{
				oneoff.setAmount(connRequest.getInstallationCharge());
			}
			
			Object chargeResponse = new BillingAPIClient(
					prop.getBillingAPIOneOffUrl(),
					prop.getBillingAPIRecurrUrl(), 
					prop.getBillingAPIUsername(),
					prop.getBillingAPIPassword()).createOneOffCharges(oneoff);
			
			logger.info(String.valueOf(chargeResponse));
			
			if (StringUtils.isBlank(String.valueOf(chargeResponse))) {
				throw new NoHttpResponseException();
			}
			
			Map<String,Object> retval = new HashMap<String,Object>();
			retval.put("Result", chargeResponse);
			manager.completeWorkItem(workItem.getId(), retval);
			
		} catch (Exception e) {
			logger.error("WIHInstallationCharge -> Failed", e);
			HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(HandlerErrorCode.CC_INSTALLATION_CHARGE));
		}
	}
	

}
