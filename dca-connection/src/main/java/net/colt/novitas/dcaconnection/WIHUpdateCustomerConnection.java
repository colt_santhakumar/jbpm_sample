package net.colt.novitas.dcaconnection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.HandlerUtils;
import com.colt.novitas.client.ServiceAPIClient;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;
import com.colt.novitas.request.ConnectionPortPair;
import com.colt.novitas.request.UpdateCloudConnRequest;
import com.colt.novitas.response.CloudConnectionRequestResponse;

public class WIHUpdateCustomerConnection extends BaseWorkItemHandler {
	
	private static Logger logger = LoggerFactory.getLogger(WIHUpdateCustomerConnection.class);
	
	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}
	

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
		
		String status = null;
		EnvironmentProperties prop = getEnvProperties(workItem);
		logger = getLogger(workItem, prop.getLogUniqueId());
		Boolean isVlanModifyFlag = false;
		try {
			isVlanModifyFlag = getIsVlanModifyFlag(workItem);
			String connectionId =  getServiceConnectionId(workItem);
			CloudConnectionRequestResponse connectionRequest = getConnectionRequest(workItem);
			status = getStatus(workItem);
			String techServId1 = getNcTechServiceId1(workItem);
			String techServId2 = getNcTechServiceId2(workItem);
			
			UpdateCloudConnRequest connReq = new UpdateCloudConnRequest();
			connReq.setStatus(status);
			//update tech_service_id / resource id if provided
			if (techServId1 != null) {
				connReq.setNcTechServiceId1(techServId1);
			}
			if (techServId2 != null) {
				connReq.setNcTechServiceId2(techServId2);
			}
			if(null != workItem.getParameter("circuit_reference_1")) {
				connReq.setResourceId1(String.valueOf(workItem.getParameter("circuit_reference_1")));
			}
			if(null != workItem.getParameter("circuit_reference_2")) {
				connReq.setResourceId2(String.valueOf(workItem.getParameter("circuit_reference_2")));
			}
			
			//for charges
			
			if ( fullUpdate(workItem) && connectionRequest != null) {
				connReq.setBandwidth(connectionRequest.getBandwidth());
				connReq.setRentalCharge(connectionRequest.getRentalCharge());
				connReq.setRentalCurrency(connectionRequest.getRentalCurrency());
				connReq.setRentalUnit(connectionRequest.getRentalUnit());
			}
			
			logger.info("isVlanModifyFlag: " + isVlanModifyFlag);
			if ("ACTIVE".equals(status) && null != isVlanModifyFlag && !isVlanModifyFlag) {
				if (null != connectionRequest.getRentalCharge()) {
					logger.info("Set base rental: " + connectionRequest.getRentalCharge());
					connReq.setBaseRental(connectionRequest.getRentalCharge());
				}
				if (null != connectionRequest.getBandwidth()) {
					logger.info("Set base bandwidth : " + connectionRequest.getBandwidth());
					connReq.setBaseBandwidth(connectionRequest.getBandwidth());
				}
			}
			/*
			 * if ("ACTIVE".equals(status) && null !=connectionRequest.getCommitmentPeriod()
			 * && connectionRequest.getCommitmentPeriod() > 0) {
			 * connReq.setCommitmentPeriod(connectionRequest.getCommitmentPeriod());
			 * connReq.setCommitmentExpiryDate(getCommitmentExpiryDate(connectionRequest.
			 * getRequestedAt(),connectionRequest.getCommitmentPeriod(),connectionRequest.
			 * getRentalUnit())); }
			 */
			if ("ACTIVE".equals(status) && null != connectionRequest.getCommitmentPeriod() && connectionRequest.getCommitmentPeriod() > 0) {
				connReq.setCommitmentPeriod(connectionRequest.getCommitmentPeriod());

				String commitmentExpiryStartDate = connectionRequest.getRequestedAt();
				String existCommitmentExpiry = null != getCustomerConnection(workItem) ? getCustomerConnection(workItem).getCommitmentExpiryDate() : null;

				if (connectionRequest.getCoterminusOption() != null && connectionRequest.getCoterminusOption() 
						&& net.colt.novitas.workitems.BaseWorkItemHandler.isCommitmentDateValid(existCommitmentExpiry)) {
					//will maintain existing commitment expiry date
					connReq.setCommitmentExpiryDate(existCommitmentExpiry);

				} else {
					connReq.setCommitmentExpiryDate(net.colt.novitas.workitems.BaseWorkItemHandler.getCommitmentExpiryDate(commitmentExpiryStartDate, connectionRequest.getCommitmentPeriod(), connectionRequest.getRentalUnit()));
				}
			}
			
			//reset the VLAN properties
			if ("MODIFYING".equals(status) && null != connectionRequest) {
				//VLAN properties
				List<com.colt.novitas.response.ConnectionPortPair> portPairs = new ArrayList<com.colt.novitas.response.ConnectionPortPair>();
				if(null != connectionRequest.getPorts()){
					for (ConnectionPortPair connectionPortPair : connectionRequest.getPorts()) {
						com.colt.novitas.response.ConnectionPortPair servicePair = new com.colt.novitas.response.ConnectionPortPair();
						servicePair.setFromPortId(connectionPortPair.getFromPortId());
						servicePair.setToPortId(connectionPortPair.getToPortId());
						servicePair.setCloudProviderFromPort(connectionPortPair.getCloudProviderFromPort());
						servicePair.setCloudProviderToPort(connectionPortPair.getCloudProviderToPort());
						servicePair.setaEndVlanMapping(connectionPortPair.getaEndVlanMapping());
					    servicePair.setaEndVlanType(connectionPortPair.getaEndVlanType());
					    servicePair.setbEndVlanMapping(connectionPortPair.getbEndVlanMapping());
					    servicePair.setbEndVlanType(connectionPortPair.getbEndVlanType());
					    servicePair.setaEndVlanIds(getServiceVLANIds(connectionPortPair.getaEndVlanIds()));
					    servicePair.setbEndVlanIds(getServiceVLANIds(connectionPortPair.getbEndVlanIds()));
					    portPairs.add(servicePair);
					}
				}
				connReq.setPorts(portPairs);
			}
			
			Object result = new ServiceAPIClient(prop.getServiceAPIBaseUrl(), 
										prop.getServiceAPIUsername(), 
										prop.getServiceAPIPassword()).updateCloudConnection(connectionId, connReq);
			
			logger.info("Results of update service conn : "  + result);
			if (result != null && "FAILED".equals(String.valueOf(result))) {
				throw new NovitasHandlerException();
			}
			
			Map<String,Object> retval = new HashMap<String,Object>();
			manager.completeWorkItem(workItem.getId(), retval);
			
		} catch (Exception e) {
			logger.error("WIHUpdateCustomerConnection - >  failed ", e);
			if ("ACTIVE".equals(status)) {
				HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(HandlerErrorCode.CC_UPDATE_SERVICE_ACTIVE_CONN_ID));
			}
			else if ("MODIFYING".equals(status)) {
				HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(HandlerErrorCode.MC_UPDATE_SERVICE_MODIFYING));
			}
			else if ("DECOMMISSIONING".equals(status)) {
				HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(HandlerErrorCode.DC_UPDATE_SERVICE_DECOMMISSIONING));
			}
			else if ("DECOMMISSIONED".equals(status)) {
				HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(HandlerErrorCode.DC_UPDATE_SERVICE_DECOMMISSIONED));
			}
		}
	}
	
	protected boolean fullUpdate(WorkItem workItem) {
		Object obj = workItem.getParameter("full_update");
		if (obj != null && obj instanceof String) {
			return "true".equalsIgnoreCase(String.valueOf(obj));
		}
		return false;
	}
	
	protected Boolean getIsVlanModifyFlag(WorkItem workItem) {
		return (Boolean) workItem.getParameter("is_vlan_modify");
	}
	
}
