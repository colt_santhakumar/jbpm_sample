package net.colt.novitas.dcaconnection;

import java.util.HashMap;
import java.util.Map;

import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.HandlerUtils;
import com.colt.novitas.client.ServiceAPIClient;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;
import com.colt.novitas.request.UpdatePortRequest;

public class WIHUpdateServicePort extends BaseWorkItemHandler {

	private static Logger logger = LoggerFactory.getLogger(WIHUpdateServicePort.class);

	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
		EnvironmentProperties prop = getEnvProperties(workItem);
		logger = getLogger(workItem, prop.getLogUniqueId());
		try {

			String dedicatedPortId = getServicePortId(workItem);
            String status ="ACTIVE";
            String operation = (String) workItem.getParameter("operation");
			UpdatePortRequest portReq = new UpdatePortRequest();
			portReq.setStatus(status);
			
			if("CREATE".equals(operation)){
				portReq.setPortExpiresOn(null);
				portReq.setPortInUse(true);
			}else if ("DELETE".equals(operation)){
				Integer expiryPeriod = (Integer) workItem.getParameter("port_expiry_period");
				portReq.setPortExpiresOn(getPortExpiryDate(null !=expiryPeriod ? expiryPeriod : 90));
				portReq.setPortInUse(false);
			}
			
			Object response = new ServiceAPIClient(prop.getServiceAPIBaseUrl(),prop.getServiceAPIUsername(),
					               prop.getServiceAPIPassword()).updatePort(dedicatedPortId, portReq);

			if ("FAILED".equals(response)) {
				logger.info("Port Expiry and Used status update failed :: "+ dedicatedPortId);
				throw new NovitasHandlerException();
			}

			Map<String, Object> retval = new HashMap<String, Object>();
			manager.completeWorkItem(workItem.getId(), retval);

		} catch (Exception e) {
			logger.error("WIHUpdateServicePort -> Failed", e);

				HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(
						HandlerErrorCode.CP_UPDATE_SERVICE_PORT_DETAILS));
			
		}
	}

	
	
}
