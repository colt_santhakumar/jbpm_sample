package net.colt.novitas.dcaconnection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.HandlerUtils;
import com.colt.novitas.client.ServiceAPIClient;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;
import com.colt.novitas.request.ConnectionPortPair;
import com.colt.novitas.request.UpdateCloudConnRequest;
import com.colt.novitas.response.CloudConnectionRequestResponse;

public class WIHUpdateCustomerConnectionAfterBoost extends BaseWorkItemHandler {

	private static Logger logger = LoggerFactory.getLogger(WIHUpdateCustomerConnectionAfterBoost.class);

	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {

		String status = null;
		EnvironmentProperties prop = getEnvProperties(workItem);
		logger = getLogger(workItem, prop.getLogUniqueId());
		try {
			String connectionId = getServiceConnectionId(workItem);
			CloudConnectionRequestResponse connectionRequest = getConnectionRequest(workItem);
			status = getStatus(workItem);
			String techServId1 = getNcTechServiceId1(workItem);
			String techServId2 = getNcTechServiceId2(workItem);

			UpdateCloudConnRequest connReq = new UpdateCloudConnRequest();
			connReq.setStatus(status);
			connReq.setBandwidth(connectionRequest.getBandwidth());
			if (connectionRequest.getBandwidth().equals(workItem.getParameter("base_bandwidth"))) {
				logger.info("Request bandwidht equal base bandwidth");
				connReq.setRentalCharge(Float.valueOf(String.valueOf(workItem.getParameter("base_rental"))).floatValue());
			} else {
				logger.info("Request bandwidht not equal to base bandwidth");
				connReq.setRentalCharge(connectionRequest.getRentalCharge());
			}

			connReq.setRentalCurrency(connectionRequest.getRentalCurrency());
			connReq.setRentalUnit(connectionRequest.getRentalUnit());

			// update tech_service_id / resource id if provided
			if (null != techServId1) {
				connReq.setNcTechServiceId1(techServId1);
			}
			if (null !=techServId2) {
				connReq.setNcTechServiceId2(techServId2);
			}
			if (null != workItem.getParameter("circuit_reference_1")) {
				connReq.setResourceId1(String.valueOf(workItem.getParameter("circuit_reference_1")));
			}
			if (null != workItem.getParameter("circuit_reference_2")) {
				connReq.setResourceId2(String.valueOf(workItem.getParameter("circuit_reference_2")));
			}

			Object result = new ServiceAPIClient(prop.getServiceAPIBaseUrl(), prop.getServiceAPIUsername(),
					prop.getServiceAPIPassword()).updateCloudConnection(connectionId, connReq);

			logger.info("Results of update service conn : " + result);
			if (result != null && "FAILED".equals(String.valueOf(result))) {
				throw new NovitasHandlerException();
			}

			Map<String, Object> retval = new HashMap<String, Object>();
			manager.completeWorkItem(workItem.getId(), retval);

		} catch (Exception e) {
			logger.error("WIHUpdateCustomerConnection - >  failed ", e);
			HandlerUtils.throwHandlerExcetpion(
					new NovitasHandlerException(HandlerErrorCode.CC_UPDATE_SERVICE_ACTIVE_CONN_ID));
		}
	}

}
