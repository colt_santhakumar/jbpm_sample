package net.colt.novitas.dcaconnection;

import java.util.HashMap;
import java.util.Map;

import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.HandlerUtils;
import com.colt.novitas.client.ServiceAPIClient;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;
import com.colt.novitas.response.CloudPortResponse;
import com.colt.novitas.response.CustomerDedicatedPortResponse;

public class WIHGetServicePort extends BaseWorkItemHandler {
	
	private static Logger logger = LoggerFactory.getLogger(WIHGetServicePort.class);
	
	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
		EnvironmentProperties prop = getEnvProperties(workItem);
		logger = getLogger(workItem, prop.getLogUniqueId());
		try {
			String portId = getServicePortId(workItem);
			String portEnd = getPortEnd(workItem);
			Object result = null;
			Map<String,Object> retval = new HashMap<String,Object>();
			ServiceAPIClient client = new ServiceAPIClient(prop.getServiceAPIBaseUrl(), 
										prop.getServiceAPIUsername(), 
										prop.getServiceAPIPassword());
			if("TO".equals(portEnd)){
				result = client.getPort(portId);
				logger.info("Get To Port call response :: " + result);
			}else if("FROM".equals(portEnd)){
				result = client.getCloudPort(portId);
				logger.info("Get Cloud Port call response :: " + result);
			}
			
			
			if (null != result && result instanceof CustomerDedicatedPortResponse) {
				
				CustomerDedicatedPortResponse response = (CustomerDedicatedPortResponse) result;
				retval.put("Result", response);
				retval.put("nc_tech_service_id", response.getNcTechServiceId());
				retval.put("resource_port_bandwidth", response.getBandwidth());
				retval.put("connections_on_port", response.getNoOfConnections());
				retval.put("port_expiry_period", response.getExpirationPeriod());
				retval.put("local_building_name", response.getLocalBuildingName());
				retval.put("building_name", response.getLocationBuildingName());
				retval.put("city_code", response.getLocationCityCode());
				retval.put("country_code", response.getLocationCountryCode());
				
				
				
			}else if (null != result && result instanceof CloudPortResponse) {
				
				CloudPortResponse response = (CloudPortResponse) result;
				retval.put("Result", response);
				retval.put("from_port_bandwidth", response.getBandwidth());
				retval.put("service_key", response.getServiceKey());
				retval.put("vlan_id", response.getVlan());
				retval.put("csp_name", response.getCloudProvider());
				retval.put("local_building_name", response.getLocalBuildingName());
				retval.put("building_name", response.getLocationBuildingName());
				retval.put("city_code", response.getLocationCityCode());
				retval.put("country_code", response.getLocationCountryCode());
				retval.put("nc_tech_service_id_1", response.getNcTechServiceId1());
				retval.put("nc_tech_service_id_2", response.getNcTechServiceId2());
				
			}else {
				throw new Exception("Get customer port call failed :" + result);
			}
			manager.completeWorkItem(workItem.getId(), retval);
			
		} catch (Exception e) {
			logger.error("Get Cloud Service Port -> Failed :: ", e);
			HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(HandlerErrorCode.CC_GET_SERVICE_PORT));
		}
	}
	
	
	

}
