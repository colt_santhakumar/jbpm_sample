package net.colt.novitas.dcaconnection;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.HandlerUtils;
import com.colt.novitas.client.RequestAPIClient;
import com.colt.novitas.exception.NovitasHandlerException;
import com.colt.novitas.request.UpdateRequestStatusRequest;
import com.colt.novitas.response.CloudConnectionRequestResponse;
import com.colt.novitas.response.UpdateRequestStatusResponse;

public class WIHUpdateRequestStatus extends BaseWorkItemHandler {
	
	private static Logger logger = LoggerFactory.getLogger(WIHUpdateRequestStatus.class);
	
	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}
	
	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
		
		String status = null;
		EnvironmentProperties prop = getEnvProperties(workItem);
		logger = getLogger(workItem, prop.getLogUniqueId());
		try {
			
			CloudConnectionRequestResponse connectionRequest = getConnectionRequest(workItem);
			
			Integer requestId = connectionRequest.getId();
			Object penaltyCharge = getPenaltyCharge(workItem);
			status = getStatus(workItem);
			Integer statusCode = getStatusCode(workItem);
			String connectionId = getServiceConnectionId(workItem);
			
			logger.info("WIHUpdateRequestStatus -- Request Id : " + requestId);
			
			UpdateRequestStatusRequest requestObject = new UpdateRequestStatusRequest();
			requestObject.setStatus(status);
			
			if(StringUtils.isNotBlank(connectionId)){
			   requestObject.setServiceId(connectionId);
			}
			if (statusCode != null) {
				requestObject.setStatusCode(statusCode);
			}
			if(null!= penaltyCharge){
			requestObject.setPenaltyCharge((Float)penaltyCharge);
			}
			
			if(INPROGRESS.equals(status)){
				addJbpmDetails(requestObject, workItem);
			}
			
			Object response = new RequestAPIClient(
									prop.getRequestAPIUrl(),
									prop.getRequestAPIUsername(),
									prop.getRequestAPIPassword()
									).updateCloudPortOrConnectionRequest(requestId, requestObject);
			
			logger.info("Return from update request object: " + response + " class " + response.getClass().getName());
			
			if (response instanceof UpdateRequestStatusResponse) {
				Integer statusId = ((UpdateRequestStatusResponse)response).getStatusId();
				logger.info("Return code from update request: " + statusId);
				if (statusId <= 0) {
					throw new NovitasHandlerException(null);
				}
			}
			else if (response instanceof String) {
			logger.info("response is string");
			if ("-1".equals((String)response) || "FAILED".equals((String)response)) {
				throw new NovitasHandlerException(null);
				}
			}
			
			
			Map<String,Object> retval = new HashMap<String,Object>();
			manager.completeWorkItem(workItem.getId(), retval);
		} catch (Exception e) {
			logger.error("WIHUpdateRequestStatus -> Failed ",  e);
			if ("INPROGRESS".equals(status)) {
				HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(getErrorCode(workItem)));
			}
			
			
		}
	}
	

}
