package net.colt.novitas.dcaconnection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.HandlerUtils;
import com.colt.novitas.client.ServiceAPIClient;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;
import com.colt.novitas.request.CreateCloudConnRequest;
import com.colt.novitas.response.CloudConnectionRequestResponse;
import com.colt.novitas.response.ConnectionPortPair;

public class WIHCreateConnection extends BaseWorkItemHandler {
	
	private static Logger logger = LoggerFactory.getLogger(WIHCreateConnection.class);
	
	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}
	

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
		EnvironmentProperties prop = getEnvProperties(workItem);
		logger = getLogger(workItem, prop.getLogUniqueId());
		try {
			
			CloudConnectionRequestResponse connectionRequest = getConnectionRequest(workItem);
			
			CreateCloudConnRequest servieConnReq = new CreateCloudConnRequest();
			servieConnReq.setName(connectionRequest.getConnectionName());
			servieConnReq.setBandwidth(connectionRequest.getBandwidth());
			servieConnReq.setRentalCharge(connectionRequest.getRentalCharge());
			servieConnReq.setRentalCurrency(connectionRequest.getRentalCurrency());
			servieConnReq.setRentalUnit(connectionRequest.getRentalUnit());
			servieConnReq.setServiceId(connectionRequest.getServiceId());
			servieConnReq.setResourceId1(null);
			servieConnReq.setResourceId2(null);
			servieConnReq.setOcn(connectionRequest.getOcn());
			servieConnReq.setCustomerName(connectionRequest.getCustomerName());
			servieConnReq.setCommitmentPeriod(connectionRequest.getCommitmentPeriod());
			
			servieConnReq.setPorts(getServicePortPair(connectionRequest.getPorts()));
			servieConnReq.setConnectionType(DCA_CONNECTION);
			String response = new ServiceAPIClient(prop.getServiceAPIBaseUrl(), 
												prop.getServiceAPIUsername(), 
												prop.getServiceAPIPassword()).createCloudConnection(servieConnReq);
			
			logger.info("Create Cloud Connection :" + response);
			
			if (StringUtils.isBlank(response)) {
				throw new NovitasHandlerException();
			}
			
			Map<String,Object> retval = new HashMap<String,Object>();
			retval.put("Result", response);
			manager.completeWorkItem(workItem.getId(), retval);
			
		} catch (Exception e) {
			logger.error("WIHCreateConnection -> Fail",  e.getMessage());
			HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(HandlerErrorCode.CC_CREATE_SERVICE_CONN));
		}
	}
	
	private List<ConnectionPortPair> getServicePortPair(List<com.colt.novitas.request.ConnectionPortPair> portpairs){
		
		List<ConnectionPortPair> servicePortPairs = new ArrayList<ConnectionPortPair>();
		for (com.colt.novitas.request.ConnectionPortPair connectionPortPair : portpairs) {
			ConnectionPortPair servicePortPair = new ConnectionPortPair();
			
			servicePortPair.setFromPortId(connectionPortPair.getFromPortId());
			servicePortPair.setToPortId(connectionPortPair.getToPortId());
			servicePortPair.setFromPortName(connectionPortPair.getFromPortName());
			servicePortPair.setToPortName(connectionPortPair.getToPortName());
			servicePortPair.setCloudProviderFromPort(connectionPortPair.getCloudProviderFromPort());
			servicePortPair.setCloudProviderToPort(connectionPortPair.getCloudProviderToPort());
			servicePortPair.setaEndVlanMapping(connectionPortPair.getaEndVlanMapping());
			servicePortPair.setbEndVlanMapping(connectionPortPair.getbEndVlanMapping());
			servicePortPair.setaEndVlanType(connectionPortPair.getaEndVlanType());
			servicePortPair.setbEndVlanType(connectionPortPair.getbEndVlanType());
			servicePortPair.setaEndVlanIds(getServiceVLANIds(connectionPortPair.getaEndVlanIds()));
			servicePortPair.setbEndVlanIds(getServiceVLANIds(connectionPortPair.getbEndVlanIds()));
			
			servicePortPairs.add(servicePortPair);
		}
		
		return servicePortPairs;
	}
	
		
}
