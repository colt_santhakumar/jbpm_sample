package net.colt.novitas.dcaconnection;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.HandlerUtils;
import com.colt.novitas.client.AzureAPIClient;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;
import com.colt.novitas.request.ProvisioningOperation;

public class WIHUpdateAzureStatus extends BaseWorkItemHandler {
	
	private static Logger logger = LoggerFactory.getLogger(WIHUpdateAzureStatus.class);
	
	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}
	

public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
	    EnvironmentProperties prop = getEnvProperties(workItem);
	    logger = getLogger(workItem, prop.getLogUniqueId());
		try {
			Map<String,Object> retval = new HashMap<String,Object>();
			String status = getStatus(workItem);
			String serviceKey = getServiceKey(workItem);
			AzureAPIClient azureClient = new AzureAPIClient(prop.getAzureApiUrl(), prop.getAzureJksPath(),  prop.getAzureJksPassword(),
			               prop.getAzureSubscriptionId(),prop.getAzureApiVersionId(),prop.getAzureRequestTimeOut());
			
             String response ="";
			if(PROVISIONING.equals(status)){
				logger.info("Provisioning Azure circuit :: "+serviceKey);
				response = azureClient.provisionCrossConnection(serviceKey);
			}else if(PROVISIONED.equals(status)){
				logger.info("Updating Azure circuit as PROVISIONED:: "+serviceKey);
				response = azureClient.UpdateExpressRouteCircuit(serviceKey, "SUCCESS",ProvisioningOperation.PROVISIONED);
			}else if(DEPROVISION.equals(status)){
				logger.info("Deprovision Azure circuit :: "+serviceKey);
				response = azureClient.UpdateExpressRouteCircuit(serviceKey, "SUCCESS",ProvisioningOperation.DE_PROVISION);
			}
			if ( StringUtils.isBlank(response) || "ERROR".equals(response)) {
				logger.info("Failed to update Azure circuit :: ");
				throw new NovitasHandlerException();
			}
			manager.completeWorkItem(workItem.getId(), retval);
		} catch (Exception e) {
			logger.error("WIHUpdateAzureStatus -> Failed", e);
			HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(HandlerErrorCode.INVALID_AZURE_SERVICE_STATUS));
		}
	}
		

}
