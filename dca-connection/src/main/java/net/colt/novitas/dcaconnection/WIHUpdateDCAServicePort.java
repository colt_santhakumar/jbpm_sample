package net.colt.novitas.dcaconnection;

import java.util.HashMap;
import java.util.Map;

import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.HandlerUtils;
import com.colt.novitas.client.ServiceAPIClient;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;
import com.colt.novitas.request.UpdateCloudPortRequest;
import com.colt.novitas.response.AzureServiceDetails;

public class WIHUpdateDCAServicePort extends BaseWorkItemHandler {
	
	private static Logger logger = LoggerFactory.getLogger(WIHUpdateDCAServicePort.class);
	
	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}
	

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
		EnvironmentProperties prop = getEnvProperties(workItem);
		logger = getLogger(workItem, prop.getLogUniqueId());
		try {
			String dedicatedPortId = getNovitasPortServiceId(workItem);
            AzureServiceDetails azureServiceDetails = getAzureServiceDetails(workItem);
			UpdateCloudPortRequest portReq = new UpdateCloudPortRequest();
			
			//portReq.setStatus("ACTIVE");
			//portReq.setResourceId1(resourceId1);
			portReq.setBandwidth(Integer.valueOf(azureServiceDetails.getBandwidth()));
			portReq.setVlan(Integer.valueOf(azureServiceDetails.getVlanId()));
			
			Object response = new ServiceAPIClient(prop.getServiceAPIBaseUrl(), 
												   prop.getServiceAPIUsername(), 
												   prop.getServiceAPIPassword()).updateCloudPort(dedicatedPortId, portReq);
			
			if ("FAILED".equals(response)) {
				logger.info("Port status update failed. Port : " + dedicatedPortId);
				throw new NovitasHandlerException();
			}
			
			Map<String,Object> retval = new HashMap<String,Object>();
			manager.completeWorkItem(workItem.getId(), retval);
			
		} catch (Exception e) {
			logger.error("WIHUpdateDCAServicePort -> Failed", e);
				HandlerUtils.throwHandlerExcetpion(
						new NovitasHandlerException(HandlerErrorCode.CP_UPDATE_SERVICE_PORT_DETAILS));
		}
	}
	
}
