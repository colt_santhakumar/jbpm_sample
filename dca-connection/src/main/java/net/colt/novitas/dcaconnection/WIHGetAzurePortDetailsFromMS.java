package net.colt.novitas.dcaconnection;

import java.util.HashMap;
import java.util.Map;

import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.HandlerUtils;
import com.colt.novitas.client.AzureAPIClient;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;
import com.colt.novitas.response.AzureServiceDetails;

public class WIHGetAzurePortDetailsFromMS extends BaseWorkItemHandler {
	
	private static Logger logger = LoggerFactory.getLogger(WIHGetAzurePortDetailsFromMS.class);
	
	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}
	

public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
	    EnvironmentProperties prop = getEnvProperties(workItem);
	    logger = getLogger(workItem, prop.getLogUniqueId());
		try {
			
			String  serviceKey = getServiceKey(workItem);
			logger.info("Getting AzureService Details for the cloud provider :: "+serviceKey);
			AzureServiceDetails azureServiceDetails = new AzureAPIClient(prop.getAzureApiUrl(), 
								                        prop.getAzureJksPath(), 
								                        prop.getAzureJksPassword(),prop.getAzureSubscriptionId(),
								                        prop.getAzureApiVersionId(),prop.getAzureRequestTimeOut())
								                        .getExpressRouteCircuit(serviceKey);
			
			Map<String,Object> retval = new HashMap<String,Object>();
			if ( null == azureServiceDetails) {
				logger.info("No AzureService Details for the service key :: "+serviceKey);
				throw new NovitasHandlerException();
			}
			logger.info("Response from MS Azure for service key "+ serviceKey+ " is :: "+azureServiceDetails);
			retval.put("azure_service_details",azureServiceDetails);
			retval.put("vlan_id", azureServiceDetails.getVlanId());
			retval.put("bandwidth", azureServiceDetails.getBandwidth());
			retval.put("azure_service_status", azureServiceDetails.getCircuitStatus());
                        retval.put("azure_provision_status",azureServiceDetails.getProvisioningState());
			manager.completeWorkItem(workItem.getId(), retval);
		} catch (Exception e) {
			logger.error("WIHGetAzurePortDetailsFromXNG -> Failed", e);
			HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(HandlerErrorCode.FAILED_TO_GET_AZURE_DETAILS));
		}
	}
		

}
