package net.colt.novitas.dcaconnection;

import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.HandlerUtils;
import com.colt.novitas.client.RequestAPIClient;
import com.colt.novitas.exception.NovitasHandlerException;
import com.colt.novitas.request.ConnectionPortPair;
import com.colt.novitas.response.CloudConnectionRequestResponse;

public class WIHGetRequest extends BaseWorkItemHandler {
	
	private static Logger logger = LoggerFactory.getLogger(WIHGetRequest.class);
	
	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}
	
	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
		EnvironmentProperties prop = getEnvProperties(workItem);
		logger = getLogger(workItem, prop.getLogUniqueId());
		try {
			Integer requestId = getRequestId(workItem);
			
			Object requestObj = new RequestAPIClient(
					prop.getRequestAPIUrl(),
					prop.getRequestAPIUsername(),
					prop.getRequestAPIPassword()
					).getCloudPortOrConnectionRequest(requestId);
			
			logger.info("WIHGetRequest -> Response: " + requestObj.toString());
			
			if (requestObj instanceof CloudConnectionRequestResponse) {
				CloudConnectionRequestResponse connReq = (CloudConnectionRequestResponse) requestObj;
				
				Map<String,Object> retval = new HashMap<String,Object>();
				retval.put("Result", connReq);
				getPortDetailsToMap(connReq.getPorts(),retval);
				retval.put("request_bandwidth", connReq.getBandwidth());
				retval.put("service_connection_id", connReq.getConnectionId());
				retval.put("novitas_service_id", connReq.getServiceId());
				retval.put("ocn", connReq.getOcn());
				manager.completeWorkItem(workItem.getId(), retval);
				
			}
			else {
				throw new Exception("Invalid connection request.");
			}
			
		} catch (Exception e) {
			logger.error("WIHGetRequest -> Error: ", e);
			HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(getErrorCode(workItem)));
		}
		
	}
	
	private void getPortDetailsToMap(List<ConnectionPortPair> ports,Map<String,Object> retval) throws Exception{
		logger.info("Extranting Port details from CloudConnection Request >>> ");
		if(null != ports && ports.size() == 2 ){
			ListIterator<ConnectionPortPair> portsIterator = ports.listIterator();
				 while (portsIterator.hasNext()) {
					int index = portsIterator.nextIndex()+1;
					ConnectionPortPair port = portsIterator.next();
					logger.info("Extranted Port details from CloudConnection Request >>> "+port+"  index ->"+index);
					retval.put("connection_port_pair_req_"+index,port);
					retval.put("service_from_port_id",port.getFromPortId());
					retval.put("service_to_port_id_"+index,port.getToPortId());
				}
				 
		}else{
			throw new Exception("Invalid connection request.");
		}
		
	}

}
