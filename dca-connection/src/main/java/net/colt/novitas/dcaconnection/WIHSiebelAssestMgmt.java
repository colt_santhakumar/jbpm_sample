package net.colt.novitas.dcaconnection;

import java.util.HashMap;
import java.util.Map;

import net.colt.novitas.dcaconnection.util.ConnectionUtil;

import org.apache.commons.lang.StringUtils;
import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.HandlerUtils;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;
import com.colt.novitas.request.ConnectionPortPair;
import com.colt.novitas.response.CloudConnectionRequestResponse;
import com.colt.novitas.siebel.assests.client.SiebelAssetsClient;
import com.colt.novitas.siebel.assets.request.SiebelAssestRequest;

/**
 * @author DRao This is a WIH file created by Dharrma Rao.
 * 
 */
public class WIHSiebelAssestMgmt extends BaseWorkItemHandler {

	private static Logger logger = LoggerFactory.getLogger(WIHSiebelAssestMgmt.class);

	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
		EnvironmentProperties prop = getEnvProperties(workItem);
		logger = getLogger(workItem, prop.getLogUniqueId());
		try {

			CloudConnectionRequestResponse connectionRequestResponse = getConnectionRequest(workItem);
			String serviceConnectionId = getServiceConnectionId(workItem);
			String novitasId = getParentServiceId(workItem);
			String status  = getStatus(workItem);
			String circuitReference =  (String) workItem.getParameter("circuit_reference");
			String localName =  (String) workItem.getParameter("local_name");
			String aLocalBuildingName =  (String) workItem.getParameter("a_local_building_name");
			String bLocalBuildingName =  (String) workItem.getParameter("b_local_building_name");
			String aBuildingName =  (String) workItem.getParameter("a_building_name");
			String bBuildingName =  (String) workItem.getParameter("b_building_name");
			String customerRegion =  (String) workItem.getParameter("customer_region");
			SiebelAssestRequest request = new SiebelAssestRequest();
			Map <String,String> attributesMap = new HashMap<String, String>();
			request.setAttributesMap(attributesMap);
			request.setPamFlag("Y");
			attributesMap.put("bandwidth",String.valueOf(connectionRequestResponse.getBandwidth()));
	        attributesMap.put("connection_type","DCA_CONNECTION");
	        attributesMap.put("last_updated","");
	        attributesMap.put("created_date",connectionRequestResponse.getRequestedAt());
	        attributesMap.put("csp_name", "Microsoft Azure");
			ConnectionPortPair portPair = getConnectionPortPair(workItem);
			if(null != portPair){
				attributesMap.put("from_port_id",portPair.getFromPortId());
		        attributesMap.put("to_port_id",portPair.getToPortId());
		        attributesMap.put("resource_id",circuitReference);
		        attributesMap.put("from_uni_type",portPair.getaEndVlanMapping());
		        attributesMap.put("to_uni_type",portPair.getbEndVlanMapping());
			}
			request.setProductName("COLT On Demand DCA Connection");
			request.setResourceId(circuitReference);//for all operations mandatory
			if(StringUtils.isNotBlank(status) && "ACTIVE".equalsIgnoreCase(status)){
				String slaId = ConnectionUtil.getSlaIdForConnection(serviceConnectionId, workItem);
				logger.info("SLA ID is ::::"+slaId);
				request.setSlaId(slaId);
				attributesMap.put("rental_charge",connectionRequestResponse.getRentalCharge().toString());
		        attributesMap.put("rental_unit",connectionRequestResponse.getRentalUnit());
		        attributesMap.put("rental_currency",connectionRequestResponse.getRentalCurrency());
			}else{
				 attributesMap.put("decommissioned_on",connectionRequestResponse.getRequestedAt());
				request.setSlaId("");
			}
			
			if("ASIA".equals(customerRegion)){
	        	attributesMap.put("A00_AccountJPN", localName);
	        	attributesMap.put("A03_A-BuildingName", aBuildingName);
	        	attributesMap.put("A04_B-BuildingName", bBuildingName);
	        	attributesMap.put("A06_B-AccountName", localName);
	        	attributesMap.put("A16_A03_A-BuildingNameJPN", aLocalBuildingName);
	        	attributesMap.put("A17_B-BuildingNameJPN", bLocalBuildingName);
	        	attributesMap.put("A18_ServiceJPN", "オンデマンド");
	        	attributesMap.put("A05_A-AccountName", connectionRequestResponse.getCustomerName());
	        	attributesMap.put("Service_Type", request.getProductName());
	        	attributesMap.put("A08_STTEnable", "No");
	        	attributesMap.put("A09_BillingDate", "");
	        	attributesMap.put("A14_A-AccountNameJPN", localName);
	        	attributesMap.put("Network", "ONNet");
	        	attributesMap.put("ResilientOptions", "Unprotected");
	        	attributesMap.put("ContractedRegion", customerRegion);
	        }
			request.setServiceId(serviceConnectionId);
			request.setServiceName(connectionRequestResponse.getConnectionName());
			request.setOcn(connectionRequestResponse.getOcn());
			request.setNovitasServiceId(novitasId);
			request.setStatus(status);
			
			request.setUrlCallback(prop.getCallBackUrl());
			
			String transactionId  = new SiebelAssetsClient(prop.getSiebelUrl(),prop.getSiebelUsername(),
					                                    prop.getSiebelPassword()).receiveAssetMgmtRequest(request);

			Map<String, Object> retval = new HashMap<String, Object>();
			if (StringUtils.isBlank(transactionId)) {
				logger.info("Failed to Update Siebel Assests Management");
				throw new NovitasHandlerException();
			}
			
			retval.put("Result", transactionId);
			manager.completeWorkItem(workItem.getId(), retval);

		} catch (Exception e) {
			logger.error("WIHSiebelAssestMgmt -> Failed", e);
				HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(
						HandlerErrorCode.CREATE_OR_UPDATE_SIEBEL_ASSETS_MGMT));
			
		}
	}
	

}
