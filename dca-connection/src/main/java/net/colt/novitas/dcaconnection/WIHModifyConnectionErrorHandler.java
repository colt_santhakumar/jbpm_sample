package net.colt.novitas.dcaconnection;

import java.util.HashMap;
import java.util.Map;

import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.client.RequestAPIClient;
import com.colt.novitas.client.ServiceAPIClient;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;
import com.colt.novitas.request.UpdateRequestStatusRequest;

public class WIHModifyConnectionErrorHandler extends BaseWorkItemHandler {
	
	private static Logger logger = LoggerFactory.getLogger(WIHModifyConnectionErrorHandler.class);

	private WorkItem workItem;
	private EnvironmentProperties prop;

	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {

		Integer requestId = null;
		this.prop = getEnvProperties(workItem);
		logger = getLogger(workItem, prop.getLogUniqueId());
		try {
			this.workItem = workItem;
			requestId = getRequestId(workItem);
			
			logger.info("Error handling started for create connection request. ID : " + requestId);
			
			//Get error details
			HandlerErrorCode errorCode = null;
			Exception exception = getWorkflowError(workItem);
			if (exception instanceof NovitasHandlerException) {
				errorCode = ((NovitasHandlerException) exception).getErrorCode();
				logger.info("Error code : " + errorCode);
			}

			if (errorCode == null) {
				logger.info("NovitasHandlerException thrown without error code. Unable to process further.");
				errorCode = HandlerErrorCode.GENERAL_ERROR_CODE;
			}

			Map<String,Object> retval = new HashMap<String,Object>();
			
			if (errorCode != null) {
				
				if (errorCode != null && errorCode.isFailRequest()) {
					failRequest(errorCode);
				}
				
				if (errorCode.isPaymentError()) {
					logger.info("Failed to create or cancel charges. Code : " + errorCode + " request : " + requestId);
					retval.put("payment_failed", "true");
				}
				
				//compensate(errorCode);
				retval.put("payment_error_code", errorCode.getErrorId());
				retval.put("payment_error_description", errorCode.getDescription());
			}
			else {
				failRequest(null);
			}
			
			logger.info("Error handling completed for request : " + requestId);

			manager.completeWorkItem(workItem.getId(), retval);
			
		} catch (Exception e) {
			logger.error("Error handling failed for create connection request. ID : " + requestId, e);
			manager.abortWorkItem(workItem.getId());
		}
		
	}
	
	protected void compensate(HandlerErrorCode errorCode) {

		logger.info("Calling compensation with error code : " + errorCode);
		
	}
	
	
	protected void failRequest(HandlerErrorCode errorCode) {
/*
		Integer requestId = null;

		try {
			
			requestId = getRequestId(workItem);

			RequestAPIClient client = getRequestClient();
			UpdateRequestStatusRequest requestObject = new UpdateRequestStatusRequest();
			requestObject.setStatus("FAILED");

			if (errorCode != null) {
				requestObject.setStatusCode(errorCode.getErrorId());
			}

			client.updateCloudPortOrConnectionRequest(requestId, requestObject);
		} catch (Exception e) {
			logger.info("Failed to update request to FAILED state. Request Id : " + requestId);
			e.printStackTrace();
		}*/

	}
	
	
	

	protected RequestAPIClient getRequestClient() {

		return new RequestAPIClient(
				prop.getRequestAPIUrl(),
				prop.getRequestAPIUsername(),
				prop.getRequestAPIPassword()
				);
	}

	protected ServiceAPIClient getServiceClient() {

		return new ServiceAPIClient(prop.getServiceAPIBaseUrl(), 
				prop.getServiceAPIUsername(), 
				prop.getServiceAPIPassword());
	}

}
