package net.colt.novitas.dcaconnection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.HandlerUtils;
import com.colt.novitas.client.ServiceAPIClient;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;
import com.colt.novitas.request.ConnectionPortPair;
import com.colt.novitas.request.UpdateCloudConnRequest;
import com.colt.novitas.response.CloudConnectionRequestResponse;

public class WIHUpdateCustomerConnectionToModifying extends BaseWorkItemHandler {
	
	private static Logger logger = LoggerFactory.getLogger(WIHUpdateCustomerConnectionToModifying.class);
	
	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}
	

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
		
		String status = null;
		EnvironmentProperties prop = getEnvProperties(workItem);
		logger = getLogger(workItem, prop.getLogUniqueId());
		try {
			String connectionId =  getServiceConnectionId(workItem);
			status = getStatus(workItem);
			
			UpdateCloudConnRequest connReq = new UpdateCloudConnRequest();
			connReq.setStatus(status);
			logger.info("Request object: " + connReq);
			Object result = new ServiceAPIClient(prop.getServiceAPIBaseUrl(), 
										prop.getServiceAPIUsername(), 
										prop.getServiceAPIPassword()).updateCloudConnection(connectionId, connReq);
			
			logger.info("Results of update service conn : "  + result);
			if (result != null && "FAILED".equals(String.valueOf(result))) {
				throw new NovitasHandlerException();
			}
			
			Map<String,Object> retval = new HashMap<String,Object>();
			manager.completeWorkItem(workItem.getId(), retval);
			
		} catch (Exception e) {
			logger.error("WIHUpdateCustomerConnection - >  failed ", e);
				HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(HandlerErrorCode.MC_UPDATE_SERVICE_MODIFYING));
		}
	}
	
}
