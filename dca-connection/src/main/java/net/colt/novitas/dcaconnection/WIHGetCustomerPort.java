package net.colt.novitas.dcaconnection;

import java.util.HashMap;
import java.util.Map;

import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.client.ServiceAPIClient;
import com.colt.novitas.response.CustomerDedicatedPortResponse;

public class WIHGetCustomerPort extends BaseWorkItemHandler {
	
	private static Logger logger = LoggerFactory.getLogger(WIHGetCustomerPort.class);
	
	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
		EnvironmentProperties prop = getEnvProperties(workItem);
		logger = getLogger(workItem, prop.getLogUniqueId());
		try {
			String portId = getServicePortId(workItem);
			
			Object result = new ServiceAPIClient(prop.getServiceAPIBaseUrl(), 
					prop.getServiceAPIUsername(), 
					prop.getServiceAPIPassword()).getPort(portId); 
			
			if (result instanceof CustomerDedicatedPortResponse) {
				logger.info("Get customer port call successful :" + result);
			}
			else {
				logger.info("Get customer port call failed :" + result);
			}
			
			Map<String,Object> retval = new HashMap<String,Object>();
			retval.put("Result", result);
			manager.completeWorkItem(workItem.getId(), retval);
			
		} catch (Exception e) {
			logger.error("WIHGetCustomerPort -> Fail : ", e);
			manager.abortWorkItem(workItem.getId());
		}
	}
		
}
