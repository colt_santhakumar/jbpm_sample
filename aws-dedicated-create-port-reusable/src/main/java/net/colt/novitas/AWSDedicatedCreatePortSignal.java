package net.colt.novitas;

import java.io.Serializable;

public class AWSDedicatedCreatePortSignal implements Serializable {

	private static final long serialVersionUID = -8715672728295009906L;

	private String status;
	private String awsRef;
	private String connectorType;
	private String technologyType;
	private String siteId;
	private String buildingId;
	
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}

	public String getAwsRef() {
		return awsRef;
	}

	public void setAwsRef(String awsRef) {
		this.awsRef = awsRef;
	}

	public String getConnectorType() {
		return connectorType;
	}

	public void setConnectorType(String connectorType) {
		this.connectorType = connectorType;
	}

	public String getTechnologyType() {
		return technologyType;
	}

	public void setTechnologyType(String technologyType) {
		this.technologyType = technologyType;
	}

	public String getSiteId() {
		return siteId;
	}

	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}

	public String getBuildingId() {
		return buildingId;
	}

	public void setBuildingId(String buildingId) {
		this.buildingId = buildingId;
	}

	@Override
	public String toString() {
		return "AWSDedicatedCreatePortSignal [status=" + status + ", awsRef=" + awsRef + ", connectorType="
				+ connectorType + ", technologyType=" + technologyType + ", siteId=" + siteId + ", buildingId="
				+ buildingId + "]";
	}


}
