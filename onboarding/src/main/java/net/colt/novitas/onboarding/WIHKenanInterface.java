package net.colt.novitas.onboarding;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.HandlerUtils;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;
import com.colt.novitas.kenan.interfaces.client.KenanInterfaceClient;
import com.colt.novitas.kenan.interfaces.request.BillOrderRequest;

/**
 * @author DRao This is a WIH file created by Dharrma Rao.
 * 
 */
public class WIHKenanInterface extends BaseWorkItemHandler {

	private static Logger logger = LoggerFactory.getLogger(WIHKenanInterface.class);

	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {

		try {
			EnvironmentProperties prop = getEnvProperties(workItem);
			logger = getLogger(workItem, prop.getLogUniqueId());
			Integer customerId = (Integer) workItem.getParameter("customer_id");
			String novitasServiceId = (String) workItem.getParameter("novitas_service_id");	
			String ocn  = (String) workItem.getParameter("ocn");
			String bcn  = (String) workItem.getParameter("bcn");
			String rentalCurrency = (String) workItem.getParameter("rental_currency");
			String countryName = (String) workItem.getParameter("country_name");
			String countryCode = (String) workItem.getParameter("country_code");
			
			BillOrderRequest billOrderRequest = new BillOrderRequest();
			
			billOrderRequest.setCustomerId(customerId);
			billOrderRequest.setNovitasServiceId(novitasServiceId);
			billOrderRequest.setOcn(ocn);
			billOrderRequest.setBcn(bcn);
			billOrderRequest.setCustomerCountryCode(countryCode);
			billOrderRequest.setCustomerCountryName(countryName);
			billOrderRequest.setBillingCurrency(rentalCurrency);
			
			
			//EnvironmentProperties prop = HandlerUtils.getEnvironmentProperties(workItem);
			billOrderRequest.setUrlCallback(prop.getCallBackUrl());
			
			String transactionId  = new KenanInterfaceClient(prop.getKenanInterfaceUrl(),prop.getKenanInterfaceUsername(),
					                                    prop.getKenanInterfacePassword()).receiveBillingOrder(billOrderRequest);

			Map<String, Object> retval = new HashMap<String, Object>();
			if (StringUtils.isBlank(transactionId)) {
				logger.info("Failed to Notify Kenan Interface");
				throw new NovitasHandlerException();
			}
			
			retval.put("Result", transactionId);
			manager.completeWorkItem(workItem.getId(), retval);

		} catch (Exception e) {
			logger.error("WIHKenanInterface -> Failed", e);
				HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(
						HandlerErrorCode.NOTIFY_ONBOARDING_KENAN_SYSTEM));
			
		}
	}
	

}