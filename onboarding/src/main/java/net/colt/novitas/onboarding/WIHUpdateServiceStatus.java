package net.colt.novitas.onboarding;

import java.util.HashMap;
import java.util.Map;

import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.HandlerUtils;
import com.colt.novitas.client.ServiceAPIClient;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;

/**
 * @author DRao This is a WIH file created by Dharrma Rao.
 * 
 */
public class WIHUpdateServiceStatus extends BaseWorkItemHandler {

	private static  Logger logger = LoggerFactory
			.getLogger(WIHUpdateServiceStatus.class);

	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {

		try {
			EnvironmentProperties prop = getEnvProperties(workItem);
			logger = getLogger(workItem, prop.getLogUniqueId());
			String novitasServiceId = (String) workItem.getParameter("novitas_service_id");
            String status = (String) workItem.getParameter("status");
			ServiceAPIClient client = new ServiceAPIClient(prop.getServiceAPIBaseUrl(), 
                    prop.getServiceAPIUsername(), 
                    prop.getServiceAPIPassword());
 
			Object response = client.updateServiceStatus(novitasServiceId, status, "");

			if ("FAILED".equals(response)) {
				logger.info("Failed to Register callback details");
				throw new NovitasHandlerException();
			}

			Map<String, Object> retval = new HashMap<String, Object>();
			manager.completeWorkItem(workItem.getId(), retval);

		} catch (Exception e) {
			logger.error("WIHUpdateServiceStatus -> Failed", e);
				HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(
						HandlerErrorCode.UPDATE_ONBOARDING_SERVICE_STATUS));
			
		}
	}

}