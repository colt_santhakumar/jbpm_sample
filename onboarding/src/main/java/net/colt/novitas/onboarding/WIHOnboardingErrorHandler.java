package net.colt.novitas.onboarding;

import java.util.HashMap;
import java.util.Map;

import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.admin.client.AdminAPIClient;
import com.colt.novitas.client.ServiceAPIClient;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;


public class WIHOnboardingErrorHandler extends BaseWorkItemHandler {
	
	private static Logger logger = LoggerFactory.getLogger(WIHOnboardingErrorHandler.class);

	private WorkItem workItem;
	private EnvironmentProperties prop;

	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {

		try {
			this.prop = getEnvProperties(workItem);
			logger = getLogger(workItem, prop.getLogUniqueId());
			logger.info("Starting error handling for onboarding request......... ");

			this.workItem = workItem;
			Integer customerId = (Integer) workItem.getParameter("customer_id");
			String novitasServiceId = (String) workItem.getParameter("novitas_service_id");	

			logger.info("Starting error handling for request : " );

			Map<String,Object> retval = new HashMap<String,Object>();
			
			HandlerErrorCode errorCode = null;
			Exception exception = getWorkflowError();
			if (exception instanceof NovitasHandlerException) {
				errorCode = ((NovitasHandlerException) exception).getErrorCode();
				logger.info("Error code : " + errorCode);
			}

			if (errorCode == null) {
				logger.info("NovitasHandlerException thrown without error code. Unable to process further.");
			}
			else  {
				logger.info("Error Description is ::::::::::::"+errorCode.getDescription());
				getAdminApiClient().updateCustomerStatus(customerId, "ONBOARD_FAILED");
				getServiceClient().updateServiceStatus(novitasServiceId, "ONBOARD_FAILED", errorCode.getDescription().replace(" ", "_"));
				
			}

			manager.completeWorkItem(workItem.getId(), retval);
			
		} catch (Exception e) {
			logger.error("Error handling for creat port has been failed.", e);
			manager.abortWorkItem(workItem.getId());
		}

	}

   
	private AdminAPIClient getAdminApiClient() {

		return new AdminAPIClient(prop.getAdminApiLoginUrl(),
				prop.getAdminApiCustomerUrl(), prop.getAdminApiUserName(),
				prop.getAdminApiPassword());
	}

	private ServiceAPIClient getServiceClient() {

		return new ServiceAPIClient(prop.getServiceAPIBaseUrl(), 
                prop.getServiceAPIUsername(), 
                prop.getServiceAPIPassword());
	}

	private Exception getWorkflowError() {
		Object obj = workItem.getParameter("error_obj");
		if (obj instanceof Exception) {
			return (Exception) obj;
		}
		return null;
	}

	
}
