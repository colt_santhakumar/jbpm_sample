package net.colt.novitas;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PortMigrateRequest implements Serializable{
    
	private static final long serialVersionUID = 8837573856265216816L;
	
	@JsonProperty("request_id")
	private Integer requestId;
	
	@JsonProperty("port_id")
	private String portId;
    
	@JsonProperty("status")
	private String status;
	
	@JsonProperty("nc_tech_service_id_1")
	private String ncTechServiceId1;
	
	@JsonProperty("nc_tech_service_id_2")
	private String ncTechServiceId2;
	
	@JsonProperty("host_address")
    private String hostAddrss;
    
	@JsonProperty("process_id")
    private String processID;

	public Integer getRequestId() {
		return requestId;
	}

	public void setRequestId(Integer requestId) {
		this.requestId = requestId;
	}

	public String getPortId() {
		return portId;
	}

	public void setPortId(String portId) {
		this.portId = portId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getNcTechServiceId1() {
		return ncTechServiceId1;
	}

	public void setNcTechServiceId1(String ncTechServiceId1) {
		this.ncTechServiceId1 = ncTechServiceId1;
	}
	
	public String getNcTechServiceId2() {
		return ncTechServiceId2;
	}

	public void setNcTechServiceId2(String ncTechServiceId2) {
		this.ncTechServiceId2 = ncTechServiceId2;
	}

	public String getHostAddrss() {
		return hostAddrss;
	}

	public void setHostAddrss(String hostAddrss) {
		this.hostAddrss = hostAddrss;
	}

	public String getProcessID() {
		return processID;
	}

	public void setProcessID(String processID) {
		this.processID = processID;
	}

    
  	
	
}
