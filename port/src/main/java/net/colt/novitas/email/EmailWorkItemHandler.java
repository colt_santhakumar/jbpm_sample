package net.colt.novitas.email;

import java.util.Arrays;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.HandlerUtils;
import org.jbpm.process.workitem.AbstractLogOrThrowWorkItemHandler;
import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.jbpm.process.workitem.email.Email;
import org.jbpm.process.workitem.email.Connection;
import org.jbpm.process.workitem.email.Recipients;
import org.jbpm.process.workitem.email.Message;
import org.jbpm.process.workitem.email.Recipient;

/**
 * WorkItemHandler for sending email.
 * 
 * Expects the following parameters:
 *  - "From" (String): sends an email from the given the email address
 *  - "To" (String): sends the email to the given email address(es),
 *                   multiple addresses must be separated using a semi-colon (';') 
 *  - "Subject" (String): the subject of the email
 *  - "Body" (String): the body of the email (using HTML)
 * Is completed immediately and does not return any result parameters.  
 * 
 * Sending an email cannot be aborted.
 * 
 */	
public class EmailWorkItemHandler extends AbstractLogOrThrowWorkItemHandler {

	private Connection connection;
	
	public EmailWorkItemHandler() {
	}

	private void setConnection(WorkItem workItem) {
		EnvironmentProperties prop = HandlerUtils.getEnvironmentProperties(workItem);

		if (prop.getSmtpHost() != null && prop.getSmtpPort() != null) {
			connection = new Connection();
			connection.setHost(prop.getSmtpHost());
			connection.setPort(prop.getSmtpPort());
			connection.setUserName(prop.getSmtpUserName());
			connection.setPassword(prop.getSmtpPassword());
            connection.setStartTls(prop.getSmtpStarTls() != null ? prop.getSmtpStarTls() : false);

		} else {
			throw new IllegalArgumentException("Connection not initialized for Email");
		}
	}
	
	public Connection getConnection() {
		return connection;
	}
	
	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
		if (connection == null) {
			setConnection(workItem);
		}

		try {
    		Email email = createEmail(workItem, connection);
    		SendHtml.sendHtml(email, getDebugFlag(workItem));
    		// avoid null pointer when used from deadline escalation handler
    	    if (manager != null) {
    	 	  manager.completeWorkItem(workItem.getId(), null);    	 	
    	    }
		} catch (Exception e) {
		    handleException(e);
		}
	}

	protected static Email createEmail(WorkItem workItem, Connection connection) { 
	    Email email = new Email();
        Message message = new Message();
        message.setFrom((String) workItem.getParameter("From"));
        message.setReplyTo( (String) workItem.getParameter("Reply-To"));
        
        // Set recipients
        Recipients recipients = new Recipients();
        String to = (String) workItem.getParameter("To");
        if ( to == null || to.trim().length() == 0 ) {
            throw new RuntimeException( "Email must have one or more to adresses" );
        }
        for (String s: to.split(";")) {
            if (s != null && !"".equals(s)) {
                Recipient recipient = new Recipient();
                recipient.setEmail(s);
                recipient.setType( "To" );
                recipients.addRecipient(recipient);
            }
        }
        
        // Set cc
        String cc = (String) workItem.getParameter("Cc");
        if ( cc != null && cc.trim().length() > 0 ) {
            for (String s: cc.split(";")) {
                if (s != null && !"".equals(s)) {
                    Recipient recipient = new Recipient();
                    recipient.setEmail(s);
                    recipient.setType( "Cc" );
                    recipients.addRecipient(recipient);
                }
            }       
        }
        
        // Set bcc
        String bcc = (String) workItem.getParameter("Bcc");
        if ( bcc != null && bcc.trim().length() > 0 ) {
            for (String s: bcc.split(";")) {
                if (s != null && !"".equals(s)) {
                    Recipient recipient = new Recipient();
                    recipient.setEmail(s);
                    recipient.setType( "Bcc" );
                    recipients.addRecipient(recipient);
                }
            }       
        }
        
        // Fill message
        message.setRecipients(recipients);
        message.setSubject((String) workItem.getParameter("Subject"));
        message.setBody((String) workItem.getParameter("Body"));
        
        // fill attachments
        String attachmentList = (String) workItem.getParameter("Attachments");
        if (attachmentList != null) {
            String[] attachments = attachmentList.split(",");
            message.setAttachments(Arrays.asList(attachments));
        }
        
        // setup email
        email.setMessage(message);
        email.setConnection(connection);
        
        return email;
	}
	
	public void abortWorkItem(WorkItem arg0, WorkItemManager arg1) {
		// Do nothing, email cannot be aborted
	}
	
	protected boolean getDebugFlag(WorkItem workItem) {
	    Object debugParam  = workItem.getParameter("Debug");
	    if (debugParam == null) {
	        return false;
	    }
	    
	    return Boolean.parseBoolean(debugParam.toString());
	}

}
