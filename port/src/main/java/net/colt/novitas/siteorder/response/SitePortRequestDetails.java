package net.colt.novitas.siteorder.response;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * @author drao
 *
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class SitePortRequestDetails implements Serializable {

	private static final long serialVersionUID = 1854641500423030247L;


	@JsonProperty("action")
	private String action;

	@JsonProperty("id")
	private Integer requestId;

	@JsonProperty("location")
	private String location;

	@JsonProperty("address")
	private String address; //"900 Coronation Road, NW10 7QP. London, United Kingdom.",

	@JsonProperty("commitment_period")
	private Integer commitmentPeriod;

	@JsonProperty("customer_name")
	private String customerName;

	@JsonProperty("last_updated")
	private Date lastUpdated;

	@JsonProperty("requested_at")
	private Date requestedAt;

	@JsonProperty("latitude")
	private Float latitude;

	@JsonProperty("location_building_name")
	private String locationBuildingName;

	@JsonProperty("location_premises_number")
	private String locationPremisesNumber;

	@JsonProperty("location_city")
	private String locationCity;

	@JsonProperty("location_country")
	private String locationCountry;

	@JsonProperty("location_id")
	private String locationId;

	@JsonProperty("location_street_name")
	private String locationStreetName;

	@JsonProperty("longitude")
	private Float longitude;

	@JsonProperty("location_name")
	private String locationName;

	@JsonProperty("installation_charge")
	private Float installationCharge;

	@JsonProperty("installation_currency")
	private String installationCurrency;

	@JsonProperty("ocn")
	private String ocn;

	@JsonProperty("bcn")
	private String bcn;

	@JsonProperty("customer_id")
	private String customerId;

	@JsonProperty("portal_user_id")
	private Integer portalUserId;

	@JsonProperty("service_id")
	private String serviceId;

	@JsonProperty("crd")
	private Date customerRequiredDate;

    @JsonProperty("cpd")
    private Date customerPromisedDate;

    @JsonProperty("postal_zip_code")
    private String postalZipCode;

    @JsonProperty("site_floor")
    private String siteFloor;

    @JsonProperty("site_room_name")
    private String siteRoomName;

    @JsonProperty("site_type")
    private String siteType;

    @JsonProperty("site_access_out_of_hours")
    private boolean siteAccessOutOfHours;

    @JsonProperty("status")
    private String status;

    @JsonProperty("status_code")
    private Integer statusCode;

    @JsonProperty("status_code_description")
    private String statusDescription;

    @JsonProperty("order_id")
    private String orderId;

    @JsonProperty("order_status")
    private String orderStatus;

    @JsonProperty("order_status_updated")
    private Date orderStatusUpdated;

    @JsonProperty("contacts")
    private SitePortContact contacts;

	@JsonProperty("ports")
	private Set<SitePortRequestPortDetails> ports;

	@JsonProperty("process_instance_id")
    private String processInstanceId;

	@JsonProperty("notice_required")
    private String noticeRequired;

	@JsonProperty("building_id")
	private String buildingId;

	@JsonProperty("send_customer_email")
	private boolean sendCustomerEmail;

	@JsonProperty("siebel_order")
	private String siebelOrder;

	public String getSiebelOrder() {
		return siebelOrder;
	}

	public void setSiebelOrder(String siebelOrder) {
		this.siebelOrder = siebelOrder;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getHostAddress() {
		return hostAddress;
	}

	public void setHostAddress(String hostAddress) {
		this.hostAddress = hostAddress;
	}

	@JsonProperty("host_address")
    private String hostAddress;

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public Integer getRequestId() {
		return requestId;
	}

	public void setRequestId(Integer requestId) {
		this.requestId = requestId;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getAddress() {
		return  locationPremisesNumber+","+locationStreetName+","+postalZipCode+","+locationCity+","+locationCountry;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Integer getCommitmentPeriod() {
		return commitmentPeriod;
	}

	public void setCommitmentPeriod(Integer commitmentPeriod) {
		this.commitmentPeriod = commitmentPeriod;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public Float getLatitude() {
		return latitude;
	}

	public void setLatitude(Float latitude) {
		this.latitude = latitude;
	}

	public String getLocationBuildingName() {
		return locationBuildingName;
	}

	public void setLocationBuildingName(String locationBuildingName) {
		this.locationBuildingName = locationBuildingName;
	}

	public String getLocationCity() {
		return locationCity;
	}

	public void setLocationCity(String locationCity) {
		this.locationCity = locationCity;
	}

	public String getLocationCountry() {
		return locationCountry;
	}

	public void setLocationCountry(String locationCountry) {
		this.locationCountry = locationCountry;
	}

	public String getLocationId() {
		return locationId ;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	public String getLocationStreetName() {
		return locationStreetName;
	}

	public void setLocationStreetName(String locationStreetName) {
		this.locationStreetName = locationStreetName;
	}

	public Float getLongitude() {
		return longitude;
	}

	public void setLongitude(Float longitude) {
		this.longitude = longitude;
	}

	public Float getInstallationCharge() {
		return installationCharge;
	}

	public void setInstallationCharge(Float installationCharge) {
		this.installationCharge = installationCharge;
	}

	public String getInstallationCurrency() {
		return installationCurrency;
	}

	public void setInstallationCurrency(String installationCurrency) {
		this.installationCurrency = installationCurrency;
	}

	public String getOcn() {
		return ocn;
	}

	public void setOcn(String ocn) {
		this.ocn = ocn;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public Date getCustomerRequiredDate() {
		return customerRequiredDate;
	}

	public void setCustomerRequiredDate(Date customerRequiredDate) {
		this.customerRequiredDate = customerRequiredDate;
	}

	public Date getCustomerPromisedDate() {
		return customerPromisedDate;
	}

	public void setCustomerPromisedDate(Date customerPromisedDate) {
		this.customerPromisedDate = customerPromisedDate;
	}

	public String getPostalZipCode() {
		return postalZipCode;
	}

	public void setPostalZipCode(String postalZipCode) {
		this.postalZipCode = postalZipCode;
	}

	public String getSiteFloor() {
		return siteFloor;
	}

	public void setSiteFloor(String siteFloor) {
		this.siteFloor = siteFloor;
	}

	public String getSiteRoomName() {
		return siteRoomName;
	}

	public void setSiteRoomName(String siteRoomName) {
		this.siteRoomName = siteRoomName;
	}

	public String getSiteType() {
		return siteType;
	}

	public void setSiteType(String siteType) {
		this.siteType = siteType;
	}

	public boolean isSiteAccessOutOfHours() {
		return siteAccessOutOfHours;
	}

	public void setSiteAccessOutOfHours(boolean siteAccessOutOfHours) {
		this.siteAccessOutOfHours = siteAccessOutOfHours;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}



	public String getStatusDescription() {
		return statusDescription;
	}

	public void setStatusDescription(String statusDescription) {
		this.statusDescription = statusDescription;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public Date getOrderStatusUpdated() {
		return orderStatusUpdated;
	}

	public void setOrderStatusUpdated(Date orderStatusUpdated) {
		this.orderStatusUpdated = orderStatusUpdated;
	}


	public SitePortContact getContacts() {
		return contacts;
	}

	public void setContacts(SitePortContact contacts) {
		this.contacts = contacts;
	}

	public Set<SitePortRequestPortDetails> getPorts() {
		return ports;
	}

	public void setPorts(Set<SitePortRequestPortDetails> ports) {
		this.ports = ports;
	}

	public String getLocationPremisesNumber() {
		return locationPremisesNumber;
	}

	public void setLocationPremisesNumber(String locationPremisesNumber) {
		this.locationPremisesNumber = locationPremisesNumber;
	}


	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public Date getRequestedAt() {
		return requestedAt;
	}

	public void setRequestedAt(Date requestedAt) {
		this.requestedAt = requestedAt;
	}

	public String getBcn() {
		return bcn;
	}

	public void setBcn(String bcn) {
		this.bcn = bcn;
	}

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public Integer getPortalUserId() {
		return portalUserId;
	}

	public void setPortalUserId(Integer portalUserId) {
		this.portalUserId = portalUserId;
	}

	public String getNoticeRequired() {
		return noticeRequired;
	}

	public void setNoticeRequired(String noticeRequired) {
		this.noticeRequired = noticeRequired;
	}

	public String getBuildingId() {
		return buildingId;
	}

	public void setBuildingId(String buildingId) {
		this.buildingId = buildingId;
	}

	public boolean isSendCustomerEmail() {
		return sendCustomerEmail;
	}

	public void setSendCustomerEmail(boolean sendCustomerEmail) {
		this.sendCustomerEmail = sendCustomerEmail;
	}

	@Override
	public String toString() {
		return "SitePortRequestDetails [action=" + action + ", requestId=" + requestId + ", location=" + location
				+ ", address=" + address + ", commitmentPeriod=" + commitmentPeriod + ", customerName=" + customerName
				+ ", lastUpdated=" + lastUpdated + ", requestedAt=" + requestedAt + ", latitude=" + latitude
				+ ", locationBuildingName=" + locationBuildingName + ", locationPremisesNumber="
				+ locationPremisesNumber + ", locationCity=" + locationCity + ", locationCountry=" + locationCountry
				+ ", locationId=" + locationId + ", locationStreetName=" + locationStreetName + ", longitude="
				+ longitude + ", locationName=" + locationName + ", installationCharge=" + installationCharge
				+ ", installationCurrency=" + installationCurrency + ", ocn=" + ocn + ", bcn=" + bcn + ", customerId="
				+ customerId + ", portalUserId=" + portalUserId + ", serviceId=" + serviceId + ", customerRequiredDate="
				+ customerRequiredDate + ", customerPromisedDate=" + customerPromisedDate + ", postalZipCode="
				+ postalZipCode + ", siteFloor=" + siteFloor + ", siteRoomName=" + siteRoomName + ", siteType="
				+ siteType + ", siteAccessOutOfHours=" + siteAccessOutOfHours + ", status=" + status + ", statusCode="
				+ statusCode + ", statusDescription=" + statusDescription + ", orderId=" + orderId + ", orderStatus="
				+ orderStatus + ", orderStatusUpdated=" + orderStatusUpdated + ", contacts=" + contacts + ", ports="
				+ ports + ", processInstanceId=" + processInstanceId + ", noticeRequired=" + noticeRequired
				+ ", buildingId=" + buildingId + ", hostAddress=" + hostAddress + ", sendCustomerEmail=" + sendCustomerEmail + "]";
	}


}