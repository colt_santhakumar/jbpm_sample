package net.colt.novitas.siteorder.request;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CustomerLocationRequestList implements Serializable {

	private static final long serialVersionUID = 1119786699963890151L;

	List<CustomerLocationRequest> customerLocationRequestList = new ArrayList<CustomerLocationRequest>();

	public CustomerLocationRequestList() {
		super();
	}

	public CustomerLocationRequestList(List<CustomerLocationRequest> customerLocationRequestList) {
		super();
		this.customerLocationRequestList = customerLocationRequestList;
	}

	@JsonProperty("customer_location_list")
	public List<CustomerLocationRequest> getCustomerLocationRequestList() {
		return customerLocationRequestList;
	}

	public void setCustomerLocationRequestList(List<CustomerLocationRequest> customerLocationRequestList) {
		this.customerLocationRequestList = customerLocationRequestList;
	}

	@Override
	public String toString() {
		return "CustomerLocationRequestList [customerLocationRequestList=" + customerLocationRequestList + "]";
	}

}
