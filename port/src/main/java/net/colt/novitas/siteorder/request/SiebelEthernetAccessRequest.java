package net.colt.novitas.siteorder.request;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SiebelEthernetAccessRequest implements Serializable {

	private static final long serialVersionUID = 4280647876917468935L;

	@JsonProperty("site_port_request_id")
	private String sitePortRequestId;

	@JsonProperty("customer_country")
	private String customerCountry;

	@JsonProperty("callback_url")
	private String callbackUrl;

	@JsonProperty("ocn")
	private String ocn;

	@JsonProperty("bcn")
	private String bcn;

	@JsonProperty("customer_currency")
	private String customerCurrency;

	@JsonProperty("customer_name")
	private String customerName;

	@JsonProperty("noviats_service_Id")
	private String noviatsServiceId;

	@JsonProperty("port_id")
	private String portId;

	@JsonProperty("bandwidth")
	private String bandwidth;

	@JsonProperty("technology")
	private String technology;

	@JsonProperty("connector_type")
	private String connectorType;

	// building, site details
	@JsonProperty("site_id")
	private String siteId;

	@JsonProperty("building_id")
	private String buildingId;

	@JsonProperty("building_name")
	private String buildingName;

	@JsonProperty("building_number")
	private String buildingNumber;

	@JsonProperty("city")
	private String city;

	@JsonProperty("country")
	private String country;

	@JsonProperty("postal_zip_code")
	private String postalZipCode;

	@JsonProperty("street_name")
	private String streetName;

	@JsonProperty("floor")
	private String floor;

	@JsonProperty("room_name")
	private String roomName;
	
    @JsonProperty("site_access_out_of_hours")
    private boolean siteAccessOutOfHours;
    
	@JsonProperty("site_notice_required")
	private String siteNoticeRequired;
	
	@JsonProperty("request_date")
	private Date requestDate;

	@JsonProperty("site")
	private SiteContact siteContact;

	@JsonProperty("technical")
	private TechnicalContact techniContact;

	@JsonProperty("user")
	private SiteContact userContact;
	
	@JsonProperty("port_name")
	private String portName;

	public String getSitePortRequestId() {
		return sitePortRequestId;
	}

	public void setSitePortRequestId(String sitePortRequestId) {
		this.sitePortRequestId = sitePortRequestId;
	}

	public String getCustomerCountry() {
		return customerCountry;
	}

	public void setCustomerCountry(String customerCountry) {
		this.customerCountry = customerCountry;
	}

	public String getCallbackUrl() {
		return callbackUrl;
	}

	public void setCallbackUrl(String callbackUrl) {
		this.callbackUrl = callbackUrl;
	}

	public String getOcn() {
		return ocn;
	}

	public void setOcn(String ocn) {
		this.ocn = ocn;
	}

	public String getBcn() {
		return bcn;
	}

	public void setBcn(String bcn) {
		this.bcn = bcn;
	}

	public String getCustomerCurrency() {
		return customerCurrency;
	}

	public void setCustomerCurrency(String customerCurrency) {
		this.customerCurrency = customerCurrency;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getNoviatsServiceId() {
		return noviatsServiceId;
	}

	public void setNoviatsServiceId(String noviatsServiceId) {
		this.noviatsServiceId = noviatsServiceId;
	}

	public String getPortId() {
		return portId;
	}

	public void setPortId(String portId) {
		this.portId = portId;
	}

	public String getBandwidth() {
		return bandwidth;
	}

	public void setBandwidth(String bandwidth) {
		this.bandwidth = bandwidth;
	}

	public String getTechnology() {
		return technology;
	}

	public void setTechnology(String technology) {
		this.technology = technology;
	}

	public String getConnectorType() {
		return connectorType;
	}

	public void setConnectorType(String connectorType) {
		this.connectorType = connectorType;
	}

	public String getSiteId() {
		return siteId;
	}

	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}

	public String getBuildingId() {
		return buildingId;
	}

	public void setBuildingId(String buildingId) {
		this.buildingId = buildingId;
	}

	public String getBuildingName() {
		return buildingName;
	}

	public void setBuildingName(String buildingName) {
		this.buildingName = buildingName;
	}

	public String getBuildingNumber() {
		return buildingNumber;
	}

	public void setBuildingNumber(String buildingNumber) {
		this.buildingNumber = buildingNumber;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPostalZipCode() {
		return postalZipCode;
	}

	public void setPostalZipCode(String postalZipCode) {
		this.postalZipCode = postalZipCode;
	}

	public String getStreetName() {
		return streetName;
	}

	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}

	public String getFloor() {
		return floor;
	}

	public void setFloor(String floor) {
		this.floor = floor;
	}

	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	public boolean isSiteAccessOutOfHours() {
		return siteAccessOutOfHours;
	}

	public void setSiteAccessOutOfHours(boolean siteAccessOutOfHours) {
		this.siteAccessOutOfHours = siteAccessOutOfHours;
	}
	
	public String getSiteNoticeRequired() {
		return siteNoticeRequired;
	}

	public void setSiteNoticeRequired(String siteNoticeRequired) {
		this.siteNoticeRequired = siteNoticeRequired;
	}
	
	public Date getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}

	public SiteContact getSiteContact() {
		return siteContact;
	}

	public void setSiteContact(SiteContact siteContact) {
		this.siteContact = siteContact;
	}

	public TechnicalContact getTechniContact() {
		return techniContact;
	}

	public void setTechniContact(TechnicalContact techniContact) {
		this.techniContact = techniContact;
	}

	public SiteContact getUserContact() {
		return userContact;
	}

	public void setUserContact(SiteContact userContact) {
		this.userContact = userContact;
	}

	public String getPortName() {
		return portName;
	}
	
	public void setPortName(String portName) {
		this.portName = portName;
	}
	
	@Override
	public String toString() {
		return "SiebelEthernetAccessRequest [sitePortRequestId=" + sitePortRequestId + ", customerCountry="
				+ customerCountry + ", callbackUrl=" + callbackUrl + ", ocn=" + ocn + ", bcn=" + bcn
				+ ", customerCurrency=" + customerCurrency + ", customerName=" + customerName + ", noviatsServiceId="
				+ noviatsServiceId + ", portId=" + portId + ", bandwidth=" + bandwidth + ", technology=" + technology
				+ ", connectorType=" + connectorType + ", siteId=" + siteId + ", buildingId=" + buildingId
				+ ", buildingName=" + buildingName + ", buildingNumber=" + buildingNumber + ", city=" + city
				+ ", country=" + country + ", postalZipCode=" + postalZipCode + ", streetName=" + streetName
				+ ", floor=" + floor + ", roomName=" + roomName + ", siteAccessOutOfHours=" + siteAccessOutOfHours
				+ ", siteNoticeRequired=" + siteNoticeRequired + ", requestDate=" + requestDate
				+ ", siteContact=" + siteContact + ", techniContact=" + techniContact + ", userContact=" + userContact
				+ ", portName=" + portName + "]";
	}

}
