package net.colt.novitas.siteorder.request;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CustomerLocationRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String siteId;
	private String customerSiteName;

	public CustomerLocationRequest() {
		super();
	}

	public CustomerLocationRequest(String siteId, String customerSiteName) {
		super();
		this.siteId = siteId;
		this.customerSiteName = customerSiteName;
	}

	@JsonProperty("site_id")
	public String getSiteId() {
		return siteId;
	}

	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}

	@JsonProperty("customer_site_name")
	public String getCustomerSiteName() {
		return customerSiteName;
	}

	public void setCustomerSiteName(String customerSiteName) {
		this.customerSiteName = customerSiteName;
	}

}
