package net.colt.novitas.siteorder.request;

public enum TempLocationStatus {

	ACTIVE, UPDATED, CANCELLED, FAILED;
}
