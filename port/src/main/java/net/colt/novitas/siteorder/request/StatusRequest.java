package net.colt.novitas.siteorder.request;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class StatusRequest implements Serializable {

	private static final long serialVersionUID = 4207287730527120840L;
	private String status;

	@JsonProperty("status")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "StatusRequest [status=" + status + "]";
	}

}
