package net.colt.novitas.siteorder.request;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CreatePortRequest implements Serializable {
    
	
	private static final long serialVersionUID = 1L;

	@JsonProperty("portal_user_id")
	private Integer portalUserId;
	
	@JsonProperty("port_name")
	private String portName;
	
	@JsonProperty("location_id")
	private String locationId;
	
	@JsonProperty("customer_currency")
	private String customerCurrency;
	
	@JsonProperty("bcn")
	private String bcn;
	
	@JsonProperty("discount_percentage")
	private Float discountPercentage;
	
	@JsonProperty("customer_name")
	private String customerName;
	
	@JsonProperty("ocn")
	private String ocn;
	    
	@JsonProperty("bandwidth")
	private Integer bandwidth;
	
	@JsonProperty("product_name")
	private String productName;
	
	@JsonProperty("connector")
	private String connector;
	
	@JsonProperty("technology")
	private String technology;
	
	@JsonProperty("rental_currency")
	private String rentalCurrency;
	
	@JsonProperty("rental_charge")
	private Float rentalCharge;
	
	@JsonProperty("rental_unit")
	private String rentalUnit;
	
	@JsonProperty("installation_charge")
	private Float installationCharge;
	
	@JsonProperty("installation_currency")
	private String installationCurrency;
	
	@JsonProperty("decommissioning_charge")
	private Float decommissioningCharge;
	
	@JsonProperty("decommissioning_currency")
	private String decommissioningCurrency;
	
	@JsonProperty("commitment_period")
	private Integer commitmentPeriod;
	
	@JsonProperty("expiration_period")
	private Integer expirationPeriod;
	
	@JsonProperty("capacity_order_port")
	private boolean isCapacityOrder;
	
	@JsonProperty("cron_execution")
	private CronExecution cronExecution;
	
	@JsonProperty("customer_port_id")
	private String customerPortID;
	
	@JsonProperty("nc_tech_service_id")
	private String ncTechServiceId;
	
	@JsonProperty("ohs_circuit_id")
	private String ohsCircuitId;
		
	public Integer getPortalUserId() {
		return portalUserId;
	}

	public void setPortalUserId(Integer portalUserId) {
		this.portalUserId = portalUserId;
	}

	public String getPortName() {
		return portName;
	}

	public void setPortName(String portName) {
		this.portName = portName;
	}

	public String getLocationId() {
		return locationId;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	public String getCustomerCurrency() {
		return customerCurrency;
	}

	public void setCustomerCurrency(String customerCurrency) {
		this.customerCurrency = customerCurrency;
	}

	public String getBcn() {
		return bcn;
	}

	public void setBcn(String bcn) {
		this.bcn = bcn;
	}

	public Float getDiscountPercentage() {
		return discountPercentage;
	}

	public void setDiscountPercentage(Float discountPercentage) {
		this.discountPercentage = discountPercentage;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getOcn() {
		return ocn;
	}

	public void setOcn(String ocn) {
		this.ocn = ocn;
	}

	public Integer getBandwidth() {
		return bandwidth;
	}

	public void setBandwidth(Integer bandwidth) {
		this.bandwidth = bandwidth;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getConnector() {
		return connector;
	}

	public void setConnector(String connector) {
		this.connector = connector;
	}

	public String getTechnology() {
		return technology;
	}

	public void setTechnology(String technology) {
		this.technology = technology;
	}

	public String getRentalCurrency() {
		return rentalCurrency;
	}

	public void setRentalCurrency(String rentalCurrency) {
		this.rentalCurrency = rentalCurrency;
	}

	public Float getRentalCharge() {
		return rentalCharge;
	}

	public void setRentalCharge(Float rentalCharge) {
		this.rentalCharge = rentalCharge;
	}

	public String getRentalUnit() {
		return rentalUnit;
	}

	public void setRentalUnit(String rentalUnit) {
		this.rentalUnit = rentalUnit;
	}

	public Float getInstallationCharge() {
		return installationCharge;
	}

	public void setInstallationCharge(Float installationCharge) {
		this.installationCharge = installationCharge;
	}

	public String getInstallationCurrency() {
		return installationCurrency;
	}

	public void setInstallationCurrency(String installationCurrency) {
		this.installationCurrency = installationCurrency;
	}

	public Float getDecommissioningCharge() {
		return decommissioningCharge;
	}

	public void setDecommissioningCharge(Float decommissioningCharge) {
		this.decommissioningCharge = decommissioningCharge;
	}

	public String getDecommissioningCurrency() {
		return decommissioningCurrency;
	}

	public void setDecommissioningCurrency(String decommissioningCurrency) {
		this.decommissioningCurrency = decommissioningCurrency;
	}

	public Integer getCommitmentPeriod() {
		return commitmentPeriod;
	}

	public void setCommitmentPeriod(Integer commitmentPeriod) {
		this.commitmentPeriod = commitmentPeriod;
	}

	public Integer getExpirationPeriod() {
		return expirationPeriod;
	}

	public void setExpirationPeriod(Integer expirationPeriod) {
		this.expirationPeriod = expirationPeriod;
	}

	public boolean isCapacityOrder() {
		return isCapacityOrder;
	}

	public void setCapacityOrder(boolean isCapacityOrder) {
		this.isCapacityOrder = isCapacityOrder;
	}

	public CronExecution getCronExecution() {
		return cronExecution;
	}

	public void setCronExecution(CronExecution cronExecution) {
		this.cronExecution = cronExecution;
	}
	
	public String getCustomerPortID() {
		return customerPortID;
	}

	public void setCustomerPortID(String customerPortID) {
		this.customerPortID = customerPortID;
	}

	public String getNcTechServiceId() {
		return ncTechServiceId;
	}

	public void setNcTechServiceId(String ncTechServiceId) {
		this.ncTechServiceId = ncTechServiceId;
	}

	public String getOhsCircuitId() {
		return ohsCircuitId;
	}

	public void setOhsCircuitId(String ohsCircuitId) {
		this.ohsCircuitId = ohsCircuitId;
	}

	@Override
	public String toString() {
		return "CreatePortRequest [portalUserId=" + portalUserId + ", portName=" + portName + ", locationId="
				+ locationId + ", customerCurrency=" + customerCurrency + ", bcn=" + bcn + ", discountPercentage="
				+ discountPercentage + ", customerName=" + customerName + ", ocn=" + ocn + ", bandwidth=" + bandwidth
				+ ", productName=" + productName + ", connector=" + connector + ", technology=" + technology
				+ ", rentalCurrency=" + rentalCurrency + ", rentalCharge=" + rentalCharge + ", rentalUnit=" + rentalUnit
				+ ", installationCharge=" + installationCharge + ", installationCurrency=" + installationCurrency
				+ ", decommissioningCharge=" + decommissioningCharge + ", decommissioningCurrency="
				+ decommissioningCurrency + ", commitmentPeriod=" + commitmentPeriod + ", expirationPeriod="
				+ expirationPeriod + ", isCapacityOrder=" + isCapacityOrder + ", cronExecution=" + cronExecution
				+ ", customerPortID=" + customerPortID + ", ncTechServiceId=" + ncTechServiceId + ", ohsCircuitId="
				+ ohsCircuitId + "]";
	}

	

}
