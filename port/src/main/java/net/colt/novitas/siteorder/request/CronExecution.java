package net.colt.novitas.siteorder.request;

public enum CronExecution {

	IMMEDIATE("IMMEDIATE"), DISABLED("DISABLED"), DEFERRED("DEFERRED"), NEVER("NEVER");

	private final String value;

	private CronExecution(String value) {
		this.value = value;
	}

	public String getValue() {
		return this.value;
	}
}
