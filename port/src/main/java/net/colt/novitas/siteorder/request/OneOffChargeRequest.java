package net.colt.novitas.siteorder.request;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;


public class OneOffChargeRequest implements Serializable {

	private static final long serialVersionUID = 5732589777284174054L;

	private Integer requestId;
	private String serviceId;
	private String serviceInstanceId;
	private String serviceInstanceType;
	private String serviceInstanceName;
	private String bcn;
	private String chargeType;
	private String description;
	private String currency;
	private Float amount;

	public OneOffChargeRequest() {

	}

	@JsonProperty("request_id")
	public Integer getRequestId() {
		return requestId;
	}

	public void setRequestId(Integer requestId) {
		this.requestId = requestId;
	}

	@JsonProperty("service_id")
	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	@JsonProperty("service_instance_id")
	public String getServiceInstanceId() {
		return serviceInstanceId;
	}

	public void setServiceInstanceId(String serviceInstanceId) {
		this.serviceInstanceId = serviceInstanceId;
	}

	@JsonProperty("service_instance_type")
	public String getServiceInstanceType() {
		return serviceInstanceType;
	}

	public void setServiceInstanceType(String serviceInstanceType) {
		this.serviceInstanceType = serviceInstanceType;
	}
	
	@JsonProperty("service_instance_name")
	public String getServiceInstanceName() {
		return serviceInstanceName;
	}

	public void setServiceInstanceName(String serviceInstanceName) {
		this.serviceInstanceName = serviceInstanceName;
	}

	@JsonProperty("bcn")
	public String getBcn() {
		return bcn;
	}

	public void setBcn(String bcn) {
		this.bcn = bcn;
	}

	@JsonProperty("charge_type")
	public String getChargeType() {
		return chargeType;
	}

	public void setChargeType(String chargeType) {
		this.chargeType = chargeType;
	}

	@JsonProperty("description")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@JsonProperty("currency")
	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	@JsonProperty("amount")
	public Float getAmount() {
		return amount;
	}

	public void setAmount(Float amount) {
		this.amount = amount;
	}

	@Override
	public String toString() {
		return "OneOffChargeRequest [requestId=" + requestId + ", serviceId="
				+ serviceId + ", serviceInstanceId=" + serviceInstanceId
				+ ", serviceInstanceType=" + serviceInstanceType
				+ ", serviceInstanceName=" + serviceInstanceName + ", bcn="
				+ bcn + ", chargeType=" + chargeType + ", description="
				+ description + ", currency=" + currency + ", amount=" + amount
				+ "]";
	}
	
	

}
