package net.colt.novitas.siteorder.response;

import java.io.Serializable;
import java.util.Currency;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomerResponse implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;


	@JsonProperty("customer_id")
	private Integer id;

	@JsonProperty("legal_name")
	private String legalName;

	@JsonProperty("ocn")
	private String ocn;

	@JsonProperty("spend_limited")
	private Boolean spendLimit;

	@JsonProperty("monthly_spend_limit")
	private Double monthlySpendLimit;

	@JsonProperty("bcn")
	private String bcn;

	@JsonProperty("rental_currency")
	private Currency rentalCurrency;

	@JsonProperty("discount_percentage")
	private Float discountPercentage;

	@JsonProperty("status")
	private String status;

	@JsonProperty("country_code")
	private String countryCode;

	@JsonProperty("language_code")
	private String languageCode;

	@JsonProperty("local_name")
	private String localName;

	@JsonProperty("colt_region")
	private String coltRegion;

	@JsonProperty("country")
	private String country;

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLegalName() {
		return legalName;
	}

	public void setLegalName(String legalName) {
		this.legalName = legalName;
	}

	public String getOcn() {
		return ocn;
	}

	public void setOcn(String ocn) {
		this.ocn = ocn;
	}

	public Boolean isSpendLimit() {
		return spendLimit;
	}

	public void setSpendLimit(Boolean spendLimit) {
		this.spendLimit = spendLimit;
	}

	public Double getMonthlySpendLimit() {
		return monthlySpendLimit;
	}

	public void setMonthlySpendLimit(Double monthlySpendLimit) {
		this.monthlySpendLimit = monthlySpendLimit;
	}

	public String getBcn() {
		return bcn;
	}

	public void setBcn(String bcn) {
		this.bcn = bcn;
	}

	public Currency getRentalCurrency() {
		return rentalCurrency;
	}

	public void setRentalCurrency(Currency rentalCurrency) {
		this.rentalCurrency = rentalCurrency;
	}

	public Float getDiscountPercentage() {
		return discountPercentage;
	}

	public void setDiscountPercentage(Float discountPercentage) {
		this.discountPercentage = discountPercentage;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getLocalName() {
		return localName;
	}

	public void setLocalName(String localName) {
		this.localName = localName;
	}

	public String getColtRegion() {
		return coltRegion;
	}

	public void setColtRegion(String coltRegion) {
		this.coltRegion = coltRegion;
	}

	public Boolean getSpendLimit() {
		return spendLimit;
	}



}
