package net.colt.novitas.siteorder.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MSPPortLabelResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty("cabinet-id")
	private String cabinetId;

	@JsonProperty("shelf_id")
	private String shelfId;

	@JsonProperty("physical_port_id")
	private String physicalPortId;

	@JsonProperty("slot_id")
	private String slotId;

	@JsonProperty("presentation_panel")
	private String presentationPanel;

	@JsonProperty("site_id")
	private String siteId;

	@JsonProperty("connector_type")
	private String connectorType;

	@JsonProperty("interface_type")
	private String interfaceType;
	
	public String getSiteId() {
		return siteId;
	}

	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}

	public String getCabinetId() {
		return cabinetId;
	}

	public void setCabinetId(String cabinetId) {
		this.cabinetId = cabinetId;
	}

	public String getShelfId() {
		return shelfId;
	}

	public void setShelfId(String shelfId) {
		this.shelfId = shelfId;
	}

	public String getPhysicalPortId() {
		return physicalPortId;
	}

	public void setPhysicalPortId(String physicalPortId) {
		this.physicalPortId = physicalPortId;
	}

	public String getSlotId() {
		return slotId;
	}

	public void setSlotId(String slotId) {
		this.slotId = slotId;
	}

	public String getPresentationPanel() {
		return presentationPanel;
	}

	public void setPresentationPanel(String presentationPanel) {
		this.presentationPanel = presentationPanel;
	}

	public String getConnectorType() {
		return connectorType;
	}

	public void setConnectorType(String connectorType) {
		this.connectorType = connectorType;
	}

	public String getInterfaceType() {
		return interfaceType;
	}

	public void setInterfaceType(String interfaceType) {
		this.interfaceType = interfaceType;
	}
	
	@Override
	public String toString() {
		return "MSPPortLabelResponse [cabinetId=" + cabinetId + ", shelfId=" + shelfId + ", physicalPortId="
				+ physicalPortId + ", slotId=" + slotId + ", presentationPanel=" + presentationPanel + ", siteId="
				+ siteId + ", connectorType=" + connectorType + ", interfaceType=" + interfaceType + "]";
	}

}
