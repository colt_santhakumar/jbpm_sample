package net.colt.novitas.siteorder.request;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;


@JsonIgnoreProperties(ignoreUnknown = true)
public class UpdateSitePortRequest implements Serializable{

	private static final long serialVersionUID = 1L;

	@JsonProperty("id")
	private Integer id;

	@JsonProperty("last_updated")
	private Date lastUpdate;

	@JsonProperty("cpd")
	private Date customerPromisedDate;

	@JsonProperty("status")
	private String status;

	@JsonProperty("status_code")
	private Integer statusCode;

	@JsonProperty("status_code_description")
	private String errorDescription;

	@JsonProperty("order_status")
	private String orderStatus;

	@JsonProperty("order_status_updated")
	private String orderStatusUpdated;

	@JsonProperty("order_id")
	private String orderId;

    @JsonProperty("port_id_map")
    private Map<Integer,Integer> portIdMap;

    @JsonProperty("host_address")
	private String hostAddress;

    @JsonProperty("process_instance_id")
	private String processInStanceId;

    @JsonProperty("site_type")
	private String siteType;

	@JsonProperty("site_floor")
	private String siteFloor;

	@JsonProperty("site_room_name")
	private String siteRoomName;

	@JsonProperty("location_premises_number")
	private String locationPremisesNumber;

	@JsonProperty("location_building_name")
	private String locationBuildingName;

	@JsonProperty("location_street_name")
	private String locationStreetName;

	@JsonProperty("location_city")
	private String locationCity;

	@JsonProperty("location_state")
    private String locationState;

    @JsonProperty("location_country")
    private String locationCountry;

    @JsonProperty("postal_zip_code")
    private String postalZipCode;

    @JsonProperty("latitude")
    private Float latitude;

    @JsonProperty("longitude")
    private Float longitude;

    @JsonProperty("location_id")
    private String locationId;

    @JsonProperty("location_name")
    private String locationName;

    @JsonProperty("capacity_validated")
    private boolean capacityValidated;

    @JsonProperty("siebel_order")
    private String siebelOrder;

    @JsonProperty("connector")
	private String connector;

	@JsonProperty("technology")
	private String technology;
    
    public String getSiebelOrder() {
		return siebelOrder;
	}

    public void setSiebelOrder(String siebelOrder) {
		this.siebelOrder = siebelOrder;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public Date getCustomerPromisedDate() {
		return customerPromisedDate;
	}

	public void setCustomerPromisedDate(Date customerPromisedDate) {
		this.customerPromisedDate = customerPromisedDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}

	public String getErrorDescription() {
		return errorDescription;
	}

	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public String getOrderStatusUpdated() {
		return orderStatusUpdated;
	}

	public void setOrderStatusUpdated(String orderStatusUpdated) {
		this.orderStatusUpdated = orderStatusUpdated;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public Map<Integer, Integer> getPortIdMap() {
		return portIdMap;
	}

	public void setPortIdMap(Map<Integer, Integer> portIdMap) {
		this.portIdMap = portIdMap;
	}

	public String getHostAddress() {
		return hostAddress;
	}

	public void setHostAddress(String hostAddress) {
		this.hostAddress = hostAddress;
	}

	public String getProcessInStanceId() {
		return processInStanceId;
	}

	public void setProcessInStanceId(String processInStanceId) {
		this.processInStanceId = processInStanceId;
	}

	public String getSiteType() {
		return siteType;
	}

	public void setSiteType(String siteType) {
		this.siteType = siteType;
	}

	public String getSiteFloor() {
		return siteFloor;
	}

	public void setSiteFloor(String siteFloor) {
		this.siteFloor = siteFloor;
	}

	public String getSiteRoomName() {
		return siteRoomName;
	}

	public void setSiteRoomName(String siteRoomName) {
		this.siteRoomName = siteRoomName;
	}

	public String getLocationPremisesNumber() {
		return locationPremisesNumber;
	}

	public void setLocationPremisesNumber(String locationPremisesNumber) {
		this.locationPremisesNumber = locationPremisesNumber;
	}

	public String getLocationBuildingName() {
		return locationBuildingName;
	}

	public void setLocationBuildingName(String locationBuildingName) {
		this.locationBuildingName = locationBuildingName;
	}

	public String getLocationStreetName() {
		return locationStreetName;
	}

	public void setLocationStreetName(String locationStreetName) {
		this.locationStreetName = locationStreetName;
	}

	public String getLocationCity() {
		return locationCity;
	}

	public void setLocationCity(String locationCity) {
		this.locationCity = locationCity;
	}

	public String getLocationState() {
		return locationState;
	}

	public void setLocationState(String locationState) {
		this.locationState = locationState;
	}

	public String getLocationCountry() {
		return locationCountry;
	}

	public void setLocationCountry(String locationCountry) {
		this.locationCountry = locationCountry;
	}

	public String getPostalZipCode() {
		return postalZipCode;
	}

	public void setPostalZipCode(String postalZipCode) {
		this.postalZipCode = postalZipCode;
	}

	public Float getLatitude() {
		return latitude;
	}

	public void setLatitude(Float latitude) {
		this.latitude = latitude;
	}

	public Float getLongitude() {
		return longitude;
	}

	public void setLongitude(Float longitude) {
		this.longitude = longitude;
	}

	public String getLocationId() {
		return locationId;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}


	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}


	public boolean isCapacityValidated() {
		return capacityValidated;
	}

	public void setCapacityValidated(boolean capacityValidated) {
		this.capacityValidated = capacityValidated;
	}

	public String getConnector() {
		return connector;
	}

	public void setConnector(String connector) {
		this.connector = connector;
	}

	public String getTechnology() {
		return technology;
	}

	public void setTechnology(String technology) {
		this.technology = technology;
	}
	
	@Override
	public String toString() {
		return "UpdateSitePortRequest [id=" + id + ", lastUpdate=" + lastUpdate
				+ ", customerPromisedDate=" + customerPromisedDate
				+ ", status=" + status + ", statusCode=" + statusCode
				+ ", errorDescription=" + errorDescription + ", orderStatus="
				+ orderStatus + ", orderStatusUpdated=" + orderStatusUpdated
				+ ", orderId=" + orderId + ", portIdMap=" + portIdMap
				+ ", hostAddress=" + hostAddress + ", processInStanceId="
				+ processInStanceId + ", siteType=" + siteType + ", siteFloor="
				+ siteFloor + ", siteRoomName=" + siteRoomName
				+ ", locationPremisesNumber=" + locationPremisesNumber
				+ ", locationBuildingName=" + locationBuildingName
				+ ", locationStreetName=" + locationStreetName
				+ ", locationCity=" + locationCity + ", locationState="
				+ locationState + ", locationCountry=" + locationCountry
				+ ", postalZipCode=" + postalZipCode + ", latitude=" + latitude
				+ ", longitude=" + longitude + ", locationId=" + locationId
				+ ", locationName=" + locationName + ", connector=" + connector + ", technology=" + technology + "]";
	}




}
