package net.colt.novitas.siteorder.response;

import java.io.Serializable;

import net.colt.novitas.siteorder.request.TempLocationStatus;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Drao
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class TempLocationResponse implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty("temp_location_id")
	private String tempLocationId;

	@JsonProperty("ocn")
	private String ocn;

	@JsonProperty("building_id")
	private String buildingId;

	@JsonProperty("created_on")
	private String createdOn;

	@JsonProperty("location_name")
	private String locationName;

	@JsonProperty("address")
	private String address;

	@JsonProperty("site_floor")
	private String siteFloor;

	@JsonProperty("site_room_name")
	private String siteRoomName;

	@JsonProperty("site_type")
	private String siteType;

	@JsonProperty("private_site")
	private Boolean privateSite;

	@JsonProperty("validated")
	private Boolean validated;

	@JsonProperty("status")
	private TempLocationStatus status;
	
	@JsonProperty("building_type")
	private String buildingType;

	public TempLocationResponse() {
		super();
	}

	

	public String getTempLocationId() {
		return tempLocationId;
	}

	public void setTempLocationId(String tempLocationId) {
		this.tempLocationId = tempLocationId;
	}

	public String getOcn() {
		return ocn;
	}

	public void setOcn(String ocn) {
		this.ocn = ocn;
	}

	public String getBuildingId() {
		return buildingId;
	}

	public void setBuildingId(String buildingId) {
		this.buildingId = buildingId;
	}

	public String getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getSiteFloor() {
		return siteFloor;
	}

	public void setSiteFloor(String siteFloor) {
		this.siteFloor = siteFloor;
	}

	public String getSiteRoomName() {
		return siteRoomName;
	}

	public void setSiteRoomName(String siteRoomName) {
		this.siteRoomName = siteRoomName;
	}

	public String getSiteType() {
		return siteType;
	}

	public void setSiteType(String siteType) {
		this.siteType = siteType;
	}

	public Boolean getPrivateSite() {
		return privateSite;
	}

	public void setPrivateSite(Boolean privateSite) {
		this.privateSite = privateSite;
	}

	public Boolean getValidated() {
		return validated;
	}

	public void setValidated(Boolean validated) {
		this.validated = validated;
	}

	public TempLocationStatus getStatus() {
		return status;
	}

	public void setStatus(TempLocationStatus status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
