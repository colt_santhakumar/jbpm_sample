package net.colt.novitas.port;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import org.apache.commons.lang.StringUtils;
import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.HandlerUtils;
import com.colt.novitas.client.ServiceAPIClient;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;
import com.colt.novitas.request.UpdatePortRequest;
import com.colt.novitas.response.GetPortRequestResponse;

public class WIHUpdateServicePort extends BaseWorkItemHandler {
	
	private static Logger logger = LoggerFactory.getLogger(WIHUpdateServicePort.class);
	
	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}
	

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
		
		EnvironmentProperties prop = getEnvProperties(workItem);
		logger = getLogger(workItem, prop.getLogUniqueId());
		try {
			String ncTechServiceId = getNcTechServiceId(workItem);
			String circuitId = String.valueOf(workItem.getParameter("circuit_reference"));
			String dedicatedPortId = String.valueOf(workItem.getParameter("port_id"));
			String presentationLabel = String.valueOf(workItem.getParameter("presentation_label"));
			UpdatePortRequest portSrvReq = new UpdatePortRequest();
			String crossReqId =  String.valueOf(workItem.getParameter("cross_connect_req_id")); 
			String crossConnId =  String.valueOf(workItem.getParameter("cross_connect_id"));
			if(StringUtils.isNotBlank(crossReqId)) portSrvReq.setCrossConnectRequestId(crossReqId); 
			if(StringUtils.isNotBlank(crossConnId)) portSrvReq.setCrossConnectId(crossConnId); 
			portSrvReq.setStatus(getStatus(workItem));
		    if(StringUtils.isNotBlank(presentationLabel))
			   portSrvReq.setPresentationLabel(presentationLabel);
		    if(StringUtils.isNotBlank(ncTechServiceId))
				   portSrvReq.setNcTechServiceId(ncTechServiceId);
		    if(StringUtils.isNotBlank(circuitId))
				   portSrvReq.setResourceId(circuitId);
		    portSrvReq.setMaxAllowedIpaConnBandwidth((Integer) workItem.getParameter("max_allowed_ipa_conn_bandwidth"));
		    
		    GetPortRequestResponse portRequest = getPortRequst(workItem);
		    
			if ("ACTIVE".equals(getStatus(workItem)) && null != portRequest.getCommitmentPeriod()
					&& portRequest.getCommitmentPeriod() > 0) {
				portSrvReq.setCommitmentPeriod(portRequest.getCommitmentPeriod());
				portSrvReq.setCommitmentExpiryDate(getCommitmentExpiryDate(portRequest.getRequestedAt(), portRequest.getCommitmentPeriod(), portRequest.getRentalUnit()));
			}
			
			Object response = new ServiceAPIClient(prop.getServiceAPIBaseUrl(),
					prop.getServiceAPIUsername(),
					prop.getServiceAPIPassword()).updatePort(dedicatedPortId, portSrvReq);
			
			if ("FAILED".equals(response)) {
				logger.info("Port status update failed. Port : " + dedicatedPortId);
				throw new NovitasHandlerException();
			}
			
			Map<String,Object> retval = new HashMap<String,Object>();
			manager.completeWorkItem(workItem.getId(), retval);
			
		} catch (Exception e) {
			logger.error("WIHUpdateServicePort -> Failed", e);
			
			if ("ACTIVE".equals(getStatus(workItem))) {
				HandlerUtils.throwHandlerExcetpion(
						new NovitasHandlerException(HandlerErrorCode.CP_UPDATE_SERVICE_PORT_DETAILS));
			} 
			else if ("DECOMMISSIONING".equals(getStatus(workItem))) {
				HandlerUtils.throwHandlerExcetpion(
						new NovitasHandlerException(HandlerErrorCode.DP_UPDATE_SERVICE_PORT_DECOMMISSIONING));
			}
			else if ("DECOMMISSIONED".equals(getStatus(workItem))) {
				HandlerUtils.throwHandlerExcetpion(
						new NovitasHandlerException(HandlerErrorCode.DP_UPDATE_SERVICE_PORT_DECOMMISSIONED));
			}
		}
	}
	
	//TODO : move to base class
	
		protected Integer getRequestId(WorkItem workItem) {
			Object obj = workItem.getParameter("request_id");
			Integer requestId = null;
			if (obj instanceof String) {
				requestId = Integer.valueOf(String.valueOf(obj));
			}
			else {
				requestId = (Integer) obj;
			}
			logger.info("Request Id : " + requestId);

			return requestId;
		}
		
		protected GetPortRequestResponse getPortRequst(WorkItem workItem) {
			return (GetPortRequestResponse) workItem.getParameter("request_obj");
		}
		

		protected String getNcTechServiceId(WorkItem workItem) {
			return (String) workItem.getParameter("nc_tech_service_id");
		}
		
		protected String getStatus(WorkItem workItem) {
			return (String) workItem.getParameter("status");
		}
		
		 private static String getCommitmentExpiryDate(String startDate, Integer commitmentPeriod, String frequency){
				
			 DateFormat fdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
			 fdf.setTimeZone(TimeZone.getTimeZone("UTC"));
			 Calendar c = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
				if (startDate != null) {
					
					try {
						c.setTime(fdf.parse(startDate));
					} catch (ParseException e) {
	                  
	                  e.printStackTrace();
					}
					c.add(Calendar.MONTH, commitmentPeriod);
					if ("MONTHLY".equals(frequency)) {
						c.set(Calendar.HOUR_OF_DAY,
								c.getActualMaximum(Calendar.HOUR_OF_DAY));
					}
					c.set(Calendar.MINUTE, c.getActualMaximum(Calendar.MINUTE));
					c.set(Calendar.SECOND, c.getActualMaximum(Calendar.SECOND));
					
					return fdf.format(c.getTime());

				} else {
					return "";
				}
			}
		
	
}
