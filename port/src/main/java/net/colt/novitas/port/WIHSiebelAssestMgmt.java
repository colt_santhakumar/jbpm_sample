package net.colt.novitas.port;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.HandlerUtils;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;
import net.colt.novitas.siteorder.response.LocationResponse;
import com.colt.novitas.response.GetPortRequestResponse;
import com.colt.novitas.siebel.assests.client.SiebelAssetsClient;
import com.colt.novitas.siebel.assets.request.SiebelAssestRequest;
import com.colt.novitas.siebel.assets.request.SlaIdType;
/**
 * @author DRao This is a WIH file created by Dharrma Rao.
 * 
 */
public class WIHSiebelAssestMgmt extends BaseWorkItemHandler {

	private static Logger logger = LoggerFactory.getLogger(WIHSiebelAssestMgmt.class);

	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
		EnvironmentProperties prop = getEnvProperties(workItem);
		logger = getLogger(workItem, prop.getLogUniqueId());
		try {

			GetPortRequestResponse portRequestResponse = getPortRequest(workItem);
			String portType = (String) workItem.getParameter("port_type");
			logger.info("Port Resquest Response is ::: "+portRequestResponse);
			String servicePortId = (String) workItem.getParameter("service_port_id");
			String presentationLabel = (String) workItem.getParameter("presentation_label");
			String status = (String) workItem.getParameter("status");
			String localName = String.valueOf(workItem.getParameter("local_name"));
			String nms = String.valueOf(workItem.getParameter("nms"));
			SiebelAssestRequest request = new SiebelAssestRequest();
			request.setPamFlag("Y");
			request.setSlaId(SlaIdType.SLA_01.value());
			request.setServiceId(servicePortId);
			request.setServiceName(portRequestResponse.getPortName());
			request.setOcn(portRequestResponse.getOcn());
			request.setNovitasServiceId(portRequestResponse.getServiceId());
			request.setStatus(status);
			
			if("DCNPORT".equals(portType)){
				request.setProductName("COLT On Demand DCNet Port");
			}else if ("LANPORT".equals(portType)){
				//request.setProductName("COLT On Demand LANLink Port");
				request.setProductName("COLT On Demand Ethernet Port");
			}else{
				request.setProductName("COLT On Demand Port");
			}

			String circuitId = String.valueOf(workItem.getParameter("circuit_reference"));
			request.setResourceId(circuitId);
			
			Map<String, String> attibutesMap = new HashMap<String, String>();
			attibutesMap.put("bandwidth", portRequestResponse.getBandwidth().toString());
			attibutesMap.put("connector", portRequestResponse.getConnector());
			//attibutesMap.put("technology", portRequestResponse.getProductName());
			//Replace 100Base-T / 100Base-TX to 1000Base-T
			attibutesMap.put("technology", null != portRequestResponse.getProductName() ? portRequestResponse.getProductName().startsWith("100BASE-T") ? "1000BASE-T" : portRequestResponse.getProductName() : null);
			
			if("Active".equals(status)){
				LocationResponse  locationResponse= getLocationResponse(workItem);
				attibutesMap.put("presentation_label", presentationLabel);
				attibutesMap.put("resource_id", circuitId);
				attibutesMap.put("site_type", locationResponse.getPmSiteType());
				attibutesMap.put("rental_charge",portRequestResponse.getRentalCharge().toString());
				attibutesMap.put("rental_unit", portRequestResponse.getRentalUnit());
				attibutesMap.put("rental_currency",portRequestResponse.getRentalCurrency());
				attibutesMap.put("location", portRequestResponse.getLocationName());
				attibutesMap.put("address", locationResponse.getAddress());

				if("PO".equals(nms)  || "ASIA".equals(nms)){
					attibutesMap.put("A00_AccountJPN", localName);
					attibutesMap.put("A03_A-BuildingName", locationResponse.getBuildingName());
					attibutesMap.put("A04_B-BuildingName", locationResponse.getBuildingName());
					attibutesMap.put("A06_B-AccountName", localName);
					attibutesMap.put("A16_A03_A-BuildingNameJPN", locationResponse.getLocalBuildingName());
					attibutesMap.put("A17_B-BuildingNameJPN", locationResponse.getLocalBuildingName());
					attibutesMap.put("A18_ServiceJPN", "オンデマンド");
					attibutesMap.put("A05_A-AccountName", portRequestResponse.getCustomerName());
					attibutesMap.put("Service_Type", request.getProductName());
					attibutesMap.put("A08_STTEnable", "No");
					attibutesMap.put("A09_BillingDate", getFirstOfFollowingMonth());
					attibutesMap.put("A14_A-AccountNameJPN", localName);
			   }	
				
				
			}else{
				attibutesMap.put("decommissioning_charge", portRequestResponse.getDecommissioningCharge().toString());
				attibutesMap.put("decommissioning_currency", portRequestResponse.getDecommissioningCurrency());
				attibutesMap.put("decommissioned_on", new Date().toString());
				if("PO".equals(nms)  || "ASIA".equals(nms)){
				   attibutesMap.put("A19_RequestedDisconnectDate", portRequestResponse.getRequestedAt());
				}
			}

			attibutesMap.put("commitment_expiry", portRequestResponse.getCommitmentPeriod().toString());
			
			attibutesMap.put("created_date", portRequestResponse.getRequestedAt());
			request.setAttributesMap(attibutesMap);
			
			request.setUrlCallback(prop.getCallBackUrl());
			
			String transactionId  = new SiebelAssetsClient(prop.getSiebelUrl(),prop.getSiebelUsername(),
					                                    prop.getSiebelPassword()).receiveAssetMgmtRequest(request);

			Map<String, Object> retval = new HashMap<String, Object>();
			if (StringUtils.isBlank(transactionId)) {
				logger.info("Failed to Update Siebel Assests Management");
				throw new NovitasHandlerException();
			}
			
			retval.put("Result", transactionId);
			manager.completeWorkItem(workItem.getId(), retval);

		} catch (Exception e) {
			logger.error("WIHSiebelAssestMgmt -> Failed", e);
				HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(
						HandlerErrorCode.CREATE_OR_UPDATE_SIEBEL_ASSETS_MGMT));
			
		}
	}
	
	protected GetPortRequestResponse getPortRequest(WorkItem workItem) {
		return (GetPortRequestResponse) workItem.getParameter("request_obj");
	}
	
	protected LocationResponse getLocationResponse(WorkItem workItem) {
		return (LocationResponse) workItem.getParameter("location");
	}
	
	private String getFirstOfFollowingMonth(){
		Calendar calendar = Calendar.getInstance();         
		calendar.add(Calendar.MONTH, 1);
		calendar.set(Calendar.DATE, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
		Date nextMonthFirstDay = calendar.getTime();
		return nextMonthFirstDay.toString();
	}

}
