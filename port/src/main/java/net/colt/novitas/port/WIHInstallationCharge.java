package net.colt.novitas.port;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.billing.client.BillingAPIClient;
import com.colt.novitas.billing.client.request.OneOffChargeRequest;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;
import com.colt.novitas.response.GetPortRequestResponse;

public class WIHInstallationCharge extends BaseWorkItemHandler {
	
	private static Logger logger = LoggerFactory.getLogger(WIHInstallationCharge.class);
	
	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}
	

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
		EnvironmentProperties prop = getEnvProperties(workItem);
		logger = getLogger(workItem, prop.getLogUniqueId());
		
		try {
			
			Integer requestId = getRequestId(workItem);
			
			GetPortRequestResponse portRequest = getPortRequst(workItem);
			
			String dedicatedPort = getDedicatedPortId(workItem);
			String portType = (String) workItem.getParameter("port_type");
			OneOffChargeRequest oneoff = new OneOffChargeRequest();
			oneoff.setRequestId(requestId);
			oneoff.setServiceId(portRequest.getServiceId());
			oneoff.setServiceInstanceId(dedicatedPort);
			oneoff.setServiceInstanceType(portType);
			oneoff.setBcn(portRequest.getBcn());
			oneoff.setChargeType("INSTALLATION");
			oneoff.setDescription("Installation for port " + portRequest.getPortName());
			oneoff.setCurrency(portRequest.getInstallationCurrency());
			oneoff.setAmount(portRequest.getInstallationCharge());	
		    oneoff.setServiceInstanceName(portRequest.getPortName());
			
			Object chargeResponse = new BillingAPIClient(
					prop.getBillingAPIOneOffUrl(),
					prop.getBillingAPIRecurrUrl(), 
					prop.getBillingAPIUsername(),
					prop.getBillingAPIPassword()).createOneOffCharges(oneoff);
			
			logger.info(String.valueOf(chargeResponse));
			
			if (StringUtils.isBlank(String.valueOf(chargeResponse))) {
				logger.info("Failed to create Installation charges");
				throw new NovitasHandlerException();
			}
			
			Map<String,Object> retval = new HashMap<String,Object>();
			retval.put("Result", chargeResponse);
			manager.completeWorkItem(workItem.getId(), retval);
			
		} catch (Exception e) {
			logger.error("WIHInstallationCharge -> Failed", e);
			throw new NovitasHandlerException(HandlerErrorCode.CP_PORT_INSTALL_CHARGE);
		}
	}
	
	//TODO : move to base class
	
		protected Integer getRequestId(WorkItem workItem) {
			Object obj = workItem.getParameter("request_id");
			Integer requestId = null;
			if (obj instanceof String) {
				requestId = Integer.valueOf(String.valueOf(obj));
			}
			else {
				requestId = (Integer) obj;
			}
			logger.info("Request Id : " + requestId);

			return requestId;
		}
		
		protected GetPortRequestResponse getPortRequst(WorkItem workItem) {
			return (GetPortRequestResponse) workItem.getParameter("request_obj");
		}
		
		protected String getDedicatedPortId(WorkItem workItem) {
			return (String) workItem.getParameter("dedicated_port_id");
		}

}
