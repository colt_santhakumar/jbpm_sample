package net.colt.novitas.port;

import java.util.HashMap;
import java.util.Map;

import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.client.RequestAPIClient;
import com.colt.novitas.response.GetPortRequestResponse;

public class WIHRefreshCache  extends BaseWorkItemHandler {
	
	private static Logger logger = LoggerFactory.getLogger(WIHRefreshCache.class);
	
	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}
	

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
		
		EnvironmentProperties prop = getEnvProperties(workItem);
		logger = getLogger(workItem, prop.getLogUniqueId());
		try {
			
            GetPortRequestResponse portRequest = getPortRequst(workItem);
			String locationId = portRequest.getLocationId();
			logger.info("Refreshing Product Cache for site id  ::"+locationId );
			
			RequestAPIClient client =  new RequestAPIClient(prop.getRequestAPIUrl(),
												            prop.getRequestAPIUsername(),
												            prop.getRequestAPIPassword());
			client.refreshProductCache(locationId, 0);
			logger.info("Product Cache refreshed successfully for site id  ::"+locationId +"/\\../\\");
			Map<String,Object> retval = new HashMap<String,Object>();
			manager.completeWorkItem(workItem.getId(), retval);
			
		} catch (Exception e) {
			logger.error("WIHRefreshCache -> Failed", e);
		}
	}
	
	protected GetPortRequestResponse getPortRequst(WorkItem workItem) {
		return (GetPortRequestResponse) workItem.getParameter("request_obj");
	}
	
	
	
}
