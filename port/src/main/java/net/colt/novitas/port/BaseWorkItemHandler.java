package net.colt.novitas.port;

import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import com.colt.novitas.EnvironmentProperties;

public abstract class BaseWorkItemHandler implements WorkItemHandler {
	
	private static Logger logger = null;
	
	protected static final String INPROGRESS = "INPROGRESS";
	protected static final String DCA_PORT = "COLT On Demand DCA Port";
	
	protected static final String DCA_PORT_DELETE_SIEBEL_SIGNAL = "delete-dca-port-siebel-signal";
	protected static final String DCA_PORT_CREATE_SIEBEL_SIGNAL = "create-dca-port-siebel-signal";
	
	protected static String LOG_UNIQUE_ID = null;
	
	protected Logger getLogger(WorkItem workItem, String uniqueLogId){
		if(null == LOG_UNIQUE_ID) {
			LOG_UNIQUE_ID = uniqueLogId;
			MDC.put("unique_log_id","J"+uniqueLogId.substring(1));
		}
		logger = LoggerFactory.getLogger(uniqueLogId+" | ProcessId-"+ workItem.getProcessInstanceId() + " Task Name  |" );
		return logger;
	}

	
	protected EnvironmentProperties getEnvProperties(WorkItem workItem){
		return (EnvironmentProperties) workItem.getParameter("env_properties");
	}
	
	protected Integer getRequestId(WorkItem workItem) {
		Object obj = workItem.getParameter("request_id");
		Integer requestId = null;
		if (obj instanceof String) {
			requestId = Integer.valueOf(String.valueOf(obj));
		}
		else {
			requestId = (Integer) obj;
		}

		return requestId;
	}
	
	

}
