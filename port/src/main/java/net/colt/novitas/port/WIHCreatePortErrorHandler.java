package net.colt.novitas.port;

import java.util.HashMap;
import java.util.Map;

import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.client.RequestAPIClient;
import com.colt.novitas.client.ServiceAPIClient;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;


public class WIHCreatePortErrorHandler extends BaseWorkItemHandler {
	
	private static Logger logger = LoggerFactory.getLogger(WIHCreatePortErrorHandler.class);

	private WorkItem workItem;
	private EnvironmentProperties prop;

	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
		this.prop = getEnvProperties(workItem);
		logger = getLogger(workItem, prop.getLogUniqueId());
		try {
			logger.info("Starting error handling for creat port request......... ");

			this.workItem = workItem;
			Integer requestId = getRequestId();

			logger.info("Starting error handling for request : " + requestId);

			Map<String,Object> retval = new HashMap<String,Object>();
			
			
			HandlerErrorCode errorCode = null;
			Exception exception = getWorkflowError();
			if (exception instanceof NovitasHandlerException) {
				errorCode = ((NovitasHandlerException) exception).getErrorCode();
				logger.info("Error code : " + errorCode);
			}

			if (errorCode == null) {
				logger.info("NovitasHandlerException thrown without error code. Unable to process further.");
				errorCode = HandlerErrorCode.GENERAL_ERROR_CODE;
			}

			
			if (errorCode != null) {
				
				if (errorCode.isPaymentError()) {
					logger.info("Failed to create or cancel charges. Code : " + errorCode + " request : " + requestId);
					retval.put("payment_failed", "true");
				}
				retval.put("error_code", errorCode.getErrorId());
				retval.put("error_description", errorCode.getDescription());
				
			}

			manager.completeWorkItem(workItem.getId(), retval);
			
		} catch (Exception e) {
			logger.error("Error handling for creat port has been failed.", e);
			manager.abortWorkItem(workItem.getId());
		}

	}


		
	protected RequestAPIClient getRequestClient() {

		return new RequestAPIClient(
				prop.getRequestAPIUrl(),
				prop.getRequestAPIUsername(),
				prop.getRequestAPIPassword()
				);
	}

	protected ServiceAPIClient getServiceClient() {

		return new ServiceAPIClient(prop.getServiceAPIBaseUrl(),
									prop.getServiceAPIUsername(),
									prop.getServiceAPIPassword());
	}

	private Exception getWorkflowError() {
		Object obj = workItem.getParameter("error_object");
		if (obj instanceof Exception) {
			return (Exception) obj;
		}
		return null;
	}

	protected Integer getRequestId() {
		logger.info("workItem : " + workItem);
		Object obj = workItem.getParameter("request_id");

		if (obj == null) {
			return null;
		}

		Integer requestId = null;
		if (obj instanceof Integer) {
			requestId = (Integer) obj;
		}
		else {
			requestId = Integer.valueOf(String.valueOf(obj));
		}

		return requestId;
	}


	protected String getDedicatedPortId(WorkItem workItem) {
		return (String) workItem.getParameter("dedicated_port_id");
	}

	

}
