package net.colt.novitas.port;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.HandlerUtils;
import com.colt.novitas.client.CallBackAPIClient;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;
import com.colt.novitas.request.CallBack;

/**
 * @author DRao This is a WIH file created by Dharrma Rao.
 *
 */
public class WIHUpdateCallBackStatus extends BaseWorkItemHandler {

	private static Logger logger = LoggerFactory.getLogger(WIHUpdateCallBackStatus.class);

	private static final String RESERVE_PORT_SIGNAL = "reserveport_signal";
	private static final String SIEBEL_ASSETS_SIGNAL = "siebelassets_signal";

	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {

		EnvironmentProperties prop = getEnvProperties(workItem);
		logger = getLogger(workItem, prop.getLogUniqueId());

		String signalName = (String) workItem.getParameter("signal_name");

		try {

			String[] signalData = (String[]) workItem.getParameter("received_signal_data");
			String deploymentId = (String) workItem.getParameter("deployment_id");
			CallBack callBack = new CallBack();

			callBack.setTransactionId(signalData[0]);
			callBack.setProcessInstanceId(workItem.getProcessInstanceId());
			callBack.setStatus(signalData[1]);
			callBack.setSignalName(signalName);
            callBack.setStatusDescription(signalData[2]);

			logger.info("deploymentId from caller:" + deploymentId);
			if(null != deploymentId) {
				callBack.setDeploymentId(deploymentId);
				logger.info("set deploymentId from caller:" + deploymentId);
			}
			else {
				callBack.setDeploymentId(prop.getPortDeploymentId());
				logger.info("set deploymentId from properties:" + deploymentId);
			}
			CallBackAPIClient callBackAPIClient = new CallBackAPIClient(prop.getInboundRestApiUrl(),prop.getInboundRestApiUserName(),prop.getInboundRestApiPassword());

			callBackAPIClient.updateStatus(callBack);

			Map<String, Object> retval = new HashMap<String, Object>();

			if("reserveport_signal".equals(signalName)){
            	retval.put("is_port_reserved", "SUCCESS".equals(signalData[1]) || "WARNING".equals(signalData[1]));

            }else if (StringUtils.isBlank(signalData[1]) || "ERROR".equals(signalData[1])) {
    				logger.info("Status in response either null not SUCCESS");
    				throw new NovitasHandlerException();
            }

			manager.completeWorkItem(workItem.getId(), retval);

		} catch (Exception e) {

			logger.error("WIHUpdateCallBackStatus -> Failed", e);

			if(signalName.equalsIgnoreCase(RESERVE_PORT_SIGNAL))
				HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(HandlerErrorCode.RESERVE_PORT_CALL_BACK_STATUS));
			if(signalName.equalsIgnoreCase(SIEBEL_ASSETS_SIGNAL))
				HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(HandlerErrorCode.PORT_SIEBEL_ASSETS_CALL_BACK_STATUS));
			else
				HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(HandlerErrorCode.UPDATE_STATUS_CB_DETAILS));
		}
	}

}