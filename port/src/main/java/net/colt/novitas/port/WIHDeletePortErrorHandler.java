package net.colt.novitas.port;

import java.util.HashMap;
import java.util.Map;

import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.client.RequestAPIClient;
import com.colt.novitas.client.ServiceAPIClient;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;
import com.colt.novitas.request.UpdateRequestStatusRequest;

public class WIHDeletePortErrorHandler  extends BaseWorkItemHandler {
	
	private static Logger logger = LoggerFactory.getLogger(WIHDeletePortErrorHandler.class);

	private WorkItem workItem;
	private EnvironmentProperties prop;

	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
		this.prop = getEnvProperties(workItem);
		logger = getLogger(workItem, prop.getLogUniqueId());
		try {
			logger.info("Starting error handling for creat port request......... ");

			this.workItem = workItem;
			Integer requestId = getRequestId();

			logger.info("Starting error handling for request : " + requestId);

			Map<String,Object> retval = new HashMap<String,Object>();
			
			
			HandlerErrorCode errorCode = null;
			Exception exception = getWorkflowError();
			if (exception instanceof NovitasHandlerException) {
				errorCode = ((NovitasHandlerException) exception).getErrorCode();
				logger.info("Error code : " + errorCode);
			}

			if (errorCode == null) {
				logger.info("NovitasHandlerException thrown without error code. Unable to process further.");
				errorCode = HandlerErrorCode.GENERAL_ERROR_CODE;
			}

			
			if (errorCode != null) {
				
				if (errorCode != null && errorCode.isFailRequest()) {
					failRequest(errorCode);
				}
				
				if (errorCode.isPaymentError()) {
					logger.info("Failed to create or cancel charges. Code : " + errorCode + " request : " + requestId);
					retval.put("payment_failed", "true");
				}
				retval.put("payment_error_code", errorCode.getErrorId());
				retval.put("payment_error_description", errorCode.getDescription());
			}
			
			logger.info("Error handling completed for request : " + requestId);

			manager.completeWorkItem(workItem.getId(), retval);
			
			
		} catch (Exception e) {
			logger.info("Error handling for creat port has been failed.");
			e.printStackTrace();
			manager.abortWorkItem(workItem.getId());
		}
	}
	
	protected void failRequest(HandlerErrorCode errorCode) {

		Integer requestId = null;

		try {
			
			requestId = getRequestId();

			RequestAPIClient client = getRequestClient();
			UpdateRequestStatusRequest requestObject = new UpdateRequestStatusRequest();
			requestObject.setStatus("FAILED");

			if (errorCode != null) {
				requestObject.setStatusCode(errorCode.getErrorId());
			}

			client.updatePortorConnectionRequest(requestId, requestObject);
		} catch (Exception e) {
			logger.info("Failed to update request to FAILED state. Request Id : " + requestId);
			e.printStackTrace();
		}
	}


	protected void failRequest(Exception exception, Integer errorCode) {

		Integer requestId = null;

		try {

			if (errorCode == null || errorCode == 0) {
				errorCode = -1;
			}

			if (errorCode == 101 || errorCode == 102) {
				logger.info("Requset failed, but status is not updated to FAILED as it's in a invalid status. Code : " + errorCode);

				return;
			}

			requestId = getRequestId();

			RequestAPIClient client = getRequestClient();
			UpdateRequestStatusRequest requestObject = new UpdateRequestStatusRequest();
			requestObject.setStatus("FAILED");

			requestObject.setStatusCode(errorCode);

			client.updatePortorConnectionRequest(requestId, requestObject);
		} catch (Exception e) {
			logger.error("Failed to update request to FAILED state. Request Id : " + requestId, e);
		}

	}

	protected Integer getRequestId() {
		//logger.info("workItem : " + workItem);
		Object obj = workItem.getParameter("request_id");

		if (obj == null) {
			return null;
		}

		Integer requestId = null;
		if (obj instanceof Integer) {
			requestId = (Integer) obj;
		}
		else {
			requestId = Integer.valueOf(String.valueOf(obj));
		}

		return requestId;
	}

	private Exception getWorkflowError() {
		Object obj = workItem.getParameter("error_object");
		if (obj instanceof Exception) {
			return (Exception) obj;
		}
		return null;
	}


	protected RequestAPIClient getRequestClient() {

		return new RequestAPIClient(
				prop.getRequestAPIUrl(),
				prop.getRequestAPIUsername(),
				prop.getRequestAPIPassword()
				);
	}

	protected ServiceAPIClient getServiceClient() {

		return new ServiceAPIClient(prop.getServiceAPIBaseUrl(),
				prop.getServiceAPIUsername(),
				prop.getServiceAPIPassword());
	}

	protected String getDedicatedPortId(WorkItem workItem) {
		return (String) workItem.getParameter("service_port_id");
	}

	protected Integer getResourcePortResponse(WorkItem workItem) {
		return (Integer) workItem.getParameter("resource_port_id");
	}

}
