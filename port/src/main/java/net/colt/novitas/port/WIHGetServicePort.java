package net.colt.novitas.port;

import java.util.HashMap;
import java.util.Map;

import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.HandlerUtils;
import com.colt.novitas.client.ServiceAPIClient;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;
import com.colt.novitas.response.CustomerDedicatedPortResponse;

public class WIHGetServicePort extends BaseWorkItemHandler {
	
	private static Logger logger = LoggerFactory.getLogger(WIHGetServicePort.class);
	
	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
		EnvironmentProperties prop = getEnvProperties(workItem);
		logger = getLogger(workItem, prop.getLogUniqueId());
		try {
			
			String portId = getPortId(workItem);
			
			Object result = new ServiceAPIClient(prop.getServiceAPIBaseUrl(),
					prop.getServiceAPIUsername(),
					prop.getServiceAPIPassword()).getPort(portId); 
			
			logger.info("Get customer port call successful :" + result);
			
			Map<String,Object> retval = new HashMap<String,Object>();
			
			if (result instanceof CustomerDedicatedPortResponse) {
				
				CustomerDedicatedPortResponse response = (CustomerDedicatedPortResponse) result;
				retval.put("Result", response);
				retval.put("resource_port_id", String.valueOf(response.getResourceId()));
				retval.put("port_type", String.valueOf(response.getPortType()));
			}
			else {
				throw new Exception("Get customer port call failed :" + result);
			}
			
			manager.completeWorkItem(workItem.getId(), retval);
			
		} catch (Exception e) {
			logger.error("WIHGetServicePort -> Fail : ", e);
			HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(HandlerErrorCode.DP_NO_MATCHING_SERVICE_PORT));
		}
	}
	
	
	protected String getPortId(WorkItem workItem) {
		
		return (String) workItem.getParameter("service_port_id");
		
		
		
	}

}
