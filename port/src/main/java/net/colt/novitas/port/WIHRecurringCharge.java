package net.colt.novitas.port;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.billing.client.BillingAPIClient;
import com.colt.novitas.billing.client.request.RecurringChargeRequest;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;
import com.colt.novitas.response.GetPortRequestResponse;

public class WIHRecurringCharge  extends BaseWorkItemHandler {
	
	private static Logger logger = LoggerFactory.getLogger(WIHRecurringCharge.class);
	
	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}
	

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
		EnvironmentProperties prop = getEnvProperties(workItem);
		logger = getLogger(workItem, prop.getLogUniqueId());
		try {
			
			Integer requestId = getRequestId(workItem);
			
			GetPortRequestResponse portRequest = getPortRequst(workItem);
			String portType = (String) workItem.getParameter("port_type");
			String dedicatedPort = getDedicatedPortId(workItem);
			
			RecurringChargeRequest recurring = new RecurringChargeRequest();
			recurring.setRequestId(requestId);
			recurring.setServiceId(portRequest.getServiceId());
			recurring.setServiceInstanceId(dedicatedPort);
			recurring.setServiceInstanceType(portType);
			recurring.setBcn(portRequest.getBcn());
			recurring.setChargeType("RENTAL");
			recurring.setDescription("Rental for port : " + portRequest.getPortName());
			recurring.setCurrency(portRequest.getRentalCurrency());
			recurring.setAmount(portRequest.getRentalCharge());
			recurring.setFrequency(portRequest.getRentalUnit());
			recurring.setServiceInstanceName(portRequest.getPortName());
			
			Object chargeResponse = new BillingAPIClient(prop.getBillingAPIOneOffUrl(),
					prop.getBillingAPIRecurrUrl(), 
					prop.getBillingAPIUsername(),
					prop.getBillingAPIPassword()).createRecurringCharges(recurring);
			
			logger.info(String.valueOf(chargeResponse));
			
			if (StringUtils.isBlank(String.valueOf(chargeResponse))) {
				logger.info("Failed to create Recurring charges");
				throw new NovitasHandlerException();
			}
			
			Map<String,Object> retval = new HashMap<String,Object>();
			retval.put("Result", chargeResponse);
			manager.completeWorkItem(workItem.getId(), retval);
			
		} catch (Exception e) {
			logger.error("WIHRecurringCharge -> Failed.", e);
			throw new NovitasHandlerException(HandlerErrorCode.CP_PORT_RECURRING_CHARGE);
		}
	}
	
	//TODO : move to base class
	
		protected Integer getRequestId(WorkItem workItem) {
			Object obj = workItem.getParameter("request_id");
			Integer requestId = null;
			if (obj instanceof String) {
				requestId = Integer.valueOf(String.valueOf(obj));
			}
			else {
				requestId = (Integer) obj;
			}
			logger.info("Request Id : " + requestId);

			return requestId;
		}
		
		protected GetPortRequestResponse getPortRequst(WorkItem workItem) {
			return (GetPortRequestResponse) workItem.getParameter("request_obj");
		}
		
	   protected String getDedicatedPortId(WorkItem workItem) {
			return (String) workItem.getParameter("dedicated_port_id");
		}
	
}
