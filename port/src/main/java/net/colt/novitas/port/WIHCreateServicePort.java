package net.colt.novitas.port;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import org.apache.commons.lang.StringUtils;
import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.HandlerUtils;
import com.colt.novitas.client.ServiceAPIClient;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;
import com.colt.novitas.request.CreatePortRequest;
import net.colt.novitas.siteorder.response.LocationResponse;
import com.colt.novitas.response.GetPortRequestResponse;


public class WIHCreateServicePort extends BaseWorkItemHandler {
	
	private static Logger logger = LoggerFactory.getLogger(WIHCreateServicePort.class);
	
	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}
	

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
		EnvironmentProperties prop = getEnvProperties(workItem);
		logger = getLogger(workItem, prop.getLogUniqueId());
		try {
			
			GetPortRequestResponse portRequest = getPortRequst(workItem);
			String portType = (String) workItem.getParameter("port_type");
			logger.info("SERVICE PORTNAME--->: " + portRequest.getPortName() +" and Port Type is :: "+portType);
			
			CreatePortRequest req = new CreatePortRequest();
			req.setMaxAllowedIpaConnBandwidth((Integer) workItem.getParameter("max_allowed_ipa_conn_bandwidth"));
			req.setName(portRequest.getPortName());
			req.setLocation(portRequest.getLocationName());
			req.setBandwidth(portRequest.getBandwidth());
			req.setConnector(portRequest.getConnector());
			req.setRentalCharge(portRequest.getRentalCharge());
			req.setRentalCurrency(portRequest.getRentalCurrency());
			req.setRentalUnit(portRequest.getRentalUnit());
			req.setPresentationLabel("");
			
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.MONTH, portRequest.getCommitmentPeriod());
			req.setCommitmentExpiry(cal.getTime());
			
			req.setServiceId(portRequest.getServiceId());
			req.setResourceId(null);
			req.setTechnology(portRequest.getProductName());
			
			req.setDecommissioningCharge(portRequest.getDecommissioningCharge());
			req.setDecommissioningCurrency(portRequest.getDecommissioningCurrency());
			req.setPortType(portType);
			//set location details
			LocationResponse location = getLocation(workItem);
			
			if (location != null) {
				req.setAddress(location.getAddress());
				req.setSiteType(location.getNovitasSiteType());
				req.setIsCrossConnectOrderable(location.getCrossConnectAllowed());
			}
			req.setLocationBuildingName(location.getBuildingName());
			req.setLocationCity(location.getCity());
            req.setLocationCountry(location.getCountry());
            req.setLocationState(location.getState());
            req.setLatitude(location.getLatitude());
            req.setLongitude(location.getLongitude());
            req.setSiteFloor(location.getFloor());   
            req.setSiteRoomName(location.getRoomName());
            req.setLocationPremisesNumber(location.getPremisesNumber());
            req.setPostalZipCode(location.getPostalZipCode());
            req.setLocationStreetName(location.getStreetName());
			req.setOcn(portRequest.getOcn());
			req.setCustomerName(portRequest.getCustomerName());
			req.setLocationId(portRequest.getLocationId());
			
			req.setPortExpiryPeriod(portRequest.getExpirationPeriod());
			req.setPortExpiresOn(getPortExpiryDate(portRequest.getExpirationPeriod()));
			req.setIsPortInUse(false);
			req.setLoaAllowed(portRequest.isLoaAllowed());
			req.setLocalBuildingName(location.getLocalBuildingName());
			req.setLocationCityCode(location.getCityCode());
            req.setLocationCountryCode(location.getCountryCode());
            if(null != portRequest.getCustomerPortID() ) {
            	req.setCustomerPortID(portRequest.getCustomerPortID());
            }
			String response = new ServiceAPIClient(prop.getServiceAPIBaseUrl(),
					prop.getServiceAPIUsername(),
					prop.getServiceAPIPassword()).createPort(req);
			
			logger.info("Create Port response :" + response);
			
			if (StringUtils.isBlank(response)) {
				throw new NovitasHandlerException();
			}
			
			
			Map<String,Object> retval = new HashMap<String,Object>();
			retval.put("Result", response);
			manager.completeWorkItem(workItem.getId(), retval);
			
		} catch (Exception e) {
			logger.error("WIHCreateServicePort -> Failed", e);
			HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(HandlerErrorCode.CP_CREATE_SERVICE_PORT));
		}
	}
	
	//TODO : move to base class
	
		protected Integer getRequestId(WorkItem workItem) {
			Object obj = workItem.getParameter("request_id");
			Integer requestId = null;
			if (obj instanceof String) {
				requestId = Integer.valueOf(String.valueOf(obj));
			}
			else {
				requestId = (Integer) obj;
			}
			logger.info("Request Id : " + requestId);

			return requestId;
		}
		
		protected GetPortRequestResponse getPortRequst(WorkItem workItem) {
			return (GetPortRequestResponse) workItem.getParameter("request_obj");
		}
		

		protected Integer getDedicatedPortId(WorkItem workItem) {
			return (Integer) workItem.getParameter("dedicated_port_id");
		}
		
		protected LocationResponse getLocation(WorkItem workItem) {
			
			Object object = workItem.getParameter("location_obj");
			if (object instanceof LocationResponse) {
				return (LocationResponse)object;
			}
			return null;
			
		}
		
	public String getPortExpiryDate(Integer commitmentPeriod) {

		if (commitmentPeriod == null) {
			return null;
		}
		
		DateFormat fdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
		fdf.setTimeZone(TimeZone.getTimeZone("UTC"));
		Calendar c = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		c.add(Calendar.DATE, commitmentPeriod);

		return fdf.format(c.getTime());

	}
		 
	
}
