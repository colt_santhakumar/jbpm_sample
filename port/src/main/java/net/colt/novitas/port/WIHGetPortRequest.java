package net.colt.novitas.port;

import java.util.HashMap;
import java.util.Map;

import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.HandlerUtils;
import com.colt.novitas.WorkflowType;
import com.colt.novitas.client.RequestAPIClient;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;
import com.colt.novitas.response.GetPortRequestResponse;


public class WIHGetPortRequest extends BaseWorkItemHandler {
	
	private static Logger logger = LoggerFactory.getLogger(WIHGetPortRequest.class);
	
	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}
	
	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
		Integer requestId = getRequestId(workItem);
		EnvironmentProperties prop = getEnvProperties(workItem);
		logger = getLogger(workItem, prop.getLogUniqueId());
		try {
			GetPortRequestResponse portResponse  = null;
			
			Object portResponseObj = new RequestAPIClient(
					prop.getRequestAPIUrl(),
					prop.getRequestAPIUsername(),
					prop.getRequestAPIPassword()
					).getPortorConnectionRequest(requestId);
			
			logger.info("Response: " + portResponseObj.toString());
			
			if (portResponseObj instanceof GetPortRequestResponse) {
				portResponse = (GetPortRequestResponse) portResponseObj;
				if("CREATE".equals(portResponse.getAction()) && null != portResponse.getCustomerPortID() && (null == portResponse.getOhsCircuitId() || null == portResponse.getNcTechServiceId()) ) {
					logger.info("NetCracker required details missed : " + requestId);
					throw new NovitasHandlerException();
				}
			}
			else {
				logger.info("Invalid port request id : " + requestId);
				throw new NovitasHandlerException();
			}
			
			
			Map<String,Object> retval = new HashMap<String,Object>();
			retval.put("Result", portResponse);
			
			if (portResponse.getPortId() != null) {
				retval.put("service_port_id", portResponse.getPortId());
			}
			retval.put("ocn", portResponse.getOcn());
			manager.completeWorkItem(workItem.getId(), retval);
			
		} catch (Exception e) {
			logger.error("WIHGetPortRequest -> Failed", e);
			HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(getErrorCode(workItem)));
		}
	}
	
	protected HandlerErrorCode getErrorCode(WorkItem workItem) {
		WorkflowType type = HandlerUtils.getWorkflowType(workItem);
		if (type == WorkflowType.DELETE_PORT) {
			return HandlerErrorCode.DP_INVALID_REQUEST;
		} 
		return HandlerErrorCode.CP_INVALID_REQUEST;
	}
	
	//TODO : move to base class
	
		protected Integer getRequestId(WorkItem workItem) {
			Object obj = workItem.getParameter("request_id");
			Integer requestId = null;
			if (obj instanceof String) {
				requestId = Integer.valueOf(String.valueOf(obj));
			}
			else {
				requestId = (Integer) obj;
			}
			logger.info("Request Id : " + requestId);

			return requestId;
		}
		
		protected GetPortRequestResponse getPortRequst(WorkItem workItem) {
			return (GetPortRequestResponse) workItem.getParameter("request_obj");
		}
		

		protected Integer getDedicatedPortId(WorkItem workItem) {
			return (Integer) workItem.getParameter("dedicated_port_id");
		}

}