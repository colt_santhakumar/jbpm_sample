package net.colt.novitas.customer.internal;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.HandlerUtils;
import com.colt.novitas.billing.client.BillingAPIClient;
import com.colt.novitas.billing.client.request.OneOffChargeRequest;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;
import com.colt.novitas.response.CloudPortResponse;
import com.colt.novitas.response.CustomerDedicatedPortResponse;

public class WIHDecommissionCharge extends BaseWorkItemHandler {
	
	private static Logger logger = LoggerFactory.getLogger(WIHDecommissionCharge.class);
	
	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}
	

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
		
		try {
			EnvironmentProperties prop = getEnvProperties(workItem);
			logger = getLogger(workItem, prop.getLogUniqueId());
			Map<String,Object> retval = new HashMap<String,Object>();
			String type  = getPortType(workItem);
			CustomerDedicatedPortResponse stdPort = null;
			CloudPortResponse cloudPort = null;
			
			String bcn = getBcn(workItem);
			OneOffChargeRequest oneoff = new OneOffChargeRequest();
			boolean hasDecommissionCharge = false;
			if("DCNPORT".equals(type) || "LANPORT".equals(type)){
				stdPort = getStdPort(workItem);
				if(null != stdPort && stdPort.getDecommissioningCharge() > 0){
					hasDecommissionCharge = true;
					oneoff.setServiceId(stdPort.getServiceId());
					oneoff.setServiceInstanceId(stdPort.getId());
					oneoff.setDescription("Decommission of port " + stdPort.getName());
					oneoff.setCurrency(stdPort.getDecommissioningCurrency());
					oneoff.setAmount(stdPort.getDecommissioningCharge());
					oneoff.setServiceInstanceName(stdPort.getName());
				}
			}else if ("DCAPORT".equals(type)){
				cloudPort = getDCAPort(workItem);
				if(null != cloudPort && cloudPort.getDecommissioningCharge() > 0){
					hasDecommissionCharge = true;
					oneoff.setServiceId(cloudPort.getServiceId());
					oneoff.setServiceInstanceId(cloudPort.getId());
					oneoff.setDescription("Decommission of port " + cloudPort.getName());
					oneoff.setCurrency(cloudPort.getDecommissioningCurrency());
					oneoff.setAmount(cloudPort.getDecommissioningCharge());
					oneoff.setServiceInstanceName(cloudPort.getName());
				}
			}
			oneoff.setBcn(bcn);
			oneoff.setServiceInstanceType(type);
			oneoff.setChargeType("DECOMISSION");
			oneoff.setRequestId(9999);
			if (hasDecommissionCharge) {
				Object chargeResponse = new BillingAPIClient(
						prop.getBillingAPIOneOffUrl(),
						prop.getBillingAPIRecurrUrl(), 
						prop.getBillingAPIUsername(),
						prop.getBillingAPIPassword()
						).createOneOffCharges(oneoff);
				logger.info("WIHDecommissionCharge ->" + String.valueOf(chargeResponse));
				
				if (StringUtils.isBlank(String.valueOf(chargeResponse))) {
					logger.info("Unable to create Decommission charge");
					throw new NovitasHandlerException();
				}
				
				retval.put("Result", chargeResponse);
			}
			
			manager.completeWorkItem(workItem.getId(), retval);
			
		} catch (Exception e) {
			logger.error("WIHDecommissionCharge --> Failed", e);
			HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(HandlerErrorCode.DP_PORT_DECOMISSING_CHARGE));
		}
	}
		
}
