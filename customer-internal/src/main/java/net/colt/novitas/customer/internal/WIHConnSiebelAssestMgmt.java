package net.colt.novitas.customer.internal;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.HandlerUtils;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;
import com.colt.novitas.response.CloudConnectionResponse;
import com.colt.novitas.response.ConnectionResponse;
import com.colt.novitas.siebel.assests.client.SiebelAssetsClient;
import com.colt.novitas.siebel.assets.request.SiebelAssestRequest;

/**
 * @author DRao This is a WIH file created by Dharma Rao.
 * 
 */
public class WIHConnSiebelAssestMgmt extends BaseWorkItemHandler {

	private static Logger logger = LoggerFactory.getLogger(WIHConnSiebelAssestMgmt.class);

	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {

		try {
			EnvironmentProperties prop = getEnvProperties(workItem);
			logger = getLogger(workItem, prop.getLogUniqueId());
			String status  =getStatus(workItem);
			String connectionType = getConnectionType(workItem);
			
			SiebelAssestRequest request = new SiebelAssestRequest();
			Map <String,String> attributesMap = new HashMap<String, String>();
			request.setAttributesMap(attributesMap);
			request.setPamFlag("Y");
			attributesMap.put("connection_type",connectionType);
		    attributesMap.put("last_updated","");
		    attributesMap.put("created_date",getcurrentTime());
		    attributesMap.put("decommissioned_on",getcurrentTime());
			request.setSlaId("");
			request.setStatus(status);
			if(DCN_CONNECTION.equals(connectionType)){
	        	request.setProductName("COLT On Demand DCNet Connection");
	        }else if(LAN_CONNECTION.equals(connectionType)){
	        	request.setProductName("COLT On Demand LANLink Connection");
	        }else if(DCA_CONNECTION.equals(connectionType)){
	        	request.setProductName("COLT On Demand DCA Connection");
	        }
			
			if(DCN_CONNECTION.equals(connectionType) || LAN_CONNECTION.equals(connectionType)){
			   ConnectionResponse connectionResponse = getStdConnection(workItem);
			   attributesMap.put("from_port_id",connectionResponse.getFromPortId());
	           attributesMap.put("to_port_id",connectionResponse.getToPortId());
	           attributesMap.put("resource_id",connectionResponse.getConnectionId());
	           attributesMap.put("from_uni_type",connectionResponse.getFromVlanMapping().name());
	           attributesMap.put("to_uni_type",connectionResponse.getToVlanMapping().name());
		       attributesMap.put("bandwidth",String.valueOf(connectionResponse.getBandwidth()));
			   request.setResourceId(connectionResponse.getResourceId());
			   request.setServiceId(connectionResponse.getConnectionId());
			   request.setOcn(connectionResponse.getOcn());
			   request.setNovitasServiceId(connectionResponse.getServiceId());
			   
			}else if(DCA_CONNECTION.equals(connectionType)){
				CloudConnectionResponse connectionResponse = getDCAConnection(workItem);
				request.setServiceId(connectionResponse.getConnectionId());
				request.setOcn(connectionResponse.getOcn());
				request.setNovitasServiceId(connectionResponse.getServiceId());
				request.setResourceId(getCircuitId(workItem));
				attributesMap.put("bandwidth",String.valueOf(connectionResponse.getBandwidth()));
	        }
			
			request.setUrlCallback(prop.getCallBackUrl());
			
			String transactionId  = new SiebelAssetsClient(prop.getSiebelUrl(),prop.getSiebelUsername(),
					                                    prop.getSiebelPassword()).receiveAssetMgmtRequest(request);

			Map<String, Object> retval = new HashMap<String, Object>();
			if (StringUtils.isBlank(transactionId)) {
				logger.info("Failed to Update Siebel Assests Management");
				throw new NovitasHandlerException();
			}
			
			retval.put("Result", transactionId);
			manager.completeWorkItem(workItem.getId(), retval);

		} catch (Exception e) {
			logger.error("WIHSiebelAssestMgmt -> Failed", e);
				HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(
						HandlerErrorCode.CREATE_OR_UPDATE_SIEBEL_ASSETS_MGMT));
			
		}
	}
	

}