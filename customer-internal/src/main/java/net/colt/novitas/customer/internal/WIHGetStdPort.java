package net.colt.novitas.customer.internal;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.HandlerUtils;
import com.colt.novitas.client.ServiceAPIClient;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;
import com.colt.novitas.response.CustomerDedicatedPortResponse;

public class WIHGetStdPort extends BaseWorkItemHandler {
	
	private static Logger logger = LoggerFactory.getLogger(WIHGetStdPort.class);
	
	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {

		try {
			EnvironmentProperties prop = getEnvProperties(workItem);
			logger = getLogger(workItem, prop.getLogUniqueId());
			String portId = getServicePortId(workItem);
			
			CustomerDedicatedPortResponse response = new ServiceAPIClient(prop.getServiceAPIBaseUrl(), 
                                                 prop.getServiceAPIUsername(), 
                                                 prop.getServiceAPIPassword()).getPort(portId); 
			
			logger.info("Get customer port call successful :" + response);
			
			Map<String,Object> retval = new HashMap<String,Object>();
			
			if (null != response) {
				
				retval.put("Result", response);
				retval.put("resource_port_id", response.getResourceId());
				retval.put("resource_port_bandwidth", response.getBandwidth());
				retval.put("port_type", response.getPortType());
				retval.put("connections_on_port", response.getNoOfConnections());
				retval.put("port_expiry_period", response.getExpirationPeriod());
				retval.put("has_cross_connect", StringUtils.isNotBlank(response.getCrossConnectId()) || StringUtils.isNotBlank(response.getCrossConnectRequestId()));
			}
			else {
				throw new Exception("Get Standard port call failed :" + portId);
			}
			
			manager.completeWorkItem(workItem.getId(), retval);
			
		} catch (Exception e) {
			logger.error("WIHGetStdPort -> Fail : ", e);
			HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(HandlerErrorCode.CC_GET_SERVICE_PORT));
		}
	}
	
		

}
