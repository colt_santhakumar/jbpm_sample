package net.colt.novitas.customer.internal;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.Protocol;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder.EndpointConfiguration;
import com.amazonaws.services.directconnect.AmazonDirectConnectClient;
import com.amazonaws.services.directconnect.AmazonDirectConnectClientBuilder;
import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.HandlerUtils;
import com.colt.novitas.WorkflowType;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.resource.client.ResourceAPIClient;
import com.colt.novitas.response.CloudConnectionResponse;
import com.colt.novitas.response.CloudPortResponse;
import com.colt.novitas.response.ConnectionPortPair;
import com.colt.novitas.response.ConnectionResponse;
import com.colt.novitas.response.CustomerDedicatedPortResponse;

public abstract class BaseWorkItemHandler implements WorkItemHandler {

	private static Logger logger = LoggerFactory.getLogger(BaseWorkItemHandler.class);

	protected static final String INPROGRESS = "INPROGRESS";
	protected static final String PROVISIONING = "PROVISIONING";
	protected static final String PROVISIONED = "PROVISIONED";
	protected static final String DEPROVISION = "DEPROVISION";

	protected static final String DCA_CREATE_CONN_SIGNAL_1 = "create_connection_signal_1";
	protected static final String DCA_CREATE_CONN_SIGNAL_2 = "create_connection_signal_2";
	protected static final String DCA_CONN_CREATE_SIEBEL_SIGNAL_1 = "create_connection_siebel_signal_1";
	protected static final String DCA_CONN_CREATE_SIEBEL_SIGNAL_2 = "create_connection_siebel_signal_2";
	protected static final String DCA_CONN_MODIFY_SIEBEL_SIGNAL = "modify_dca_conn_siebel_signal";
	protected static final String DCA_CONN_DELETE_SIEBEL_SIGNAL = "delete_dca_conn_siebel_signal";

	protected static final String DCA_CONN_CREATE_SMART_SIGNAL_1 = "create_conn_smart_signal_1";
	protected static final String DCA_CONN_CREATE_SMART_SIGNAL_2 = "create_conn_smart_signal_1";
	protected static final String DCA_CONN_DELETE_SMART_SIGNAL_1 = "delete_dca_conn_smart_signal_1";
	protected static final String DCA_CONN_DELETE_SMART_SIGNAL_2 = "delete_dca_conn_smart_signal_2";
	protected static final String DCA_CONNECTION = "DCACONNECTION";
	protected static final String DCN_CONNECTION = "DCNCONNECTION";
	protected static final String LAN_CONNECTION = "LANCONNECTION";
	protected static final String DCA_PORT = "DCAPORT";
	protected static final String DCN_PORT = "DCNPORT";
	protected static final String LAN_PORT = "LANPORT";
	protected static String LOG_UNIQUE_ID = null;

	protected EnvironmentProperties getEnvProperties(WorkItem workItem) {
		return (EnvironmentProperties) workItem.getParameter("env_properties");
	}

	protected Logger getLogger(WorkItem workItem, String uniqueLogId) {
		if (null == LOG_UNIQUE_ID) {
			LOG_UNIQUE_ID = uniqueLogId;
			MDC.put("unique_log_id", "J" + uniqueLogId.substring(1));
		}
		logger = LoggerFactory
				.getLogger(uniqueLogId + "|ProcessId-" + workItem.getProcessInstanceId() + "|" + workItem.getName());
		return logger;
	}

	protected String getRequestId(WorkItem workItem) {
		return String.valueOf(workItem.getParameter("request_id"));
	}

	protected String getPortType(WorkItem workItem) {
		return String.valueOf(workItem.getParameter("port_type"));
	}

	protected String getCustomerId(WorkItem workItem) {
		return String.valueOf(workItem.getParameter("customer_id"));
	}

	protected String getTransactionId(WorkItem workItem) {
		return String.valueOf(workItem.getParameter("transaction_id"));
	}

	protected String getSignalName(WorkItem workItem) {
		return String.valueOf(workItem.getParameter("signal_name"));
	}

	protected String[] getSignalData(WorkItem workItem) {
		return (String[]) workItem.getParameter("received_signal_data");
	}

	protected String getStatus(WorkItem workItem) {
		return String.valueOf(workItem.getParameter("status"));
	}

	protected String getCustomerName(WorkItem workItem) {
		return String.valueOf(workItem.getParameter("customer_name"));
	}

	protected String getOCN(WorkItem workItem) {
		return String.valueOf(workItem.getParameter("ocn"));
	}

	protected String getParentServiceId(WorkItem workItem) {
		return String.valueOf(workItem.getParameter("novitas_service_id"));
	}

	protected String getNovitasPortServiceId(WorkItem workItem) {
		return String.valueOf(workItem.getParameter("service_port_id"));
	}

	protected String getProcessDefId(WorkItem workItem) {
		return String.valueOf(workItem.getParameter("process_def_id"));
	}

	protected Integer getStatusCode(WorkItem workItem) {
		return Integer.valueOf(String.valueOf(workItem.getParameter("status_code")));
	}

	protected Exception getWorkflowError(WorkItem workItem) {
		Object obj = workItem.getParameter("error_object");
		if (obj instanceof Exception) {
			return (Exception) obj;
		}
		return null;
	}

	protected String getBcn(WorkItem workItem) {
		return String.valueOf(workItem.getParameter("bcn"));
	}

	protected HandlerErrorCode getErrorCode(WorkItem workItem) {
		WorkflowType type = HandlerUtils.getWorkflowType(workItem);
		if (type == WorkflowType.DELETE_PORT) {
			return HandlerErrorCode.DP_UPADTE_REQUEST_INPROGRESS;
		}
		return HandlerErrorCode.CP_UPADTE_REQUEST_INPROGRESS;
	}

	protected ConnectionResponse getStdConnection(WorkItem workItem) {
		return (ConnectionResponse) workItem.getParameter("std_connection_obj");
	}

	protected CloudConnectionResponse getDCAConnection(WorkItem workItem) {
		return (CloudConnectionResponse) workItem.getParameter("dca_connection_obj");
	}

	protected CustomerDedicatedPortResponse getStdPort(WorkItem workItem) {
		return (CustomerDedicatedPortResponse) workItem.getParameter("std_port_obj");
	}

	protected CloudPortResponse getDCAPort(WorkItem workItem) {
		return (CloudPortResponse) workItem.getParameter("dca_port_obj");
	}

	protected ConnectionPortPair getConnectionPortPair(WorkItem workItem) {
		return (ConnectionPortPair) workItem.getParameter("connection_port_pair");
	}

	protected String getConnectionType(WorkItem workItem) {

		return String.valueOf(workItem.getParameter("connection_type"));
	}

	protected String getServiceConnectionId(WorkItem workItem) {
		return String.valueOf(workItem.getParameter("service_connection_id"));
	}

	protected String getServicePortId(WorkItem workItem) {
		return String.valueOf(workItem.getParameter("service_port_id"));
	}

	protected Object getPenaltyCharge(WorkItem workItem) {
		return workItem.getParameter("penalty_charge");
	}

	protected String getCircuitId(WorkItem workItem) {
		return String.valueOf(workItem.getParameter("circuit_id"));
	}

	protected String getBandwidth(WorkItem workItem) {
		return String.valueOf(workItem.getParameter("bandwidth"));
	}

	protected String getcurrentTime() {
		DateFormat fdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
		fdf.setTimeZone(TimeZone.getTimeZone("UTC"));
		Calendar c = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		return fdf.format(c.getTime());
	}

	protected String getEvcId(WorkItem workItem) {
		return String.valueOf(workItem.getParameter("evc_id"));
	}

	protected String getTargetAction(WorkItem workItem) {
		return String.valueOf(workItem.getParameter("target_action"));
	}

	protected String getChargeType(WorkItem workItem) {
		return String.valueOf(workItem.getParameter("charge_type"));
	}

	protected String getOperationType(WorkItem workItem) {
		return String.valueOf(workItem.getParameter("operation_type"));
	}

	protected Integer getIntValue(Object val) {
		if (val instanceof Integer) {
			logger.info("Integer value : " + val);
			return (Integer) val;
		}

		logger.info("String value : " + Integer.valueOf(String.valueOf(val)));
		return Integer.valueOf(String.valueOf(val));
	}

	protected String getPortExpiryDate(Integer commitmentPeriod) {

		DateFormat fdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
		fdf.setTimeZone(TimeZone.getTimeZone("UTC"));
		Calendar c = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		c.add(Calendar.DATE, commitmentPeriod);
		// c.set(Calendar.HOUR_OF_DAY,c.getActualMaximum(Calendar.HOUR_OF_DAY));
		// c.set(Calendar.MINUTE, c.getActualMaximum(Calendar.MINUTE));
		// c.set(Calendar.SECOND, c.getActualMaximum(Calendar.SECOND));

		return fdf.format(c.getTime());

	}

	protected String getJbpmUrl() {
		String url = "http://%s:8080/business-central";
		try {
			String host = InetAddress.getLocalHost().getHostName();
			url = url.replaceAll("%s", host);
		} catch (UnknownHostException e) {
			logger.error(e.getMessage());
		}
		return url;
	}

	protected Integer getBlueplanetWorkflowId(WorkItem workItem) {
		return getIntValue(workItem.getParameter("blueplanet_workflow_id"));
	}

	protected String[] getVarName(WorkItem workItem) {
		String vars = String.valueOf(workItem.getParameter("var_name"));
		if (vars == null || "".equals(vars)) {
			return null;
		}
		return vars.split(",");
	}

	protected String getResourcePortId(WorkItem workItem) {
		return String.valueOf(workItem.getParameter("resource_port_id"));
	}

	protected String getJbpmUrl(String url) {
		try {
			String host = InetAddress.getLocalHost().getHostName();
			url = url.replaceAll("%s", host);
		} catch (UnknownHostException e) {
			logger.error(e.getMessage());
		}
		return url;
	}

	protected AmazonDirectConnectClient getAmazonDirectConnectClient(String region, EnvironmentProperties prop) {
		ClientConfiguration config = new ClientConfiguration();
		ResourceAPIClient conf = new ResourceAPIClient(prop.getConfigServiceUrl());
		String proxyHost = conf.getProperty(prop.getEnvironment(), "redirect.proxy.host").getValue();
		int proxyPort = Integer.valueOf(conf.getProperty(prop.getEnvironment(), "redirect.proxy.port").getValue());
		config.setProtocol(Protocol.HTTPS);
		config.setProxyHost(proxyHost);
		config.setProxyPort(proxyPort);
		logger.info("AWS Connection time out |" + prop.getAwsConnectionTimeOut());
		config.setConnectionTimeout(prop.getAwsConnectionTimeOut());
		config.setSocketTimeout(prop.getAwsConnectionTimeOut());
		BasicAWSCredentials basic = new BasicAWSCredentials(prop.getAwsAccessKey(), prop.getAwsSecretKey());
		String endPoint = conf.getProperty(prop.getEnvironment(), "aws.endpoint." + region).getValue();
		EndpointConfiguration endpointConfiguration = new EndpointConfiguration(endPoint, region);
		AmazonDirectConnectClientBuilder clientBuilder = AmazonDirectConnectClientBuilder.standard()
				.withClientConfiguration(config)
				// .withRegion(region)
				.withEndpointConfiguration(endpointConfiguration)
				.withCredentials(new AWSStaticCredentialsProvider(basic));
		// clientBuilder.setClientConfiguration(config);
		// clientBuilder.setRegion(region);
		// clientBuilder.setCredentials(new AWSStaticCredentialsProvider(basic));

		// clientBuilder.setEndpointConfiguration(endpointConfiguration);
		AmazonDirectConnectClient client = (AmazonDirectConnectClient) clientBuilder.build();
		logger.info("AWS enpoint for region |" + region + "|" + endPoint);
		return client;
	}

	protected String getAwshConnectionId(WorkItem workItem) {
		return String.valueOf(workItem.getParameter("awsh_connection_id"));
	}

	protected String getAWSRegion(WorkItem workItem) {
		String reg = String.valueOf(workItem.getParameter("aws_region"));
		/*Regions[] regions = Regions.values();
		for (Regions regions2 : regions) {
			if (regions2.getName().equals(reg) || regions2.name().equals(reg)) {
				return regions2;
			}
		}*/
		return reg;
	}

	protected Integer getPortalUserId(WorkItem workItem) {
		return (Integer) workItem.getParameter("portal_user_id");
	}

	protected String getNms(WorkItem workItem) {
		return (String) workItem.getParameter("nms");
	}

}
