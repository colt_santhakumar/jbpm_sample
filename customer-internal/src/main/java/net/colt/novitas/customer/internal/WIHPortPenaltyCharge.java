package net.colt.novitas.customer.internal;


import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.HandlerUtils;
import com.colt.novitas.billing.client.BillingAPIClient;
import com.colt.novitas.billing.client.request.OneOffChargeRequest;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;
import com.colt.novitas.response.CloudPortResponse;
import com.colt.novitas.response.CustomerDedicatedPortResponse;

public class WIHPortPenaltyCharge extends BaseWorkItemHandler {
	
	private static Logger logger = LoggerFactory.getLogger(WIHPortPenaltyCharge.class);
	
	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}
	

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
		
		try {
			EnvironmentProperties prop = getEnvProperties(workItem);
			logger = getLogger(workItem, prop.getLogUniqueId());
			Map<String,Object> retval = new HashMap<String,Object>();
			OneOffChargeRequest oneoff = new OneOffChargeRequest();
			oneoff.setRequestId(-1);
			String portType = getPortType(workItem);
			oneoff.setServiceInstanceType(portType);
			oneoff.setBcn(getBcn(workItem));
			oneoff.setChargeType("PENALTY");
			if(DCA_PORT.equals(portType)){
			    CloudPortResponse portRequest = getDCAPort(workItem);
   			    oneoff.setServiceId(portRequest.getServiceId());
				oneoff.setServiceInstanceId(portRequest.getServiceId());
				oneoff.setDescription("Penalty for port " + portRequest.getName());
				oneoff.setCurrency(portRequest.getRentalCurrency());
				oneoff.setAmount(portRequest.getPenaltyCharge());
				oneoff.setServiceInstanceName(portRequest.getName());
			}else if(DCN_PORT.equals(portType) ||LAN_PORT.equals(portType)){
				CustomerDedicatedPortResponse portRequest = getStdPort(workItem);
	   			    oneoff.setServiceId(portRequest.getServiceId());
					oneoff.setServiceInstanceId(portRequest.getServiceId());
					oneoff.setDescription("Penalty for port " + portRequest.getName());
					oneoff.setCurrency(portRequest.getRentalCurrency());
					oneoff.setAmount(portRequest.getPenaltyCharge());
					oneoff.setServiceInstanceName(portRequest.getName());
			}
			
			if (oneoff.getAmount() != null && oneoff.getAmount() > 0) {
				
				Object chargeResponse = new BillingAPIClient(prop.getBillingAPIOneOffUrl(),
						                prop.getBillingAPIRecurrUrl(), 
						                prop.getBillingAPIUsername(),
						                prop.getBillingAPIPassword()).createOneOffCharges(oneoff);
				
				logger.info("WIHDecommissionCharge ->" + String.valueOf(chargeResponse));
				
				if (StringUtils.isBlank(String.valueOf(chargeResponse))) {
					logger.info("Unable to create penalty charge");
					throw new NovitasHandlerException();
				}
				
				retval.put("Result", chargeResponse);
			}
			
			manager.completeWorkItem(workItem.getId(), retval);
			
		} catch (Exception e) {
			logger.error("WIHPenaltyCharge -> Failed.", e);
			HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(HandlerErrorCode.DP_PORT_PENALTY_CHARGE));
		}
	}
		
	
}
