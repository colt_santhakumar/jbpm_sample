package net.colt.novitas.customer.internal;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.httpclient.NoHttpResponseException;
import org.apache.commons.lang.StringUtils;
import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.HandlerUtils;
import com.colt.novitas.billing.client.BillingAPIClient;
import com.colt.novitas.billing.client.request.OneOffChargeRequest;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;
import com.colt.novitas.response.CloudConnectionResponse;
import com.colt.novitas.response.ConnectionResponse;

public class WIHPenaltyCharge extends BaseWorkItemHandler {
	
	private static Logger logger = LoggerFactory.getLogger(WIHPenaltyCharge.class);
	
	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}
	

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
		
		
		try {
			EnvironmentProperties prop = getEnvProperties(workItem);
			logger = getLogger(workItem, prop.getLogUniqueId());
			String connectionType = getConnectionType(workItem);
			String bcn = getBcn(workItem);
			OneOffChargeRequest oneoff = new OneOffChargeRequest();
			if(DCN_CONNECTION.equals(connectionType) || LAN_CONNECTION.equals(connectionType)){
				ConnectionResponse response = getStdConnection(workItem);
				oneoff.setRequestId(-1);
				oneoff.setServiceId(response.getServiceId());
				oneoff.setServiceInstanceId(response.getConnectionId());
				oneoff.setServiceInstanceType(connectionType);
				oneoff.setBcn(bcn);
				oneoff.setChargeType("PENALTY");
				oneoff.setDescription( "PENALTY for connection " + response.getName());
				oneoff.setCurrency(response.getRentalCurrency());
				oneoff.setServiceInstanceName(response.getName());
				oneoff.setAmount(null !=response.getPenaltyCharge() ? (Float)response.getPenaltyCharge() : null);
				
			}else if(DCA_CONNECTION.equals(connectionType)){
				CloudConnectionResponse response = getDCAConnection(workItem);
				oneoff.setRequestId(-1);
				oneoff.setServiceId(response.getServiceId());
				oneoff.setServiceInstanceId(response.getConnectionId());
				oneoff.setServiceInstanceType(DCA_CONNECTION);
				oneoff.setBcn(bcn);
				oneoff.setChargeType("PENALTY");
				oneoff.setDescription( "PENALTY for connection " + response.getName());
				oneoff.setCurrency(response.getRentalCurrency());
				oneoff.setServiceInstanceName(response.getName());
				oneoff.setAmount(null !=response.getPenaltyCharge() ? (Float)response.getPenaltyCharge() : null);
			}

			Object chargeResponse = new BillingAPIClient(
					prop.getBillingAPIOneOffUrl(),
					prop.getBillingAPIRecurrUrl(), 
					prop.getBillingAPIUsername(),
					prop.getBillingAPIPassword()).createOneOffCharges(oneoff);
			
			logger.info(String.valueOf(chargeResponse));
			
			if (StringUtils.isBlank(String.valueOf(chargeResponse))) {
				throw new NoHttpResponseException();
			}
			
			Map<String,Object> retval = new HashMap<String,Object>();
			retval.put("Result", chargeResponse);
			manager.completeWorkItem(workItem.getId(), retval);
			
		} catch (Exception e) {
			logger.error("WIHInstallationCharge -> Failed", e);
			HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(HandlerErrorCode.CC_INSTALLATION_CHARGE));
		}
	}
	

}
	
