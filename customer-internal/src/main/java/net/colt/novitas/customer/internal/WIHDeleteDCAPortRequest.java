package net.colt.novitas.customer.internal;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.HandlerUtils;
import com.colt.novitas.client.RequestAPIClient;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;
import com.colt.novitas.request.DeleteRequest;

public class WIHDeleteDCAPortRequest extends BaseWorkItemHandler {
	
	private static Logger logger = LoggerFactory.getLogger(WIHDeleteDCAPortRequest.class);
	
	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {

		try {
			EnvironmentProperties prop = getEnvProperties(workItem);
			logger = getLogger(workItem, prop.getLogUniqueId());
			String portId = getServicePortId(workItem);
			String customerId = getCustomerId(workItem);
			String bcn = getBcn(workItem);
			//Integer portalUserId = getPortalUserId(workItem);
			Map<String,Object> retval = new HashMap<String,Object>();
			DeleteRequest req = new DeleteRequest();
			req.setBcn(bcn);
			req.setPortalUserId(-9998);
			req.setCronExecution("NEVER");
			RequestAPIClient client = new RequestAPIClient(prop.getRequestAPIUrl(), 
										prop.getRequestAPIUsername(), 
										prop.getRequestAPIPassword());
			Integer result = Integer.valueOf(client.deleteCloudPort(portId,Integer.valueOf(customerId),req));
			
			logger.info("Delete CloudPort Request ID|" + result);
			if (result > 0 ) {
				retval.put("Result", result);
			}else {
				throw new Exception("Failed to create Delete CloudPort Request|" + portId);
			}
			manager.completeWorkItem(workItem.getId(), retval);
			
		} catch (Exception e) {
			logger.error("WIHDeleteDCAPortRequest -> Failed :: ", e);
			HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(HandlerErrorCode.DP_INVALID_REQUEST));
		}
	}

	
	
	

}
