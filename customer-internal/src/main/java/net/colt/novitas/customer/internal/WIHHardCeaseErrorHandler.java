package net.colt.novitas.customer.internal;

import java.util.HashMap;
import java.util.Map;

import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;

public class WIHHardCeaseErrorHandler extends BaseWorkItemHandler {
	
	private WorkItem workItem;
	private EnvironmentProperties prop;
	
	private static Logger logger = LoggerFactory.getLogger(WIHHardCeaseErrorHandler.class);

	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {

		 Map<String,Object> retval = new HashMap<String,Object>();
			try {
				this.workItem = workItem;
				this.prop = getEnvProperties(workItem);
				logger = getLogger(workItem, prop.getLogUniqueId());
				logger.info("Error handling started for request. ID : "+prop.getLogUniqueId() +"-----"+prop.getPortOrConnId() );
				
				//Get error details
				HandlerErrorCode errorCode = null;
				Exception exception = getWorkflowError(workItem);
				if (exception instanceof NovitasHandlerException) {
					errorCode = ((NovitasHandlerException) exception).getErrorCode();
					logger.info("Error code : " + errorCode);
				}

				if (errorCode == null) {
					logger.info("NovitasHandlerException thrown without error code. Unable to process further.");
					retval.put("Result", "NovitasHandlerException thrown without error code. Unable to process further.");
				}

				if (errorCode != null) {
					retval.put("Result", errorCode.getDescription());
				}
				
			} catch (Exception e) {
				logger.info("Error handling failed for request. ID : "+prop.getLogUniqueId() +"-----"+prop.getPortOrConnId());
				manager.abortWorkItem(workItem.getId());
			}
			 logger.info("Error handling completed for request : ");
			 manager.completeWorkItem(workItem.getId(), retval);
		}

}
