package net.colt.novitas.customer.internal;

import java.util.HashMap;
import java.util.Map;

import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.HandlerUtils;
import com.colt.novitas.client.ServiceAPIClient;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;
import com.colt.novitas.response.CloudPortResponse;

public class WIHGetDCAPort extends BaseWorkItemHandler {
	
	private static Logger logger = LoggerFactory.getLogger(WIHGetDCAPort.class);
	
	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {

		try {
			EnvironmentProperties prop = getEnvProperties(workItem);
			logger = getLogger(workItem, prop.getLogUniqueId());
			String portId = getServicePortId(workItem);
			Map<String,Object> retval = new HashMap<String,Object>();
			CloudPortResponse result = new ServiceAPIClient(prop.getServiceAPIBaseUrl(), 
										prop.getServiceAPIUsername(), 
										prop.getServiceAPIPassword()).getCloudPort(portId);
			
			logger.info("Get Cloud Port call response :: " + result);
			if (null != result ) {
				retval.put("Result", result);
				retval.put("resource_port_id_1", result.getResourceId1());
				retval.put("resource_port_id_2", result.getResourceId2());
				retval.put("service_key", result.getServiceKey());
				retval.put("vlan_id", result.getVlan());
				retval.put("csp_name", result.getCloudProvider());
				retval.put("aws_region", result.getRegion());
				retval.put("awsh_connection_id", result.getAwsConnId());
				retval.put("nni_circuit_id", result.getNniCircuitId());
			}else {
				throw new Exception("Get DCA port call failed :" + portId);
			}
			manager.completeWorkItem(workItem.getId(), retval);
			
		} catch (Exception e) {
			logger.error("Get DCA Service Port -> Failed :: ", e);
			HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(HandlerErrorCode.CC_GET_SERVICE_PORT));
		}
	}
	
	
	

}
