package net.colt.novitas.customer.internal;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.HandlerUtils;
import com.colt.novitas.client.CallBackAPIClient;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;
import com.colt.novitas.request.CallBack;

/**
 * @author DRao This is a WIH file created by Dharrma Rao.
 * 
 */
public class WIHUpdateCallBackStatus extends BaseWorkItemHandler {

	private static Logger logger = LoggerFactory
			.getLogger(WIHUpdateCallBackStatus.class);

	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
		String signalName = null;
		try {
			EnvironmentProperties prop = getEnvProperties(workItem);
			logger = getLogger(workItem, prop.getLogUniqueId());
			String[] signalData = getSignalData(workItem);
            signalName = getSignalName(workItem);
			CallBack callBack = new CallBack();

			callBack.setTransactionId(signalData[0]);
			callBack.setProcessInstanceId(workItem.getProcessInstanceId());
			callBack.setStatus(signalData[1]);
			callBack.setSignalName(signalName);
            callBack.setStatusDescription(signalData[2]);
			
			CallBackAPIClient callBackAPIClient = new CallBackAPIClient(prop.getInboundRestApiUrl(),prop.getInboundRestApiUserName(),prop.getInboundRestApiPassword());


			callBackAPIClient.updateStatus(callBack);

			if (StringUtils.isBlank(signalData[1]) || !"SUCCESS".equals(signalData[1])) {
				logger.info(signalName +" callback response either null or not SUCCESS");
				throw new NovitasHandlerException();
			}
			logger.info(signalName +" callback response SUCCESS -->"+callBack.getTransactionId());
			Map<String, Object> retval = new HashMap<String, Object>();
			manager.completeWorkItem(workItem.getId(), retval);

		} catch (Exception e) {
			logger.error("WIHUpdateCallBackStatus -> Failed", e.getMessage());
			if(StringUtils.isNotBlank(signalName)){
				if(DCA_CREATE_CONN_SIGNAL_1.equals(signalName) || DCA_CREATE_CONN_SIGNAL_2.equals(signalName)){
					HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(
							HandlerErrorCode.CREATE_CONNECTION_CALL_BACK_STATUS));
				}else if(DCA_CONN_CREATE_SIEBEL_SIGNAL_1.equals(signalName) || DCA_CONN_CREATE_SIEBEL_SIGNAL_2.equals(signalName) ||
						DCA_CONN_MODIFY_SIEBEL_SIGNAL.equals(signalName) || DCA_CONN_DELETE_SIEBEL_SIGNAL.equals(signalName)){
					HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(
							HandlerErrorCode.CONNECTION_SIEBEL_ASSETS_CALL_BACK_STATUS));
				}else if(DCA_CONN_CREATE_SMART_SIGNAL_1.equals(signalName) || DCA_CONN_CREATE_SMART_SIGNAL_2.equals(signalName) ||
						DCA_CONN_DELETE_SMART_SIGNAL_1.equals(signalName) || DCA_CONN_DELETE_SMART_SIGNAL_2.equals(signalName)){
					HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(
							HandlerErrorCode.CONNECTION_SMARTS_CALL_BACK_STATUS));
				}
			}else{
				HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(
						HandlerErrorCode.UPDATE_STATUS_CB_DETAILS));
			}
		}
	}

}
