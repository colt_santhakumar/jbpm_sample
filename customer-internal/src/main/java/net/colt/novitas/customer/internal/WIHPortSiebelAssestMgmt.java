package net.colt.novitas.customer.internal;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.HandlerUtils;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;
import com.colt.novitas.response.CloudPortResponse;
import com.colt.novitas.response.CustomerDedicatedPortResponse;
import com.colt.novitas.siebel.assests.client.SiebelAssetsClient;
import com.colt.novitas.siebel.assets.request.SiebelAssestRequest;
import com.colt.novitas.siebel.assets.request.SlaIdType;

/**
 * @author DRao This is a WIH file created by Dharrma Rao.
 * 
 */
public class WIHPortSiebelAssestMgmt extends BaseWorkItemHandler {

	private static Logger logger = LoggerFactory.getLogger(WIHPortSiebelAssestMgmt.class);

	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {

		try {
			EnvironmentProperties prop = getEnvProperties(workItem);
			logger = getLogger(workItem, prop.getLogUniqueId());
			String portType = getPortType(workItem);
			String status = getStatus(workItem);
			
			SiebelAssestRequest request = new SiebelAssestRequest();
			Map<String, String> attibutesMap = new HashMap<String, String>();
			request.setPamFlag("Y");
			request.setSlaId(SlaIdType.SLA_01.value());
			attibutesMap.put("address", "");
			attibutesMap.put("decommissioned_on", new Date().toString());
			
			request.setStatus(status);
			
			if("DCNPORT".equals(portType) || "LANPORT".equals(portType)){
				CustomerDedicatedPortResponse resp = getStdPort(workItem);
				attibutesMap.put("technology", resp.getTechnology());
				request.setServiceId(resp.getId());
				request.setOcn(resp.getOcn());
				request.setNovitasServiceId(resp.getServiceId());
				attibutesMap.put("bandwidth", String.valueOf(resp.getBandwidth()));
				attibutesMap.put("commitment_expiry", resp.getCommitmentExpiry());
				attibutesMap.put("created_date", getcurrentTime());
				request.setProductName("COLT On Demand DCNet Port");
			}else {
				CloudPortResponse portResponse = getDCAPort(workItem);
				request.setServiceId(portResponse.getId());
				request.setOcn(portResponse.getOcn());
				request.setNovitasServiceId(portResponse.getServiceId());
				request.setResourceId(String.valueOf(portResponse.getVlan()));
				attibutesMap.put("commitment_expiry", portResponse.getCommitmentExpiry());
				attibutesMap.put("created_date", getcurrentTime());
				attibutesMap.put("bandwidth", String.valueOf(portResponse.getBandwidth()));
				if ("LANLINKPORT".equals(portType)){
					request.setProductName("COLT On Demand LANLink Port");
				}else if (DCA_PORT.equals(portType)){
					request.setProductName("COLT On Demand DCA Port");
				}
			}
			
			//attibutesMap.put("decommissioning_charge", portRequestResponse.getDecommissioningCharge().toString());
			//attibutesMap.put("decommissioning_currency", portRequestResponse.getDecommissioningCurrency());
			request.setAttributesMap(attibutesMap);
			request.setUrlCallback(prop.getCallBackUrl());
			
			String transactionId  = new SiebelAssetsClient(prop.getSiebelUrl(),prop.getSiebelUsername(),
					                                    prop.getSiebelPassword()).receiveAssetMgmtRequest(request);

			Map<String, Object> retval = new HashMap<String, Object>();
			if (StringUtils.isBlank(transactionId)) {
				logger.info("Failed to Update Siebel Assests Management");
				throw new NovitasHandlerException();
			}
			
			retval.put("Result", transactionId);
			manager.completeWorkItem(workItem.getId(), retval);

		} catch (Exception e) {
			logger.error("WIHSiebelAssestMgmt -> Failed", e);
				HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(
						HandlerErrorCode.CREATE_OR_UPDATE_SIEBEL_ASSETS_MGMT));
			
		}
	}
	
	
	

}