package net.colt.novitas.customer.internal;

import java.util.HashMap;
import java.util.Map;

import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.HandlerUtils;
import com.colt.novitas.client.ServiceAPIClient;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;
import com.colt.novitas.request.UpdateCloudPortRequest;
import com.colt.novitas.request.UpdatePortRequest;

public class WIHUpdateServicePort extends BaseWorkItemHandler {
	
	private static Logger logger = LoggerFactory.getLogger(WIHUpdateServicePort.class);
	
	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}
	

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
		
		try {
			EnvironmentProperties prop = getEnvProperties(workItem);
			logger = getLogger(workItem, prop.getLogUniqueId());
			String dedicatedPortId = getNovitasPortServiceId(workItem);
			String portType = getPortType(workItem);
			ServiceAPIClient client = new ServiceAPIClient(prop.getServiceAPIBaseUrl(), 
												   prop.getServiceAPIUsername(), 
												   prop.getServiceAPIPassword());
			Object response = null;
			if(DCA_PORT.equals(portType) || "AWS_HOSTED".equals(portType) || "AZURE_EXPRESS".equals(portType)){
				 UpdateCloudPortRequest req = new UpdateCloudPortRequest();
					req.setStatus(getStatus(workItem));
					response = client.updateCloudPort(dedicatedPortId, req);
			}else if(DCN_PORT.equals(portType) || LAN_PORT.equals(portType)){
				UpdatePortRequest req = new  UpdatePortRequest();
				req.setStatus(getStatus(workItem));
				response = client.updatePort(dedicatedPortId, req);	
			}
			
			if ("FAILED".equals(response)) {
				logger.info("Port status update failed. Port : " + dedicatedPortId);
				throw new NovitasHandlerException();
			}
			
			Map<String,Object> retval = new HashMap<String,Object>();
			manager.completeWorkItem(workItem.getId(), retval);
			
		} catch (Exception e) {
			logger.error("WIHUpdateServicePort -> Failed", e);
			if ("DECOMMISSIONED".equals(getStatus(workItem))) {
				HandlerUtils.throwHandlerExcetpion(
						new NovitasHandlerException(HandlerErrorCode.DP_UPDATE_SERVICE_PORT_DECOMMISSIONED));
			}else if ("DECOMMISSIONING".equals(getStatus(workItem))) {
				HandlerUtils.throwHandlerExcetpion(
						new NovitasHandlerException(HandlerErrorCode.DP_UPDATE_SERVICE_PORT_DECOMMISSIONING));
			}else if("ACTIVE".equals(getStatus(workItem))) {
				HandlerUtils.throwHandlerExcetpion(
						new NovitasHandlerException(HandlerErrorCode.DP_UPADTE_RESOURCE_PORT_AVAILABLE));
		    }
		}
	}
	
}
