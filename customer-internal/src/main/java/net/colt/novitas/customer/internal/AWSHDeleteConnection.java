package net.colt.novitas.customer.internal;

import java.util.HashMap;
import java.util.Map;

import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.services.directconnect.model.DeleteConnectionRequest;
import com.amazonaws.services.directconnect.model.DeleteConnectionResult;
import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.HandlerUtils;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;

public class AWSHDeleteConnection extends BaseWorkItemHandler{

	private static Logger logger = LoggerFactory.getLogger(AWSHDeleteConnection.class);

	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
		EnvironmentProperties prop = HandlerUtils.getEnvironmentProperties(workItem);
		logger = getLogger(workItem, prop.getLogUniqueId());
		try {
			
			String awsConnectionId = getAwshConnectionId(workItem);
			String region = getAWSRegion(workItem); 
			DeleteConnectionRequest req = new DeleteConnectionRequest();
			req.setConnectionId(awsConnectionId);
			DeleteConnectionResult res = null;
			if(!prop.isAWSMocked()){
				logger.info("Deleting AWS Hosted Connection|"+awsConnectionId);
				res = getAmazonDirectConnectClient(region, prop).deleteConnection(req);
			}else{
				logger.info("Mock AWS Hosted Delete Connection|"+awsConnectionId);
				res = new DeleteConnectionResult();
				res.setConnectionId(awsConnectionId);
				res.setRegion(region);
				res.setConnectionState("deleted");
			}
			
			Map<String, Object> retval = new HashMap<String, Object>();
					
			retval.put("Result", res);
			retval.put("aws_conn_status", res.getConnectionState());
			manager.completeWorkItem(workItem.getId(), retval);

		} catch (Exception e) {
			logger.error("AWSHDeleteConnection -> Failed", e);
				HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(
						HandlerErrorCode.CP_AWSH_DELET_PORT));
			
		}
	}

}
