package net.colt.novitas.customer.internal;

import java.util.HashMap;
import java.util.Map;

import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.HandlerUtils;
import com.colt.novitas.client.ServiceAPIClient;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;
import com.colt.novitas.request.UpdateCloudConnRequest;
import com.colt.novitas.request.UpdateConnRequest;

public class WIHUpdateCustomerConnection extends BaseWorkItemHandler {
	
	private static Logger logger = LoggerFactory.getLogger(WIHUpdateCustomerConnection.class);
	
	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}
	

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
		
		String status = null;
		
		try {
			EnvironmentProperties prop = getEnvProperties(workItem);
			logger = getLogger(workItem, prop.getLogUniqueId());
			String connectionId =  getServiceConnectionId(workItem);
			status = getStatus(workItem);
			String connectionType = getConnectionType(workItem);
			ServiceAPIClient client = new ServiceAPIClient(prop.getServiceAPIBaseUrl(), 
                                                 prop.getServiceAPIUsername(), 
                                                 prop.getServiceAPIPassword());
			Object result = null;
			if(DCN_CONNECTION.equals(connectionType) || LAN_CONNECTION.equals(connectionType)){
				UpdateConnRequest connReq = new UpdateConnRequest();
				connReq.setStatus(status);
				result = client.updateConnection(connectionId, connReq );
			}else if(DCA_CONNECTION.equals(connectionType)){
				UpdateCloudConnRequest connReq = new UpdateCloudConnRequest();
				connReq.setStatus(status);
				result = client.updateCloudConnection(connectionId, connReq );
			}
			
			logger.info("Results of update service conn : "  + result);
			if (result != null && "FAILED".equals(String.valueOf(result))) {
				throw new NovitasHandlerException();
			}
			
			Map<String,Object> retval = new HashMap<String,Object>();
			manager.completeWorkItem(workItem.getId(), retval);
			
		} catch (Exception e) {
			logger.error("WIHUpdateCustomerConnection - >  failed ", e);
			if ("ACTIVE".equals(status)) {
				HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(HandlerErrorCode.CC_UPDATE_SERVICE_ACTIVE_CONN_ID));
			}
			else if ("MODIFYING".equals(status)) {
				HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(HandlerErrorCode.MC_UPDATE_SERVICE_MODIFYING));
			}
			else if ("DECOMMISSIONING".equals(status)) {
				HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(HandlerErrorCode.DC_UPDATE_SERVICE_DECOMMISSIONING));
			}
			else if ("DECOMMISSIONED".equals(status)) {
				HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(HandlerErrorCode.DC_UPDATE_SERVICE_DECOMMISSIONED));
			}
		}
	}

}
