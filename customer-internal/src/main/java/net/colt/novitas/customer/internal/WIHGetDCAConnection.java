package net.colt.novitas.customer.internal;

import java.util.HashMap;
import java.util.Map;

import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.HandlerUtils;
import com.colt.novitas.client.ServiceAPIClient;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;
import com.colt.novitas.response.CloudConnectionResponse;

public class WIHGetDCAConnection extends BaseWorkItemHandler {
	
	private static Logger logger = LoggerFactory.getLogger(WIHGetDCAConnection.class);
	
	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}
	

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
		
		try {
			EnvironmentProperties prop = getEnvProperties(workItem);
			logger = getLogger(workItem, prop.getLogUniqueId());
			String connctionId = getServiceConnectionId(workItem);
			
			CloudConnectionResponse response = new ServiceAPIClient(prop.getServiceAPIBaseUrl(), 
						prop.getServiceAPIUsername(), 
						prop.getServiceAPIPassword()).getCloudConnection(connctionId);
			
			logger.info("Get DCA Connection :" + response);
			
			if (null == response ) {
				throw new NovitasHandlerException();
			}
			
			CloudConnectionResponse cloudConnectionResponse  = (CloudConnectionResponse) response;
			
			Map<String,Object> retval = new HashMap<String,Object>();
			retval.put("Result", response);
			retval.put("circuit_id_1", cloudConnectionResponse.getResourceId1());
			retval.put("circuit_id_2", cloudConnectionResponse.getResourceId2());
			retval.put("penalty_charge", cloudConnectionResponse.getPenaltyCharge());
			retval.put("bandwidth", String.valueOf(cloudConnectionResponse.getBandwidth()));
			retval.put("ocn", cloudConnectionResponse.getOcn());
			
			manager.completeWorkItem(workItem.getId(), retval);
			
		} catch (Exception e) {
			logger.error("WIHGetDCAConnection -> Fail : ", e);
			HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(HandlerErrorCode.DC_GET_SERVICE_CONNECTION));
		}
	}
	

}
