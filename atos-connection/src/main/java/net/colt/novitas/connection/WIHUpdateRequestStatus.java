package net.colt.novitas.connection;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.HandlerUtils;
import com.colt.novitas.WorkflowType;
import com.colt.novitas.client.RequestAPIClient;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;
import com.colt.novitas.request.UpdateRequestStatusRequest;
import com.colt.novitas.response.GetConnectionRequestResponse;
import com.colt.novitas.response.UpdateRequestStatusResponse;

public class WIHUpdateRequestStatus extends BaseWorkItemHandler {
	
	private static Logger logger = LoggerFactory.getLogger(WIHUpdateRequestStatus.class);
	
	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}
	
	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
		
		String status = null;
		
		try {
			EnvironmentProperties prop = getEnvProperties(workItem);
			logger = getLogger(workItem, prop.getLogUniqueId());
			GetConnectionRequestResponse connectionRequest = getPortRequst(workItem);
			
			Integer requestId = connectionRequest.getId();
			Object penaltyCharge = workItem.getParameter("penalty_charge");
			status = (String) workItem.getParameter("status");
			Integer statusCode = getStatusCode(workItem);
			
			logger.info("WIHUpdateRequestStatus -- Request Id : " + requestId);
			
			UpdateRequestStatusRequest requestObject = new UpdateRequestStatusRequest();
			requestObject.setStatus(status);
			String connectionId = (String) workItem.getParameter("service_id");
			if(StringUtils.isNotBlank(connectionId)){
			   requestObject.setServiceId(connectionId);
			}
			if (statusCode != null) {
				requestObject.setStatusCode(statusCode);
			}
			if(null!= penaltyCharge){
			requestObject.setPenaltyCharge((Float)penaltyCharge);
			}
			
			if("INPROGRESS".equals(status)){
				addJbpmDetails(requestObject, workItem);
			}
			
			Object response = new RequestAPIClient(
					prop.getRequestAPIUrl(),
					prop.getRequestAPIUsername(),
					prop.getRequestAPIPassword()
					).updatePortorConnectionRequest(requestId, requestObject);
			
			logger.info("Return from update request object: " + response + " class " + response.getClass().getName());
			
			if (response instanceof UpdateRequestStatusResponse) {
				Integer statusId = ((UpdateRequestStatusResponse)response).getStatusId();
				logger.info("Return code from update request: " + statusId);
				if (statusId <= 0) {
					throw new NovitasHandlerException(null);
				}
			}
			else if (response instanceof String) {
			logger.info("response is string");
			if ("-1".equals((String)response) || "FAILED".equals((String)response)) {
				throw new NovitasHandlerException(null);
				}
			}
			
			
			Map<String,Object> retval = new HashMap<String,Object>();
			manager.completeWorkItem(workItem.getId(), retval);
		} catch (Exception e) {
			logger.error("WIHUpdateRequestStatus -> Failed ",  e);
			if ("INPROGRESS".equals(status)) {
				HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(getErrorCode(workItem)));
			}
			
			
		}
	}
	
	protected HandlerErrorCode getErrorCode(WorkItem workItem) {
		WorkflowType type = HandlerUtils.getWorkflowType(workItem);
		if (type == WorkflowType.MODIFY_CONNECTION) {
			return HandlerErrorCode.MC_UPADTE_REQUEST_INPROGRESS;
		} 
		else if (type == WorkflowType.DELETE_CONNECTION) {
			return HandlerErrorCode.DC_UPADTE_REQUEST_INPROGRESS;
		}
		return HandlerErrorCode.CC_UPADTE_REQUEST_INPROGRESS;
	}
	
	protected Integer getStatusCode(WorkItem workItem) {
		return getIntValue(workItem.getParameter("status_code"));
	}
	
	protected GetConnectionRequestResponse getPortRequst(WorkItem workItem) {
		return (GetConnectionRequestResponse) workItem.getParameter("request_obj");
	}
	
	protected Integer getRequestId(WorkItem workItem) {
		Object obj = workItem.getParameter("request_id");
		Integer requestId = null;
		if (obj instanceof String) {
			requestId = Integer.valueOf(String.valueOf(obj));
		}
		else {
			requestId = (Integer) obj;
		}
		logger.info("Request Id : " + requestId);

		return requestId;
	}
	
	private Integer getIntValue(Object val) {
		if (val == null) {
			return null;
		}
		if (val instanceof Integer) {
			logger.info("Integer value : " + val);
			return (Integer) val;
		}
		
		logger.info("String value : " + Integer.valueOf(String.valueOf(val)));
		return Integer.valueOf(String.valueOf(val));
	}
	
   private void addJbpmDetails(UpdateRequestStatusRequest req,WorkItem workItem){
		
		req.setProcessInstanceId(String.valueOf(workItem.getProcessInstanceId()));
		try {
			req.setHostAddress(InetAddress.getLocalHost().getHostName());
		} catch (UnknownHostException e) {
			
		}
	}

}
