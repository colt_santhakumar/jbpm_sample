package net.colt.novitas.connection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.HandlerUtils;
import com.colt.novitas.client.ServiceAPIClient;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;
import com.colt.novitas.request.ConnectionRequestService;
import com.colt.novitas.request.RequestVLANIdRange;
import com.colt.novitas.response.GetConnectionRequestResponse;
import com.colt.novitas.response.ServiceVLANIdRange;

public class WIHCreateConnection extends BaseWorkItemHandler {
	
	private static Logger logger = LoggerFactory.getLogger(WIHCreateConnection.class);
	
	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}
	

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
		EnvironmentProperties prop = getEnvProperties(workItem);
		logger = getLogger(workItem, prop.getLogUniqueId());
		try {
			
			GetConnectionRequestResponse connectionRequest = getPortRequst(workItem);
			String from_port_type = (String) workItem.getParameter("from_port_type");
			String to_port_type = (String) workItem.getParameter("to_port_type");
			String connectionType = getConnectionType(from_port_type,to_port_type);
            logger.info("Connection Type -----> "+connectionType);
			ConnectionRequestService servieConnReq = new ConnectionRequestService();
			servieConnReq.setName(connectionRequest.getConnectionName());
			servieConnReq.setFromPortId(connectionRequest.getFromPort());
			servieConnReq.setToPortId(connectionRequest.getToPort());
			servieConnReq.setBandwidth(connectionRequest.getBandwidth());
			servieConnReq.setRentalCharge(connectionRequest.getRentalCharge());
			servieConnReq.setRentalCurrency(connectionRequest.getRentalCurrency());
			servieConnReq.setRentalUnit(connectionRequest.getRentalUnit());
			servieConnReq.setServiceId(connectionRequest.getServiceId());
			servieConnReq.setResourceId(null);
			servieConnReq.setOcn(connectionRequest.getOcn());
			servieConnReq.setCustomerName(connectionRequest.getCustomerName());
			servieConnReq.setCommitmentPeriod(connectionRequest.getCommitmentPeriod());
			servieConnReq.setConnectionType(connectionType);
			//VLAN properties
			servieConnReq.setFromVlanMapping(connectionRequest.getFromVlanMapping());
			servieConnReq.setToVlanMapping(connectionRequest.getToVlanMapping());
			servieConnReq.setFromVlanType(connectionRequest.getFromVlanType());
			servieConnReq.setToVlanType(connectionRequest.getToVlanType());
			servieConnReq.setFromPortVLANIdRange(getServiceVLANIds(connectionRequest.getFromPortVLANIdRange()));
			servieConnReq.setToPortVLANIdRange(getServiceVLANIds(connectionRequest.getToPortVLANIdRange()));
			String response = new ServiceAPIClient(prop.getServiceAPIBaseUrl(), 
					                               prop.getServiceAPIUsername(), 
					                               prop.getServiceAPIPassword()).createConnection(servieConnReq );
			
			logger.info("Create Connection :" + response);
			
			if (StringUtils.isBlank(response)) {
				throw new NovitasHandlerException();
			}
			
			Map<String,Object> retval = new HashMap<String,Object>();
			retval.put("Result", response);
			retval.put("connection_type", connectionType);
			retval.put("service_instance_type",connectionType);
			manager.completeWorkItem(workItem.getId(), retval);
			
		} catch (Exception e) {
			logger.error("WIHCreateConnection -> Fail",  e.getMessage());
			HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(HandlerErrorCode.CC_CREATE_SERVICE_CONN));
		}
	}
	
	protected GetConnectionRequestResponse getPortRequst(WorkItem workItem) {
		return (GetConnectionRequestResponse) workItem.getParameter("request_obj");
	}
	
	protected List<ServiceVLANIdRange> getServiceVLANIds(List<RequestVLANIdRange> requestRanges) {
		
		List<ServiceVLANIdRange> serviceRanges = new ArrayList<ServiceVLANIdRange>();
		if (requestRanges != null && ! requestRanges.isEmpty()) {
			for (RequestVLANIdRange requestVLANIdRange : requestRanges) {
				serviceRanges.add(new ServiceVLANIdRange(requestVLANIdRange.getFromIdRange(), requestVLANIdRange.getToIdRange()));
			}
		}
		
		return serviceRanges;
		
	}
	
	
	private String getConnectionType(String fromPortTpe,String toPortType){
		
		if(("LANPORT".equals(fromPortTpe) || "LANPORT".equals(toPortType)) ){
			return "LANCONNECTION";
		}else{
			return "DCNCONNECTION";
		}
	}
		
}
