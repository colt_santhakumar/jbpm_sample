package net.colt.novitas.connection;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.HandlerUtils;
import com.colt.novitas.client.CallBackAPIClient;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;
import com.colt.novitas.request.CallBack;

/**
 * @author DRao This is a WIH file created by Dharrma Rao.
 * 
 */
public class WIHUpdateCallBackStatus extends BaseWorkItemHandler {

	private static Logger logger = LoggerFactory.getLogger(WIHUpdateCallBackStatus.class);
	
	private static final String CREATE_CONNECTION_SIGNAL = "create_connection_signal";
	private static final String CREATE_CONNECTION_SIEBEL_ASSETS_SIGNAL = "create_connection_siebel_signal";
	private static final String MODIFY_CONNECTION_SIEBEL_ASSETS_SIGNAL = "modify_conn_siebel_signal";
	private static final String DELETE_CONNECTION_SIEBEL_ASSETS_SIGNAL = "delete_conn_siebel_signal";
	private static final String CREATE_CONNECTION_SMARTS_SIGNAL = "create_conn_smart_signal";
	private static final String DELETE_CONNECTION_SMARTS_SIGNAL = "delete_conn_smart_signal";

	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
		EnvironmentProperties prop = getEnvProperties(workItem);
		logger = getLogger(workItem, prop.getLogUniqueId());
		String signalName = (String) workItem.getParameter("signal_name");
		try {
             
			String[] signalData = (String[]) workItem.getParameter("received_signal_data");
			CallBack callBack = new CallBack();

			callBack.setTransactionId(signalData[0]);
			callBack.setProcessInstanceId(workItem.getProcessInstanceId());
			callBack.setStatus(signalData[1]);
			callBack.setSignalName(signalName);
            callBack.setStatusDescription(signalData[2]);
			
			CallBackAPIClient callBackAPIClient = new CallBackAPIClient(prop.getInboundRestApiUrl(),prop.getInboundRestApiUserName(),prop.getInboundRestApiPassword());


			callBackAPIClient.updateStatus(callBack);

			if (StringUtils.isBlank(signalData[1]) || !"SUCCESS".equals(signalData[1])) {
				logger.info(signalName +" callback response either null or not SUCCESS");
				throw new NovitasHandlerException();
			}

			Map<String, Object> retval = new HashMap<String, Object>();
			manager.completeWorkItem(workItem.getId(), retval);

		} catch (Exception e) {
			logger.error("WIHUpdateCallBackStatus -> Failed", e.getMessage());

			if (signalName.equalsIgnoreCase(CREATE_CONNECTION_SIGNAL)) {
				HandlerUtils.throwHandlerExcetpion(
						new NovitasHandlerException(HandlerErrorCode.CREATE_CONNECTION_CALL_BACK_STATUS));
			} else if (signalName.equalsIgnoreCase(CREATE_CONNECTION_SIEBEL_ASSETS_SIGNAL)) {
				HandlerUtils.throwHandlerExcetpion(
						new NovitasHandlerException(HandlerErrorCode.CONNECTION_SIEBEL_ASSETS_CALL_BACK_STATUS));
			} else if (signalName.equalsIgnoreCase(MODIFY_CONNECTION_SIEBEL_ASSETS_SIGNAL)) {
				HandlerUtils.throwHandlerExcetpion(
						new NovitasHandlerException(HandlerErrorCode.CONNECTION_SIEBEL_ASSETS_CALL_BACK_STATUS));
			} else if (signalName.equalsIgnoreCase(DELETE_CONNECTION_SIEBEL_ASSETS_SIGNAL)) {
				HandlerUtils.throwHandlerExcetpion(
						new NovitasHandlerException(HandlerErrorCode.CONNECTION_SIEBEL_ASSETS_CALL_BACK_STATUS));
			} else if (signalName.equalsIgnoreCase(CREATE_CONNECTION_SMARTS_SIGNAL)) {
				HandlerUtils.throwHandlerExcetpion(
						new NovitasHandlerException(HandlerErrorCode.CONNECTION_SMARTS_CALL_BACK_STATUS));
			} else if (signalName.equalsIgnoreCase(DELETE_CONNECTION_SMARTS_SIGNAL)) {
				HandlerUtils.throwHandlerExcetpion(
						new NovitasHandlerException(HandlerErrorCode.CONNECTION_SMARTS_CALL_BACK_STATUS));
			} else {
				HandlerUtils
						.throwHandlerExcetpion(new NovitasHandlerException(HandlerErrorCode.UPDATE_STATUS_CB_DETAILS));
			}
		}
	}

}