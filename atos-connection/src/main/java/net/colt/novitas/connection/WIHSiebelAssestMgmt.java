package net.colt.novitas.connection;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import net.colt.novitas.connection.util.ConnectionUtil;

import org.apache.commons.lang.StringUtils;
import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.HandlerUtils;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;
import com.colt.novitas.response.GetConnectionRequestResponse;
import com.colt.novitas.siebel.assests.client.SiebelAssetsClient;
import com.colt.novitas.siebel.assets.request.SiebelAssestRequest;

/**
 * @author DRao This is a WIH file created by Dharrma Rao.
 * 
 */
public class WIHSiebelAssestMgmt extends BaseWorkItemHandler {

	private static Logger logger = LoggerFactory.getLogger(WIHSiebelAssestMgmt.class);

	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {

		try {
			EnvironmentProperties prop = getEnvProperties(workItem);
			logger = getLogger(workItem, prop.getLogUniqueId());
			GetConnectionRequestResponse connectionRequestResponse = (GetConnectionRequestResponse) workItem.getParameter("request_obj");
			String connectionType = (String) workItem.getParameter("connection_type");
			String serviceConnectionId = (String) workItem.getParameter("service_id");
			String novitasId = (String) workItem.getParameter("novitas_service_id");	
			String status  = (String) workItem.getParameter("status");
			String ncTechServId =  (String) workItem.getParameter("nc_tech_service_id");
			String localName =  (String) workItem.getParameter("local_name");
			String aLocalBuildingName =  (String) workItem.getParameter("a_local_building_name");
			String aBuildingName = String.valueOf(workItem.getParameter("a_building_name"));
			String bBuildingName = String.valueOf(workItem.getParameter("b_building_name"));
			String bLocalBuildingName =  (String) workItem.getParameter("b_local_building_name");
			String customerRegion =  (String) workItem.getParameter("customer_region");
			SiebelAssestRequest request = new SiebelAssestRequest();
			Map <String,String> attributesMap = new HashMap<String, String>();
			request.setAttributesMap(attributesMap);
			request.setPamFlag("Y");
			attributesMap.put("from_port_id",connectionRequestResponse.getFromPort());
	        attributesMap.put("to_port_id",connectionRequestResponse.getToPort());
	        attributesMap.put("resource_id",connectionRequestResponse.getConnectionId());
	        attributesMap.put("from_uni_type",connectionRequestResponse.getFromVlanMapping());
	        attributesMap.put("to_uni_type",connectionRequestResponse.getToVlanMapping());
	        attributesMap.put("bandwidth",String.valueOf(connectionRequestResponse.getBandwidth()));
	       
	        attributesMap.put("connection_type",connectionType);
	        attributesMap.put("last_updated","");
	        attributesMap.put("created_date",connectionRequestResponse.getRequestedAt());
	        
	        if("DCNCONNECTION".equals(connectionType)){
	        	request.setProductName("COLT On Demand DCNet Connection");
	        }else if("LANCONNECTION".equals(connectionType)){
	        	//request.setProductName("COLT On Demand LANLink Connection");
	        	request.setProductName("COLT On Demand Ethernet Connection");
	        }
	        
	        if("ASIA".equals(customerRegion)){
	        	attributesMap.put("A00_AccountJPN", localName);
	        	attributesMap.put("A03_A-BuildingName", aBuildingName);
	        	attributesMap.put("A04_B-BuildingName", bBuildingName);
	        	attributesMap.put("A06_B-AccountName", localName);
	        	attributesMap.put("A16_A03_A-BuildingNameJPN", aLocalBuildingName);
	        	attributesMap.put("A17_B-BuildingNameJPN", bLocalBuildingName);
	        	attributesMap.put("A18_ServiceJPN", "オンデマンド");
	        	attributesMap.put("A05_A-AccountName", connectionRequestResponse.getCustomerName());
	        	attributesMap.put("Service_Type", request.getProductName());
	        	attributesMap.put("A08_STTEnable", "No");
	        	attributesMap.put("A09_BillingDate", getFirstOfFollowingMonth());
	        	attributesMap.put("A14_A-AccountNameJPN", localName);
	        	attributesMap.put("Network", "ONNet");
	        	attributesMap.put("ResilientOptions", "Unprotected");
	        	attributesMap.put("ContractedRegion", customerRegion);
	        	if(!"ACTIVE".equalsIgnoreCase(status))
	        	  attributesMap.put("A19_RequestedDisconnectDate", connectionRequestResponse.getRequestedAt());
	        }
			
			request.setResourceId(ncTechServId);
			
			if(StringUtils.isNotBlank(status) && "ACTIVE".equalsIgnoreCase(status)){
				String slaId = ConnectionUtil.getSlaIdForConnection(serviceConnectionId, workItem);
				logger.info("SLA ID is ::::"+slaId);
				request.setSlaId(slaId);
				attributesMap.put("rental_charge",connectionRequestResponse.getRentalCharge().toString());
		        attributesMap.put("rental_unit",connectionRequestResponse.getRentalUnit());
		        attributesMap.put("rental_currency",connectionRequestResponse.getRentalCurrency());
			}else{
				 attributesMap.put("decommissioned_on",connectionRequestResponse.getRequestedAt());
				request.setSlaId("");
			}
			request.setServiceId(serviceConnectionId);
			request.setOcn(connectionRequestResponse.getOcn());
			request.setNovitasServiceId(novitasId);
			request.setStatus(status);
			
			request.setUrlCallback(prop.getCallBackUrl());
			
			String transactionId  = new SiebelAssetsClient(prop.getSiebelUrl(),prop.getSiebelUsername(),
					                                    prop.getSiebelPassword()).receiveAssetMgmtRequest(request);

			Map<String, Object> retval = new HashMap<String, Object>();
			if (StringUtils.isBlank(transactionId)) {
				logger.info("Failed to Update Siebel Assests Management");
				throw new NovitasHandlerException();
			}
			
			retval.put("Result", transactionId);
			manager.completeWorkItem(workItem.getId(), retval);

		} catch (Exception e) {
			logger.error("WIHSiebelAssestMgmt -> Failed", e);
				HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(
						HandlerErrorCode.CREATE_OR_UPDATE_SIEBEL_ASSETS_MGMT));
			
		}
	}
	
	private String getFirstOfFollowingMonth(){
		Calendar calendar = Calendar.getInstance();         
		calendar.add(Calendar.MONTH, 1);
		calendar.set(Calendar.DATE, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
		Date nextMonthFirstDay = calendar.getTime();
		return nextMonthFirstDay.toString();
	}

}