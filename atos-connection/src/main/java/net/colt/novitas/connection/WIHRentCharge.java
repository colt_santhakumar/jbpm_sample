package net.colt.novitas.connection;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import org.apache.commons.lang.StringUtils;
import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.HandlerUtils;
import com.colt.novitas.billing.client.BillingAPIClient;
import com.colt.novitas.billing.client.request.RecurringChargeRequest;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;
import com.colt.novitas.response.GetConnectionRequestResponse;

public class WIHRentCharge extends BaseWorkItemHandler {

	private static Logger logger = LoggerFactory.getLogger(WIHRentCharge.class);

	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
       
		try {
			EnvironmentProperties prop = getEnvProperties(workItem);
			logger = getLogger(workItem, prop.getLogUniqueId());
			GetConnectionRequestResponse connRequest = getConnectionRequst(workItem);

			String dedicateConn = getConnectionId(workItem);
			String connectionType = (String) workItem.getParameter("connection_type");
			logger.info("Service Instance type ----> "+ connectionType);
			RecurringChargeRequest recurring = new RecurringChargeRequest();
			recurring.setRequestId(connRequest.getId());
			recurring.setServiceId(connRequest.getServiceId());
			recurring.setServiceInstanceId(dedicateConn);
			recurring.setServiceInstanceType(connectionType);
			recurring.setBcn(connRequest.getBcn());
			recurring.setChargeType("RENTAL");
			recurring.setDescription("Rental for connection : "
					+ connRequest.getConnectionName());
			recurring.setCurrency(connRequest.getRentalCurrency());
			recurring.setAmount(connRequest.getRentalCharge());
			recurring.setFrequency(connRequest.getRentalUnit());
			if (null != connRequest.getCommitmentPeriod()
					&& connRequest.getCommitmentPeriod() > 0) {
				recurring.setCommitmentType("COMMIT");
				recurring.setCommitmentExpiryDate(getCommitmentExpiryDate(
						connRequest.getRequestedAt(),
						connRequest.getCommitmentPeriod(),
						connRequest.getRentalUnit()));
			}
			recurring.setServiceInstanceName(connRequest.getConnectionName());
			
			Object chargeResponse = new BillingAPIClient(
					prop.getBillingAPIOneOffUrl(),
					prop.getBillingAPIRecurrUrl(),
					prop.getBillingAPIUsername(), prop.getBillingAPIPassword())
					.createRecurringCharges(recurring);

			logger.info(String.valueOf(chargeResponse));

			if (StringUtils.isBlank(String.valueOf(chargeResponse))) {
				throw new NovitasHandlerException();
			}

			Map<String, Object> retval = new HashMap<String, Object>();
			retval.put("Result", chargeResponse);
			manager.completeWorkItem(workItem.getId(), retval);

		} catch (Exception e) {
			logger.error("WIHRentCharge -> Failed", e);
			HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(
					HandlerErrorCode.CC_RECURRING_CHARGE));
		}
	}

	protected GetConnectionRequestResponse getConnectionRequst(WorkItem workItem) {
		return (GetConnectionRequestResponse) workItem
				.getParameter("request_obj");
	}

	protected String getConnectionId(WorkItem workItem) {
		return (String) workItem.getParameter("connection_id");
	}

	 private static String getCommitmentExpiryDate(String startDate,Integer commitmentPeriod,String frequency){
			
		 DateFormat fdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
		 fdf.setTimeZone(TimeZone.getTimeZone("UTC"));
		 Calendar c = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
			if (startDate != null) {
				
				try {
					c.setTime(fdf.parse(startDate));
				} catch (ParseException e) {
                 
                  e.printStackTrace();
				}
				c.add(Calendar.MONTH, commitmentPeriod);
				if ("MONTHLY".equals(frequency)) {
					c.set(Calendar.HOUR_OF_DAY,
							c.getActualMaximum(Calendar.HOUR_OF_DAY));
				}
				c.set(Calendar.MINUTE, c.getActualMaximum(Calendar.MINUTE));
				c.set(Calendar.SECOND, c.getActualMaximum(Calendar.SECOND));
				
				return fdf.format(c.getTime());

			} else {
				return "";
			}
		}

}
