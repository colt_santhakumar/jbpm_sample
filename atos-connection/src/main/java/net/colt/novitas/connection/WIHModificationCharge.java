package net.colt.novitas.connection;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.httpclient.NoHttpResponseException;
import org.apache.commons.lang.StringUtils;
import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.HandlerUtils;
import com.colt.novitas.billing.client.BillingAPIClient;
import com.colt.novitas.billing.client.request.OneOffChargeRequest;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;
import com.colt.novitas.response.GetConnectionRequestResponse;

public class WIHModificationCharge  extends BaseWorkItemHandler {
	
	private static Logger logger = LoggerFactory.getLogger(WIHModificationCharge.class);
	
	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
		try {
			EnvironmentProperties prop = getEnvProperties(workItem);
			logger = getLogger(workItem, prop.getLogUniqueId());			
			GetConnectionRequestResponse connRequest = getConnectionRequst(workItem);
			String connectionType = (String) workItem.getParameter("connection_type");
			OneOffChargeRequest modificationCharge = new OneOffChargeRequest();
			modificationCharge.setRequestId(connRequest.getId());
			modificationCharge.setServiceId(connRequest.getServiceId());
			modificationCharge.setServiceInstanceId(connRequest.getConnectionId());
			modificationCharge.setServiceInstanceType(connectionType);
			modificationCharge.setBcn(connRequest.getBcn());
			modificationCharge.setChargeType("MODIFICATION");
			modificationCharge.setDescription("Modification of connection " + connRequest.getConnectionName());
			modificationCharge.setCurrency(connRequest.getModificationCurrency());
			modificationCharge.setAmount(connRequest.getModificationCharge());

			Map<String,Object> retval = new HashMap<String,Object>();
			modificationCharge.setServiceInstanceName(connRequest.getConnectionName());
			
			if (modificationCharge.getAmount() != null && modificationCharge.getAmount() > 0) {
				logger.info("Charge is non-zero. Updating...");
				
				Object chargeResponse = new BillingAPIClient(
						prop.getBillingAPIOneOffUrl(),
						prop.getBillingAPIRecurrUrl(), 
						prop.getBillingAPIUsername(),
						prop.getBillingAPIPassword()).createOneOffCharges(modificationCharge);
				
				logger.info(String.valueOf(chargeResponse));
				
				if (StringUtils.isBlank(String.valueOf(chargeResponse))) {
					throw new NoHttpResponseException();
				}
				retval.put("Result", chargeResponse);
			}
			else {
				logger.info("Charge is zero. Skipping...");
			}
			
			manager.completeWorkItem(workItem.getId(), retval);
			
		} catch (Exception e) {
			logger.error("WIHModificationCharge -> Failed", e);
			HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(HandlerErrorCode.MC_MODIFY_CONNECTION_CHARGE));
		}
	}
	
	protected GetConnectionRequestResponse getConnectionRequst(WorkItem workItem) {
		return (GetConnectionRequestResponse) workItem.getParameter("request_obj");
	}
	
}
