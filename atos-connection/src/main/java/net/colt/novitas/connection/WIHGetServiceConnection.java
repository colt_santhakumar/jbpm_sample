package net.colt.novitas.connection;

import java.util.HashMap;
import java.util.Map;

import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.HandlerUtils;
import com.colt.novitas.WorkflowType;
import com.colt.novitas.client.ServiceAPIClient;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;
import com.colt.novitas.response.ConnectionResponse;

public class WIHGetServiceConnection extends BaseWorkItemHandler {
	
	private static Logger logger = LoggerFactory.getLogger(WIHGetServiceConnection.class);
	
	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}
	

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
		
		try {
			EnvironmentProperties prop = getEnvProperties(workItem);
			logger = getLogger(workItem, prop.getLogUniqueId());
			String connctionId = getConnectionId(workItem);
			
			ConnectionResponse response = new ServiceAPIClient(prop.getServiceAPIBaseUrl(), 
                                                               prop.getServiceAPIUsername(), 
                                                               prop.getServiceAPIPassword()).getConnection(connctionId);
			
			logger.info("Get service Connection :" + response);
			
			if (!(response instanceof ConnectionResponse)) {
				throw new NovitasHandlerException();
			}
			
			Map<String,Object> retval = new HashMap<String,Object>();
			retval.put("Result", response);
			retval.put("nc_tech_service_id", response.getNcTechServiceId());
			retval.put("penalty_charge", response.getPenaltyCharge());
			retval.put("connection_type", response.getConnectionType());
			
			manager.completeWorkItem(workItem.getId(), retval);
			
		} catch (Exception e) {
			logger.error("WIHGetServiceConnection -> Fail : ", e.getMessage());
			HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(getErrorCode(workItem)));
		}
	}
	
	protected HandlerErrorCode getErrorCode(WorkItem workItem) {
		WorkflowType type = HandlerUtils.getWorkflowType(workItem);
		if (type == WorkflowType.DELETE_CONNECTION) {
			return HandlerErrorCode.DC_GET_SERVICE_CONNECTION;
		}
		return HandlerErrorCode.MC_GET_SERVICE_CONNECTION;
	}
	
	protected String getConnectionId(WorkItem workItem) {
		
		return (String) workItem.getParameter("service_connection_id");
		
		
	}

}
