package net.colt.novitas.connection;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.HandlerUtils;
import com.colt.novitas.client.ServiceAPIClient;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;
import com.colt.novitas.request.UpdatePortRequest;

public class WIHUpdateServicePort extends BaseWorkItemHandler {

	private static Logger logger = LoggerFactory.getLogger(WIHUpdateServicePort.class);

	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {

		try {
			EnvironmentProperties prop = getEnvProperties(workItem);
			logger = getLogger(workItem, prop.getLogUniqueId());
			String dedicatedPortId = getDedicatedPortId(workItem);
            String operation = (String) workItem.getParameter("operation");
			UpdatePortRequest portReq = new UpdatePortRequest();
			portReq.setStatus("ACTIVE");
			if("CREATE".equals(operation)){
				portReq.setPortExpiresOn(null);
				portReq.setPortInUse(true);
			}else if ("DELETE".equals(operation)){
				Integer expiryPeriod = (Integer) workItem.getParameter("port_expiry_period");
				portReq.setPortExpiresOn(getPortExpiryDate(null!=expiryPeriod ? expiryPeriod : 90));
				portReq.setPortInUse(false);
			}
			
			Object response = new ServiceAPIClient(prop.getServiceAPIBaseUrl(),
					prop.getServiceAPIUsername(), prop.getServiceAPIPassword())
					.updatePort(dedicatedPortId, portReq);

			if ("FAILED".equals(response)) {
				logger.info("Port Expiry and Used status update failed :: "
						+ dedicatedPortId);
				throw new NovitasHandlerException();
			}

			Map<String, Object> retval = new HashMap<String, Object>();
			manager.completeWorkItem(workItem.getId(), retval);

		} catch (Exception e) {
			logger.error("WIHUpdateServicePort -> Failed", e);

				HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(
						HandlerErrorCode.CP_UPDATE_SERVICE_PORT_DETAILS));
			
		}
	}

	protected String getDedicatedPortId(WorkItem workItem) {
		return (String) workItem.getParameter("service_port_id");
	}
	
	 protected String getPortExpiryDate(Integer commitmentPeriod) {

			DateFormat fdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
			fdf.setTimeZone(TimeZone.getTimeZone("UTC"));
			Calendar c = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
			c.add(Calendar.DATE, commitmentPeriod);
			// c.set(Calendar.HOUR_OF_DAY,c.getActualMaximum(Calendar.HOUR_OF_DAY));
			// c.set(Calendar.MINUTE, c.getActualMaximum(Calendar.MINUTE));
			// c.set(Calendar.SECOND, c.getActualMaximum(Calendar.SECOND));

			return fdf.format(c.getTime());

		}

	
}
