package net.colt.novitas.jbpm.models;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Converter {
	
	public static <T> Object convert(Object resp, Class<T> typeRef) {
		ObjectMapper mapper = new ObjectMapper();
    	mapper.disable(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES);
		return new ObjectMapper().convertValue(resp,typeRef);
	}

}
