package net.colt.novitas.capacity.request;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SiebelCeaseOrderRequest implements Serializable {

	private static final long serialVersionUID = 4280647876917468935L;

	@JsonProperty("site_port_request_id")
	private String sitePortRequestId;

	@JsonProperty("customer_country")
	private String customerCountry;
	
	@JsonProperty("customer_city")
	private String customerCity;

	@JsonProperty("callback_url")
	private String callbackUrl;

	@JsonProperty("ocn")
	private String ocn;

	@JsonProperty("bcn")
	private String bcn;

	@JsonProperty("customer_currency")
	private String customerCurrency;

	@JsonProperty("customer_name")
	private String customerName;

	@JsonProperty("noviats_service_Id")
	private String noviatsServiceId;

	@JsonProperty("port_id")
	private String portId;

		
	@JsonProperty("request_date")
	private String requestDate;

	@JsonProperty("port_name")
	private String portName;
	
	@JsonProperty("customer_requested_date")
	private String crd;
	
	@JsonProperty("order_type")
	private String orderTpe = "CEASE";
	
	@JsonProperty("order_status")
	private String orderStatus;
	
	@JsonProperty("customer_region")
	private String customerRegion;
	
	@JsonProperty("is_ull")
	private boolean isUll;
		
		
	public boolean isUll() {
		return isUll;
	}

	public void setUll(boolean isUll) {
		this.isUll = isUll;
	}
	

	public String getCustomerRegion() {
		return customerRegion;
	}

	public void setCustomerRegion(String customerRegion) {
		this.customerRegion = customerRegion;
	}

	public String getSitePortRequestId() {
		return sitePortRequestId;
	}

	public void setSitePortRequestId(String sitePortRequestId) {
		this.sitePortRequestId = sitePortRequestId;
	}

	public String getCustomerCountry() {
		return customerCountry;
	}

	public void setCustomerCountry(String customerCountry) {
		this.customerCountry = customerCountry;
	}

	public String getCustomerCity() {
		return customerCity;
	}

	public void setCustomerCity(String customerCity) {
		this.customerCity = customerCity;
	}

	public String getCallbackUrl() {
		return callbackUrl;
	}

	public void setCallbackUrl(String callbackUrl) {
		this.callbackUrl = callbackUrl;
	}

	public String getOcn() {
		return ocn;
	}

	public void setOcn(String ocn) {
		this.ocn = ocn;
	}

	public String getBcn() {
		return bcn;
	}

	public void setBcn(String bcn) {
		this.bcn = bcn;
	}

	public String getCustomerCurrency() {
		return customerCurrency;
	}

	public void setCustomerCurrency(String customerCurrency) {
		this.customerCurrency = customerCurrency;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getNoviatsServiceId() {
		return noviatsServiceId;
	}

	public void setNoviatsServiceId(String noviatsServiceId) {
		this.noviatsServiceId = noviatsServiceId;
	}

	public String getPortId() {
		return portId;
	}

	public void setPortId(String portId) {
		this.portId = portId;
	}

	public String getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(String requestDate) {
		this.requestDate = requestDate;
	}

	public String getPortName() {
		return portName;
	}

	public void setPortName(String portName) {
		this.portName = portName;
	}

	public String getCrd() {
		return crd;
	}

	public void setCrd(String crd) {
		this.crd = crd;
	}

	public String getOrderTpe() {
		return orderTpe;
	}

	public void setOrderTpe(String orderTpe) {
		this.orderTpe = orderTpe;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	@Override
	public String toString() {
		return "SiebelCeaseOrderRequest [sitePortRequestId=" + sitePortRequestId + ", customerCountry="
				+ customerCountry + ", customerCity=" + customerCity + ", callbackUrl=" + callbackUrl + ", ocn=" + ocn
				+ ", bcn=" + bcn + ", customerCurrency=" + customerCurrency + ", customerName=" + customerName
				+ ", noviatsServiceId=" + noviatsServiceId + ", portId=" + portId + ", requestDate=" + requestDate
				+ ", portName=" + portName + ", crd=" + crd + ", orderTpe=" + orderTpe + ", orderStatus=" + orderStatus
				+ "]";
	}

    

	
	
	
}
