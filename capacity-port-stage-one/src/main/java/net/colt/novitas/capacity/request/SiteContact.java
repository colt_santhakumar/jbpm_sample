package net.colt.novitas.capacity.request;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonProperty;

public class SiteContact implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonProperty("name")
	private String name;

	@JsonProperty("phone")
	private String phone;

	@JsonProperty("email")
	private String email;

	public SiteContact() {
	}

	public SiteContact(String name, String phone, String email) {
		super();
		this.name = name;
		this.phone = phone;
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "SiteContact [name=" + name + ", phone=" + phone + ", email=" + email + "]";
	}

}
