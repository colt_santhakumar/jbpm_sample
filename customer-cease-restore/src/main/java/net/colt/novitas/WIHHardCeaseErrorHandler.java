package net.colt.novitas;

import java.util.HashMap;
import java.util.Map;

import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;


public class WIHHardCeaseErrorHandler extends CeaseBaseWorkItemHandler {
	
	private static Logger logger = LoggerFactory.getLogger(WIHHardCeaseErrorHandler.class);

	private WorkItem workItem;
	private EnvironmentProperties prop;

	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {

		try {
			this.prop = getEnvProperties(workItem);
			logger = getLogger(workItem, prop.getLogUniqueId());
			logger.info("Starting error handling for onboarding request......... ");

			this.workItem = workItem;
			String customerId = (String) workItem.getParameter("customer_id");

			logger.info("Starting error handling for request : " );

			Map<String,Object> retval = new HashMap<String,Object>();
			
			HandlerErrorCode errorCode = null;
			Exception exception = getWorkflowError();
			if (exception instanceof NovitasHandlerException) {
				errorCode = ((NovitasHandlerException) exception).getErrorCode();
				logger.info("Error code : " + errorCode);
			
			}

			manager.completeWorkItem(workItem.getId(), retval);
			
		} catch (Exception e) {
			logger.error("Error handling for creat port has been failed.", e);
			manager.abortWorkItem(workItem.getId());
		}

	}


	private Exception getWorkflowError() {
		Object obj = workItem.getParameter("error_obj");
		if (obj instanceof Exception) {
			return (Exception) obj;
		}
		return null;
	}

	
}
