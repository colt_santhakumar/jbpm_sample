package net.colt.novitas;

import java.util.HashMap;
import java.util.Map;

import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.HandlerUtils;
import com.colt.novitas.admin.client.AdminAPIClient;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;

/**
 * @author DRao This is a WIH file created by Dharrma Rao.
 * 
 */
public class WIHUpdateCustomerStatus extends CeaseBaseWorkItemHandler {

	private static Logger logger = LoggerFactory.getLogger(WIHUpdateCustomerStatus.class);

	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {

		try {
			EnvironmentProperties prop = getEnvProperties(workItem);
			logger = getLogger(workItem, prop.getLogUniqueId());
            Integer customerId = Integer.valueOf(getCustomerId(workItem));
            String status = getStatus(workItem);
			AdminAPIClient client =  new AdminAPIClient(prop.getAdminApiLoginUrl(),
					                 prop.getAdminApiCustomerUrl(), 
					                 prop.getAdminApiUserName(), prop.getAdminApiPassword());
			
			String response = client.updateCustomerStatus(customerId,status);
			
			if(null !=response && "FAILED".equals(response)){
				logger.error("WIHUpdateCoustomerStatus -> Failed");
				throw new NovitasHandlerException();
			}

			Map<String, Object> retval = new HashMap<String, Object>();
			manager.completeWorkItem(workItem.getId(), retval);

		} catch (Exception e) {
			logger.error("WIHUpdateCustomerStatus -> Failed", e);
			if("true".equals(String.valueOf(workItem.getParameter("is_error_handling")))) {
				manager.abortWorkItem(workItem.getId());
			}else {
				HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(
						HandlerErrorCode.UPDATE_ONBOARDING_CUSTOMER_STATUS));
			}
			
		}
	}

}