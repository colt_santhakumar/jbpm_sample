package net.colt.novitas;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.HandlerUtils;
import com.colt.novitas.client.JBPMClient;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;


public class WIHSubProcessStatusCheck extends CeaseBaseWorkItemHandler {
	
	private static Logger logger = LoggerFactory.getLogger(WIHSubProcessStatusCheck.class);
	private static final String[] STATUSES = {"PENDING","ACTIVE"};
	
	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
		
		try {
			EnvironmentProperties prop = getEnvProperties(workItem);
			logger = getLogger(workItem, prop.getLogUniqueId());
			Map<Integer,String> reportMap = getReportMap(workItem);
			Map<String,Integer> workflowIdMap = getWorkFlowsIdMap(workItem);
			String deploymentId = prop.getNcInternalCustomerDeploymentId();
			JBPMClient jbpmClient = new JBPMClient(getJbpmUrl(prop.getIntnalJBPMUrl()), prop.getIntnalJBPMUsername(),
					                prop.getIntnalJBPMPassword());
			List<String> statusList = Arrays.asList(STATUSES);
			Iterator<Map.Entry<String,Integer>> workFlowIterator = workflowIdMap.entrySet().iterator();
			Map<String,Object> retval = new HashMap<String,Object>();
			while (workFlowIterator.hasNext()) {
			     Map.Entry<String,Integer> entry = workFlowIterator.next();
	             logger.info("Getting internal workflow status for ::"+entry);
                 if(entry.getKey().split(";").length > 0) {
                	 deploymentId = entry.getKey().split(";")[1];
                 }
                 logger.info("deplyment id --|"+deploymentId);
	             String status = jbpmClient.getStatus(deploymentId, entry.getValue());
				 if(!statusList.contains(status)){
					 String reason = "SUCCESS";
					if("ABORTED".equals(status)){
					  logger.info("Internal workflow aborted. Internal Workflow Id : " + entry);
					  retval.put("status", "FAILED");
					  reason = (String) jbpmClient.getVariable(entry.getValue(), "reason");
					  if(null == reason ){
						  reason = "No Failed reason retriviwed from sub process";
					  }
					  logger.info("Internal workflow aborted. " + entry+":: Reason is :: "+reason);
				    }
					logger.info("Adding completed subprocess to report. " +entry.getValue(), entry.getKey()+"-----"+reason);
					reportMap.put(entry.getValue(), entry.getKey()+"-----"+reason);
					workFlowIterator.remove();
				 }
			}
			
			logger.info("Internal workflow Map. " +workflowIdMap);
			logger.info("Report Map. " +reportMap);
			retval.put("workflow_id_map", workflowIdMap);
			retval.put("report_map", reportMap);
			
			manager.completeWorkItem(workItem.getId(), retval);
			
		} catch (Exception e) {
			logger.error("SubProcessStatusCheck -> Fail" , e);
			HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(HandlerErrorCode.SUBPROCESS_STATUS_CHECK_FAILED));
		}
	}
	
	
	
	//support comma separated
	protected String[] getVarName(WorkItem workItem) {
		String vars = (String) workItem.getParameter("var_name");
		if (vars == null || "".equals(vars)) {
			return null;
		}
		return vars.split(",");
	}
	
		
	
}
