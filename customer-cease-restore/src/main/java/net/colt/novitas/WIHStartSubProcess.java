package net.colt.novitas;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.client.JBPMClient;


public class WIHStartSubProcess extends CeaseBaseWorkItemHandler{
	
	private static Logger logger = LoggerFactory.getLogger(WIHStartSubProcess.class);
	
	private WorkItem workItem;
	private EnvironmentProperties prop;
	
	private String log_unique_id = MDC.get("unique_log_id");
	
	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}
	
	@SuppressWarnings("unchecked")
	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
		
		String action = null;
		this.workItem = workItem;
		try {
			this.prop = getEnvProperties(workItem);
			logger = getLogger(workItem, prop.getLogUniqueId());
			Map<String, Integer> workflowIdMap = new TreeMap<String, Integer>();
			String processId = "";
			if(null != workItem.getParameter("log_unique_id")) {
				log_unique_id = String.valueOf(workItem.getParameter("log_unique_id"));
			}
			action = getOperationType(workItem);
			java.util.Map<String,String> idMap = (java.util.Map<String,String>) workItem.getParameter("id_map");
			java.util.Map<String,String> idMapNc = (java.util.Map<String,String>) workItem.getParameter("id_map_nc");
			logger.info("Creating Internal workflow for :" + action +"IDs --->"+idMap);
			String depId = prop.getInternalCustomerDeploymentId();
			String ncDepId = prop.getNcInternalCustomerDeploymentId();
			if(SOFTCEASE.equalsIgnoreCase(action)){
				processId= "customer-internal.soft-cease";
				workflowIdMap.putAll(startAll(idMap, "connection_id", "connection_type", processId,depId));
				workflowIdMap.putAll(startAll(idMapNc, "connection_id", "connection_type", processId,ncDepId));
			}else if(SOFTRESTORE.equalsIgnoreCase(action)){
				processId = "customer-internal.soft-restore";
				workflowIdMap.putAll(startAll(idMap, "connection_id", "connection_type", processId,depId));
				workflowIdMap.putAll(startAll(idMapNc, "connection_id", "connection_type", processId,ncDepId));
		    }else if (HARD_CEASE.equalsIgnoreCase(action)){
		    	String portOrConn = (String)workItem.getParameter("port_conn");
		    	if("connection".equalsIgnoreCase(portOrConn)){
		    		processId =  "customer-internal.hard-connection-cease";
		    		workflowIdMap.putAll(startAll(idMap, "connection_id", "connection_type", processId,depId));
		    		workflowIdMap.putAll(startAll(idMapNc, "connection_id", "connection_type", processId,ncDepId));
		    	}else if("port".equalsIgnoreCase(portOrConn)){
		    		workflowIdMap.putAll(startAll(idMap, "port_id", "port_type", "",ncDepId));
		    	}
		    	 
		    }
			
			Map<String,Object> retval = new HashMap<String,Object>();
			retval.put("Result", workflowIdMap);//set to 'workflow_id_map'
			manager.completeWorkItem(workItem.getId(), retval);
			
		} catch (Exception e) {
			logger.error("WIHStartSubProcess -> Fail ", e);
		}
	}
	
	private Integer startSubProcess(String processId,Map<String, Object> params,String deplymentId){
		return new JBPMClient(getJbpmUrl(prop.getIntnalJBPMUrl()),
				prop.getIntnalJBPMUsername(),
				prop.getIntnalJBPMPassword()).startProcess(deplymentId, processId, params);
	}
	
	private Map<String,Integer> startAll(Map<String,String> idMap,String firstKey,String secondKey,String processId,String deploymentId){
		Map<String, Integer> workflowIdMap = new TreeMap<String, Integer>();
		logger.info("Starting no.of work flows|ID|Type >>" + idMap.size()+"|"+firstKey+"|"+secondKey);
		for (Map.Entry<String,String> entry: idMap.entrySet()) {
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put("customer_id", getCustomerId(workItem));
			params.put(firstKey, entry.getKey());
			params.put(secondKey, entry.getValue());
			params.put("log_unique_id",log_unique_id);
			logger.info("Work flows for >>" +entry.getKey() +"::"+entry.getValue());
			if("port_type".equals(secondKey)) {
				if("AWS_DEDICATED".equals(entry.getValue()))
				  processId = "customer-internal.aws-deicated-port-hard-cease";
				else
					processId = "customer-internal.hard-port-cease";
			}
			Integer instanceId = startSubProcess(processId, params,deploymentId);
			workflowIdMap.put(entry.getKey()+";"+deploymentId, instanceId);
			logger.info("Internal workflow ID for :" + entry.getKey()+";"+deploymentId + " " + instanceId);
		}
		return workflowIdMap;
	}


	
}
