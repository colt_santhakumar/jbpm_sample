package net.colt.novitas;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.HandlerUtils;
import com.colt.novitas.client.ServiceAPIClient;
import com.colt.novitas.exception.NovitasHandlerException;
import com.colt.novitas.response.CloudConnectionResponse;

public class WIHGetDCAConnections extends CeaseBaseWorkItemHandler {
	
	private static Logger logger = LoggerFactory.getLogger(WIHGetDCAConnections.class);
	
	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}
	

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
		
		try {
			EnvironmentProperties prop = getEnvProperties(workItem);
			logger = getLogger(workItem, prop.getLogUniqueId());
			String customerId = getCustomerId(workItem);
            List<CloudConnectionResponse> response = new ServiceAPIClient(prop.getServiceAPIBaseUrl(), 
						prop.getServiceAPIUsername(), 
						prop.getServiceAPIPassword()).getActiveDCAConnections(customerId);
			
			logger.info("Get Active Cloud Connection :" + response);
			
			/*if (!(response instanceof CloudConnectionResponse)) {
				throw new NovitasHandlerException();
			}*/
			Map<String,Object> retval = new HashMap<String,Object>();
			Map<String,String> connectionIdMap = new HashMap<String,String>();
			Map<String,String> ncConnectionIdMap = new HashMap<String,String>();
			for (CloudConnectionResponse connectionResponse : response) {
				if(ACTIVE.equals(connectionResponse.getStatus().name())){
					if(StringUtils.isNotBlank(connectionResponse.getNcTechServiceId1()) || StringUtils.isNotBlank(connectionResponse.getNcTechServiceId2())){
						ncConnectionIdMap.put(connectionResponse.getConnectionId(),"DCACONNECTION");
					}else {
					    connectionIdMap.put(connectionResponse.getConnectionId(),"DCACONNECTION");
					}
					retval.put("novitas_service_id", connectionResponse.getServiceId());
				}
			}
			logger.info("Active DCA connection map details : "+connectionIdMap);
			retval.put("dca_connection_map", connectionIdMap);
			retval.put("dca_nc_connection_map", ncConnectionIdMap);
			
			manager.completeWorkItem(workItem.getId(), retval);
			
		} catch (Exception e) {
			logger.error("WIHGetDCAConnections -> Fail : ", e.getMessage());
			HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(getErrorCode(workItem)));
		}
	}
	

}
