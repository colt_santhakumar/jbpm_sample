package net.colt.novitas;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Map;
import java.util.TreeMap;

import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.HandlerUtils;
import com.colt.novitas.WorkflowType;
import com.colt.novitas.exception.HandlerErrorCode;

@SuppressWarnings("unchecked")
public abstract class CeaseBaseWorkItemHandler implements WorkItemHandler {
	
	private static Logger logger = LoggerFactory.getLogger(CeaseBaseWorkItemHandler.class);
	
	protected static final  String ACTIVE  = "ACTIVE";
	protected static final String  SOFTCEASE = "SOFTCEASE";
	protected static final String SOFTRESTORE = "SOFTRESTORE";
	protected static final String HARD_CEASE = "HARDCEASE";
	protected static final String SOFTCEASE_INPROGRESS = "SOFTCEASE_INPROGRESS";
	protected static final String SOFTRESTORE_INPROGRESS = "SOFTRESTORE_INPROGRESS";
	protected static final String HARDCEASE_INPROGRESS = "HARDCEASE_INPROGRESS";
	
	protected static String LOG_UNIQUE_ID = null;
	
	protected EnvironmentProperties getEnvProperties(WorkItem workItem){
		return (EnvironmentProperties) workItem.getParameter("env_properties");
	}
	
	protected Logger getLogger(WorkItem workItem, String uniqueLogId){
		if(null == LOG_UNIQUE_ID) {
			LOG_UNIQUE_ID = uniqueLogId;
			MDC.put("unique_log_id","J"+uniqueLogId.substring(1));
		}
		logger = LoggerFactory.getLogger(uniqueLogId+"|ProcessId-"+ workItem.getProcessInstanceId()+"|"+workItem.getName());
		return logger;
	}
	
	protected String getCustomerId(WorkItem workItem){
		return (String) workItem.getParameter("customer_id");
	}
	 	
	protected String getStatus(WorkItem workItem){
		return (String) workItem.getParameter("status");
	}
	
	protected String getAction (WorkItem workItem){
		
		return (String) workItem.getParameter("action");
	}
	
	protected Integer getRequestId(WorkItem workItem) {
		Object obj = workItem.getParameter("request_id");
		Integer requestId = null;
		if (obj instanceof String) {
			requestId = Integer.valueOf(String.valueOf(obj));
		}
		else {
			requestId = (Integer) obj;
		}
		logger.info("Request Id : " + requestId);

		return requestId;
	}
	
	protected String getProcessDefId(WorkItem workItem){
		return (String) workItem.getParameter("process_def_id");
	}
	
	protected Integer getStatusCode(WorkItem workItem) {
		return Integer.valueOf((String)workItem.getParameter("status_code"));
	}
	
	protected Exception getWorkflowError(WorkItem workItem) {
		Object obj = workItem.getParameter("error_object");
		if (obj instanceof Exception) {
			return (Exception) obj;
		}
		return null;
	}
	
	
	protected HandlerErrorCode getErrorCode(WorkItem workItem) {
		WorkflowType type = HandlerUtils.getWorkflowType(workItem);
		if (type == WorkflowType.DELETE_PORT) {
			return HandlerErrorCode.DP_UPADTE_REQUEST_INPROGRESS;
		} 
		return HandlerErrorCode.CP_UPADTE_REQUEST_INPROGRESS;
	}
	
	 
	 protected String getTargetAction(WorkItem workItem) {
		 return (String) workItem.getParameter("target_action");
	 }
	 
	
	protected Map<String,Integer> getWorkFlowsIdMap(WorkItem workItem){
		 
		 return (TreeMap<String,Integer>) workItem.getParameter("workflow_id_map");
	 }
	
    protected Map<Integer,String> getReportMap(WorkItem workItem){
		 
		 return (TreeMap<Integer,String>) workItem.getParameter("report_map");
	 }
	
	 protected String getOperationType(WorkItem workItem){
		 return (String) workItem.getParameter("operation_type");
	 }
	 
	 
	 private Integer getIntValue(Object val) {
			if (val instanceof Integer) {
				logger.info("Integer value : " + val);
				return (Integer) val;
			}
			
			logger.info("String value : " + Integer.valueOf(String.valueOf(val)));
			return Integer.valueOf(String.valueOf(val));
		}
	 
	 public static String getJbpmUrl(String url){
			try {
				String host = InetAddress.getLocalHost().getHostName();
				url = url.replaceAll("%s", host);
			} catch (UnknownHostException e) {
				logger.error(e.getMessage());
			}
			return url;
		}
	 
	}

    
