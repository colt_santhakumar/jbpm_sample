package net.colt.novitas;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.HandlerUtils;
import com.colt.novitas.client.ServiceAPIClient;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;
import com.colt.novitas.response.CloudPortResponse;

public class WIHGetDCAServicePort extends CeaseBaseWorkItemHandler {
	
	private static Logger logger = LoggerFactory.getLogger(WIHGetDCAServicePort.class);
	
	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {

		try {
			EnvironmentProperties prop = getEnvProperties(workItem);
			logger = getLogger(workItem, prop.getLogUniqueId());
			String cutomerId = getCustomerId(workItem);
			
			List<CloudPortResponse> result = new ServiceAPIClient(prop.getServiceAPIBaseUrl(), 
												 prop.getServiceAPIUsername(), 
												 prop.getServiceAPIPassword()).getActiveCloudPorts(cutomerId); 
			
			logger.info("Get Active DCA customer port call successfull :" + result);
			Map<String,Object> retval = new HashMap<String,Object>();
			Map<String,String> portMap = new HashMap<String,String>();
			for (CloudPortResponse portResponse : result) {
				if (ACTIVE.equals(portResponse.getStatus().name())) {
					portMap.put(portResponse.getId(),portResponse.getCloudProvider());
					retval.put("novitas_service_id", portResponse.getServiceId());
				}
			}
			retval.put("Result", portMap);
			manager.completeWorkItem(workItem.getId(), retval);
			
		} catch (Exception e) {
			logger.error("WIHGetServicePort -> Fail : ", e);
			HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(HandlerErrorCode.DP_NO_MATCHING_SERVICE_PORT));
		}
	}
	

}
