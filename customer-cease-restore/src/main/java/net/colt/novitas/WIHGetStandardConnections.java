package net.colt.novitas;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.HandlerUtils;
import com.colt.novitas.client.ServiceAPIClient;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;
import com.colt.novitas.response.ConnectionResponse;

public class WIHGetStandardConnections extends CeaseBaseWorkItemHandler {
	
	private static Logger logger = LoggerFactory.getLogger(WIHGetStandardConnections.class);
	
	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}
	

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
		
		try {
			EnvironmentProperties prop = getEnvProperties(workItem);
			logger = getLogger(workItem, prop.getLogUniqueId());
			String customerId = getCustomerId(workItem);
			List<ConnectionResponse> response = new ServiceAPIClient(prop.getServiceAPIBaseUrl(), 
                                                               prop.getServiceAPIUsername(), 
                                                               prop.getServiceAPIPassword()).getActiveConnections(customerId);
			
			logger.info("Get Active Connections :" + response);
			
			/*if (!(response instanceof ConnectionResponse)) {
				throw new NovitasHandlerException();
			}*/
			Map<String,String> connMap = new HashMap<String,String>();
			Map<String,String> ncConnMap = new HashMap<String,String>();
			Map<String,Object> retval = new HashMap<String,Object>();
			for (ConnectionResponse connectionResponse : response) {
				if(ACTIVE.equals(connectionResponse.getStatus().name())){
					if(StringUtils.isNotBlank(connectionResponse.getNcTechServiceId())){
						ncConnMap.put(connectionResponse.getConnectionId(),connectionResponse.getConnectionType());
					}else {
						connMap.put(connectionResponse.getConnectionId(),connectionResponse.getConnectionType());
					}
					retval.put("novitas_service_id", connectionResponse.getServiceId());
					
				}
			}
			logger.info("Active Standard connection map details : "+connMap);
			retval.put("std_connection_map", connMap);
			retval.put("std_nc_connection_map", ncConnMap);
			
			manager.completeWorkItem(workItem.getId(), retval);
			
		} catch (Exception e) {
			logger.error("WIHGetStandardConnections -> Fail : ", e.getMessage());
			HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(HandlerErrorCode.DC_GET_SERVICE_CONNECTION));
		}
	}
	

}
