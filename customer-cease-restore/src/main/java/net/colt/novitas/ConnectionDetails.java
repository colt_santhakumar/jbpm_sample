package net.colt.novitas;

import java.io.Serializable;

import com.amazonaws.util.StringUtils;

public class ConnectionDetails implements Serializable{
  
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String connectionId;
	private String type;
	private String ncTechServiceId1;
	private String ncTechServiceId2;
	private String circuitId1;
	private String circuitId2;
	private Integer bandwidth;
	private String nms;
	private Integer ceaseReqId;
	
	public String getConnectionId() {
		return connectionId;
	}
	public void setConnectionId(String connectionId) {
		this.connectionId = connectionId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getNcTechServiceId1() {
		return ncTechServiceId1;
	}
	public void setNcTechServiceId1(String ncTechServiceId1) {
		this.ncTechServiceId1 = ncTechServiceId1;
	}
	public String getNcTechServiceId2() {
		return ncTechServiceId2;
	}
	public void setNcTechServiceId2(String ncTechServiceId2) {
		this.ncTechServiceId2 = ncTechServiceId2;
	}
	
	public String getCircuitId1() {
		return circuitId1;
	}
	public void setCircuitId1(String circuitId1) {
		this.circuitId1 = circuitId1;
	}
	public String getCircuitId2() {
		return circuitId2;
	}
	public void setCircuitId2(String circuitId2) {
		this.circuitId2 = circuitId2;
	}
	public Integer getBandwidth() {
		return bandwidth;
	}
	public void setBandwidth(Integer bandwidth) {
		this.bandwidth = bandwidth;
	}
	public boolean isLegacy() {
		return !StringUtils.isNullOrEmpty(this.circuitId1) ;
	}
	public String getNms() {
		return nms;
	}
	public void setNms(String nms) {
		this.nms = nms;
	}
	public Integer getCeaseReqId() {
		return ceaseReqId;
	}
	public void setCeaseReqId(Integer ceaseReqId) {
		this.ceaseReqId = ceaseReqId;
	}
	
	
}
