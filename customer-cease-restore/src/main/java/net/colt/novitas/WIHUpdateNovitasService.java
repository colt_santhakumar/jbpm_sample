package net.colt.novitas;

import java.util.HashMap;
import java.util.Map;

import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.HandlerUtils;
import com.colt.novitas.client.ServiceAPIClient;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;

/**
 * @author DRao This is a WIH file created by Dharrma Rao.
 * 
 */
public class WIHUpdateNovitasService extends CeaseBaseWorkItemHandler {

	private static Logger logger = LoggerFactory.getLogger(WIHUpdateNovitasService.class);

	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {

		try {
			EnvironmentProperties prop = getEnvProperties(workItem);
			logger = getLogger(workItem, prop.getLogUniqueId());
			String novitasServiceId = (String) workItem.getParameter("novitas_service_id");
			String servicestatus = (String) workItem.getParameter("service_status");
            String status = getStatus(workItem);
			ServiceAPIClient client = new ServiceAPIClient(prop.getServiceAPIBaseUrl(), 
                                      prop.getServiceAPIUsername(), 
                                      prop.getServiceAPIPassword());
 
			Object response = client.updateServiceStatus(novitasServiceId,servicestatus,status);

			if ("FAILED".equals(response)) {
				logger.info("Failed to Update NovitasService Errocode status");
				throw new NovitasHandlerException();
			}

			Map<String, Object> retval = new HashMap<String, Object>();
			manager.completeWorkItem(workItem.getId(), retval);

		} catch (Exception e) {
			logger.error("WIHUpdateNovitasService -> Failed", e);
			if("true".equals(String.valueOf(workItem.getParameter("is_error_handling")))) {
				manager.abortWorkItem(workItem.getId());
			}else {
				HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(
						HandlerErrorCode.NOVITAS_SERVICE_ERROR_DETAILS));
			}
			
		}
	}

}