package net.colt.novitas;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.HandlerUtils;
import com.colt.novitas.client.ServiceAPIClient;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;
import com.colt.novitas.response.CustomerDedicatedPortResponse;

public class WIHGetStandardPorts extends CeaseBaseWorkItemHandler {
	
	private static Logger logger = LoggerFactory.getLogger(WIHGetStandardPorts.class);
	
	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {

		try {
			EnvironmentProperties prop = getEnvProperties(workItem);
			logger = getLogger(workItem, prop.getLogUniqueId());
			String customerId = getCustomerId(workItem);
			List<CustomerDedicatedPortResponse> result = new ServiceAPIClient(prop.getServiceAPIBaseUrl(), 
												         prop.getServiceAPIUsername(), 
												         prop.getServiceAPIPassword()).getActivePorts(customerId); 
			
			logger.info("Get Active DCA customer port call successfull :" + result);
			
			Map<String,Object> retval = new HashMap<String,Object>();
			Map<String,String> portMap = new HashMap<String,String>();
			for (CustomerDedicatedPortResponse portResponse : result) {
				if (ACTIVE.equals(portResponse.getStatus().name())) {
					portMap.put(portResponse.getId(),portResponse.getPortType());
					retval.put("novitas_service_id", portResponse.getServiceId());
				}
			}
			retval.put("Result",  portMap);
			manager.completeWorkItem(workItem.getId(), retval);
			
		} catch (Exception e) {
			logger.error("WIHGetCustomerPort -> Fail : ", e);
			HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(HandlerErrorCode.CC_GET_SERVICE_PORT));
		}
	}
	
	
	protected String getPortId(WorkItem workItem) {
		
		return (String) workItem.getParameter("service_port_id");
		
		
		
	}

}
