package net.colt.novitas.workitems.response;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SitePortRequestPortDetails implements Serializable {
	
	private static final long serialVersionUID = -4929975060216836608L;

	@JsonProperty("id")
	private Integer id;
	
	@JsonProperty("port_name")
	private String portName;
	
	@JsonProperty("request_id")
	private Integer requestId;
	
	@JsonProperty("bandwidth")
	private String bandwidth;
	
	@JsonProperty("connector")
	private String connector;
	
	@JsonProperty("technology")
	private String technology;
    
	@JsonProperty("product_name")
	private String productName;
	
	@JsonProperty("installation_charge")
	private Float installationCharge;
	
	@JsonProperty("requested_at")
	private Date requestedAt;

	@JsonProperty("rental_charge")
	private Float rentalCharge;

	@JsonProperty("rental_currency")
	private String rentalCurrency;

	@JsonProperty("rental_unit")
	private String rentalUnit;

	@JsonProperty("installation_currency")
	private String installationCurrency;

	@JsonProperty("decommissioning_charge")
	private Float decommissioningCharge;

	@JsonProperty("decommissioning_currency")
	private String decommissioningCurrency;

	@JsonProperty("penalty")
	private Float penalty;

	@JsonProperty("penalty_currency")
	private String penaltyCurrency;
    
	@JsonProperty("expiration_period")
	private Integer expirationPeriod;
	
	@JsonProperty("discount_percentage")
	private Float discountPercentage;

	@JsonProperty("customer_port_id")
	private String customerPortID;
	
	@JsonProperty("commitment_period")
	private Integer commitmentPeriod;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPortName() {
		return portName;
	}

	public void setPortName(String portName) {
		this.portName = portName;
	}

	public Integer getRequestId() {
		return requestId;
	}

	public void setRequestId(Integer requestId) {
		this.requestId = requestId;
	}

	public String getBandwidth() {
		return bandwidth;
	}

	public void setBandwidth(String bandwidth) {
		this.bandwidth = bandwidth;
	}

	public String getConnector() {
		return connector;
	}

	public void setConnector(String connector) {
		this.connector = connector;
	}

	public String getTechnology() {
		return technology;
	}

	public void setTechnology(String technology) {
		this.technology = technology;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Float getInstallationCharge() {
		return installationCharge;
	}

	public void setInstallationCharge(Float installationCharge) {
		this.installationCharge = installationCharge;
	}

	public Date getRequestedAt() {
		return requestedAt;
	}

	public void setRequestedAt(Date requestedAt) {
		this.requestedAt = requestedAt;
	}

	public Float getRentalCharge() {
		return rentalCharge;
	}

	public void setRentalCharge(Float rentalCharge) {
		this.rentalCharge = rentalCharge;
	}

	public String getRentalCurrency() {
		return rentalCurrency;
	}

	public void setRentalCurrency(String rentalCurrency) {
		this.rentalCurrency = rentalCurrency;
	}

	public String getRentalUnit() {
		return rentalUnit;
	}

	public void setRentalUnit(String rentalUnit) {
		this.rentalUnit = rentalUnit;
	}

	public String getInstallationCurrency() {
		return installationCurrency;
	}

	public void setInstallationCurrency(String installationCurrency) {
		this.installationCurrency = installationCurrency;
	}

	public Float getDecommissioningCharge() {
		return decommissioningCharge;
	}

	public void setDecommissioningCharge(Float decommissioningCharge) {
		this.decommissioningCharge = decommissioningCharge;
	}

	public String getDecommissioningCurrency() {
		return decommissioningCurrency;
	}

	public void setDecommissioningCurrency(String decommissioningCurrency) {
		this.decommissioningCurrency = decommissioningCurrency;
	}

	public Float getPenalty() {
		return penalty;
	}

	public void setPenalty(Float penalty) {
		this.penalty = penalty;
	}

	public String getPenaltyCurrency() {
		return penaltyCurrency;
	}

	public void setPenaltyCurrency(String penaltyCurrency) {
		this.penaltyCurrency = penaltyCurrency;
	}

	public Integer getExpirationPeriod() {
		return expirationPeriod;
	}

	public void setExpirationPeriod(Integer expirationPeriod) {
		this.expirationPeriod = expirationPeriod;
	}

	public Float getDiscountPercentage() {
		return discountPercentage;
	}

	public void setDiscountPercentage(Float discountPercentage) {
		this.discountPercentage = discountPercentage;
	}
	

	public String getCustomerPortID() {
		return customerPortID;
	}

	public void setCustomerPortID(String customerPortID) {
		this.customerPortID = customerPortID;
	}

	public Integer getCommitmentPeriod() {
		return commitmentPeriod;
	}

	public void setCommitmentPeriod(Integer commitmentPeriod) {
		this.commitmentPeriod = commitmentPeriod;
	}

	@Override
	public String toString() {
		return "SitePortRequestPortDetails [id=" + id + ", portName=" + portName + ", requestId=" + requestId
				+ ", bandwidth=" + bandwidth + ", connector=" + connector + ", technology=" + technology
				+ ", productName=" + productName + ", installationCharge=" + installationCharge + ", requestedAt="
				+ requestedAt + ", rentalCharge=" + rentalCharge + ", rentalCurrency=" + rentalCurrency
				+ ", rentalUnit=" + rentalUnit + ", installationCurrency=" + installationCurrency
				+ ", decommissioningCharge=" + decommissioningCharge + ", decommissioningCurrency="
				+ decommissioningCurrency + ", penalty=" + penalty + ", penaltyCurrency=" + penaltyCurrency
				+ ", expirationPeriod=" + expirationPeriod + ", discountPercentage=" + discountPercentage
				+ ", customerPortID=" + customerPortID + ", commitmentPeriod=" + commitmentPeriod + "]";
	}

}
