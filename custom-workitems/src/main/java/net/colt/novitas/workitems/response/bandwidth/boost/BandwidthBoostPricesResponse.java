package net.colt.novitas.workitems.response.bandwidth.boost;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;
import java.util.List;

@Getter
@Setter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class BandwidthBoostPricesResponse {

    @JsonProperty("expires_at_dt")
    private Date expiresAtDate;

    @JsonProperty("bandwidth_boost_prices")
    private List<BandwidthBoostPricesResponseDto> bandwidthBoostPricesResponseDtoList;
}

