package net.colt.novitas.workitems.response;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.ToString;
import net.colt.novitas.workitems.constants.NetworkPlatform;
import net.colt.novitas.workitems.constants.PortStatus;
import net.colt.novitas.workitems.constants.RentalUnit;

@JsonIgnoreProperties(ignoreUnknown = true)
@ToString
public class PortResponse implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private String id;
	private String name;
	private PortStatus status;
	private String location;
	private Integer usedBandwidth;
	private Integer availableBandwidth;
	private Integer bandwidth;
	private Integer noOfConnections;
	private String connector;
	private String technology;
	private String presentationLabel;
	private Float rentalCharge;
	private RentalUnit rentalUnit;
	private String rentalCurrency;
	private String address;
	private String siteType;
	private String commitmentExpiry;
	private Float penaltyCharge;
	private String serviceId;
	private String resourceId;
	private List<ServiceVLANIdRange> portVLANIdRange;
	private Boolean hasPToPMapping;
	private Integer maxAllowedConnections;
	private String customerName;
	private String ocn;
	private String siteFloor;
	private String siteRoomName;
	private String locationPremisesNumber;
	private String locationBuildingName;
	private String locationStreetName;
	private String locationCity;
	private String locationState;
	private String locationCountry;
	private String postalZipCode;
	private Float latitude;
	private Float longitude;
	private String locationId;
	private String portType;
	private Integer expirationPeriod;
	private String expiresOn;
	private Boolean inUse;

	@JsonProperty("decommissioning_charge")
	private Float decommissioningCharge;

	@JsonProperty("decommissioning_currency")
	private String decommissioningCurrency;

	@JsonProperty("loa_allowed")
	private boolean loaAllowed;

	@JsonProperty("generated_loas")
	private List<ColtLOAResponse> generatedLoas;

	@JsonProperty("nms")
	private String nms;

	@JsonProperty("local_building_name")
	private String localBuildingName;

	@JsonProperty("location_city_code")
	private String locationCityCode;

	@JsonProperty("location_country_code")
	private String locationCountryCode;

	@JsonProperty("resource_port_name")
	private String resourcePortName;

	@JsonProperty("olo_service_id")
	private String oloServiceId;

	@JsonProperty("nc_tech_service_id")
	private String ncTechServiceId;

	@JsonProperty("on_shared_site")
	private Boolean onSharedSite;

	@JsonProperty("location_building_id")
	private String locationBuildingId;

	@JsonProperty("location_building_type")
	private String locationBuildingType;

	@JsonProperty("location_building_address")
	private String locationBuildingAddress;

	@JsonProperty("cross_connect_allowed")
	private Boolean crossConnectAllowed;

	@JsonProperty("cross_connect_id")
	private String crossConnectId;

	@JsonProperty("cross_connect_request_id")
	private String crossConnectRequestId;

	@JsonProperty("refunds")
	private List<RefundResponse> refunds;

    @JsonProperty("alternate_discount_tier")
    private String alternateDiscountTier;

	@JsonProperty("network_platform")
	private NetworkPlatform networkPlatform;

	@JsonProperty("max_allowed_ipa_conn_bandwidth")
	private Integer maxAllowedIpaConnBandwidth;

	public PortResponse() {
		super();
	}

	@JsonProperty("expiration_period")
	public Integer getExpirationPeriod() {
		return expirationPeriod;
	}

	public void setExpirationPeriod(Integer expirationPeriod) {
		this.expirationPeriod = expirationPeriod;
	}

	@JsonProperty("expires_on")
	public String getExpiresOn() {
		return expiresOn;
	}

	public void setExpiresOn(String expiresOn) {
		this.expiresOn = expiresOn;
	}

	@JsonProperty("in_use")
	public Boolean getInUse() {
		return inUse;
	}

	public void setInUse(Boolean inUse) {
		this.inUse = inUse;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("status")
	public PortStatus getStatus() {
		return status;
	}

	public void setStatus(PortStatus status) {
		this.status = status;
	}

	@JsonProperty("location")
	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	@JsonProperty("used_bandwidth")
	public Integer getUsedBandwidth() {
		return usedBandwidth;
	}

	public void setUsedBandwidth(Integer usedBandwidth) {
		this.usedBandwidth = usedBandwidth;
	}

	@JsonProperty("available_bandwidth")
	public Integer getAvailableBandwidth() {
		return availableBandwidth;
	}

	public void setAvailableBandwidth(Integer availableBandwidth) {
		this.availableBandwidth = availableBandwidth;
	}

	@JsonProperty("bandwidth")
	public Integer getBandwidth() {
		return bandwidth;
	}

	public void setBandwidth(Integer bandwidth) {
		this.bandwidth = bandwidth;
	}

	@JsonProperty("no_of_connections")
	public Integer getNoOfConnections() {
		return noOfConnections;
	}

	public void setNoOfConnections(Integer noOfConnections) {
		this.noOfConnections = noOfConnections;
	}

	@JsonProperty("connector")
	public String getConnector() {
		return connector;
	}

	public void setConnector(String connector) {
		this.connector = connector;
	}

	@JsonProperty("technology")
	public String getTechnology() {
		return technology;
	}

	public void setTechnology(String technology) {
		this.technology = technology;
	}

	@JsonProperty("presentation_label")
	public String getPresentationLabel() {
		return presentationLabel;
	}

	public void setPresentationLabel(String presentationLabel) {
		this.presentationLabel = presentationLabel;
	}

	@JsonProperty("rental_charge")
	public Float getRentalCharge() {
		return rentalCharge;
	}

	public void setRentalCharge(Float rentalCharge) {
		this.rentalCharge = rentalCharge;
	}

	@JsonProperty("rental_unit")
	public RentalUnit getRentalUnit() {
		return rentalUnit;
	}

	public void setRentalUnit(RentalUnit rentalUnit) {
		this.rentalUnit = rentalUnit;
	}

	@JsonProperty("rental_currency")
	public String getRentalCurrency() {
		return rentalCurrency;
	}

	public void setRentalCurrency(String rentalCurrency) {
		this.rentalCurrency = rentalCurrency;
	}

	@JsonProperty("address")
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@JsonProperty("site_type")
	public String getSiteType() {
		return siteType;
	}

	public void setSiteType(String siteType) {
		this.siteType = siteType;
	}

	@JsonProperty("commitment_expiry")
	public String getCommitmentExpiry() {
		return commitmentExpiry;
	}

	public void setCommitmentExpiry(String commitmentExpiry) {
		this.commitmentExpiry = commitmentExpiry;
	}

	@JsonProperty("penalty_charge")
	public Float getPenaltyCharge() {
		return penaltyCharge;
	}

	public void setPenaltyCharge(Float penaltyCharge) {
		this.penaltyCharge = penaltyCharge;
	}

	@JsonProperty("service_id")
	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	@JsonProperty("resource_id")
	public String getResourceId() {
		return this.resourceId;
	}

	public void setResourceId(String resourceId) {
		this.resourceId = resourceId;
	}

	@JsonProperty("port_vlan_id_range")
	public List<ServiceVLANIdRange> getPortVLANIdRange() {
		return portVLANIdRange;
	}

	public void setPortVLANIdRange(List<ServiceVLANIdRange> portVLANIdRange) {
		this.portVLANIdRange = portVLANIdRange;
	}

	@JsonProperty("has_p_to_p_mapping")
	public Boolean getHasPToPMapping() {
		return hasPToPMapping;
	}

	public void setHasPToPMapping(Boolean hasPToPMapping) {
		this.hasPToPMapping = hasPToPMapping;
	}

	@JsonProperty("max_allowed_connections")
	public Integer getMaxAllowedConnections() {
		return maxAllowedConnections;
	}

	public void setMaxAllowedConnections(Integer maxAllowedConnections) {
		this.maxAllowedConnections = maxAllowedConnections;
	}

	@JsonProperty("customer_name")
	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	@JsonProperty("ocn")
	public String getOcn() {
		return ocn;
	}

	public void setOcn(String ocn) {
		this.ocn = ocn;
	}

	@JsonProperty("site_floor")
	public String getSiteFloor() {
		return siteFloor;
	}

	public void setSiteFloor(String siteFloor) {
		this.siteFloor = siteFloor;
	}

	@JsonProperty("site_room_name")
	public String getSiteRoomName() {
		return siteRoomName;
	}

	public void setSiteRoomName(String siteRoomName) {
		this.siteRoomName = siteRoomName;
	}

	@JsonProperty("location_premises_number")
	public String getLocationPremisesNumber() {
		return locationPremisesNumber;
	}

	public void setLocationPremisesNumber(String locationPremisesNumber) {
		this.locationPremisesNumber = locationPremisesNumber;
	}

	@JsonProperty("location_state")
	public String getLocationState() {
		return locationState;
	}

	public void setLocationState(String locationState) {
		this.locationState = locationState;
	}

	@JsonProperty("postal_zip_code")
	public String getPostalZipCode() {
		return postalZipCode;
	}

	public void setPostalZipCode(String postalZipCode) {
		this.postalZipCode = postalZipCode;
	}

	@JsonProperty("location_building_name")
	public String getLocationBuildingName() {
		return locationBuildingName;
	}

	public void setLocationBuildingName(String locationBuildingName) {
		this.locationBuildingName = locationBuildingName;
	}

	@JsonProperty("location_street_name")
	public String getLocationStreetName() {
		return locationStreetName;
	}

	public void setLocationStreetName(String locationStreetName) {
		this.locationStreetName = locationStreetName;
	}

	@JsonProperty("location_city")
	public String getLocationCity() {
		return locationCity;
	}

	public void setLocationCity(String locationCity) {
		this.locationCity = locationCity;
	}

	@JsonProperty("location_country")
	public String getLocationCountry() {
		return locationCountry;
	}

	public void setLocationCountry(String locationCountry) {
		this.locationCountry = locationCountry;
	}

	@JsonProperty("latitude")
	public Float getLatitude() {
		return latitude;
	}

	public void setLatitude(Float latitude) {
		this.latitude = latitude;
	}

	@JsonProperty("longitude")
	public Float getLongitude() {
		return longitude;
	}

	public void setLongitude(Float longitude) {
		this.longitude = longitude;
	}

	@JsonProperty("location_id")
	public String getLocationId() {
		return locationId;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	@JsonProperty(value = "port_type")
	public String getPortType() {
		return portType;
	}

	public void setPortType(String portType) {
		this.portType = portType;
	}

	public Float getDecommissioningCharge() {
		return decommissioningCharge;
	}

	public void setDecommissioningCharge(Float decommissioningCharge) {
		this.decommissioningCharge = decommissioningCharge;
	}

	public String getDecommissioningCurrency() {
		return decommissioningCurrency;
	}

	public void setDecommissioningCurrency(String decommissioningCurrency) {
		this.decommissioningCurrency = decommissioningCurrency;
	}

	public List<ColtLOAResponse> getGeneratedLoas() {
		return generatedLoas;
	}

	public void setGeneratedLoas(List<ColtLOAResponse> generatedLoas) {
		this.generatedLoas = generatedLoas;
	}

	public boolean isLoaAllowed() {
		return loaAllowed;
	}

	public void setLoaAllowed(boolean loaAllowed) {
		this.loaAllowed = loaAllowed;
	}

	public String getNms() {
		return nms;
	}

	public void setNms(String nms) {
		this.nms = nms;
	}

	public String getLocalBuildingName() {
		return localBuildingName;
	}

	public void setLocalBuildingName(String localBuildingName) {
		this.localBuildingName = localBuildingName;
	}

	public String getLocationCityCode() {
		return locationCityCode;
	}

	public void setLocationCityCode(String locationCityCode) {
		this.locationCityCode = locationCityCode;
	}

	public String getLocationCountryCode() {
		return locationCountryCode;
	}

	public void setLocationCountryCode(String locationCountryCode) {
		this.locationCountryCode = locationCountryCode;
	}

	public String getResourcePortName() {
		return resourcePortName;
	}

	public void setResourcePortName(String resourcePortName) {
		this.resourcePortName = resourcePortName;
	}

	public String getOloServiceId() {
		return oloServiceId;
	}

	public void setOloServiceId(String oloServiceId) {
		this.oloServiceId = oloServiceId;
	}

	public String getNcTechServiceId() {
		return ncTechServiceId;
	}

	public void setNcTechServiceId(String ncTechServiceId) {
		this.ncTechServiceId = ncTechServiceId;
	}

	public Boolean getOnSharedSite() {
		return onSharedSite;
	}

	public void setOnSharedSite(Boolean onSharedSite) {
		this.onSharedSite = onSharedSite;
	}

	public String getLocationBuildingId() {
		return locationBuildingId;
	}

	public void setLocationBuildingId(String locationBuildingId) {
		this.locationBuildingId = locationBuildingId;
	}

	public String getLocationBuildingType() {
		return locationBuildingType;
	}

	public void setLocationBuildingType(String locationBuildingType) {
		this.locationBuildingType = locationBuildingType;
	}

	public String getLocationBuildingAddress() {
		return locationBuildingAddress;
	}

	public void setLocationBuildingAddress(String locationBuildingAddress) {
		this.locationBuildingAddress = locationBuildingAddress;
	}

	public Boolean isCrossConnectAllowed() {
		return crossConnectAllowed;
	}

	public void setCrossConnectAllowed(Boolean crossConnectAllowed) {
		this.crossConnectAllowed = crossConnectAllowed;
	}

	public String getCrossConnectId() {
		return crossConnectId;
	}

	public void setCrossConnectId(String crossConnectId) {
		this.crossConnectId = crossConnectId;
	}

	public String getCrossConnectRequestId() {
		return crossConnectRequestId;
	}

	public void setCrossConnectRequestId(String crossConnectRequestId) {
		this.crossConnectRequestId = crossConnectRequestId;
	}

	public Boolean getCrossConnectAllowed() {
		return crossConnectAllowed;
	}

	public List<RefundResponse> getRefunds() {
		return refunds;
	}

	public void setRefunds(List<RefundResponse> refunds) {
		this.refunds = refunds;
	}

	public String getAlternateDiscountTier() {
		return alternateDiscountTier;
	}

	public void setAlternateDiscountTier(String alternateDiscountTier) {
		this.alternateDiscountTier = alternateDiscountTier;
	}

	public NetworkPlatform getNetworkPlatform() {
		return networkPlatform;
	}

	public void setNetworkPlatform(NetworkPlatform networkPlatform) {
		this.networkPlatform = networkPlatform;
	}

	public Integer getMaxAllowedIpaConnBandwidth() {
		return maxAllowedIpaConnBandwidth;
	}

	public void setMaxAllowedIpaConnBandwidth(Integer maxAllowedIpaConnBandwidth) {
		this.maxAllowedIpaConnBandwidth = maxAllowedIpaConnBandwidth;
	}

}