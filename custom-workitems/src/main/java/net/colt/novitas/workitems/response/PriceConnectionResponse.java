package net.colt.novitas.workitems.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

//TODO: shridhar review if this needs update
@JsonIgnoreProperties(ignoreUnknown = true)
public class PriceConnectionResponse implements Serializable{

	private static final long serialVersionUID = -5626753536285641284L;
	
	private Integer id = null;
	private String description = null;
	private String siteType1 = null;
	private String siteType2 = null;
	private Float rentalCharge = null;
	private String rentalUnit = null;
	private String rentalCurrency = null;
	private Float installationCharge = null;
	private String installationCurrency = null;
	private Float modificationCharge = null;
	private String modificationCurrency = null;
	private Float decommissioningCharge = null;
	private String decommissioningCurrency = null;
	private String comment = null;
	private Integer commitmentPeriod = null;
	private Integer bandwidth = null;
	private Boolean coterminusOption;
	private String availability;

	@JsonProperty("product_name")
	private String productName;

	@JsonProperty("connector")
	private String connector;

	@JsonProperty("expiration_period")
	private Integer expirationPeriod;


	@JsonProperty("service")
	private String service;



	@JsonProperty("building_id")
	private String buildingId;

	@JsonProperty("monthly_rental_charge")
	private Float monthlyRentalCharge;

	@JsonProperty("monthly_rental_currency")
	private String monthlyRentalCurrency;



	public PriceConnectionResponse() {
		super();
	}

	/*public PriceConnectionResponse(Integer id, String description, String siteType1, String siteType2,
			Float rentalCharge, String rentalUnit, String rentalCurrency, Float installationCharge,
			String installationCurrency, Float modificationCharge, String modificationCurrency,
			Float decommissioningCharge, String decommissioningCurrency, String comment, Integer commitmentPeriod,
			Integer bandwidth, Boolean coterminusOption, PriceAvailability availability) {
		super();
		this.id = id;
		this.description = description;
		this.siteType1 = siteType1;
		this.siteType2 = siteType2;
		this.rentalCharge = rentalCharge;
		this.rentalUnit = rentalUnit;
		this.rentalCurrency = rentalCurrency;
		this.installationCharge = installationCharge;
		this.installationCurrency = installationCurrency;
		this.modificationCharge = modificationCharge;
		this.modificationCurrency = modificationCurrency;
		this.decommissioningCharge = decommissioningCharge;
		this.decommissioningCurrency = decommissioningCurrency;
		this.comment = comment;
		this.commitmentPeriod = commitmentPeriod;
		this.bandwidth = bandwidth;
		this.coterminusOption = coterminusOption;
		this.availability = availability;
	}*/

	@JsonProperty("id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@JsonProperty("description")
	public String getDescription() {
		return description;
	}

	public String getName(){
		//might be wrong !!!!
		//TODO  check
		//price name == product name ??
		return productName;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSiteType1() {
		return siteType1;
	}

	public void setSiteType1(String siteType1) {
		this.siteType1 = siteType1;
	}

	public String getSiteType2() {
		return siteType2;
	}

	public void setSiteType2(String siteType2) {
		this.siteType2 = siteType2;
	}

	@JsonProperty("rental_charge")
	public Float getRentalCharge() {
		return rentalCharge;
	}

	public void setRentalCharge(Float rentalCharge) {
		this.rentalCharge = rentalCharge;
	}

	@JsonProperty("rental_unit")
	public String getRentalUnit() {
		return rentalUnit;
	}

	public void setRentalUnit(String setRentalUnit) {
		this.rentalUnit = setRentalUnit;
	}

	@JsonProperty("rental_currency")
	public String getRentalCurrency() {
		return rentalCurrency;
	}

	public void setRentalCurrency(String rentalCurrency) {
		this.rentalCurrency = rentalCurrency;
	}

	@JsonProperty("installation_charge")
	public Float getInstallationCharge() {
		return installationCharge;
	}

	public void setInstallationCharge(Float installationCharge) {
		this.installationCharge = installationCharge;
	}

	@JsonProperty("installation_currency")
	public String getInstallationCurrency() {
		return installationCurrency;
	}

	public void setInstallationCurrency(String installationCurrency) {
		this.installationCurrency = installationCurrency;
	}

	@JsonProperty("modification_charge")
	public Float getModificationCharge() {
		return modificationCharge;
	}

	public void setModificationCharge(Float modificationCharge) {
		this.modificationCharge = modificationCharge;
	}

	@JsonProperty("modification_currency")
	public String getModificationCurrency() {
		return modificationCurrency;
	}

	public void setModificationCurrency(String modificationCurrency) {
		this.modificationCurrency = modificationCurrency;
	}

	@JsonProperty("decommissioning_charge")
	public Float getDecommissioningCharge() {
		return decommissioningCharge;
	}

	public void setDecommissioningCharge(Float decommissioningCharge) {
		this.decommissioningCharge = decommissioningCharge;
	}

	@JsonProperty("decommissioning_currency")
	public String getDecommissioningCurrency() {
		return decommissioningCurrency;
	}

	public void setDecommissioningCurrency(String decommissioningCurrency) {
		this.decommissioningCurrency = decommissioningCurrency;
	}

	@JsonProperty("comment")
	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	@JsonProperty("commitment_period")
	public Integer getCommitmentPeriod() {
		return commitmentPeriod;
	}

	public void setCommitmentPeriod(Integer commitmentPeriod) {
		this.commitmentPeriod = commitmentPeriod;
	}

	@JsonProperty("bandwidth")
	public Integer getBandwidth() {
		return bandwidth;
	}

	public void setBandwidth(Integer bandwidth) {
		this.bandwidth = bandwidth;
	}

	@JsonProperty("coterminus_option")
	public Boolean getCoterminusOption() {
		return coterminusOption;
	}

	public void setCoterminusOption(Boolean coterminusOption) {
		this.coterminusOption = coterminusOption;
	}

	@JsonProperty("availability")
	public String getAvailability() {
		return availability;
	}

	public void setAvailability(String availability) {
		this.availability = availability;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getConnector() {
		return connector;
	}

	public void setConnector(String connector) {
		this.connector = connector;
	}

	public Integer getExpirationPeriod() {
		return expirationPeriod;
	}

	public void setExpirationPeriod(Integer expirationPeriod) {
		this.expirationPeriod = expirationPeriod;
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public String getBuildingId() {
		return buildingId;
	}

	public void setBuildingId(String buildingId) {
		this.buildingId = buildingId;
	}

	public Float getMonthlyRentalCharge() {
		return monthlyRentalCharge;
	}

	public void setMonthlyRentalCharge(Float monthlyRentalCharge) {
		this.monthlyRentalCharge = monthlyRentalCharge;
	}

	public String getMonthlyRentalCurrency() {
		return monthlyRentalCurrency;
	}

	public void setMonthlyRentalCurrency(String monthlyRentalCurrency) {
		this.monthlyRentalCurrency = monthlyRentalCurrency;
	}

}
