package net.colt.novitas.workitems.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CreatePortAndConnectionResponse implements Serializable {
	
	
	private static final long serialVersionUID = 1L;
	
	private String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public CreatePortAndConnectionResponse() {
		super();
	}

	public CreatePortAndConnectionResponse(String id) {
		super();
		this.id = id;
	}

}
