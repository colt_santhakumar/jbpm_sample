package net.colt.novitas.workitems.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LocationAddressBookResponse implements Serializable{

	private static final long serialVersionUID = 1L;

	@JsonProperty("id")
	private String siteId;

	@JsonProperty("name")
	private String siteName;

	@JsonProperty("address")
	private String siteAddress;

	@JsonProperty("city")
	private String siteCity;

	@JsonProperty("country")
	private String siteCountry;

	@JsonProperty("number_of_ports")
	private Integer noOfPorts;

	@JsonProperty("customer_site_name")
	private String customerSiteName;

	@JsonProperty("site_type")
	private String siteType;

	@JsonProperty("private_site")
	private Boolean privateSite;

	@JsonProperty("building_id")
	private String buildingId;

	@JsonProperty("building_name")
	private String buildingName;

	@JsonProperty("validated")
	private Boolean validated;

	@JsonProperty("building_type")
	private String buildingType;

	@JsonProperty("room")
	private String room;

	@JsonProperty("floor")
	private String floor;

	public LocationAddressBookResponse() {
		super();
	}

	public String getSiteId() {
		return siteId;
	}

	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}

	public String getSiteName() {
		return siteName;
	}

	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}

	public String getSiteAddress() {
		return siteAddress;
	}

	public void setSiteAddress(String siteAddress) {
		this.siteAddress = siteAddress;
	}

	public String getSiteCity() {
		return siteCity;
	}

	public void setSiteCity(String siteCity) {
		this.siteCity = siteCity;
	}

	public String getSiteCountry() {
		return siteCountry;
	}

	public void setSiteCountry(String siteCountry) {
		this.siteCountry = siteCountry;
	}

	public Integer getNoOfPorts() {
		return noOfPorts;
	}

	public void setNoOfPorts(Integer noOfPorts) {
		this.noOfPorts = noOfPorts;
	}

	public String getCustomerSiteName() {
		return customerSiteName;
	}

	public void setCustomerSiteName(String customerSiteName) {
		this.customerSiteName = customerSiteName;
	}

	public String getSiteType() {
		return siteType;
	}

	public void setSiteType(String siteType) {
		this.siteType = siteType;
	}

	public Boolean getPrivateSite() {
		return privateSite;
	}

	public void setPrivateSite(Boolean privateSite) {
		this.privateSite = privateSite;
	}

	public String getBuildingId() {
		return buildingId;
	}

	public void setBuildingId(String buildingId) {
		this.buildingId = buildingId;
	}

	public String getBuildingName() {
		return buildingName;
	}

	public void setBuildingName(String buildingName) {
		this.buildingName = buildingName;
	}

	public Boolean getValidated() {
		return validated;
	}

	public void setValidated(Boolean validated) {
		this.validated = validated;
	}

	public String getBuildingType() {
		return buildingType;
	}

	public void setBuildingType(String buildingType) {
		this.buildingType = buildingType;
	}

	public String getRoom() {
		return room;
	}

	public void setRoom(String room) {
		this.room = room;
	}

	public String getFloor() {
		return floor;
	}

	public void setFloor(String floor) {
		this.floor = floor;
	}
	
	@Override
	public String toString() {
		return "LocationAddressBookResponse{" +
				"siteId='" + siteId + '\'' +
				", siteName='" + siteName + '\'' +
				", siteAddress='" + siteAddress + '\'' +
				", siteCity='" + siteCity + '\'' +
				", siteCountry='" + siteCountry + '\'' +
				", noOfPorts=" + noOfPorts +
				", customerSiteName='" + customerSiteName + '\'' +
				", siteType='" + siteType + '\'' +
				", privateSite=" + privateSite +
				", buildingId='" + buildingId + '\'' +
				", buildingName='" + buildingName + '\'' +
				", validated=" + validated +
				", buildingType='" + buildingType + '\'' +
				", room='" + room + '\'' +
				", floor='" + floor + '\'' +
				'}';
	}
}
