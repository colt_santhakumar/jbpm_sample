package net.colt.novitas.workitems;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.nio.client.CloseableHttpAsyncClient;
import org.apache.http.impl.nio.client.HttpAsyncClients;
import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;

public class WIHRefreshCache  extends BaseWorkItemHandler {
	
	private static Logger logger = LoggerFactory.getLogger(WIHRefreshCache.class);
	
	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}
	

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
		
		EnvironmentProperties prop = getEnvProperties(workItem);
		logger = getLogger(workItem, prop.getLogUniqueId());
		try {
			
			String locationId = String.valueOf(workItem.getParameter("location_id"));
			logger.info("Refreshing Product Cache for site id  ::"+locationId );
			
			
			refreshProductCache(locationId, 0,prop);
			logger.info("Product Cache refreshed successfully for site id  ::"+locationId +"/\\../\\");
			Map<String,Object> retval = new HashMap<String,Object>();
			manager.completeWorkItem(workItem.getId(), retval);
			
		} catch (Exception e) {
			logger.error("WIHRefreshCache -> Failed", e);
		}
	}
	
	
	public void  refreshProductCache(String siteId, int minutes,EnvironmentProperties prop) {

		String url = prop.getRequestAPIUrl() + "/product?service_type=ETHERNETPORT&location_id=" + encodeString(siteId);
		CloseableHttpAsyncClient client = HttpAsyncClients.createDefault();
		client.start ();
		HttpGet getMethod = new HttpGet(url);
		// Set Credentials
		String authString = prop.getRequestAPIUsername() + ":" + prop.getRequestAPIPassword();
		byte[] authEncBytes = Base64.getEncoder().encode((byte[]) authString.getBytes());
		String authStringEnc = new String(authEncBytes);
		// Add Headers
		getMethod.addHeader("Content-Type", "application/json");
		getMethod.addHeader("Authorization", "Basic " + authStringEnc);
		getMethod.addHeader("cache-refresh", String.valueOf(minutes));
		Future<HttpResponse> future = client.execute(getMethod,null);
		logger.info("\nSending 'GET' request to URL : " + url);
         try{
        	 logger.info("Refresh Cache Response"+future.get(1000, TimeUnit.MILLISECONDS));
            }catch(TimeoutException | InterruptedException | ExecutionException e){
            	logger.info("Exception occured"+e);	
		    }
	}
	
	public static String encodeString(String value) {

		String encodedValue = null;
		try {
			encodedValue = URLEncoder.encode(value.replaceAll("/", "||"), "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return encodedValue;

	}
	
	
	
}
