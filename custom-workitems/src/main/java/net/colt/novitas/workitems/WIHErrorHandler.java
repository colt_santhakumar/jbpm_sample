package net.colt.novitas.workitems;

import java.util.HashMap;
import java.util.Map;

import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;


public class WIHErrorHandler extends BaseWorkItemHandler {
	
	private static Logger logger = LoggerFactory.getLogger(WIHErrorHandler.class);


	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
		try {
			EnvironmentProperties prop = getEnvProperties(workItem);
			logger = getLogger(workItem,prop.getLogUniqueId());
			logger.info("Starting error handling ......... ");
			Integer requestId = getRequestId(workItem);
			logger.info("Starting error handling for request : " + requestId);
			Map<String,Object> retval = new HashMap<String,Object>();
			HandlerErrorCode errorCode = null;
			Exception exception = getWorkflowError(workItem);
			if (null != exception && exception instanceof NovitasHandlerException) {
				errorCode = ((NovitasHandlerException) exception).getErrorCode();
				logger.info("Error code : " + errorCode);
			}

			if (errorCode == null) {
				logger.info("NovitasHandlerException thrown without error code. Unable to process further.");
				errorCode = HandlerErrorCode.GENERAL_ERROR_CODE;
			}
					
			if (errorCode.isPaymentError()) {
				logger.info("Failed to create or cancel charges. Code : " + errorCode + " request : " + requestId);
				if(HandlerErrorCode.CONNECTION_SMARTS_CALL_BACK_STATUS.equals(errorCode)){
					retval.put("is_smarts_callback_failed", true);
				}else{
					retval.put("payment_failed", "true");
				}
			}
			retval.put("error_code", errorCode.getErrorId());
			retval.put("error_description", errorCode.getDescription());
		    logger.info("Error handling completed for request : " + requestId);
		    manager.completeWorkItem(workItem.getId(), retval);
			
			
		} catch (Exception e) {
			logger.error("Error handling has been failed.", e);
			manager.abortWorkItem(workItem.getId());
		}

	}

	

}
