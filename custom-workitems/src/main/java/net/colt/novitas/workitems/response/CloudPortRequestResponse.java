package net.colt.novitas.workitems.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import net.colt.novitas.workitems.BaseWorkItemHandler;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CloudPortRequestResponse implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonProperty("id")
	protected Integer id;

	@JsonProperty("service_id")
	protected String serviceId;

	@JsonProperty("portal_user_id")
	protected Integer portalUserId;

	@JsonProperty("action")
	protected String action;

	@JsonProperty("status")
	protected String status;

	@JsonProperty("bandwidth")
	protected Integer bandwidth;

	@JsonProperty("requested_at")
	protected String requestedAt;

	@JsonProperty("rental_charge")
	protected Float rentalCharge;

	@JsonProperty("currency")
	protected String currency;

	@JsonProperty("rental_unit")
	protected String rentalUnit;

	@JsonProperty("installation_charge")
	protected Float installationCharge;

	@JsonProperty("installation_currency")
	protected String installationCurrency;

	@JsonProperty("decommissioning_charge")
	protected Float decommissioningCharge;

	@JsonProperty("decommissioning_currency")
	protected String decommissioningCurrency;

	@JsonProperty("status_code")
	protected Integer statusCode;

	@JsonProperty("status_code_description")
	protected String statusCodeDescription;

	@JsonProperty("bcn")
	protected String bcn;

	@JsonProperty("ocn")
	protected String ocn;

	@JsonProperty("customer_name")
	protected String customerName;

	@JsonProperty("customer_local_name")
	private String customerLocalName;

	@JsonProperty("commitment_period")
	protected Integer commitmentPeriod;

	@JsonProperty("rental_currency")
	protected String rentalCurrency;

	@JsonProperty("description")
	protected String description;

	@JsonProperty("port_id")
	private String portId;

	@JsonProperty("port_name")
	private String portName;

	@JsonProperty("location_id")
	private String locationId;

	@JsonProperty("building_id")
	private String buildingId;

	@JsonProperty("location_name")
	private String locationName;

	@JsonProperty("product_name")
	private String productName;

	@JsonProperty("last_updated")
	private String lastStatus;

	@JsonProperty("penalty")
	private Float penalty;

	@JsonProperty("penalty_currency")
	private String penaltyCurrency;

	@JsonProperty("site_type")
	private String siteType;

	@JsonProperty("site_floor")
	private String siteFloor;

	@JsonProperty("site_room_name")
	private String siteRoomName;

	@JsonProperty("location_premises_number")
	private String locationPremisesNumber;

	@JsonProperty("location_building_name")
	private String locationBuildingName;

	@JsonProperty("location_street_name")
	private String locationStreetName;

	@JsonProperty("location_city")
	private String locationCity;

	@JsonProperty("location_state")
	private String locationState;

	@JsonProperty("location_country")
	private String locationCountry;

	@JsonProperty("postal_zip_code")
	private String postalZipCode;

	@JsonProperty("latitude")
	private Float latitude;

	@JsonProperty("longitude")
	private Float longitude;

	@JsonProperty("cloud_provider")
	private String cloudProvider;

	@JsonProperty("azure_service_key")
	private String serviceKey;

	@JsonProperty("aws_account_no")
	private String awsAccountno;

	@JsonProperty("aws_region")
	private String region;

	@JsonProperty("cloud_region")
	private String cloudRegion;

	@JsonProperty("csp_key")
	private String pairingKey;

	@JsonProperty("aws_ref")
	private String awsRef;

	@JsonProperty("connector_type")
	private String connectorType;

	@JsonProperty("technology_type")
	private String technologyType;

	@JsonProperty("process_instance_id")
	private String processInstanceId;

	@JsonProperty("order_id")
	private String orderId;

	@JsonProperty("order_status")
	private String orderStatus;

	@JsonProperty("expiration_period")
	private Integer expirationPeriod;

	@JsonProperty("loa")
	private byte[] loa;
	
	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getCustomerLocalName() {
		return customerLocalName;
	}

	public void setCustomerLocalName(String customerLocalName) {
		this.customerLocalName = customerLocalName;
	}

	public String getCloudRegion() {
		return cloudRegion;
	}

	public void setCloudRegion(String cloudRegion) {
		this.cloudRegion = cloudRegion;
	}

	public String getPairingKey() {
		return pairingKey;
	}

	public void setPairingKey(String pairingKey) {
		this.pairingKey = pairingKey;
	}

	public Integer getExpirationPeriod() {
		return expirationPeriod;
	}

	public void setExpirationPeriod(Integer expirationPeriod) {
		this.expirationPeriod = expirationPeriod;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public Integer getPortalUserId() {
		return portalUserId;
	}

	public void setPortalUserId(Integer portalUserId) {
		this.portalUserId = portalUserId;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getBandwidth() {
		return bandwidth;
	}

	public void setBandwidth(Integer bandwidth) {
		this.bandwidth = bandwidth;
	}

	public String getRequestedAt() {
		return requestedAt;
	}

	public void setRequestedAt(String requestedAt) {
		this.requestedAt = requestedAt;
	}

	public Float getRentalCharge() {
		return rentalCharge;
	}

	public void setRentalCharge(Float rentalCharge) {
		this.rentalCharge = rentalCharge;
	}

	public String getRentalUnit() {
		return rentalUnit;
	}

	public void setRentalUnit(String rentalUnit) {
		this.rentalUnit = rentalUnit;
	}

	public Float getInstallationCharge() {
		return installationCharge;
	}

	public void setInstallationCharge(Float installationCharge) {
		this.installationCharge = installationCharge;
	}

	public String getInstallationCurrency() {
		return installationCurrency;
	}

	public void setInstallationCurrency(String installationCurrency) {
		this.installationCurrency = installationCurrency;
	}

	public Float getDecommissioningCharge() {
		return decommissioningCharge;
	}

	public void setDecommissioningCharge(Float decommissioningCharge) {
		this.decommissioningCharge = decommissioningCharge;
	}

	public String getDecommissioningCurrency() {
		return decommissioningCurrency;
	}

	public void setDecommissioningCurrency(String decommissioningCurrency) {
		this.decommissioningCurrency = decommissioningCurrency;
	}

	public Integer getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatusCodeDescription() {
		return statusCodeDescription;
	}

	public void setStatusCodeDescription(String statusCodeDescription) {
		this.statusCodeDescription = statusCodeDescription;
	}

	public String getBcn() {
		return bcn;
	}

	public void setBcn(String bcn) {
		this.bcn = bcn;
	}

	public String getOcn() {
		return ocn;
	}

	public void setOcn(String ocn) {
		this.ocn = ocn;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public Integer getCommitmentPeriod() {
		return commitmentPeriod;
	}

	public void setCommitmentPeriod(Integer commitmentPeriod) {
		this.commitmentPeriod = commitmentPeriod;
	}

	public String getRentalCurrency() {
		return rentalCurrency;
	}

	public void setRentalCurrency(String rentalCurrency) {
		this.rentalCurrency = rentalCurrency;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPortId() {
		return portId;
	}

	public void setPortId(String portId) {
		this.portId = portId;
	}

	public String getPortName() {
		return portName;
	}

	public void setPortName(String portName) {
		this.portName = portName;
	}

	public String getLocationId() {
		return locationId;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getLastStatus() {
		return lastStatus;
	}

	public void setLastStatus(String lastStatus) {
		this.lastStatus = lastStatus;
	}

	public Float getPenalty() {
		return penalty;
	}

	public void setPenalty(Float penalty) {
		this.penalty = penalty;
	}

	public String getPenaltyCurrency() {
		return penaltyCurrency;
	}

	public void setPenaltyCurrency(String penaltyCurrency) {
		this.penaltyCurrency = penaltyCurrency;
	}

	public String getSiteType() {
		return siteType;
	}

	public void setSiteType(String siteType) {
		this.siteType = siteType;
	}

	public String getSiteFloor() {
		return siteFloor;
	}

	public void setSiteFloor(String siteFloor) {
		this.siteFloor = siteFloor;
	}

	public String getSiteRoomName() {
		return siteRoomName;
	}

	public void setSiteRoomName(String siteRoomName) {
		this.siteRoomName = siteRoomName;
	}

	public String getLocationPremisesNumber() {
		return locationPremisesNumber;
	}

	public void setLocationPremisesNumber(String locationPremisesNumber) {
		this.locationPremisesNumber = locationPremisesNumber;
	}

	public String getLocationBuildingName() {
		return locationBuildingName;
	}

	public void setLocationBuildingName(String locationBuildingName) {
		this.locationBuildingName = locationBuildingName;
	}

	public String getLocationStreetName() {
		return locationStreetName;
	}

	public void setLocationStreetName(String locationStreetName) {
		this.locationStreetName = locationStreetName;
	}

	public String getLocationCity() {
		return locationCity;
	}

	public void setLocationCity(String locationCity) {
		this.locationCity = locationCity;
	}

	public String getLocationState() {
		return locationState;
	}

	public void setLocationState(String locationState) {
		this.locationState = locationState;
	}

	public String getLocationCountry() {
		return locationCountry;
	}

	public void setLocationCountry(String locationCountry) {
		this.locationCountry = locationCountry;
	}

	public String getPostalZipCode() {
		return postalZipCode;
	}

	public void setPostalZipCode(String postalZipCode) {
		this.postalZipCode = postalZipCode;
	}

	public Float getLatitude() {
		return latitude;
	}

	public void setLatitude(Float latitude) {
		this.latitude = latitude;
	}

	public Float getLongitude() {
		return longitude;
	}

	public void setLongitude(Float longitude) {
		this.longitude = longitude;
	}

	public String getCloudProvider() {
		return cloudProvider;
	}

	public void setCloudProvider(String cloudProvider) {
		this.cloudProvider = cloudProvider;
	}

	public String getServiceKey() {
		return serviceKey;
	}

	public void setServiceKey(String serviceKey) {
		this.serviceKey = serviceKey;
	}

	public String getAwsAccountno() {
		return awsAccountno;
	}

	public void setAwsAccountno(String awsAccountno) {
		this.awsAccountno = awsAccountno;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getAwsRef() {
		return awsRef;
	}

	public void setAwsRef(String awsRef) {
		this.awsRef = awsRef;
	}

	public String getConnectorType() {
		return connectorType;
	}

	public void setConnectorType(String connectorType) {
		this.connectorType = connectorType;
	}

	public String getTechnologyType() {
		return technologyType;
	}

	public void setTechnologyType(String technologyType) {
		this.technologyType = technologyType;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public String getBuildingId() {
		return buildingId;
	}

	public void setBuildingId(String buildingId) {
		this.buildingId = buildingId;
	}

	public byte[] getLoa() {
		return loa;
	}

	public void setLoa(byte[] loa) {
		this.loa = loa;
	}

	@Override
	public String toString() {
		return BaseWorkItemHandler.toString(this);
	}
}