package net.colt.novitas.workitems.constants;

public enum RentalUnit {
    
    MONTHLY, HOURLY;
    
    public static RentalUnit validate(String value) {
        try {
            return RentalUnit.valueOf(value);
        } catch (Exception e) {
            return null;
        }
    }

}
