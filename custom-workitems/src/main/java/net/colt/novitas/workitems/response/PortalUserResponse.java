package net.colt.novitas.workitems.response;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class PortalUserResponse implements Serializable {

	private static final long serialVersionUID = -93095396184990172L;

	private String portalUserId;

	private String name;

	private String title;

	private String userId;

	private String customerId;

	private String status;

	@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssZ")
	private Date lastLoggedInDate;

	private Integer termsAndConditionsId;

	@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssZ")
	private Date tcAcceptedOn;

	private String email;

	private String telephoneNo;

	private String preferredLanguage;

}
