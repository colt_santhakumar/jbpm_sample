package net.colt.novitas.workitems.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ReserveVLANResponse implements Serializable {

	private static final long serialVersionUID = 1691901837651195505L;

	@JsonProperty("vlan_id")
	private String vlanId;

	@JsonProperty("vlan_description")
	private String vlanDescription;

	@JsonProperty("circuit_id")
	private String circuitId;

	@JsonProperty("sar_name")
	private String sarName;

	@JsonProperty("sar_port")
	private String sarPort;

	@JsonProperty("vcpe_name")
	private String vcpeName;

	@JsonProperty("device_ip_address")
	private String deviceIpAddress;

	@JsonProperty("device_city_name")
	private String deviceCityName;

	@JsonProperty("device_country_code")
	private String deviceCountryCode;

	@JsonProperty("status")
	private String status;

	@JsonProperty("error_code")
	private String errorCode;

	@JsonProperty("error_description")
	private String errorDescription;

	public ReserveVLANResponse() {

	}

	public String getDeviceCityName() {
		return deviceCityName;
	}

	public void setDeviceCityName(String deviceCityName) {
		this.deviceCityName = deviceCityName;
	}

	public String getDeviceCountryCode() {
		return deviceCountryCode;
	}

	public void setDeviceCountryCode(String deviceCountryCode) {
		this.deviceCountryCode = deviceCountryCode;
	}

	public String getDeviceIpAddress() {
		return deviceIpAddress;
	}

	public void setDeviceIpAddress(String deviceIpAddress) {
		this.deviceIpAddress = deviceIpAddress;
	}

	public String getVlanId() {
		return vlanId;
	}

	public void setVlanId(String vlanId) {
		this.vlanId = vlanId;
	}

	public String getVlanDescription() {
		return vlanDescription;
	}

	public void setVlanDescription(String vlanDescription) {
		this.vlanDescription = vlanDescription;
	}

	public String getCircuitId() {
		return circuitId;
	}

	public void setCircuitId(String circuitId) {
		this.circuitId = circuitId;
	}

	public String getSarName() {
		return sarName;
	}

	public void setSarName(String sarName) {
		this.sarName = sarName;
	}

	public String getSarPort() {
		return sarPort;
	}

	public void setSarPort(String sarPort) {
		this.sarPort = sarPort;
	}

	public String getVcpeName() {
		return vcpeName;
	}

	public void setVcpeName(String vcpeName) {
		this.vcpeName = vcpeName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorDescription() {
		return errorDescription;
	}

	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}


	public ReserveVLANResponse(String vlanId, String vlanDescription, String circuitId, String sarName, String sarPort,
			String vcpeName, String deviceIpAddress, String deviceCityName, String deviceCountryCode, String status,
			String errorCode, String errorDescription) {
		super();
		this.vlanId = vlanId;
		this.vlanDescription = vlanDescription;
		this.circuitId = circuitId;
		this.sarName = sarName;
		this.sarPort = sarPort;
		this.vcpeName = vcpeName;
		this.deviceIpAddress = deviceIpAddress;
		this.deviceCityName = deviceCityName;
		this.deviceCountryCode = deviceCountryCode;
		this.status = status;
		this.errorCode = errorCode;
		this.errorDescription = errorDescription;
	}

	@Override
	public String toString() {
		return "ReserveVLANResponse [vlanId=" + vlanId + ", vlanDescription=" + vlanDescription + ", circuitId="
				+ circuitId + ", sarName=" + sarName + ", sarPort=" + sarPort + ", vcpeName=" + vcpeName
				+ ", deviceIpAddress=" + deviceIpAddress + ", deviceCityName=" + deviceCityName + ", deviceCountryCode="
				+ deviceCountryCode + ", status=" + status + ", errorCode=" + errorCode + ", errorDescription="
				+ errorDescription + "]";
	}

}
