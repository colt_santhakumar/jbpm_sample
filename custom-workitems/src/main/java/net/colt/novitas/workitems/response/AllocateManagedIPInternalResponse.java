package net.colt.novitas.workitems.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class AllocateManagedIPInternalResponse implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonProperty("management_ipv4")
	private String managementIPv4;

}