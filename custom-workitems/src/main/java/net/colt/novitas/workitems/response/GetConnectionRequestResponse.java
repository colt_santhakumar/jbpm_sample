package net.colt.novitas.workitems.response;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import net.colt.novitas.workitem.request.RequestVLANIdRange;
import net.colt.novitas.workitems.BaseWorkItemHandler;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
@JsonIgnoreProperties(ignoreUnknown = true)
public class GetConnectionRequestResponse implements Serializable {

	private static final long serialVersionUID = -3889856842640778210L;
    @JsonProperty("id")
	private Integer id;
    @JsonProperty("service_id")
	private String serviceId;
    @JsonProperty("portal_user_id")
	private Integer portalUserId;
    @JsonProperty("action")
	private String action;
    @JsonProperty("status")
	private String status;
    @JsonProperty("from_port")
	private String fromPort;
    @JsonProperty("to_port")
	private String toPort;
    @JsonProperty("from_port_name")
	private String fromPortName;
    @JsonProperty("to_port_name")
	private String toPortName;
    @JsonProperty("bandwidth")
	private Integer bandwidth;
    @JsonProperty("description")
	private String description;
	@JsonProperty("requested_at")
	private String requestedAt;
	@JsonProperty("last_updated")
	private String lastUpdated;
	@JsonProperty("rental_charge")
	private Float rentalCharge;
	@JsonProperty("rental_unit")
	private String rentalUnit;
	@JsonProperty("rental_currency")
	private String rentalCurrency;
	@JsonProperty("price_id")
	private Integer priceId;
	@JsonProperty("connection_name")
	private String connectionName;
	@JsonProperty("installation_charge")
	private Float installationCharge;
	@JsonProperty("installation_currency")
	private String installationCurrency;
	@JsonProperty("connection_id")
	private String connectionId;
	@JsonProperty("old_bandwidth")
	private Integer oldBandwidth;
	@JsonProperty("old_rental_unit")
	private String oldRentalUnit;
	@JsonProperty("old_rental_charge")
	private Float oldRentalCharge;
	@JsonProperty("old_rental_currency")
	private String oldRentalCurrency;
	@JsonProperty("modification_charge")
	private Float modificationCharge;
	@JsonProperty("modification_currency")
	private String modificationCurrency;
	@JsonProperty("decommissioning_charge")
	private Float decommissioningCharge;
	@JsonProperty("decommissioning_currency")
	private String decommissioningCurrency;
	@JsonProperty("status_code")
	private Integer statusCode;
	@JsonProperty("status_code_description")
	private String statusCodeDescription;
	@JsonProperty("bcn")
	private String bcn;
	@JsonProperty("a_end_vlan_mapping")
	private String fromVlanMapping;
	@JsonProperty("b_end_vlan_mapping")
	private String toVlanMapping;
	@JsonProperty("a_end_vlan_type")
	private String fromVlanType;
	@JsonProperty("b_end_vlan_type")
	private String toVlanType;
	@JsonProperty("a_end_vlan_ids")
	private List<RequestVLANIdRange> fromPortVLANIdRange;
	@JsonProperty("b_end_vlan_ids")
	private List<RequestVLANIdRange> toPortVLANIdRange;
	@JsonProperty("ocn")
	private String ocn;
	@JsonProperty("customer_name")
	private String customerName;
	@JsonProperty("penalty_charge")
	private Float penaltyCharge; 
	@JsonProperty("commitment_period")
	private Integer commitmentPeriod; 
	@JsonProperty("coterminus_option")
	private Boolean coterminusOption;
	
	@JsonProperty("enni_colt_port")
	private String eniColtPort;
	
	@JsonProperty("enni_olo_port")
	private String eniOloPort;
    
	@JsonProperty("enni_vlan")
	private Integer eniVlan;

	@JsonProperty("depends_on_request_ids")
	private String dependsOnRequestIds;

	@JsonProperty("has_dependent_requests")
	private boolean hasDependentRequests;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public Integer getPortalUserId() {
		return portalUserId;
	}

	public void setPortalUserId(Integer portalUserId) {
		this.portalUserId = portalUserId;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getFromPort() {
		return fromPort;
	}

	public void setFromPort(String fromPort) {
		this.fromPort = fromPort;
	}

	public String getToPort() {
		return toPort;
	}

	public void setToPort(String toPort) {
		this.toPort = toPort;
	}

	public String getFromPortName() {
		return fromPortName;
	}

	public void setFromPortName(String fromPortName) {
		this.fromPortName = fromPortName;
	}

	public String getToPortName() {
		return toPortName;
	}

	public void setToPortName(String toPortName) {
		this.toPortName = toPortName;
	}

	public Integer getBandwidth() {
		return bandwidth;
	}

	public void setBandwidth(Integer bandwidth) {
		this.bandwidth = bandwidth;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getRequestedAt() {
		return requestedAt;
	}

	public void setRequestedAt(String requestedAt) {
		this.requestedAt = requestedAt;
	}

	public String getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(String lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public Float getRentalCharge() {
		return rentalCharge;
	}

	public void setRentalCharge(Float rentalCharge) {
		this.rentalCharge = rentalCharge;
	}

	public String getRentalUnit() {
		return rentalUnit;
	}

	public void setRentalUnit(String rentalUnit) {
		this.rentalUnit = rentalUnit;
	}

	public String getRentalCurrency() {
		return rentalCurrency;
	}

	public void setRentalCurrency(String rentalCurrency) {
		this.rentalCurrency = rentalCurrency;
	}

	public Integer getPriceId() {
		return priceId;
	}

	public void setPriceId(Integer priceId) {
		this.priceId = priceId;
	}

	public String getConnectionName() {
		return connectionName;
	}

	public void setConnectionName(String connectionName) {
		this.connectionName = connectionName;
	}

	public Float getInstallationCharge() {
		return installationCharge;
	}

	public void setInstallationCharge(Float installationCharge) {
		this.installationCharge = installationCharge;
	}

	public String getInstallationCurrency() {
		return installationCurrency;
	}

	public void setInstallationCurrency(String installationCurrency) {
		this.installationCurrency = installationCurrency;
	}

	public String getConnectionId() {
		return connectionId;
	}

	public void setConnectionId(String connectionId) {
		this.connectionId = connectionId;
	}

	public Integer getOldBandwidth() {
		return oldBandwidth;
	}

	public void setOldBandwidth(Integer oldBandwidth) {
		this.oldBandwidth = oldBandwidth;
	}

	public String getOldRentalUnit() {
		return oldRentalUnit;
	}

	public void setOldRentalUnit(String oldRentalUnit) {
		this.oldRentalUnit = oldRentalUnit;
	}

	public Float getOldRentalCharge() {
		return oldRentalCharge;
	}

	public void setOldRentalCharge(Float oldRentalCharge) {
		this.oldRentalCharge = oldRentalCharge;
	}

	public String getOldRentalCurrency() {
		return oldRentalCurrency;
	}

	public void setOldRentalCurrency(String oldRentalCurrency) {
		this.oldRentalCurrency = oldRentalCurrency;
	}

	public Float getModificationCharge() {
		return modificationCharge;
	}

	public void setModificationCharge(Float modificationCharge) {
		this.modificationCharge = modificationCharge;
	}

	public String getModificationCurrency() {
		return modificationCurrency;
	}

	public void setModificationCurrency(String modificationCurrency) {
		this.modificationCurrency = modificationCurrency;
	}

	public Float getDecommissioningCharge() {
		return decommissioningCharge;
	}

	public void setDecommissioningCharge(Float decommissioningCharge) {
		this.decommissioningCharge = decommissioningCharge;
	}

	public String getDecommissioningCurrency() {
		return decommissioningCurrency;
	}

	public void setDecommissioningCurrency(String decommissioningCurrency) {
		this.decommissioningCurrency = decommissioningCurrency;
	}

	public Integer getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatusCodeDescription() {
		return statusCodeDescription;
	}

	public void setStatusCodeDescription(String statusCodeDescription) {
		this.statusCodeDescription = statusCodeDescription;
	}

	public String getBcn() {
		return bcn;
	}

	public void setBcn(String bcn) {
		this.bcn = bcn;
	}

	public String getFromVlanMapping() {
		return fromVlanMapping;
	}

	public void setFromVlanMapping(String fromVlanMapping) {
		this.fromVlanMapping = fromVlanMapping;
	}

	public String getToVlanMapping() {
		return toVlanMapping;
	}

	public void setToVlanMapping(String toVlanMapping) {
		this.toVlanMapping = toVlanMapping;
	}

	public String getFromVlanType() {
		return fromVlanType;
	}

	public void setFromVlanType(String fromVlanType) {
		this.fromVlanType = fromVlanType;
	}

	public String getToVlanType() {
		return toVlanType;
	}

	public void setToVlanType(String toVlanType) {
		this.toVlanType = toVlanType;
	}

	public List<RequestVLANIdRange> getFromPortVLANIdRange() {
		return fromPortVLANIdRange;
	}

	public void setFromPortVLANIdRange(
			List<RequestVLANIdRange> fromPortVLANIdRange) {
		this.fromPortVLANIdRange = fromPortVLANIdRange;
	}

	public List<RequestVLANIdRange> getToPortVLANIdRange() {
		return toPortVLANIdRange;
	}

	public void setToPortVLANIdRange(List<RequestVLANIdRange> toPortVLANIdRange) {
		this.toPortVLANIdRange = toPortVLANIdRange;
	}

	public String getOcn() {
		return ocn;
	}

	public void setOcn(String ocn) {
		this.ocn = ocn;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}


	/**
	 * @return the penaltyCharge
	 */
	public Float getPenaltyCharge() {
		return penaltyCharge;
	}


	/**
	 * @param penaltyCharge the penaltyCharge to set
	 */
	public void setPenaltyCharge(Float penaltyCharge) {
		this.penaltyCharge = penaltyCharge;
	}


	/**
	 * @return the commitmentPeriod
	 */
	public Integer getCommitmentPeriod() {
		return commitmentPeriod;
	}


	/**
	 * @param commitmentPeriod the commitmentPeriod to set
	 */
	public void setCommitmentPeriod(Integer commitmentPeriod) {
		this.commitmentPeriod = commitmentPeriod;
	}


	/**
	 * @return the coterminusOption
	 */
	public Boolean getCoterminusOption() {
		return coterminusOption;
	}


	/**
	 * @param coterminusOption the coterminusOption to set
	 */
	public void setCoterminusOption(Boolean coterminusOption) {
		this.coterminusOption = coterminusOption;
	}

	public String getEniColtPort() {
		return eniColtPort;
	}

	public void setEniColtPort(String eniColtPort) {
		this.eniColtPort = eniColtPort;
	}

	public String getEniOloPort() {
		return eniOloPort;
	}

	public void setEniOloPort(String eniOloPort) {
		this.eniOloPort = eniOloPort;
	}

	public List<RequestVLANIdRange> getEniVlan() {
		List<RequestVLANIdRange> ids = new ArrayList<>();
		RequestVLANIdRange vlan = new RequestVLANIdRange();
		vlan.setFromIdRange(eniVlan);
		vlan.setToIdRange(eniVlan);
		ids.add(vlan);
		return ids;
	}


	public void setEniVlan(Integer eniVlan) {
		
		this.eniVlan = eniVlan;
	}

	public String getDependsOnRequestIds() {
		return dependsOnRequestIds;
	}

	public void setDependsOnRequestIds(String dependsOnRequestIds) {
		this.dependsOnRequestIds = dependsOnRequestIds;
	}

	public boolean isHasDependentRequests() {
		return hasDependentRequests;
	}

	public void setHasDependentRequests(boolean hasDependentRequests) {
		this.hasDependentRequests = hasDependentRequests;
	}

	@Override
	public String toString() {
		return BaseWorkItemHandler.toString(this);
	}

    


}
