package net.colt.novitas.workitems.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

/**
 * virtualCircuit: { "id": "UniqueVirtualCircuitId123" "displayName":
 * "ProviderTestVirtualCircuit" "providerName": "AlexanderGrahamBell",
 * "providerServiceName": "Layer2 Service", "providerServiceId":
 * "ocid1.providerservice.oc1.eu-frankfurt-1.abcd", "serviceType": "LAYER2",
 * "bgpManagement": "CUSTOMER_MANAGED", "bandwidthShapeName": "2Gbps"
 * "oracleBgpAsn": "561", "customerBgpAsn": "1234", "crossConnectMappings": [{
 * "crossConnectOrCrossConnectGroupId": "CrossConnect1", "vlan": 1234,
 * "oracleBgpPeeringIp": "10.0.0.19/31", "customerBgpPeeringIp": "10.0.0.18/31",
 * }], "lifecycleState": "PROVISIONING", "providerState": "ACTIVE",
 * "bgpSessionState": "DOWN", "region": "IAD" }
 */

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
@ToString
public class OracleVirtualCircuitResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2536518881940008325L;

	private String id;

	private String displayName;

	private String providerName;

	private String providerServiceName;

	private String providerServiceId;

	private String serviceType;

	private String bgpManagement;

	private String bandwidthShapeName;

	private String oracleBgpAsn;

	private String customerBgpAsn;

	private List<OracleCrossConnectMappingResponse> crossConnectMappings;

	private String lifecycleState;

	private String providerState;

	private String bgpSessionState;

	private String region;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getProviderName() {
		return providerName;
	}

	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}

	public String getProviderServiceName() {
		return providerServiceName;
	}

	public void setProviderServiceName(String providerServiceName) {
		this.providerServiceName = providerServiceName;
	}

	public String getProviderServiceId() {
		return providerServiceId;
	}

	public void setProviderServiceId(String providerServiceId) {
		this.providerServiceId = providerServiceId;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public String getBgpManagement() {
		return bgpManagement;
	}

	public void setBgpManagement(String bgpManagement) {
		this.bgpManagement = bgpManagement;
	}

	public String getBandwidthShapeName() {
		return bandwidthShapeName;
	}

	public void setBandwidthShapeName(String bandwidthShapeName) {
		this.bandwidthShapeName = bandwidthShapeName;
	}

	public String getOracleBgpAsn() {
		return oracleBgpAsn;
	}

	public void setOracleBgpAsn(String oracleBgpAsn) {
		this.oracleBgpAsn = oracleBgpAsn;
	}

	public String getCustomerBgpAsn() {
		return customerBgpAsn;
	}

	public void setCustomerBgpAsn(String customerBgpAsn) {
		this.customerBgpAsn = customerBgpAsn;
	}

	public List<OracleCrossConnectMappingResponse> getCrossConnectMappings() {
		return crossConnectMappings;
	}

	public void setCrossConnectMappings(List<OracleCrossConnectMappingResponse> crossConnectMappings) {
		this.crossConnectMappings = crossConnectMappings;
	}

	public String getLifecycleState() {
		return lifecycleState;
	}

	public void setLifecycleState(String lifecycleState) {
		this.lifecycleState = lifecycleState;
	}

	public String getProviderState() {
		return providerState;
	}

	public void setProviderState(String providerState) {
		this.providerState = providerState;
	}

	public String getBgpSessionState() {
		return bgpSessionState;
	}

	public void setBgpSessionState(String bgpSessionState) {
		this.bgpSessionState = bgpSessionState;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}
	
}
