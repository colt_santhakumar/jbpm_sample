package net.colt.novitas.workitems.constants;

public enum CurrencyV1 {
	CHF,
    DKK,
    EUR,
    GBP,
    HKD,
    JPY,
    NOK,
    SEK,
    SGD,
    USD;
}
