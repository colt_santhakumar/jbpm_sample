package net.colt.novitas.workitems.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SiteContact implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonProperty("name")
	private String siteName;

	@JsonProperty("phone")
	private String phone;

	@JsonProperty("email")
	private String email;

	public SiteContact() {
	}

	public SiteContact(String siteName, String phone, String email) {
		super();
		this.siteName = siteName;
		this.phone = phone;
		this.email = email;
	}

	public String getSiteName() {
		return siteName;
	}

	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "SitePortContactInfo [siteName=" + siteName + ", phone=" + phone + ", email=" + email + "]";
	}

}
