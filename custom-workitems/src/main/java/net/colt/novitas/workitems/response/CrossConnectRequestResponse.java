package net.colt.novitas.workitems.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import net.colt.novitas.workitems.BaseWorkItemHandler;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CrossConnectRequestResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty("request_id")
	private Integer requestId;

	@JsonProperty("cross_connect_id")
	private String crossConnectId;

	@JsonProperty("action")
	private String action;

	@JsonProperty("bcn")
	private String bcn;

	@JsonProperty("commitment_period")
	private Integer commitmentPeriod;

	@JsonProperty("cron_execution")
	private String cronExecution;

	@JsonProperty("customer_name")
	private String customerName;

	@JsonProperty("customer_pp_port")
	private String customerPpPort;

	@JsonProperty("customer_pp_floor")
	private String customerPpFloor;

	@JsonProperty("customer_pp_room")
	private String customerPpRoom;

	@JsonProperty("customer_pp_cabinet")
	private String customerPpCabinet;

	@JsonProperty("customer_pp_device")
	private String customerPpDevice;

	@JsonProperty("description")
	private String description;

	@JsonProperty("host_address")
	private String hostAddress;

	@JsonProperty("installation_charge")
	private Float installationCharge;

	@JsonProperty("installation_currency")
	private String installationCurrency;

	@JsonProperty("last_status")
	private String lastStatus;

	@JsonProperty("loa_validated")
	private Boolean loaValidated;

	@JsonProperty("ocn")
	private String ocn;

	@JsonProperty("penalty_charge")
	private Float penaltyCharge;

	@JsonProperty("penalty_currency")
	private String penaltyCurrency;

	@JsonProperty("port_id")
	private String portId;

	@JsonProperty("port_request_id")
	private Integer portRequestId;

	@JsonProperty("site_port_request_id")
	private String sitePortRequestId;

	@JsonProperty("process_instance_id")
	private Integer processInstanceId;

	@JsonProperty("rental_charge")
	private Float rentalCharge;

	@JsonProperty("rental_currency")
	private String rentalCurrency;

	@JsonProperty("rental_unit")
	private String rentalUnit;

	@JsonProperty("requested_at")
	private String requestedAt;

	@JsonProperty("service_id")
	private String serviceId;

	@JsonProperty("status")
	private String status;

	@JsonProperty("status_code")
	private String statusCode;

	@JsonProperty("portal_user_id")
	private Integer portalUserId;

	@JsonProperty("decommissioning_charge")
	protected Float decommissioningCharge;

	@JsonProperty("decommissioning_currency")
	protected String decommissioningCurrency;
	
	@JsonProperty("connector")
	private String connector;
	
	@JsonProperty("loa")
	private byte[] loa;

	public Integer getRequestId() {
		return requestId;
	}

	public void setRequestId(Integer requestId) {
		this.requestId = requestId;
	}

	public String getCrossConnectId() {
		return crossConnectId;
	}

	public void setCrossConnectId(String crossConnectId) {
		this.crossConnectId = crossConnectId;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getBcn() {
		return bcn;
	}

	public void setBcn(String bcn) {
		this.bcn = bcn;
	}

	public Integer getCommitmentPeriod() {
		return commitmentPeriod;
	}

	public void setCommitmentPeriod(Integer commitmentPeriod) {
		this.commitmentPeriod = commitmentPeriod;
	}

	public String getCronExecution() {
		return cronExecution;
	}

	public void setCronExecution(String cronExecution) {
		this.cronExecution = cronExecution;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerPpPort() {
		return customerPpPort;
	}

	public void setCustomerPpPort(String customerPpPort) {
		this.customerPpPort = customerPpPort;
	}

	public String getCustomerPpFloor() {
		return customerPpFloor;
	}

	public void setCustomerPpFloor(String customerPpFloor) {
		this.customerPpFloor = customerPpFloor;
	}

	public String getCustomerPpRoom() {
		return customerPpRoom;
	}

	public void setCustomerPpRoom(String customerPpRoom) {
		this.customerPpRoom = customerPpRoom;
	}

	public String getCustomerPpCabinet() {
		return customerPpCabinet;
	}

	public void setCustomerPpCabinet(String customerPpCabinet) {
		this.customerPpCabinet = customerPpCabinet;
	}


	public String getCustomerPpDevice() {
		return customerPpDevice;
	}

	public void setCustomerPpDevice(String customerPpDevice) {
		this.customerPpDevice = customerPpDevice;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getHostAddress() {
		return hostAddress;
	}

	public void setHostAddress(String hostAddress) {
		this.hostAddress = hostAddress;
	}

	public Float getInstallationCharge() {
		return installationCharge;
	}

	public void setInstallationCharge(Float installationCharge) {
		this.installationCharge = installationCharge;
	}

	public String getInstallationCurrency() {
		return installationCurrency;
	}

	public void setInstallationCurrency(String installationCurrency) {
		this.installationCurrency = installationCurrency;
	}

	public Boolean isLoaValidated() {
		return loaValidated;
	}

	public void setLoaValidated(Boolean loaValidated) {
		this.loaValidated = loaValidated;
	}

	public String getOcn() {
		return ocn;
	}

	public void setOcn(String ocn) {
		this.ocn = ocn;
	}

	public Float getPenaltyCharge() {
		return penaltyCharge;
	}

	public void setPenaltyCharge(Float penaltyCharge) {
		this.penaltyCharge = penaltyCharge;
	}

	public String getPenaltyCurrency() {
		return penaltyCurrency;
	}

	public void setPenaltyCurrency(String penaltyCurrency) {
		this.penaltyCurrency = penaltyCurrency;
	}

	public String getPortId() {
		return portId;
	}

	public void setPortId(String portId) {
		this.portId = portId;
	}

	public Integer getPortRequestId() {
		return portRequestId;
	}

	public void setPortRequestId(Integer portRequestId) {
		this.portRequestId = portRequestId;
	}

	public Integer getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(Integer processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public Float getRentalCharge() {
		return rentalCharge;
	}

	public void setRentalCharge(Float rentalCharge) {
		this.rentalCharge = rentalCharge;
	}

	public String getRentalCurrency() {
		return rentalCurrency;
	}

	public void setRentalCurrency(String rentalCurrency) {
		this.rentalCurrency = rentalCurrency;
	}

	public String getRentalUnit() {
		return rentalUnit;
	}

	public void setRentalUnit(String rentalUnit) {
		this.rentalUnit = rentalUnit;
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public Integer getPortalUserId() {
		return portalUserId;
	}

	public void setPortalUserId(Integer portalUserId) {
		this.portalUserId = portalUserId;
	}

	public Boolean getLoaValidated() {
		return loaValidated;
	}

	public String getLastStatus() {
		return lastStatus;
	}

	public void setLastStatus(String lastStatus) {
		this.lastStatus = lastStatus;
	}

	public String getRequestedAt() {
		return requestedAt;
	}

	public void setRequestedAt(String requestedAt) {
		this.requestedAt = requestedAt;
	}

	public String getSitePortRequestId() {
		return sitePortRequestId;
	}

	public void setSitePortRequestId(String sitePortRequestId) {
		this.sitePortRequestId = sitePortRequestId;
	}

	public Float getDecommissioningCharge() {
		return decommissioningCharge;
	}

	public void setDecommissioningCharge(Float decommissioningCharge) {
		this.decommissioningCharge = decommissioningCharge;
	}

	public String getDecommissioningCurrency() {
		return decommissioningCurrency;
	}

	public void setDecommissioningCurrency(String decommissioningCurrency) {
		this.decommissioningCurrency = decommissioningCurrency;
	}

	public String getConnector() {
		return connector;
	}

	public void setConnector(String connector) {
		this.connector = connector;
	}
	
	public byte[] getLoa() {
		return loa;
	}

	public void setLoa(byte[] loa) {
		this.loa = loa;
	}

	@Override
	public String toString() {
		return BaseWorkItemHandler.toString(this);
	}
    
	
}
