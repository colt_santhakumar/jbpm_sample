package net.colt.novitas.workitems.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ServiceVLANIdRange implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5943217351124354129L;
	
	@JsonProperty("from_id_range")
	private Integer fromIdRange;
	
	@JsonProperty("to_id_range")
	private Integer toIdRange;
	
	@JsonProperty("cloud")
	private boolean isCloud;

	public ServiceVLANIdRange() {
		super();
	}

	public Integer getFromIdRange() {
		return fromIdRange;
	}

	public void setFromIdRange(Integer fromIdRange) {
		this.fromIdRange = fromIdRange;
	}

	public Integer getToIdRange() {
		return toIdRange;
	}

	public void setToIdRange(Integer toIdRange) {
		this.toIdRange = toIdRange;
	}

	public ServiceVLANIdRange(Integer fromIdRange, Integer toIdRange) {
		super();
		this.fromIdRange = fromIdRange;
		this.toIdRange = toIdRange;
	}
	

	public boolean isCloud() {
		return isCloud;
	}

	public void setCloud(boolean isCloud) {
		this.isCloud = isCloud;
	}

	@Override
	public String toString() {
		return "ServiceVLANIdRange [fromIdRange=" + fromIdRange
				+ ", toIdRange=" + toIdRange + ", isCloud=" + isCloud + "]";
	}

	

}
