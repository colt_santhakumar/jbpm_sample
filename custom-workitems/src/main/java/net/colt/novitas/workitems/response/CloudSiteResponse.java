package net.colt.novitas.workitems.response;

import java.io.Serializable;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import net.colt.novitas.workitems.BaseWorkItemHandler;

/**
 * Class to hold the Cloud Site details
 * @author Dharma
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CloudSiteResponse implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty("service")
	private String service;

	@JsonProperty("region_id")
	private String regionId;

	@JsonProperty("region_name")
	private String regionName;

	@JsonProperty("location")
	private String location;

	@JsonProperty("site_id")
	private String siteId;

	@JsonProperty("aws_ref")
	private String awsRef;

	@JsonProperty("nni_circuit_Id")
	private String nniCircuitId;

	@JsonProperty("site_type")
	private String siteType;

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public String getRegionId() {
		return regionId;
	}

	public void setRegionId(String regionId) {
		this.regionId = regionId;
	}

	public String getRegionName() {
		return regionName;
	}

	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getSiteId() {
		return siteId;
	}

	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}

	public String getAwsRef() {
		return awsRef;
	}

	public void setAwsRef(String awsRef) {
		this.awsRef = awsRef;
	}

	public String getNniCircuitId() {
		return nniCircuitId;
	}

	public void setNniCircuitId(String nniCircuitId) {
		this.nniCircuitId = nniCircuitId;
	}

	public String getSiteType() {
		return siteType;
	}

	public void setSiteType(String siteType) {
		this.siteType = siteType;
	}

	@Override
	public String toString() {
		return BaseWorkItemHandler.toString(this);
	}
	
}
