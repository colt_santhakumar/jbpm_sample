package net.colt.novitas.workitems.response;
import java.io.Serializable;   
import java.util.ArrayList;
import java.util.List;

import net.colt.novitas.workitem.request.ConnectionPortPair;
import net.colt.novitas.workitem.request.RequestVLANIdRange;
import net.colt.novitas.workitems.BaseWorkItemHandler;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown= true)
public class CloudConnectionRequestResponse  implements Serializable{
    
	private static final long serialVersionUID = 1L;

	@JsonProperty("id")
    protected Integer id;
    
    @JsonProperty("service_id")
    protected String serviceId;
    
    @JsonProperty("portal_user_id")
    protected Integer portalUserId;
    
    @JsonProperty("action")
    protected String action;
    
    @JsonProperty("status")
    protected String status;
    
    @JsonProperty("bandwidth")
    protected Integer bandwidth;
    
    @JsonProperty("requested_at")
    protected String requestedAt;
    
    @JsonProperty("rental_charge")
    protected Float rentalCharge;
    
    @JsonProperty("rental_unit")
    protected String rentalUnit;
    
    @JsonProperty("installation_charge")
    protected Float installationCharge;
    
    @JsonProperty("installation_currency")
    protected String installationCurrency;
    
    @JsonProperty("decommissioning_charge")
    protected Float decommissioningCharge;
    
    @JsonProperty("decommissioning_currency")
    protected String decommissioningCurrency;
    
    @JsonProperty("status_code")
    protected Integer statusCode;
    
    @JsonProperty("status_code_description")
    protected String statusCodeDescription;
    
    @JsonProperty("bcn")
    protected String bcn;
    
    @JsonProperty("ocn")
    protected String ocn;
    
    @JsonProperty("customer_name")
    protected String customerName;
    
    @JsonProperty("commitment_period")
    protected Integer commitmentPeriod;
    
    @JsonProperty("rental_currency")
    protected String rentalCurrency;
    
    @JsonProperty("description")
    protected String description;
    
    @JsonProperty("last_updated")
	private String lastUpdated;	
    
    @JsonProperty("price_id")
	private Integer priceId;
    
    @JsonProperty("connection_name")
	private String connectionName;
    
    @JsonProperty("connection_id")
	private String connectionId;
    
    @JsonProperty("old_bandwidth")
	private Integer oldBandwidth;
    
    @JsonProperty("old_rental_unit")
	private String oldRentalUnit;
    
    @JsonProperty("old_rental_charge")
	private Float oldRentalCharge;
    
    @JsonProperty("old_rental_currency")
	private String oldRentalCurrency;
    
    @JsonProperty("modification_charge")
	private Float modificationCharge;
    
    @JsonProperty("modification_currency")
	private String modificationCurrency;
	/*private String fromVlanMapping;
	private String toVlanMapping;
	private String fromVlanType;
	private String toVlanType;
	private List<VlanIdRangeRequest> fromPortVLANIdRange;
	private List<VlanIdRangeRequest> toPortVLANIdRange;*/
    @JsonProperty("penalty_charge")
	private Float penaltyCharge;
    
    @JsonProperty("coterminus_option")
	private Boolean coterminusOption;
	
	@JsonProperty("component_connections")
	private List<ConnectionPortPair> ports;
	
	@JsonProperty("enni_colt_port")
	private String eniColtPort;
	
	@JsonProperty("enni_olo_port")
	private String eniOloPort;
    
	@JsonProperty("enni_vlan")
	private Integer eniVlan;
	
	public String getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(String lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public Integer getPriceId() {
		return priceId;
	}

	public void setPriceId(Integer priceId) {
		this.priceId = priceId;
	}

	public String getConnectionName() {
		return connectionName;
	}

	public void setConnectionName(String connectionName) {
		this.connectionName = connectionName;
	}
	
	public String getConnectionId() {
		return connectionId;
	}

	public void setConnectionId(String connectionId) {
		this.connectionId = connectionId;
	}
	
	public Integer getOldBandwidth() {
		return oldBandwidth;
	}

	public void setOldBandwidth(Integer oldBandwidth) {
		this.oldBandwidth = oldBandwidth;
	}

	public String getOldRentalUnit() {
		return oldRentalUnit;
	}

	public void setOldRentalUnit(String oldRentalUnit) {
		this.oldRentalUnit = oldRentalUnit;
	}

	public Float getOldRentalCharge() {
		return oldRentalCharge;
	}

	public void setOldRentalCharge(Float oldRentalCharge) {
		this.oldRentalCharge = oldRentalCharge;
	}

	public String getOldRentalCurrency() {
		return oldRentalCurrency;
	}

	public void setOldRentalCurrency(String oldRentalCurrency) {
		this.oldRentalCurrency = oldRentalCurrency;
	}

	public Float getModificationCharge() {
		return modificationCharge;
	}

	public void setModificationCharge(Float modificationCharge) {
		this.modificationCharge = modificationCharge;
	}

	public String getModificationCurrency() {
		return modificationCurrency;
	}

	public void setModificationCurrency(String modificationCurrency) {
		this.modificationCurrency = modificationCurrency;
	}

	public Float getPenaltyCharge() {
		return penaltyCharge;
	}

	public void setPenaltyCharge(Float penaltyCharge) {
		this.penaltyCharge = penaltyCharge;
	}

	public Boolean getCoterminusOption() {
		return coterminusOption;
	}

	public void setCoterminusOption(Boolean coterminusOption) {
		this.coterminusOption = coterminusOption;
	}
	
    public List<ConnectionPortPair> getPorts() {
        return ports;
    }

    public void setPorts(List<ConnectionPortPair> ports) {
        this.ports = ports;
    }

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public Integer getPortalUserId() {
		return portalUserId;
	}

	public void setPortalUserId(Integer portalUserId) {
		this.portalUserId = portalUserId;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getBandwidth() {
		return bandwidth;
	}

	public void setBandwidth(Integer bandwidth) {
		this.bandwidth = bandwidth;
	}

	public String getRequestedAt() {
		return requestedAt;
	}

	public void setRequestedAt(String requestedAt) {
		this.requestedAt = requestedAt;
	}

	public Float getRentalCharge() {
		return rentalCharge;
	}

	public void setRentalCharge(Float rentalCharge) {
		this.rentalCharge = rentalCharge;
	}

	public String getRentalUnit() {
		return rentalUnit;
	}

	public void setRentalUnit(String rentalUnit) {
		this.rentalUnit = rentalUnit;
	}

	public Float getInstallationCharge() {
		return installationCharge;
	}

	public void setInstallationCharge(Float installationCharge) {
		this.installationCharge = installationCharge;
	}

	public String getInstallationCurrency() {
		return installationCurrency;
	}

	public void setInstallationCurrency(String installationCurrency) {
		this.installationCurrency = installationCurrency;
	}

	public Float getDecommissioningCharge() {
		return decommissioningCharge;
	}

	public void setDecommissioningCharge(Float decommissioningCharge) {
		this.decommissioningCharge = decommissioningCharge;
	}

	public String getDecommissioningCurrency() {
		return decommissioningCurrency;
	}

	public void setDecommissioningCurrency(String decommissioningCurrency) {
		this.decommissioningCurrency = decommissioningCurrency;
	}

	public Integer getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatusCodeDescription() {
		return statusCodeDescription;
	}

	public void setStatusCodeDescription(String statusCodeDescription) {
		this.statusCodeDescription = statusCodeDescription;
	}

	public String getBcn() {
		return bcn;
	}

	public void setBcn(String bcn) {
		this.bcn = bcn;
	}

	public String getOcn() {
		return ocn;
	}

	public void setOcn(String ocn) {
		this.ocn = ocn;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public Integer getCommitmentPeriod() {
		return commitmentPeriod;
	}

	public void setCommitmentPeriod(Integer commitmentPeriod) {
		this.commitmentPeriod = commitmentPeriod;
	}

	public String getRentalCurrency() {
		return rentalCurrency;
	}

	public void setRentalCurrency(String rentalCurrency) {
		this.rentalCurrency = rentalCurrency;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getEniColtPort() {
		return eniColtPort;
	}

	public void setEniColtPort(String eniColtPort) {
		this.eniColtPort = eniColtPort;
	}

	public String getEniOloPort() {
		return eniOloPort;
	}

	public void setEniOloPort(String eniOloPort) {
		this.eniOloPort = eniOloPort;
	}

	public List<RequestVLANIdRange> getEniVlan() {
		List<RequestVLANIdRange> ids = new ArrayList<>();
		RequestVLANIdRange vlan = new RequestVLANIdRange();
		vlan.setFromIdRange(eniVlan);
		vlan.setToIdRange(eniVlan);
		ids.add(vlan);
		return ids;
	}


	public void setEniVlan(Integer eniVlan) {
		
		this.eniVlan = eniVlan;
	}
	
	

	@Override
	public String toString() {
		return BaseWorkItemHandler.toString(this);
	}

}