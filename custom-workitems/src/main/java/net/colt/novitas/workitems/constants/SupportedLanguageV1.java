package net.colt.novitas.workitems.constants;

public enum SupportedLanguageV1 {
	DE,
    EN,
    ES,
    FR,
    IT,
    JA;

}
