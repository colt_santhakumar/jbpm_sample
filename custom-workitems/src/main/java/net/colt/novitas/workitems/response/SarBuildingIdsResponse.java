package net.colt.novitas.workitems.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.ToString;

@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class SarBuildingIdsResponse implements Serializable {

	private static final long serialVersionUID = -8251140370636851744L;

	@JsonProperty("from_port_site_type")
	private String fromPortSiteType;

	@JsonProperty("building_id")
	private String buildingId;

	@JsonProperty("priority")
	private Integer priority;
	
	@JsonProperty("maximum_allowed_bandwidth")
	private Integer maximumAllowedBandwidth;

	public SarBuildingIdsResponse() {
		super();
	}

	public SarBuildingIdsResponse(String fromPortSiteType, String buildingId, Integer priority,
			Integer maximumAllowedBandwidth) {
		super();
		this.fromPortSiteType = fromPortSiteType;
		this.buildingId = buildingId;
		this.priority = priority;
		this.maximumAllowedBandwidth = maximumAllowedBandwidth;
	}

	public Integer getPriority() {
		return priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	public String getFromPortSiteType() {
		return fromPortSiteType;
	}

	public void setFromPortSiteType(String fromPortSiteType) {
		this.fromPortSiteType = fromPortSiteType;
	}

	public String getBuildingId() {
		return buildingId;
	}

	public void setBuildingId(String buildingId) {
		this.buildingId = buildingId;
	}

	public Integer getMaximumAllowedBandwidth() {
		return maximumAllowedBandwidth;
	}

	public void setMaximumAllowedBandwidth(Integer maximumAllowedBandwidth) {
		this.maximumAllowedBandwidth = maximumAllowedBandwidth;
	}

}