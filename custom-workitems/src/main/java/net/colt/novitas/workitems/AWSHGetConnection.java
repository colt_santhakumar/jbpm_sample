package net.colt.novitas.workitems;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.services.directconnect.model.Connection;
import com.amazonaws.services.directconnect.model.DescribeHostedConnectionsRequest;
import com.amazonaws.services.directconnect.model.DescribeHostedConnectionsResult;
import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.HandlerUtils;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;

public class AWSHGetConnection extends BaseWorkItemHandler{
	private static Logger logger = LoggerFactory.getLogger(AWSHGetConnection.class);

	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
		EnvironmentProperties prop = HandlerUtils.getEnvironmentProperties(workItem);
		logger = getLogger(workItem, prop.getLogUniqueId());
		try {
			String awsConnectionId = getAwshConnectionId(workItem);
			String awsInterConnectionId = getAwsInterConnectionId(workItem);
			String region = getAWSRegion(workItem); 
			DescribeHostedConnectionsRequest req = new DescribeHostedConnectionsRequest();
			req.setConnectionId(awsInterConnectionId);
			DescribeHostedConnectionsResult  res = null;
			if(!prop.isAWSMocked()){
			  logger.info("Getting AWS Hosted Connection Details");
			  res = getAmazonDirectConnectClient(region,  prop,getColtRegion(workItem)).describeHostedConnections(req);
			} else{
				logger.info("Mock AWS Hosted Connection Details");
				res = new DescribeHostedConnectionsResult ();
				List<Connection> connections = new ArrayList<>();
				Connection conn = new Connection();
				connections.add(conn);
				res.setConnections(connections);
				conn.setConnectionId(awsConnectionId);
				conn.setRegion(region);
				conn.setConnectionState("available");
			  }
				  
			logger.info("AWSH Connection details for connectionId|"+awsConnectionId+"|"+res);
			if(null==res.getConnections() && res.getConnections().isEmpty()){
				throw new NovitasHandlerException();
			}
			Map<String, Object> retval = new HashMap<String, Object>();
			List<Connection> connections = res.getConnections();
			Connection conn = null;
			for (Connection connection : connections) {
				if(connection.getConnectionId().equals(awsConnectionId)){
					logger.info("Connection Find|"+connection.getConnectionName()+"|"+connection.getConnectionState());
				    conn = connection;
				    break;
				}
			}
			if(null == conn){
				throw new NovitasHandlerException();
			}
			retval.put("Result", conn);
			retval.put("status", conn.getConnectionState());
			manager.completeWorkItem(workItem.getId(), retval);

		} catch (Exception e) {
			logger.error("AWSHGetConnection -> Failed", e);
				HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(
						HandlerErrorCode.CP_AWSH_GET_CONNECTION));
			
		}
	}

}
