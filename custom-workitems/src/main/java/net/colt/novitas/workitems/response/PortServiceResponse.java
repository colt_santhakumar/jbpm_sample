package net.colt.novitas.workitems.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.*;

import java.io.Serializable;
import java.util.Date;

@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class PortServiceResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1860164814895157105L;
	@JsonProperty("id")
	private String portId;
	@JsonProperty("service_id")
	private String novitasServiceId;
	@JsonProperty("name")
	private String portName;
	private String status;
	@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssZ")
	private Date requestedAt;
	@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssZ")
	private Date lastUpdated;
	private String ocn;
	private String customerName;
	@JsonProperty("csp_key")
	private String pairingKey;

	private Integer bandwidth;
	private Integer availableBandwidth;
	private Integer usedBandwidth;
	private Integer noOfConnections;
	private Integer maxAllowedConnections = 1;

	private String currency;
	private Float rentalCharge;
	private String rentalUnit;
	private Float installationCharge;
	private Float decommissioningCharge;
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssZ")
	private Date decommissionedOn;
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssZ")
	private Date commitmentExpiry;
	private Float penaltyCharge;

	private String novitasSiteType;
	private Integer vlan;
	private String interconnect;
	private String interconnectAttachmentName;
	private String ncTechServiceId;
	@JsonProperty("resource_id")
	private String circuitReference;

	private String locationId;
	private String cityCode;
	private String countryCode;
	private String siteFloor;
	private String siteRoomName;
	private String locationBuildingId;
	private String locationPremisesNumber;
	private String locationBuildingName;
	private String locationStreetName;
	private String locationCity;
	private String locationCityCode;
	private String locationState;
	private String locationCountryCode;
	private String locationCountry;
	private String postalZipCode;
	private String localBuildingName;
	private String address;
	private Float latitude;
	private Float longitude;

	private String cloudProvider;
	private String cloudRegion;

	public String getPortId() {
		return portId;
	}

	public void setPortId(String portId) {
		this.portId = portId;
	}

	public String getNovitasServiceId() {
		return novitasServiceId;
	}

	public void setNovitasServiceId(String novitasServiceId) {
		this.novitasServiceId = novitasServiceId;
	}

	public String getPortName() {
		return portName;
	}

	public void setPortName(String portName) {
		this.portName = portName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getRequestedAt() {
		return requestedAt;
	}

	public void setRequestedAt(Date requestedAt) {
		this.requestedAt = requestedAt;
	}

	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public String getOcn() {
		return ocn;
	}

	public void setOcn(String ocn) {
		this.ocn = ocn;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getPairingKey() {
		return pairingKey;
	}

	public void setPairingKey(String pairingKey) {
		this.pairingKey = pairingKey;
	}

	public Integer getBandwidth() {
		return bandwidth;
	}

	public void setBandwidth(Integer bandwidth) {
		this.bandwidth = bandwidth;
	}

	public Integer getAvailableBandwidth() {
		return availableBandwidth;
	}

	public void setAvailableBandwidth(Integer availableBandwidth) {
		this.availableBandwidth = availableBandwidth;
	}

	public Integer getUsedBandwidth() {
		return usedBandwidth;
	}

	public void setUsedBandwidth(Integer usedBandwidth) {
		this.usedBandwidth = usedBandwidth;
	}

	public Integer getNoOfConnections() {
		return noOfConnections;
	}

	public void setNoOfConnections(Integer noOfConnections) {
		this.noOfConnections = noOfConnections;
	}

	public Integer getMaxAllowedConnections() {
		return maxAllowedConnections;
	}

	public void setMaxAllowedConnections(Integer maxAllowedConnections) {
		this.maxAllowedConnections = maxAllowedConnections;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public Float getRentalCharge() {
		return rentalCharge;
	}

	public void setRentalCharge(Float rentalCharge) {
		this.rentalCharge = rentalCharge;
	}

	public String getRentalUnit() {
		return rentalUnit;
	}

	public void setRentalUnit(String rentalUnit) {
		this.rentalUnit = rentalUnit;
	}

	public Float getInstallationCharge() {
		return installationCharge;
	}

	public void setInstallationCharge(Float installationCharge) {
		this.installationCharge = installationCharge;
	}

	public Float getDecommissioningCharge() {
		return decommissioningCharge;
	}

	public void setDecommissioningCharge(Float decommissioningCharge) {
		this.decommissioningCharge = decommissioningCharge;
	}

	public Date getDecommissionedOn() {
		return decommissionedOn;
	}

	public void setDecommissionedOn(Date decommissionedOn) {
		this.decommissionedOn = decommissionedOn;
	}

	public Date getCommitmentExpiry() {
		return commitmentExpiry;
	}

	public void setCommitmentExpiry(Date commitmentExpiry) {
		this.commitmentExpiry = commitmentExpiry;
	}

	public Float getPenaltyCharge() {
		return penaltyCharge;
	}

	public void setPenaltyCharge(Float penaltyCharge) {
		this.penaltyCharge = penaltyCharge;
	}

	public String getNovitasSiteType() {
		return novitasSiteType;
	}

	public void setNovitasSiteType(String novitasSiteType) {
		this.novitasSiteType = novitasSiteType;
	}

	public Integer getVlan() {
		return vlan;
	}

	public void setVlan(Integer vlan) {
		this.vlan = vlan;
	}

	public String getInterconnect() {
		return interconnect;
	}

	public void setInterconnect(String interconnect) {
		this.interconnect = interconnect;
	}

	public String getInterconnectAttachmentName() {
		return interconnectAttachmentName;
	}

	public void setInterconnectAttachmentName(String interconnectAttachmentName) {
		this.interconnectAttachmentName = interconnectAttachmentName;
	}

	public String getNcTechServiceId() {
		return ncTechServiceId;
	}

	public void setNcTechServiceId(String ncTechServiceId) {
		this.ncTechServiceId = ncTechServiceId;
	}

	public String getCircuitReference() {
		return circuitReference;
	}

	public void setCircuitReference(String circuitReference) {
		this.circuitReference = circuitReference;
	}

	public String getLocationId() {
		return locationId;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	public String getCityCode() {
		return cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getSiteFloor() {
		return siteFloor;
	}

	public void setSiteFloor(String siteFloor) {
		this.siteFloor = siteFloor;
	}

	public String getSiteRoomName() {
		return siteRoomName;
	}

	public void setSiteRoomName(String siteRoomName) {
		this.siteRoomName = siteRoomName;
	}

	public String getLocationBuildingId() {
		return locationBuildingId;
	}

	public void setLocationBuildingId(String locationBuildingId) {
		this.locationBuildingId = locationBuildingId;
	}

	public String getLocationPremisesNumber() {
		return locationPremisesNumber;
	}

	public void setLocationPremisesNumber(String locationPremisesNumber) {
		this.locationPremisesNumber = locationPremisesNumber;
	}

	public String getLocationBuildingName() {
		return locationBuildingName;
	}

	public void setLocationBuildingName(String locationBuildingName) {
		this.locationBuildingName = locationBuildingName;
	}

	public String getLocationStreetName() {
		return locationStreetName;
	}

	public void setLocationStreetName(String locationStreetName) {
		this.locationStreetName = locationStreetName;
	}

	public String getLocationCity() {
		return locationCity;
	}

	public void setLocationCity(String locationCity) {
		this.locationCity = locationCity;
	}

	public String getLocationCityCode() {
		return locationCityCode;
	}

	public void setLocationCityCode(String locationCityCode) {
		this.locationCityCode = locationCityCode;
	}

	public String getLocationState() {
		return locationState;
	}

	public void setLocationState(String locationState) {
		this.locationState = locationState;
	}

	public String getLocationCountryCode() {
		return locationCountryCode;
	}

	public void setLocationCountryCode(String locationCountryCode) {
		this.locationCountryCode = locationCountryCode;
	}

	public String getLocationCountry() {
		return locationCountry;
	}

	public void setLocationCountry(String locationCountry) {
		this.locationCountry = locationCountry;
	}

	public String getPostalZipCode() {
		return postalZipCode;
	}

	public void setPostalZipCode(String postalZipCode) {
		this.postalZipCode = postalZipCode;
	}

	public String getLocalBuildingName() {
		return localBuildingName;
	}

	public void setLocalBuildingName(String localBuildingName) {
		this.localBuildingName = localBuildingName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Float getLatitude() {
		return latitude;
	}

	public void setLatitude(Float latitude) {
		this.latitude = latitude;
	}

	public Float getLongitude() {
		return longitude;
	}

	public void setLongitude(Float longitude) {
		this.longitude = longitude;
	}

	public String getCloudProvider() {
		return cloudProvider;
	}

	public void setCloudProvider(String cloudProvider) {
		this.cloudProvider = cloudProvider;
	}

	public String getCloudRegion() {
		return cloudRegion;
	}

	public void setCloudRegion(String cloudRegion) {
		this.cloudRegion = cloudRegion;
	}

}
