package net.colt.novitas.workitems.constants;

public enum NetworkPlatform {
    IQNET,
    MMSP
}