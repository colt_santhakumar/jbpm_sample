package net.colt.novitas.workitems.response;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import net.colt.novitas.workitems.BaseWorkItemHandler;
import net.colt.novitas.workitems.constants.PortStatus;
import net.colt.novitas.workitems.constants.RentalUnit;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CloudPortResponse implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonProperty("id")
	private String id;

	@JsonProperty("name")
	private String name;

	@JsonProperty("status")
	private PortStatus status;

	@JsonProperty("location")
	private String location;

	@JsonProperty("used_bandwidth")
	private Integer usedBandwidth;

	@JsonProperty("available_bandwidth")
	private Integer availableBandwidth;

	@JsonProperty("bandwidth")
	private Integer bandwidth;

	@JsonProperty("no_of_connections")
	private Integer noOfConnections;

	@JsonProperty("rental_charge")
	private Float rentalCharge;

	@JsonProperty("rental_unit")
	private RentalUnit rentalUnit;

	@JsonProperty("rental_currency")
	private String rentalCurrency;

	@JsonProperty("address")
	private String address;

	@JsonProperty("site_type")
	private String siteType;

	@JsonProperty("commitment_expiry")
	private String commitmentExpiry;

	@JsonProperty("penalty_charge")
	private Float penaltyCharge;

	@JsonProperty("service_id")
	private String serviceId;

	@JsonProperty("port_vlan_id_range")
	private List<ServiceVLANIdRange> portVLANIdRange;

	@JsonProperty("has_p_to_p_mapping")
	private Boolean hasPToPMapping;

	@JsonProperty("max_allowed_connections")
	private Integer maxAllowedConnections;

	@JsonProperty("customer_name")
	private String customerName;

	@JsonProperty("ocn")
	private String ocn;

	@JsonProperty("site_floor")
	private String siteFloor;

	@JsonProperty("site_room_name")
	private String siteRoomName;

	@JsonProperty("location_premises_number")
	private String locationPremisesNumber;

	@JsonProperty("location_building_name")
	private String locationBuildingName;

	@JsonProperty("location_street_name")
	private String locationStreetName;

	@JsonProperty("location_city")
	private String locationCity;

	@JsonProperty("location_state")
	private String locationState;

	@JsonProperty("location_country")
	private String locationCountry;

	@JsonProperty("postal_zip_code")
	private String postalZipCode;

	@JsonProperty("latitude")
	private Float latitude;

	@JsonProperty("longitude")
	private Float longitude;

	@JsonProperty("location_id")
	private String locationId;

	@JsonProperty("cloud_provider")
	private String cloudProvider;

	@JsonProperty("azure_service_key")
	private String serviceKey;

	@JsonProperty("pairing_key")
	private String pairingKey;

	@JsonProperty("port_1")
	private String port1;

	@JsonProperty("port_2")
	private String port2;

	@JsonProperty("vlan")
	private Integer vlan;

	@JsonProperty("resource_id")
	private String resourceId;
	
	@JsonProperty("resource_id_1")
	private String resourceId1;

	@JsonProperty("resource_id_2")
	private String resourceId2;

	@JsonProperty("decommissioning_charge")
	private Float decommissioningCharge;

	@JsonProperty("decommissioning_currency")
	private String decommissioningCurrency;

	@JsonProperty("aws_account_no")
	private String awsAccountno;

	@JsonProperty("aws_connection_id")
	private String awsConnId;

	@JsonProperty("nni_circuit_id")
	private String nniCircuitId;

	@JsonProperty("aws_ref")
	private String awsRef;

	@JsonProperty("connector_type")
	private String connectorType;

	@JsonProperty("technology_type")
	private String technologyType;

	@JsonProperty("aws_region")
	private String region;

	@JsonProperty("nms")
	private String nms;

	@JsonProperty("local_building_name")
	private String localBuildingName;

	@JsonProperty("location_city_code")
	private String locationCityCode;

	@JsonProperty("location_country_code")
	private String locationCountryCode;

	@JsonProperty("resource_port_name_1")
	private String resourcePortName1;

	@JsonProperty("resource_port_name_2")
	private String resourcePortName2;

	@JsonProperty("nc_tech_service_id_1")
	private String ncTechServiceId1;

	@JsonProperty("nc_tech_service_id_2")
	private String ncTechServiceId2;

	@JsonProperty("cloud_region")
	private String cloudRegion;

	@JsonProperty("csp_key")
	private String cspKey;

	@JsonProperty("interconnect_attachment_name")
	private String interconnectAttachmentName;

	@JsonProperty("interconnect")
	private String interConnect;

	@JsonProperty("nc_tech_service_id")
	private String ncTechServiceId;

	@JsonProperty("novitas_site_type")
	private String novitasSiteType;

	public String getCloudRegion() {
		return cloudRegion;
	}

	public void setCloudRegion(String cloudRegion) {
		this.cloudRegion = cloudRegion;
	}

	public String getInterconnectAttachmentName() {
		return interconnectAttachmentName;
	}

	public void setInterconnectAttachmentName(String interconnectAttachmentName) {
		this.interconnectAttachmentName = interconnectAttachmentName;
	}

	public String getCspKey() {
		return cspKey;
	}

	public void setCspKey(String cspKey) {
		this.cspKey = cspKey;
	}

	public String getInterConnect() {
		return interConnect;
	}

	public void setInterConnect(String interConnect) {
		this.interConnect = interConnect;
	}

	public String getNcTechServiceId() {
		return ncTechServiceId;
	}

	public void setNcTechServiceId(String ncTechServiceId) {
		this.ncTechServiceId = ncTechServiceId;
	}

	public String getNovitasSiteType() {
		return novitasSiteType;
	}

	public void setNovitasSiteType(String novitasSiteType) {
		this.novitasSiteType = novitasSiteType;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public PortStatus getStatus() {
		return status;
	}

	public void setStatus(PortStatus status) {
		this.status = status;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public Integer getUsedBandwidth() {
		return usedBandwidth;
	}

	public void setUsedBandwidth(Integer usedBandwidth) {
		this.usedBandwidth = usedBandwidth;
	}

	public Integer getAvailableBandwidth() {
		return availableBandwidth;
	}

	public void setAvailableBandwidth(Integer availableBandwidth) {
		this.availableBandwidth = availableBandwidth;
	}

	public Integer getBandwidth() {
		return bandwidth;
	}

	public void setBandwidth(Integer bandwidth) {
		this.bandwidth = bandwidth;
	}

	public Integer getNoOfConnections() {
		return noOfConnections;
	}

	public void setNoOfConnections(Integer noOfConnections) {
		this.noOfConnections = noOfConnections;
	}

	public Float getRentalCharge() {
		return rentalCharge;
	}

	public void setRentalCharge(Float rentalCharge) {
		this.rentalCharge = rentalCharge;
	}

	public RentalUnit getRentalUnit() {
		return rentalUnit;
	}

	public void setRentalUnit(RentalUnit rentalUnit) {
		this.rentalUnit = rentalUnit;
	}

	public String getRentalCurrency() {
		return rentalCurrency;
	}

	public void setRentalCurrency(String rentalCurrency) {
		this.rentalCurrency = rentalCurrency;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getSiteType() {
		return siteType;
	}

	public void setSiteType(String siteType) {
		this.siteType = siteType;
	}

	public String getCommitmentExpiry() {
		return commitmentExpiry;
	}

	public void setCommitmentExpiry(String commitmentExpiry) {
		this.commitmentExpiry = commitmentExpiry;
	}

	public Float getPenaltyCharge() {
		return penaltyCharge;
	}

	public void setPenaltyCharge(Float penaltyCharge) {
		this.penaltyCharge = penaltyCharge;
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public List<ServiceVLANIdRange> getPortVLANIdRange() {
		return portVLANIdRange;
	}

	public void setPortVLANIdRange(List<ServiceVLANIdRange> portVLANIdRange) {
		this.portVLANIdRange = portVLANIdRange;
	}

	public Boolean getHasPToPMapping() {
		return hasPToPMapping;
	}

	public void setHasPToPMapping(Boolean hasPToPMapping) {
		this.hasPToPMapping = hasPToPMapping;
	}

	public Integer getMaxAllowedConnections() {
		return maxAllowedConnections;
	}

	public void setMaxAllowedConnections(Integer maxAllowedConnections) {
		this.maxAllowedConnections = maxAllowedConnections;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getOcn() {
		return ocn;
	}

	public void setOcn(String ocn) {
		this.ocn = ocn;
	}

	public String getSiteFloor() {
		return siteFloor;
	}

	public void setSiteFloor(String siteFloor) {
		this.siteFloor = siteFloor;
	}

	public String getSiteRoomName() {
		return siteRoomName;
	}

	public void setSiteRoomName(String siteRoomName) {
		this.siteRoomName = siteRoomName;
	}

	public String getLocationPremisesNumber() {
		return locationPremisesNumber;
	}

	public void setLocationPremisesNumber(String locationPremisesNumber) {
		this.locationPremisesNumber = locationPremisesNumber;
	}

	public String getLocationBuildingName() {
		return locationBuildingName;
	}

	public void setLocationBuildingName(String locationBuildingName) {
		this.locationBuildingName = locationBuildingName;
	}

	public String getLocationStreetName() {
		return locationStreetName;
	}

	public void setLocationStreetName(String locationStreetName) {
		this.locationStreetName = locationStreetName;
	}

	public String getLocationCity() {
		return locationCity;
	}

	public void setLocationCity(String locationCity) {
		this.locationCity = locationCity;
	}

	public String getLocationState() {
		return locationState;
	}

	public void setLocationState(String locationState) {
		this.locationState = locationState;
	}

	public String getLocationCountry() {
		return locationCountry;
	}

	public void setLocationCountry(String locationCountry) {
		this.locationCountry = locationCountry;
	}

	public String getPostalZipCode() {
		return postalZipCode;
	}

	public void setPostalZipCode(String postalZipCode) {
		this.postalZipCode = postalZipCode;
	}

	public Float getLatitude() {
		return latitude;
	}

	public void setLatitude(Float latitude) {
		this.latitude = latitude;
	}

	public Float getLongitude() {
		return longitude;
	}

	public void setLongitude(Float longitude) {
		this.longitude = longitude;
	}

	public String getLocationId() {
		return locationId;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	public String getCloudProvider() {
		return cloudProvider;
	}

	public void setCloudProvider(String cloudProvider) {
		this.cloudProvider = cloudProvider;
	}

	public String getServiceKey() {
		return serviceKey;
	}

	public void setServiceKey(String serviceKey) {
		this.serviceKey = serviceKey;
	}

	public String getPairingKey() {
		return pairingKey;
	}

	public void setPairingKey(String pairingKey) {
		this.pairingKey = pairingKey;
	}

	public String getPort1() {
		return port1;
	}

	public void setPort1(String port1) {
		this.port1 = port1;
	}

	public String getPort2() {
		return port2;
	}

	public void setPort2(String port2) {
		this.port2 = port2;
	}

	public Integer getVlan() {
		return vlan;
	}

	public void setVlan(Integer vlan) {
		this.vlan = vlan;
	}

	public String getResourceId() {
		return resourceId;
	}

	public void setResourceId(String resourceId) {
		this.resourceId = resourceId;
	}
	
	public String getResourceId1() {
		return resourceId1;
	}

	public void setResourceId1(String resourceId1) {
		this.resourceId1 = resourceId1;
	}

	public String getResourceId2() {
		return resourceId2;
	}

	public void setResourceId2(String resourceId2) {
		this.resourceId2 = resourceId2;
	}

	public Float getDecommissioningCharge() {
		return decommissioningCharge;
	}

	public void setDecommissioningCharge(Float decommissioningCharge) {
		this.decommissioningCharge = decommissioningCharge;
	}

	public String getDecommissioningCurrency() {
		return decommissioningCurrency;
	}

	public void setDecommissioningCurrency(String decommissioningCurrency) {
		this.decommissioningCurrency = decommissioningCurrency;
	}

	public String getAwsAccountno() {
		return awsAccountno;
	}

	public void setAwsAccountno(String awsAccountno) {
		this.awsAccountno = awsAccountno;
	}

	public String getAwsRef() {
		return awsRef;
	}

	public void setAwsRef(String awsRef) {
		this.awsRef = awsRef;
	}

	public String getAwsConnId() {
		return awsConnId;
	}

	public void setAwsConnId(String awsConnId) {
		this.awsConnId = awsConnId;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getNniCircuitId() {
		return nniCircuitId;
	}

	public void setNniCircuitId(String nniCircuitId) {
		this.nniCircuitId = nniCircuitId;
	}

	public String getConnectorType() {
		return connectorType;
	}

	public void setConnectorType(String connectorType) {
		this.connectorType = connectorType;
	}

	public String getTechnologyType() {
		return technologyType;
	}

	public void setTechnologyType(String technologyType) {
		this.technologyType = technologyType;
	}

	public String getNms() {
		return nms;
	}

	public void setNms(String nms) {
		this.nms = nms;
	}

	public String getLocalBuildingName() {
		return localBuildingName;
	}

	public void setLocalBuildingName(String localBuildingName) {
		this.localBuildingName = localBuildingName;
	}

	public String getLocationCityCode() {
		return locationCityCode;
	}

	public void setLocationCityCode(String locationCityCode) {
		this.locationCityCode = locationCityCode;
	}

	public String getLocationCountryCode() {
		return locationCountryCode;
	}

	public void setLocationCountryCode(String locationCountryCode) {
		this.locationCountryCode = locationCountryCode;
	}

	public String getResourcePortName1() {
		return resourcePortName1;
	}

	public void setResourcePortName1(String resourcePortName1) {
		this.resourcePortName1 = resourcePortName1;
	}

	public String getResourcePortName2() {
		return resourcePortName2;
	}

	public void setResourcePortName2(String resourcePortName2) {
		this.resourcePortName2 = resourcePortName2;
	}

	public String getNcTechServiceId1() {
		return ncTechServiceId1;
	}

	public void setNcTechServiceId1(String ncTechServiceId1) {
		this.ncTechServiceId1 = ncTechServiceId1;
	}

	public String getNcTechServiceId2() {
		return ncTechServiceId2;
	}

	public void setNcTechServiceId2(String ncTechServiceId2) {
		this.ncTechServiceId2 = ncTechServiceId2;
	}

	@Override
	public String toString() {
		return BaseWorkItemHandler.toString(this);
	}

}
