package net.colt.novitas.workitems.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import net.colt.novitas.workitems.BaseWorkItemHandler;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LocationResponse implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	

	@JsonProperty("site_id")
	private String siteId;

	@JsonProperty("site_name")
	private String siteName;

	@JsonProperty("address")
	private String address;

	@JsonProperty("pm_site_type")
	private String pmSiteType;

	@JsonProperty("composite_site_type")
	private String compositeSiteType;

	@JsonProperty("novitas_site_type")
	private String novitasSiteType;

	@JsonProperty("country")
	private String country;

	@JsonProperty("city")
	private String city;

	@JsonProperty("latitude")
	private Float latitude;

	@JsonProperty("longitude")
	private Float longitude;

	@JsonProperty("building_name")
	private String buildingName;

	@JsonProperty("street_name")
	private String streetName;

	@JsonProperty("floor")
	private String floor;

	@JsonProperty("room_name")
	private String roomName;

	@JsonProperty("premises_number")
	private String premisesNumber;

	@JsonProperty("state")
	private String state;

	@JsonProperty("postal_zip_code")
	private String postalZipCode;

	@JsonProperty("building_id")
	private String buildingId;

	@JsonProperty("building_category")
	private String buildingCategory;

	@JsonProperty("colt_region")
	private String coltRegion;

	@JsonProperty("local_building_name")
	private String localBuildingName;

	@JsonProperty("city_code")
	private String cityCode;

	@JsonProperty("country_code")
	private String countryCode;

	@JsonProperty("cross_connect_allowed")
	private Boolean crossConnectAllowed = false;

	@JsonProperty("is_kdc")
	private Boolean isKdc = false;

	@JsonProperty("is_private_site")
	private boolean isPrivateSite;

	public String getSiteId() {
		return siteId;
	}

	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}

	public String getSiteName() {
		return siteName;
	}

	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPmSiteType() {
		return pmSiteType;
	}

	public void setPmSiteType(String pmSiteType) {
		this.pmSiteType = pmSiteType;
	}

	public String getCompositeSiteType() {
		return compositeSiteType;
	}

	public void setCompositeSiteType(String compositeSiteType) {
		this.compositeSiteType = compositeSiteType;
	}

	public String getNovitasSiteType() {
		return novitasSiteType;
	}

	public void setNovitasSiteType(String novitasSiteType) {
		this.novitasSiteType = novitasSiteType;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Float getLatitude() {
		return latitude;
	}

	public void setLatitude(Float latitude) {
		this.latitude = latitude;
	}

	public Float getLongitude() {
		return longitude;
	}

	public void setLongitude(Float longitude) {
		this.longitude = longitude;
	}

	public String getBuildingName() {
		return buildingName;
	}

	public void setBuildingName(String buildingName) {
		this.buildingName = buildingName;
	}

	public String getStreetName() {
		return streetName;
	}

	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}

	public String getFloor() {
		return floor;
	}

	public void setFloor(String floor) {
		this.floor = floor;
	}

	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	public String getPremisesNumber() {
		return premisesNumber;
	}

	public void setPremisesNumber(String premisesNumber) {
		this.premisesNumber = premisesNumber;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPostalZipCode() {
		return postalZipCode;
	}

	public void setPostalZipCode(String postalZipCode) {
		this.postalZipCode = postalZipCode;
	}

	public String getBuildingId() {
		return buildingId;
	}

	public void setBuildingId(String buildingId) {
		this.buildingId = buildingId;
	}

	public String getBuildingCategory() {
		return buildingCategory;
	}

	public void setBuildingCategory(String buildingCategory) {
		this.buildingCategory = buildingCategory;
	}

	public String getColtRegion() {
		return coltRegion;
	}

	public void setColtRegion(String coltRegion) {
		this.coltRegion = coltRegion;
	}

	public String getLocalBuildingName() {
		return localBuildingName;
	}

	public void setLocalBuildingName(String localBuildingName) {
		this.localBuildingName = localBuildingName;
	}

	public String getCityCode() {
		return cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public Boolean getCrossConnectAllowed() {
		return crossConnectAllowed;
	}

	public void setCrossConnectAllowed(Boolean crossConnectAllowed) {
		this.crossConnectAllowed = crossConnectAllowed;
	}

	public Boolean getIsKdc() {
		return isKdc;
	}

	public void setIsKdc(Boolean isKdc) {
		this.isKdc = isKdc;
	}

	public boolean isPrivateSite() {
		return isPrivateSite;
	}

	public void setPrivateSite(boolean isPrivateSite) {
		this.isPrivateSite = isPrivateSite;
	}
    
	@Override
	public String toString() {
		return BaseWorkItemHandler.toString(this);
	}

}
