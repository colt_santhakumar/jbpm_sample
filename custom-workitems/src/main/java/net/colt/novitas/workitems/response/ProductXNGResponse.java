package net.colt.novitas.workitems.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import net.colt.novitas.workitems.BaseWorkItemHandler;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductXNGResponse implements Serializable {

	private static final long serialVersionUID = -2556409621309704307L;

    @JsonProperty("portType")
	private String portType;
    
    @JsonProperty("bandwidth")
	private String bandwidth;

    @JsonProperty("connectorType")
	private String connectorType;

	public ProductXNGResponse() {
		super();
	}

	public ProductXNGResponse(String portType, String bandwidth, String connectorType) {
		super();
		this.portType = portType;
		this.bandwidth = bandwidth;
		this.connectorType = connectorType;
	}

	public String getPortType() {
		return portType;
	}

	public void setPortType(String portType) {
		this.portType = portType;
	}

	public String getBandwidth() {
		return bandwidth;
	}

	public void setBandwidth(String bandwidth) {
		this.bandwidth = bandwidth;
	}

	public String getConnectorType() {
		return connectorType;
	}

	public void setConnectorType(String connectorType) {
		this.connectorType = connectorType;
	}
	
	@Override
	public String toString() {
		return BaseWorkItemHandler.toString(this);
	}

}
