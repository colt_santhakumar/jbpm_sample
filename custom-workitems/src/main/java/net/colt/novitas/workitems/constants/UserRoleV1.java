package net.colt.novitas.workitems.constants;

public enum UserRoleV1 {
	
	OnDemandFullRole, 
	OnDemandFlexRole, 
	OnDemandPrimaryRole, 
	OnDemandReadOnlyRole;

}
