package net.colt.novitas.workitems.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SarDetails implements Serializable {

	private static final long serialVersionUID = 3029219995690759892L;

	@JsonProperty("building_id")
	private String buildingId;

	@JsonProperty("country")
	private String country;

	@JsonProperty("credentials")
	private String credentials;

	@JsonProperty("free_nni_bandwidth")
	private Integer freeNniBandwidth;

	@JsonProperty("management_ip")
	private String managementIp;

	@JsonProperty("metro")
	private String metro;

	@JsonProperty("nni_id")
	private Integer nniId;

	@JsonProperty("nni_vlan_mapping")
	private String nniVlanMapping;

	@JsonProperty("nni_vlan_type")
	private String nniVlanType;

	@JsonProperty("nni_cpe")
	private String nniCpe;

	@JsonProperty("nni_port")
	private String nniPort;

	@JsonProperty("sar_name")
	private String sarName;

	@JsonProperty("sar_port")
	private String sarPort;

	@JsonProperty("circuit_id")
	private String circuitId;

	public SarDetails() {
		super();
	}


	public SarDetails(String buildingId, String country, String credentials, Integer freeNniBandwidth,
			String managementIp, String metro, Integer nniId, String nniVlanMapping, String nniVlanType, String nniCpe,
			String nniPort, String sarName, String sarPort, String circuitId) {
		super();
		this.buildingId = buildingId;
		this.country = country;
		this.credentials = credentials;
		this.freeNniBandwidth = freeNniBandwidth;
		this.managementIp = managementIp;
		this.metro = metro;
		this.nniId = nniId;
		this.nniVlanMapping = nniVlanMapping;
		this.nniVlanType = nniVlanType;
		this.nniCpe = nniCpe;
		this.nniPort = nniPort;
		this.sarName = sarName;
		this.sarPort = sarPort;
		this.circuitId = circuitId;
	}



	public String getNniVlanMapping() {
		return nniVlanMapping;
	}

	public void setNniVlanMapping(String nniVlanMapping) {
		this.nniVlanMapping = nniVlanMapping;
	}

	public String getNniVlanType() {
		return nniVlanType;
	}

	public void setNniVlanType(String nniVlanType) {
		this.nniVlanType = nniVlanType;
	}

	public String getBuildingId() {
		return buildingId;
	}

	public String getCountry() {
		return country;
	}

	public String getCredentials() {
		return credentials;
	}

	public Integer getFreeNniBandwidth() {
		return freeNniBandwidth;
	}

	public String getManagementIp() {
		return managementIp;
	}

	public String getMetro() {
		return metro;
	}

	public Integer getNniId() {
		return nniId;
	}

	public String getNniPort() {
		return nniPort;
	}

	public String getSarName() {
		return sarName;
	}

	public String getSarPort() {
		return sarPort;
	}

	public void setBuildingId(String buildingId) {
		this.buildingId = buildingId;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public void setCredentials(String credentials) {
		this.credentials = credentials;
	}

	public void setFreeNniBandwidth(Integer freeNniBandwidth) {
		this.freeNniBandwidth = freeNniBandwidth;
	}

	public void setManagementIp(String managementIp) {
		this.managementIp = managementIp;
	}

	public void setMetro(String metro) {
		this.metro = metro;
	}

	public void setNniId(Integer nniId) {
		this.nniId = nniId;
	}

    public String getNniCpe() {
		return nniCpe;
	}

	public void setNniCpe(String nniCpe) {
		this.nniCpe = nniCpe;
	}

	public void setNniPort(String nniPort) {
		this.nniPort = nniPort;
	}

	public void setSarName(String sarName) {
		this.sarName = sarName;
	}

	public void setSarPort(String sarPort) {
		this.sarPort = sarPort;
	}

	public String getCircuitId() {
		return circuitId;
	}

	public void setCircuitId(String circuitId) {
		this.circuitId = circuitId;
	}


	@Override
	public String toString() {
		return "SarDetails [buildingId=" + buildingId + ", country=" + country + ", credentials=" + credentials
				+ ", freeNniBandwidth=" + freeNniBandwidth + ", managementIp=" + managementIp + ", metro=" + metro
				+ ", nniId=" + nniId + ", nniVlanMapping=" + nniVlanMapping + ", nniVlanType=" + nniVlanType
				+ ", nniCpe=" + nniCpe + ", nniPort=" + nniPort + ", sarName=" + sarName + ", sarPort=" + sarPort
				+ ", circuitId=" + circuitId + "]";
	}

}