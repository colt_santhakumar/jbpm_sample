package net.colt.novitas.workitems.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import net.colt.novitas.workitem.request.RequestVLANIdRange;
import net.colt.novitas.workitems.constants.VlanMapping;
import net.colt.novitas.workitems.constants.VlanType;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public class IpaccessServiceResponse implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty("connection_id")
    private String connectionId;

    @JsonProperty("bandwidth")
    private Integer bandwidth;

    @JsonProperty("connection_type")
    private String connectionType;

    @JsonProperty("created_date")
    private String createdDate;

    @JsonProperty("customer_name")
    private String customerName;

    @JsonProperty("decommissioned_on")
    private Date decommissionedOn;

    @JsonProperty("last_updated")
    private Date lastUpdated;

    @JsonProperty("connection_name")
    private String connectionName;

    @JsonProperty("ocn")
    private String ocn;

    @JsonProperty("rental_charge")
    private Float rentalCharge;

    @JsonProperty("rental_unit")
    private String rentalUnit;

    @JsonProperty("penalty_charge")
    private Float penaltyCharge;

    @JsonProperty("currency")
    private String currency;

    @JsonProperty("resource_id")
    private String resourceId;

    @JsonProperty("status")
    private String status;

    @JsonProperty("port_vlan_mapping")
    private String portVlanMapping;

    @JsonProperty("port_vlan_type")
    private String portVlanType;

    @JsonProperty("port_vlan_ids")
    private List<RequestVLANIdRange> portVlanIds;

    @JsonProperty("from_port_id")
    private String fromPortId;

    @JsonProperty("novitas_service_id")
    private String novitasServiceId;

    @JsonProperty("xng_circuit_id")
    private String xngCircuitId;

    @JsonProperty("nni_type")
    private String nniType;

    @JsonProperty("nni_vlan_mapping")
    private VlanMapping nniVlanMapping;

    @JsonProperty("nni_vlan_type")
    private VlanType nniVlanType;

    @JsonProperty("nni_vlan_id")
    private Integer nniVlanId;

    @JsonProperty("nms")
    private String nms;

    @JsonProperty("service_id")
    private String serviceId;

    @JsonProperty("commitment_expiry_date")
    private String commitmentExpiryDate;

    @JsonProperty("commitment_period")
    private Integer commitmentPeriod;

    @JsonProperty("nc_tech_service_id")
    private String ncTechServiceId;

    @JsonProperty("nni_nc_tech_service_id")
    private String nniNcTechServiceId;

    @JsonProperty("cease_request_id")
    private String ceaseRequestId;

    @JsonProperty("cpe_public_ip")
    private String cpePublicIp;

    @JsonProperty("cpe_private_ip")
    private String cpePrivateIp;

    @JsonProperty("private_ip_range")
    private String privateIpRange;

    @JsonProperty("sar_vlan_ip")
    private String sarVlanIp;

    @JsonProperty("sar_name")
    private String sarName;

    @JsonProperty("sar_port")
    private String sarPort;

    @JsonProperty("sar_management_ip")
    private String sarManagementIp;

    @JsonProperty("customer_ip_range")
    private String customerIpRange;

    @JsonProperty("lan_ip_list")
    private List<String> lanIpList;

    @JsonProperty("wan_ip_list")
    private List<String> wanIpList;

    @JsonProperty("from_port_address")
    private String fromPortAddress;

    @JsonProperty("from_port_country")
    private String fromPortCountry;

    @JsonProperty("ip_access_type")
    private String ipAccessType;

    @JsonProperty("vas_container_id")
    private Integer vasContainerId;

    @JsonProperty("base_bandwidth")
    private Integer baseBandwidth;

    @JsonProperty("base_rental")
    private Float baseRental;

    @JsonProperty("bandwidth_boost")
    private List<BandwidthBoostPricesResponseDto> bandwidthBoostPricesResponseDtos;
    
    @JsonProperty("iqnet_nni_circuit")
    private String iqnetNniCircuit;

}
