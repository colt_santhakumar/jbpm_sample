package net.colt.novitas.workitems.constants;

public enum ColtRegionV1 {
	EU, ASIA;
}
