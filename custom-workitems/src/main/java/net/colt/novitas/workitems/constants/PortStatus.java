package net.colt.novitas.workitems.constants;

public enum PortStatus {
	PENDING, ACTIVE, ABORTED,DECOMMISSIONING, DECOMMISSIONED;
}
