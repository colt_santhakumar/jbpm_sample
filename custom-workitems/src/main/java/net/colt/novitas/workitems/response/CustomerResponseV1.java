package net.colt.novitas.workitems.response;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonProperty;

import net.colt.novitas.workitems.constants.ColtRegionV1;
import net.colt.novitas.workitems.constants.CountryCodeV1;
import net.colt.novitas.workitems.constants.CountryV1;
import net.colt.novitas.workitems.constants.CurrencyV1;
import net.colt.novitas.workitems.constants.StatusV1;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomerResponseV1 implements Serializable {
	
	 /**
	  *
	  */	
	  private static final long serialVersionUID = 1L;
	
	  @JsonProperty("id")
	  private Integer id = null;

	  @JsonProperty("status")
	  private StatusV1 status = null;

	  @JsonProperty("name")
	  private String name = null;

	  @JsonProperty("local_name")
	  private String localName = null;

	  @JsonProperty("ocn")
	  private String ocn = null;

	  @JsonProperty("bcn")
	  private String bcn = null;

	  @JsonProperty("currency")
	  private CurrencyV1 currency = null;

	  @JsonProperty("is_spend_limited")
	  private Boolean isSpendLimited = null;

	  @JsonProperty("monthly_spend_limit")
	  private Double monthlySpendLimit = null;

	  @JsonProperty("colt_region")
	  private ColtRegionV1 coltRegion = null;

	  @JsonProperty("country_code")
	  private CountryCodeV1 countryCode = null;

	  @JsonProperty("country")
	  private CountryV1 country = null;

	  @JsonProperty("language_code")
	  private String languageCode = null;

	  @JsonProperty("ocn_hash")
	  private String ocnHash = null;

	  @JsonProperty("performance_tier")
	  private String performanceTier = null;

	  @JsonProperty("is_mfa_required")
	  private Boolean isMfaRequired = null;


	  public Integer getId() {
	    return id;
	  }

	  public void setId(Integer id) {
	    this.id = id;
	  }

	  public StatusV1 getStatus() {
		    return status;
		  }
	  
	  public void setStatus(StatusV1 status) {
	    this.status = status;
	  }

	  public String getName() {
	    return name;
	  }

	  public void setName(String name) {
	    this.name = name;
	  }

	  public String getLocalName() {
	    return localName;
	  }

	  public void setLocalName(String localName) {
	    this.localName = localName;
	  }

	  public String getOcn() {
	    return ocn;
	  }

	  public void setOcn(String ocn) {
	    this.ocn = ocn;
	  }

	  public String getBcn() {
	    return bcn;
	  }

	  public void setBcn(String bcn) {
	    this.bcn = bcn;
	  }

	  public CurrencyV1 getCurrency() {
	    return currency;
	  }

	  public void setCurrency(CurrencyV1 currency) {
	    this.currency = currency;
	  }

	 public Boolean isIsSpendLimited() {
	    return isSpendLimited;
	  }

	  public void setIsSpendLimited(Boolean isSpendLimited) {
	    this.isSpendLimited = isSpendLimited;
	  }

	  public Double getMonthlySpendLimit() {
	    return monthlySpendLimit;
	  }

	  public void setMonthlySpendLimit(Double monthlySpendLimit) {
	    this.monthlySpendLimit = monthlySpendLimit;
	  }

	  public ColtRegionV1 getColtRegion() {
	    return coltRegion;
	  }

	  public void setColtRegion(ColtRegionV1 coltRegion) {
	    this.coltRegion = coltRegion;
	  }

	  public CountryCodeV1 getCountryCode() {
	    return countryCode;
	  }

	  public void setCountryCode(CountryCodeV1 countryCode) {
	    this.countryCode = countryCode;
	  }

	  public CountryV1 getCountry() {
	    return country;
	  }

	  public void setCountry(CountryV1 country) {
	    this.country = country;
	  }

	  public String getLanguageCode() {
	    return languageCode;
	  }

	  public void setLanguageCode(String languageCode) {
	    this.languageCode = languageCode;
	  }

	  public String getOcnHash() {
	    return ocnHash;
	  }

	  public void setOcnHash(String ocnHash) {
	    this.ocnHash = ocnHash;
	  }

	  public String getPerformanceTier() {
	    return performanceTier;
	  }

	  public void setPerformanceTier(String performanceTier) {
	    this.performanceTier = performanceTier;
	  }

	  public Boolean isIsMfaRequired() {
	    return isMfaRequired;
	  }

	  public void setIsMfaRequired(Boolean isMfaRequired) {
	    this.isMfaRequired = isMfaRequired;
	  }

}
