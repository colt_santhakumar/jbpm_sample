package net.colt.novitas.workitems.response.equinix;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@Getter
@Setter
public class EquinixPortRequestResponse implements Serializable {

    private static final long serialVersionUID = -500695983201900838L;

    private Integer id;

    private String novitasServiceId;

    private String serviceId;

    private String name;

    private String status;

    private String action;

    private String requestedAt;

    private String description;

    private String lastUpdated;

    private String portalUserId;

    private String requestedBy;

    private Integer statusCode;

    private Integer processInstanceId;

    private String customerName;

    private String ocn;

    private String bcn;

    private Float rentalCharge;

    private String currency;

    private String rentalUnit;

    private Float installationCharge;

    private Float decommissioningCharge;

    private Integer commitmentPeriod;

    private Float penaltyCharge;

    private Float latitude;

    private Float longitude;

    private String country;

    private String countryCode;

    private String state;

    private String city;

    private String cityCode;

    private String streetName;

    private String postalZipCode;

    private String buildingId;

    private String buildingName;

    private String premisesNumber;

    private String siteName;

    private Integer bandwidth;

    private String cspKey;

    private String cloudProvider;

    private String cloudRegion;

}
