package net.colt.novitas.workitems.response;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class BandwidthBoostPricesResponse implements Serializable {
	
	private static final long serialVersionUID = 1L;

    @JsonProperty("expires_at_dt")
    private Date expiresAtDate;

    @JsonProperty("bandwidth_boost_prices")
    private List<BandwidthBoostPricesResponseDto> bandwidthBoostPricesResponseDtoList;
}

