package net.colt.novitas.workitems;

import java.net.InetAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.TimeZone;

import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.Protocol;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder.EndpointConfiguration;
import com.amazonaws.services.directconnect.AmazonDirectConnectClient;
import com.amazonaws.services.directconnect.AmazonDirectConnectClientBuilder;
import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.resource.client.ResourceAPIClient;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.colt.novitas.workitem.request.RequestVLANIdRange;

public abstract class BaseWorkItemHandler implements WorkItemHandler {

	private static Logger logger = LoggerFactory.getLogger(BaseWorkItemHandler.class);

	protected static final String INPROGRESS = "INPROGRESS";
	protected static final String DCA_PORT = "COLT On Demand DCA Port";

	protected static final String DCA_PORT_DELETE_SIEBEL_SIGNAL = "delete-dca-port-siebel-signal";
	protected static final String DCA_PORT_CREATE_SIEBEL_SIGNAL = "create-dca-port-siebel-signal";

	protected static String LOG_UNIQUE_ID = null;

	protected Logger getLogger(WorkItem workItem, String uniqueLogId) {
		if (null == LOG_UNIQUE_ID) {
			LOG_UNIQUE_ID = uniqueLogId;
			if (null != uniqueLogId) {
				MDC.put("unique_log_id", "J" + uniqueLogId.substring(1));
			} else {
				Random rand = new Random();
				MDC.put("unique_log_id", "J" + String.valueOf(rand.nextInt(1000)));
			}
		}
		logger = LoggerFactory
				.getLogger(uniqueLogId + "|ProcessId-" + workItem.getProcessInstanceId() + "|" + workItem.getName());
		return logger;
	}

	protected EnvironmentProperties getEnvProperties(WorkItem workItem) {
		return (EnvironmentProperties) workItem.getParameter("env_properties");
	}

	protected String getOcn(WorkItem workItem) {
		return String.valueOf(workItem.getParameter("ocn"));
	}

	protected String getLocationId(WorkItem workItem) {
		return String.valueOf(workItem.getParameter("location_id"));
	}

	protected String getCloudProvider(WorkItem workItem) {
		Object val = workItem.getParameter("cloud_provider");
		return null == val || "".equals(val) ? null : String.valueOf(val);
	}

	protected Integer getRequestId(WorkItem workItem) {
		Object obj = workItem.getParameter("request_id");
		Integer requestId = null;
		if (obj instanceof String) {
			requestId = Integer.valueOf(String.valueOf(obj));
		} else {
			requestId = (Integer) obj;
		}
		logger.info("Request Id : " + requestId);

		return requestId;
	}

	public Exception getWorkflowError(WorkItem workItem) {
		Object obj = workItem.getParameter("error_object");
		if (obj instanceof Exception) {
			return (Exception) obj;
		}
		return null;
	}

	public static String getJbpmUrl(String url) {
		try {
			String host = InetAddress.getLocalHost().getHostName();
			url = url.replaceAll("%s", host);
		} catch (UnknownHostException e) {
			logger.error(e.getMessage());
		}
		return url;
	}

	protected AmazonDirectConnectClient getAmazonDirectConnectClient(String region, EnvironmentProperties prop,
			String coltRegion) {
		ClientConfiguration config = new ClientConfiguration();
		ResourceAPIClient conf = new ResourceAPIClient(prop.getConfigServiceUrl());
		String proxyHost = conf.getProperty(prop.getEnvironment(), "redirect.proxy.host").getValue();
		int proxyPort = Integer.valueOf(conf.getProperty(prop.getEnvironment(), "redirect.proxy.port").getValue());
		config.setProtocol(Protocol.HTTPS);
		config.setProxyHost(proxyHost);
		config.setProxyPort(proxyPort);
		config.setConnectionTimeout(prop.getAwsConnectionTimeOut());
		config.setSocketTimeout(prop.getAwsConnectionTimeOut());
		String accessKey = prop.getAwsAccessKey();
		String secretKey = prop.getAwsSecretKey();
		logger.info("Colt Region|" + coltRegion);
		if ("PO".equals(coltRegion) || coltRegion.contains("ASIA")) {
			accessKey = prop.getAwsAsiaAccessKey();
			secretKey = prop.getAwsAsiaSecretKey();
		}
		logger.info("AWS Account|Access Key | secret Key - | " + accessKey + " | " + secretKey);
		BasicAWSCredentials basic = new BasicAWSCredentials(accessKey, secretKey);
		String endPoint = conf.getProperty(prop.getEnvironment(), "aws.endpoint." + region).getValue();
		EndpointConfiguration endpointConfiguration = new EndpointConfiguration(endPoint, region);
		AmazonDirectConnectClientBuilder clientBuilder = AmazonDirectConnectClientBuilder.standard()
				.withClientConfiguration(config)
				// .withRegion(region)
				.withEndpointConfiguration(endpointConfiguration)
				.withCredentials(new AWSStaticCredentialsProvider(basic));
		// clientBuilder.setClientConfiguration(config);
		// clientBuilder.setRegion(region);
		// clientBuilder.setCredentials(new AWSStaticCredentialsProvider(basic));

		// clientBuilder.setEndpointConfiguration(endpointConfiguration);
		AmazonDirectConnectClient client = (AmazonDirectConnectClient) clientBuilder.build();
		logger.info("AWS enpoint for region |" + region + "|" + endPoint);
		return client;
	}

	protected String getAwsInterConnectionId(WorkItem workItem) {
		return String.valueOf(workItem.getParameter("aws_interconnection_id"));
	}

	protected String getAwsAccountNo(WorkItem workItem) {
		return String.valueOf(workItem.getParameter("aws_account_no"));
	}

	protected String getAwshConnectionId(WorkItem workItem) {
		return String.valueOf(workItem.getParameter("awsh_connection_id"));
	}

	protected String getAWSRegion(WorkItem workItem) {
		return String.valueOf(workItem.getParameter("aws_region"));
		/*
		 * Regions[] regions = Regions.values(); for (Regions regions2 : regions) {
		 * if(regions2.getName().equals(reg) || regions2.name().equals(reg)){ return
		 * regions2; } } return null;
		 */
	}

	public static String getCommitmentExpiryDate(String startDate, Integer commitmentPeriod, String frequency) {
		DateFormat fdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
		fdf.setTimeZone(TimeZone.getTimeZone("UTC"));
		Calendar c = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		if (startDate != null) {

			try {
				c.setTime(fdf.parse(startDate));
			} catch (ParseException e) {
				logger.info("Date Parse exception");
				e.printStackTrace();
			}
			c.add(Calendar.MONTH, commitmentPeriod);
			if ("MONTHLY".equals(frequency)) {
				c.set(Calendar.HOUR_OF_DAY, c.getActualMaximum(Calendar.HOUR_OF_DAY));
			}
			c.set(Calendar.MINUTE, c.getActualMaximum(Calendar.MINUTE));
			c.set(Calendar.SECOND, c.getActualMaximum(Calendar.SECOND));
			return fdf.format(c.getTime());

		} else {
			return "";
		}
	}

	public static String getXConnctCommitmentExpiryDate(String startDate, Integer commitmentPeriod, String frequency) {
		DateFormat fdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
		fdf.setTimeZone(TimeZone.getTimeZone("UTC"));
		Calendar c = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		if (startDate != null) {

			try {
				c.setTime(fdf.parse(startDate));
			} catch (ParseException e) {
				logger.info("Date Parse exception");
				e.printStackTrace();
			}
			c.add(Calendar.MONTH, commitmentPeriod);
			if ("MONTHLY".equals(frequency)) {
				c.set(Calendar.HOUR_OF_DAY, c.getActualMaximum(Calendar.HOUR_OF_DAY));
			}
			c.add(Calendar.DAY_OF_MONTH, -1);
			c.set(Calendar.MINUTE, c.getActualMaximum(Calendar.MINUTE));
			c.set(Calendar.SECOND, c.getActualMaximum(Calendar.SECOND));

			return fdf.format(c.getTime());

		} else {
			return "";
		}
	}

	public static String getStringFromList(List<RequestVLANIdRange> requestVLANIdRanges) {

		if (requestVLANIdRanges == null || requestVLANIdRanges.isEmpty()) {
			return null;
		}

		StringBuilder sb = new StringBuilder();
		for (RequestVLANIdRange requestVLANIdRange : requestVLANIdRanges) {
			if (sb.length() > 0) {
				sb.append(",");
			}

			if (null != requestVLANIdRange.getFromIdRange() && null != requestVLANIdRange.getToIdRange()
					&& !requestVLANIdRange.getFromIdRange().equals(requestVLANIdRange.getToIdRange())) {

				sb.append("[").append(requestVLANIdRange.getFromIdRange()).append("-")
						.append(requestVLANIdRange.getToIdRange()).append("]");
			} else if (null != requestVLANIdRange.getFromIdRange())
				sb.append(requestVLANIdRange.getFromIdRange());

		}
		logger.info("Converted VLANs--> " + sb.toString());
		return sb.toString();
	}

	public static String getPortExpiryDate(Integer commitmentPeriod) {

		DateFormat fdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
		fdf.setTimeZone(TimeZone.getTimeZone("UTC"));
		Calendar c = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		c.add(Calendar.DATE, commitmentPeriod);

		return fdf.format(c.getTime());

	}

	public static URI getPOCallbackUrl(String url) {

		try {
			return new URI(url);
		} catch (URISyntaxException e) {
			logger.error("Calback URL error", e);
		}
		return null;

	}

	protected String getColtRegion(WorkItem workItem) {
		return String.valueOf(workItem.getParameter("colt_region"));
	}

	public static String getFirstOfFollowingMonth() {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MONTH, 1);
		calendar.set(Calendar.DATE, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
		Date nextMonthFirstDay = calendar.getTime();
		return nextMonthFirstDay.toString();
	}

	public static <T> String toString(T obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (JsonProcessingException e) {
			return obj.toString();
		}
	}

	public static boolean isCommitmentDateValid(String strDate) {

		if (null != strDate && !strDate.isEmpty()) {

			DateFormat fdf = new SimpleDateFormat(strDate.length() > 20 ? 
					"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'" : "yyyy-MM-dd'T'HH:mm:ss'Z'");
			try {                                 
				Date date = fdf.parse(strDate);

				return date.after(new Date());

			} catch (ParseException e) {
				e.printStackTrace();
			}

		}
		return false;
	}

	public static boolean isCommitmentOffsetDateValid(OffsetDateTime strDate) {

		if (null != strDate) {
			return strDate.isAfter(OffsetDateTime.now());
		}
		return false;
	}

	public static String getCommitmentExpiryDate(Integer commitmentPeriod, String frequency) {
		DateFormat fdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
		// '2011-12-03T10:15:30+01:00'.

		fdf.setTimeZone(TimeZone.getTimeZone("UTC"));
		Calendar c = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		c.setTime(new Date());
		c.add(Calendar.MONTH, commitmentPeriod);
		if ("MONTHLY".equals(frequency)) {
			c.set(Calendar.HOUR_OF_DAY, c.getActualMaximum(Calendar.HOUR_OF_DAY));
		}
		c.add(Calendar.DAY_OF_MONTH, -1);
		c.set(Calendar.MINUTE, c.getActualMaximum(Calendar.MINUTE));
		c.set(Calendar.SECOND, c.getActualMaximum(Calendar.SECOND));
		return fdf.format(c.getTime());

	}

	public static OffsetDateTime getOffsetDateTimeCommitmentExpiryDate(Integer commitmentPeriod, String frequency) {
		DateFormat fdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
		// '2011-12-03T10:15:30+01:00'.

		fdf.setTimeZone(TimeZone.getTimeZone("UTC"));
		Calendar c = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		c.setTime(new Date());
		c.add(Calendar.MONTH, commitmentPeriod);
		if ("MONTHLY".equals(frequency)) {
			c.set(Calendar.HOUR_OF_DAY, c.getActualMaximum(Calendar.HOUR_OF_DAY));
		}
		c.add(Calendar.DAY_OF_MONTH, -1);
		c.set(Calendar.MINUTE, c.getActualMaximum(Calendar.MINUTE));
		c.set(Calendar.SECOND, c.getActualMaximum(Calendar.SECOND));

		Instant instant = c.getTime().toInstant();
		OffsetDateTime offsetDateTime = instant.atOffset(ZoneOffset.UTC);

		return offsetDateTime;

	}

	public static String stringFormatOfOffSetDateTime(OffsetDateTime date) {

		return date.toZonedDateTime().format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssZ"));
	}

}
