package net.colt.novitas.workitems.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import net.colt.novitas.workitems.BaseWorkItemHandler;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PortOrderResponse implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty("request_id")
	private String requestId;
	
	@JsonProperty("olo_order_id")
	private String oloOrderId;

	
	@JsonProperty("olo_service_id")
	private String oloServiceId;
	
	@JsonProperty("status")
	private PortOrderStatus status;

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public String getOloOrderId() {
		return oloOrderId;
	}

	public void setOloOrderId(String oloOrderId) {
		this.oloOrderId = oloOrderId;
	}

	public String getOloServiceId() {
		return oloServiceId;
	}

	public void setOloServiceId(String oloServiceId) {
		this.oloServiceId = oloServiceId;
	}

	public PortOrderStatus getStatus() {
		return status;
	}

	public void setStatus(PortOrderStatus status) {
		this.status = status;
	}
	
	
	@Override
	public String toString() {
		return BaseWorkItemHandler.toString(this);
	}
	
	

}
