package net.colt.novitas.workitems.constants;

public enum StatusType {

    SUCCESS,
    ERROR,
    WARNING;

    public String value() {
        return name();
    }

    public static StatusType fromValue(String v) {
        return valueOf(v);
    }

}