package net.colt.novitas.workitems.constants;

public enum CountryCodeV1 {
	AT,
    AU,
    BE,
    BG,
    CA,
    CH,
    CS,
    CZ,
    DE,
    DK,
    ES,
    FI,
    FR,
    GB,
    HR,
    HK,
    HU,
    IE,
    IT,
    JP,
    LU,
    NL,
    PL,
    PT,
    RO,
    SG,
    SK,
    SE,
    US;

}
