package net.colt.novitas.workitems.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import net.colt.novitas.workitems.constants.StatusType;

@JsonIgnoreProperties(ignoreUnknown = true)
public class XngIpaConnectionResponse implements Serializable {

	private static final long serialVersionUID = 8353418756374251345L;

	@JsonProperty("transaction_id")
	private String transactionId;

	@JsonProperty("iqnet_nni_circuit")
	private String iqnetNniCircuit;

	@JsonProperty("status_type")
	private StatusType statusType;

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getIqnetNniCircuit() {
		return iqnetNniCircuit;
	}

	public void setIqnetNniCircuit(String iqnetNniCircuit) {
		this.iqnetNniCircuit = iqnetNniCircuit;
	}

	public StatusType getStatusType() {
		return statusType;
	}

	public void setStatusType(StatusType statusType) {
		this.statusType = statusType;
	}

}
