package net.colt.novitas.workitems.constants;

public enum NovitasServiceStatus {

	ACTIVE, INACTIVE, ARCHIVED;

}
