package net.colt.novitas.workitems.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
@ToString
public class OracleCrossConnectMappingResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1703910340386830049L;

	private String crossConnectOrCrossConnectGroupId;

	private Integer vlan;

	private String oracleBgpPeeringIp;

	private String customerBgpPeeringIp;

	public String getCrossConnectOrCrossConnectGroupId() {
		return crossConnectOrCrossConnectGroupId;
	}

	public void setCrossConnectOrCrossConnectGroupId(String crossConnectOrCrossConnectGroupId) {
		this.crossConnectOrCrossConnectGroupId = crossConnectOrCrossConnectGroupId;
	}

	public Integer getVlan() {
		return vlan;
	}

	public void setVlan(Integer vlan) {
		this.vlan = vlan;
	}

	public String getOracleBgpPeeringIp() {
		return oracleBgpPeeringIp;
	}

	public void setOracleBgpPeeringIp(String oracleBgpPeeringIp) {
		this.oracleBgpPeeringIp = oracleBgpPeeringIp;
	}

	public String getCustomerBgpPeeringIp() {
		return customerBgpPeeringIp;
	}

	public void setCustomerBgpPeeringIp(String customerBgpPeeringIp) {
		this.customerBgpPeeringIp = customerBgpPeeringIp;
	}

}
