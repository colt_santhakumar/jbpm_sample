package net.colt.novitas.workitems.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import net.colt.novitas.workitems.BaseWorkItemHandler;

@JsonIgnoreProperties(ignoreUnknown = true)
public class NetCrackerTechnicalOrderResponse implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty("correlation_id")
	private String correlationId;
	
	@JsonProperty("status")
	private String status;
	
	@JsonProperty("order_status")
	private String orderStatus;
	
	@JsonProperty("technical_service_id")
	private String technicalServiceId;
	
	
	@JsonProperty("service_id")
	private String serviceId;
	
	@JsonProperty("circuit_reference")
	private String circuit_reference;
	
	
	public String getCorrelationId() {
		return correlationId;
	}

	public void setCorrelationId(String correlationId) {
		this.correlationId = correlationId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public String getTechnicalServiceId() {
		return technicalServiceId;
	}

	public void setTechnicalServiceId(String technicalServiceId) {
		this.technicalServiceId = technicalServiceId;
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public String getCircuit_reference() {
		return circuit_reference;
	}

	public void setCircuit_reference(String circuit_reference) {
		this.circuit_reference = circuit_reference;
	}

	@Override
	public String toString() {
		return BaseWorkItemHandler.toString(this);
	}
	
	

}
