package net.colt.novitas.workitems.response;


import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RefundResponse implements Serializable{

	private static final long serialVersionUID = -8324697943455411549L;

	@JsonProperty("refund_amount")
	private Float refundAmount;

	@JsonProperty("charge_type")
	private String chargeType;

	@JsonProperty("percentage_refund")
	private Float percentageRefund;

	@JsonProperty("comment")
	private String comment;

	@JsonProperty("start_date")
	private String startDate;

	@JsonProperty("end_date")
	private String endDate;


	public RefundResponse() {
		super();
	}

	public RefundResponse(Float refundAmount, String chargeType, Float percentageRefund, String comment,
			String startDate, String endDate) {
		super();
		this.refundAmount = refundAmount;
		this.chargeType = chargeType;
		this.percentageRefund = percentageRefund;
		this.comment = comment;
		this.startDate = startDate;
		this.endDate = endDate;
	}

	public Float getRefundAmount() {
		return refundAmount;
	}

	public void setRefundAmount(Float refundAmount) {
		this.refundAmount = refundAmount;
	}

	public String getChargeType() {
		return chargeType;
	}

	public void setChargeType(String chargeType) {
		this.chargeType = chargeType;
	}

	public Float getPercentageRefund() {
		return percentageRefund;
	}

	public void setPercentageRefund(Float percentageRefund) {
		this.percentageRefund = percentageRefund;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

}
