package net.colt.novitas.workitems.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import net.colt.novitas.workitems.BaseWorkItemHandler;

@JsonIgnoreProperties(ignoreUnknown = true)
public class NetCrackerHandoverDetailsResponse implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty("order_status")
	private String orderStatus;
	
	@JsonProperty("technical_service_id")
	private String technicalServiceId;
	
	@JsonProperty("cabinet-id")
	private String cabinetId;
	
	@JsonProperty("shelf_id")
	private String shelfId;
	
	@JsonProperty("physical_port_id")
	private String physicalPortId;
	
	@JsonProperty("slot_id")
	private String slotId;
	
	@JsonProperty("vlan_tag_id")
	private String vlanTagId;
	
	@JsonProperty("site_id")
	private String siteId;
	
	@JsonProperty("presentation_panel")
	private String presentationPanel;
	
	@JsonProperty("interconnect_ref")
	private String interConnectRef;

	@JsonProperty("circuit_reference")
	private String circuitReference;
	
	@JsonProperty("network_type")
	private String networkType;
	
	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public String getTechnicalServiceId() {
		return technicalServiceId;
	}

	public void setTechnicalServiceId(String technicalServiceId) {
		this.technicalServiceId = technicalServiceId;
	}

	public String getCabinetId() {
		return cabinetId;
	}

	public void setCabinetId(String cabinetId) {
		this.cabinetId = cabinetId;
	}

	public String getShelfId() {
		return shelfId;
	}

	public void setShelfId(String shelfId) {
		this.shelfId = shelfId;
	}

	public String getPhysicalPortId() {
		return physicalPortId;
	}

	public void setPhysicalPortId(String physicalPortId) {
		this.physicalPortId = physicalPortId;
	}

	public String getSlotId() {
		return slotId;
	}

	public void setSlotId(String slotId) {
		this.slotId = slotId;
	}

	public String getVlanTagId() {
		return vlanTagId;
	}

	public void setVlanTagId(String vlanTagId) {
		this.vlanTagId = vlanTagId;
	}

	public String getSiteId() {
		return siteId;
	}

	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}

	public String getPresentationPanel() {
		return presentationPanel;
	}

	public void setPresentationPanel(String presentationPanel) {
		this.presentationPanel = presentationPanel;
	}

	public String getInterConnectRef() {
		return interConnectRef;
	}

	public void setInterConnectRef(String interConnectRef) {
		this.interConnectRef = interConnectRef;
	}
	
    public String getCircuitReference() {
        return circuitReference;
    }
    
    public void setCircuitReference(String value) {
        this.circuitReference = value;
    }

	public String getNetworkType() {
		return networkType;
	}

	public void setNetworkType(String networkType) {
		this.networkType = networkType;
	}

	@Override
	public String toString() {
		return BaseWorkItemHandler.toString(this);
	}


}