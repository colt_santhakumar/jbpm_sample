package net.colt.novitas.workitems.response;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ColtLOAResponse implements Serializable, Comparable<ColtLOAResponse> {

	private static final long serialVersionUID = -4699970622870689354L;
	
	@JsonInclude(Include.NON_NULL)
	@JsonProperty("port_id")
	private String portId;

	@JsonProperty("loa_id")
	private Integer loaId;

	@JsonProperty("issued_on")
	private Date issuedOn;

	@JsonProperty("expires_on")
	private Date expiresOn;

	@JsonProperty("issued_to")
	private String issuedTo;

	@JsonProperty("requested_by")
	private String requestedBy;

	@JsonInclude(Include.NON_NULL)
	@JsonProperty("address")
	private String address;

	@JsonInclude(Include.NON_NULL)
	@JsonProperty("floor")
	private String floor;

	@JsonInclude(Include.NON_NULL)
	@JsonProperty("room")
	private String room;

	@JsonInclude(Include.NON_NULL)
	@JsonProperty("presentation_label")
	private String presentationLabel;

	@JsonInclude(Include.NON_NULL)
	@JsonProperty("technology")
	private String technology;

	@JsonInclude(Include.NON_NULL)
	@JsonProperty("connector")
	private String connector;

	public String getPortId() {
		return portId;
	}

	public void setPortId(String portId) {
		this.portId = portId;
	}

	public Integer getLoaId() {
		return loaId;
	}

	public void setLoaId(Integer loaId) {
		this.loaId = loaId;
	}

	public Date getIssuedOn() {
		return issuedOn;
	}

	public void setIssuedOn(Date issuedOn) {
		this.issuedOn = issuedOn;
	}

	public Date getExpiresOn() {
		return expiresOn;
	}

	public void setExpiresOn(Date expiresOn) {
		this.expiresOn = expiresOn;
	}

	public String getIssuedTo() {
		return issuedTo;
	}

	public void setIssuedTo(String issuedTo) {
		this.issuedTo = issuedTo;
	}

	public String getRequestedBy() {
		return requestedBy;
	}

	public void setRequestedBy(String requestedBy) {
		this.requestedBy = requestedBy;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getFloor() {
		return floor;
	}

	public void setFloor(String floor) {
		this.floor = floor;
	}

	public String getRoom() {
		return room;
	}

	public void setRoom(String room) {
		this.room = room;
	}

	public String getPresentationLabel() {
		return presentationLabel;
	}

	public void setPresentationLabel(String presentationLabel) {
		this.presentationLabel = presentationLabel;
	}

	public String getTechnology() {
		return technology;
	}

	public void setTechnology(String technology) {
		this.technology = technology;
	}

	public String getConnector() {
		return connector;
	}

	public void setConnector(String connector) {
		this.connector = connector;
	}

	@Override
	public String toString() {
		return "ColtLOAResponse [portId=" + portId + ", loaId=" + loaId
				+ ", issuedOn=" + issuedOn + ", expiresOn=" + expiresOn
				+ ", issuedTo=" + issuedTo + ", requestedBy=" + requestedBy
				+ ", address=" + address + ", floor=" + floor + ", room="
				+ room + ", presentationLabel=" + presentationLabel
				+ ", technology=" + technology + ", connector=" + connector
				+ "]";
	}

	@Override
	public int compareTo(ColtLOAResponse o) {
		return this.loaId.compareTo(o.getLoaId());
	}


}
