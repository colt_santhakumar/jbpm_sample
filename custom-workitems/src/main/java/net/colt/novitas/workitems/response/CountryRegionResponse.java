package net.colt.novitas.workitems.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import net.colt.novitas.workitems.BaseWorkItemHandler;

/**
 * @author DRao
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CountryRegionResponse implements Serializable {

	private static final long serialVersionUID = 6175949191426046814L;

	@JsonProperty("country")
	private String country;

	@JsonProperty("region")
	private String region;

	@JsonProperty("address_format")
	private String addressFormat;

	@JsonProperty("country_code")
	private String countryCode;
	
	@JsonProperty("ipa_allowed")
	private boolean ipaAllowed;
	
	
	public boolean isIpaAllowed() {
		return ipaAllowed;
	}

	public void setIpaAllowed(boolean ipaAllowed) {
		this.ipaAllowed = ipaAllowed;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getAddressFormat() {
		return addressFormat;
	}

	public void setAddressFormat(String addressFormat) {
		this.addressFormat = addressFormat;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	
	@Override
	public String toString() {
		return BaseWorkItemHandler.toString(this);
	}

}
