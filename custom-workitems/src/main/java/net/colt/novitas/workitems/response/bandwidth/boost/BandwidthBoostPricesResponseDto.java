package net.colt.novitas.workitems.response.bandwidth.boost;

import java.io.Serializable;
import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.*;

@Getter
@Setter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class BandwidthBoostPricesResponseDto implements Serializable {

	private static final long serialVersionUID = 1L;
	
    @JsonProperty("bandwidth_mb")
    private Integer bandwidth;

    @JsonProperty("rental_boost_charge")
    private BigDecimal rentalBoostCharge;

}
