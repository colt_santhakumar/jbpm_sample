package net.colt.novitas.workitems.response;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AllocateSubnetInternalResponse implements Serializable {

	private static final long serialVersionUID = 8253804038221669350L;

	@JsonProperty("customer_ip_range")
	private String customerIpRange;

	@JsonProperty("cpe_public_ip")
	private String cpePublicIp;

	@JsonProperty("sar_vlan_ip")
	private String sarVlanIp;

	@JsonProperty("private_ip_range")
	private String privateIPRange;

	@JsonProperty("cpe_private_ip")
	private String cpePrivateIp;

	@JsonProperty("wan_ip_list")
	private List<String> wanIPList;

	@JsonProperty("lan_ip_list")
	private List<String> lanIPList;

	public List<String> getWanIPList() {
		return wanIPList;
	}

	public void setWanIPList(List<String> wanIPList) {
		this.wanIPList = wanIPList;
	}

	public List<String> getLanIPList() {
		return lanIPList;
	}

	public void setLanIPList(List<String> lanIPList) {
		this.lanIPList = lanIPList;
	}

	public String getCustomerIpRange() {
		return customerIpRange;
	}

	public void setCustomerIpRange(String customerIpRange) {
		this.customerIpRange = customerIpRange;
	}

	public String getPrivateIPRange() {
		return privateIPRange;
	}

	public void setPrivateIPRange(String privateIPRange) {
		this.privateIPRange = privateIPRange;
	}

	public String getCpePublicIp() {
		return cpePublicIp;
	}

	public void setCpePublicIp(String cpePublicIp) {
		this.cpePublicIp = cpePublicIp;
	}

	public String getCpePrivateIp() {
		return cpePrivateIp;
	}

	public void setCpePrivateIp(String cpePrivateIp) {
		this.cpePrivateIp = cpePrivateIp;
	}

	public String getSarVlanIp() {
		return sarVlanIp;
	}

	public void setSarVlanIp(String sarVlanIp) {
		this.sarVlanIp = sarVlanIp;
	}

}
