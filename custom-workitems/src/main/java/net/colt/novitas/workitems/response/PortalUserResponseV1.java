package net.colt.novitas.workitems.response;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import net.colt.novitas.workitems.constants.PortalUserStatusV1;
import net.colt.novitas.workitems.constants.SupportedLanguageV1;
import net.colt.novitas.workitems.constants.UserRoleV1;
import net.colt.novitas.workitems.constants.UserTypeV1;

@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class PortalUserResponseV1 implements Serializable {

	private static final long serialVersionUID = -93095396184990172L;

	@JsonProperty("id")
	private Integer id = null;

	@JsonProperty("customer_id")
	private Integer customerId = null;

	@JsonProperty("username")
	private String username = null;

	@JsonProperty("status")
	private PortalUserStatusV1 status = null;

	@JsonProperty("name")
	private String name = null;

	@JsonProperty("title")
	private String title = null;

	@JsonProperty("email")
	private String email = null;

	@JsonProperty("telephone_number")
	private String telephoneNumber = null;

	@JsonProperty("preferred_language")
	private SupportedLanguageV1 preferredLanguage = null;

	@JsonProperty("most_recent_login_dt")
	private String mostRecentLoginDt = null;

	@JsonProperty("most_recent_user_role")
	private UserRoleV1 mostRecentUserRole = null;

	@JsonProperty("tc_accepted_dt")
	private String tcAcceptedDt = null;

	@JsonProperty("tc_accepted_id")
	private Integer tcAcceptedId = null;

	@JsonProperty("user_type")
	private UserTypeV1 userType = null;

	@JsonProperty("are_concurrent_sessions_allowed")
	private Boolean areConcurrentSessionsAllowed = null;

	@JsonProperty("user_hash")
	private String userHash = null;

	@JsonProperty("api_user")
	private Boolean apiUser = null;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public PortalUserStatusV1 getStatus() {
		return status;
	}

	public void setStatus(PortalUserStatusV1 status) {
		this.status = status;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelephoneNumber() {
		return telephoneNumber;
	}

	public void setTelephoneNumber(String telephoneNumber) {
		this.telephoneNumber = telephoneNumber;
	}

	public SupportedLanguageV1 getPreferredLanguage() {
		return preferredLanguage;
	}

	public void setPreferredLanguage(SupportedLanguageV1 preferredLanguage) {
		this.preferredLanguage = preferredLanguage;
	}

	public String getMostRecentLoginDt() {
		return mostRecentLoginDt;
	}

	public void setMostRecentLoginDt(String mostRecentLoginDt) {
		this.mostRecentLoginDt = mostRecentLoginDt;
	}

	public UserRoleV1 getMostRecentUserRole() {
		return mostRecentUserRole;
	}

	public void setMostRecentUserRole(UserRoleV1 mostRecentUserRole) {
		this.mostRecentUserRole = mostRecentUserRole;
	}

	public String getTcAcceptedDt() {
		return tcAcceptedDt;
	}

	public void setTcAcceptedDt(String tcAcceptedDt) {
		this.tcAcceptedDt = tcAcceptedDt;
	}

	public Integer getTcAcceptedId() {
		return tcAcceptedId;
	}

	public void setTcAcceptedId(Integer tcAcceptedId) {
		this.tcAcceptedId = tcAcceptedId;
	}

	public UserTypeV1 getUserType() {
		return userType;
	}

	public void setUserType(UserTypeV1 userType) {
		this.userType = userType;
	}

	public Boolean isAreConcurrentSessionsAllowed() {
		return areConcurrentSessionsAllowed;
	}

	public void setAreConcurrentSessionsAllowed(Boolean areConcurrentSessionsAllowed) {
		this.areConcurrentSessionsAllowed = areConcurrentSessionsAllowed;
	}

	public String getUserHash() {
		return userHash;
	}

	public void setUserHash(String userHash) {
		this.userHash = userHash;
	}

	public Boolean isApiUser() {
		return apiUser;
	}

	public void setApiUser(Boolean apiUser) {
		this.apiUser = apiUser;
	}

}
