package net.colt.novitas.workitems.constants;

public enum PortalUserStatusV1 {
	ACTIVE,
    ARCHIVED;

}
