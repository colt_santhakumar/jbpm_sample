package net.colt.novitas.workitems.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class NovitasServiceResponse implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    private String serviceId;
    
    private Integer customerId;
    
    private String status;
    
    @JsonProperty("discount_tier_name")
    private String discountTierName;
    
    @JsonProperty("rental_unit")
    private String rentalUnit;
    
    @JsonProperty("pricing_model")
    private String pricingModel;
    
    @JsonProperty("ux")
    private String ux;
    
    public NovitasServiceResponse() {
        super();
    }
    
    public String getServiceId() {
        return serviceId;
    }
    
    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }
    
    public Integer getCustomerId() {
        return customerId;
    }
    
    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }
    
    public String getStatus() {
        return status;
    }
    
    public void setStatus(String status) {
        this.status = status;
    }
    
    public String getDiscountTierName() {
        return discountTierName;
    }
    
    public void setDiscountTierName(String discountTierName) {
        this.discountTierName = discountTierName;
    }
    
    public String getRentalUnit() {
        return rentalUnit;
    }
    
    public String getPricingModel() {
        return pricingModel;
    }
    
    public void setPricingModel(String pricingModel) {
        this.pricingModel = pricingModel;
    }
    
    public void setRentalUnit(String rentalUnit) {
        this.rentalUnit = rentalUnit;
    }
    
    public String getUX() {
        return ux;
    }
    
    public void setUX(String ux) {
        this.ux = ux;
    }
    
}
