package net.colt.novitas.workitems.constants;

public enum ConnectionStatus {
	PENDING, ACTIVE, MODIFYING , DECOMMISSIONING, DECOMMISSIONED;

}
