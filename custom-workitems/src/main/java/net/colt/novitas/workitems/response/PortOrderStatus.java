package net.colt.novitas.workitems.response;

import com.fasterxml.jackson.annotation.JsonCreator;

public enum PortOrderStatus {

	INPROGRESS,
	COMPLETED,
	CANCELLED,
	FAILED;
	

	@JsonCreator
	public static PortOrderStatus fromValue(String text) {
	    for (PortOrderStatus b : PortOrderStatus.values()) {
	    	if (b.toString().equals(text)) {
	    		return b;
	      }
	    }
	    return null;
	  }
	  
	public static PortOrderStatus convertPortOrderStatus(String state){
		
		switch (state){
			case "acknowledge":
			case "pending":
			case "partial":
			case "inProgress":
				return INPROGRESS;
			case "completed":
				return COMPLETED;
			case "cancelled":
				return CANCELLED;
			case "rejected":
			case "held":
			case "failed":
				return FAILED;
				
		}
		return FAILED;
	}
	
}
