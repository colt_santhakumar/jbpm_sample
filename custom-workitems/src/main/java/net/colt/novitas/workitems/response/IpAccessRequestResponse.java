package net.colt.novitas.workitems.response;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import net.colt.novitas.workitem.request.RequestVLANIdRange;

@JsonIgnoreProperties(ignoreUnknown = true)
public class IpAccessRequestResponse implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty("action")
	private String action;

	@JsonProperty("port_vlan_mapping")
	private String portVlanMapping;

	@JsonProperty("port_vlan_type")
	private String portVlanType;

	@JsonProperty("port_vlan_ids")
	private List<RequestVLANIdRange> portVlanIds;

	@JsonProperty("bandwidth")
	private Integer bandwidth;

	@JsonProperty("bcn")
	private String bcn;

	@JsonProperty("commitment_period")
	private Integer commitmentPeriod;

	@JsonProperty("connection_id")
	private String connectionId;

	@JsonProperty("connection_name")
	private String connectionName;

	@JsonProperty("coterminus_option")
	private Boolean coterminusOption;

	@JsonProperty("currency")
	private String currency;

	@JsonProperty("customer_name")
	private String customerName;

	@JsonProperty("decommissioning_charge")
	private Float decommissioningCharge;

	@JsonProperty("description")
	private String description;

	@JsonProperty("from_port_id")
	private String fromPortId;

	@JsonProperty("novitas_service_id")
	private String novitasServiceId;

	@JsonProperty("id")
	private Integer requestId;

	@JsonProperty("installation_charge")
	private Float installationCharge;

	@JsonProperty("last_updated")
	private Date lastUpdated;

	@JsonProperty("modification_charge")
	private Float modificationCharge;

	@JsonProperty("ocn")
	private String ocn;

	@JsonProperty("old_bandwidth")
	private Integer oldBandwidth;

	@JsonProperty("old_rental_charge")
	private Float oldRentalCharge;

	@JsonProperty("old_rental_unit")
	private String oldRentalUnit;

	@JsonProperty("penalty_charge")
	private Float penaltyCharge;

	@JsonProperty("portal_user_id")
	private Integer portalUserId;

	@JsonProperty("price_id")
	private Integer priceId;

	@JsonProperty("rental_charge")
	private Float rentalCharge;

	@JsonProperty("rental_unit")
	private String rentalUnit;

	@JsonProperty("requested_at")
	private String requestedAt;

	@JsonProperty("requested_by")
	private String requestedBy;

	@JsonProperty("requester_email")
	private String requesterEmail;

	@JsonProperty("service_id")
	private String serviceId;

	@JsonProperty("status")
	private String status;

	@JsonProperty("status_code")
	private Integer statusCode;

	@JsonProperty("customer_local_name")
	private String customerLocalName;

	@JsonProperty("colt_region")
	private String coltRegion;

	public IpAccessRequestResponse() {
		super();
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public Integer getBandwidth() {
		return bandwidth;
	}

	public void setBandwidth(Integer bandwidth) {
		this.bandwidth = bandwidth;
	}

	public String getBcn() {
		return bcn;
	}

	public void setBcn(String bcn) {
		this.bcn = bcn;
	}

	public Integer getCommitmentPeriod() {
		return commitmentPeriod;
	}

	public void setCommitmentPeriod(Integer commitmentPeriod) {
		this.commitmentPeriod = commitmentPeriod;
	}

	public String getConnectionId() {
		return connectionId;
	}

	public void setConnectionId(String connectionId) {
		this.connectionId = connectionId;
	}

	public String getConnectionName() {
		return connectionName;
	}

	public void setConnectionName(String connectionName) {
		this.connectionName = connectionName;
	}

	public Boolean getCoterminusOption() {
		return coterminusOption;
	}

	public void setCoterminusOption(Boolean coterminusOption) {
		this.coterminusOption = coterminusOption;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public Float getDecommissioningCharge() {
		return decommissioningCharge;
	}

	public void setDecommissioningCharge(Float decommissioningCharge) {
		this.decommissioningCharge = decommissioningCharge;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getFromPortId() {
		return fromPortId;
	}

	public void setFromPortId(String fromPortId) {
		this.fromPortId = fromPortId;
	}

	public String getNovitasServiceId() {
		return novitasServiceId;
	}

	public void setNovitasServiceId(String novitasServiceId) {
		this.novitasServiceId = novitasServiceId;
	}

	public Integer getRequestId() {
		return requestId;
	}

	public void setRequestId(Integer requestId) {
		this.requestId = requestId;
	}

	public Float getInstallationCharge() {
		return installationCharge;
	}

	public void setInstallationCharge(Float installationCharge) {
		this.installationCharge = installationCharge;
	}

	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public Float getModificationCharge() {
		return modificationCharge;
	}

	public void setModificationCharge(Float modificationCharge) {
		this.modificationCharge = modificationCharge;
	}

	public String getOcn() {
		return ocn;
	}

	public void setOcn(String ocn) {
		this.ocn = ocn;
	}

	public Integer getOldBandwidth() {
		return oldBandwidth;
	}

	public void setOldBandwidth(Integer oldBandwidth) {
		this.oldBandwidth = oldBandwidth;
	}

	public Float getOldRentalCharge() {
		return oldRentalCharge;
	}

	public void setOldRentalCharge(Float oldRentalCharge) {
		this.oldRentalCharge = oldRentalCharge;
	}

	public String getOldRentalUnit() {
		return oldRentalUnit;
	}

	public void setOldRentalUnit(String oldRentalUnit) {
		this.oldRentalUnit = oldRentalUnit;
	}

	public Float getPenaltyCharge() {
		return penaltyCharge;
	}

	public void setPenaltyCharge(Float penaltyCharge) {
		this.penaltyCharge = penaltyCharge;
	}

	public Integer getPortalUserId() {
		return portalUserId;
	}

	public void setPortalUserId(Integer portalUserId) {
		this.portalUserId = portalUserId;
	}

	public Integer getPriceId() {
		return priceId;
	}

	public void setPriceId(Integer priceId) {
		this.priceId = priceId;
	}

	public Float getRentalCharge() {
		return rentalCharge;
	}

	public void setRentalCharge(Float rentalCharge) {
		this.rentalCharge = rentalCharge;
	}

	public String getRentalUnit() {
		return rentalUnit;
	}

	public void setRentalUnit(String rentalUnit) {
		this.rentalUnit = rentalUnit;
	}

	public String getRequestedAt() {
		return requestedAt;
	}

	public void setRequestedAt(String requestedAt) {
		this.requestedAt = requestedAt;
	}

	public String getRequestedBy() {
		return requestedBy;
	}

	public void setRequestedBy(String requestedBy) {
		this.requestedBy = requestedBy;
	}

	public String getRequesterEmail() {
		return requesterEmail;
	}

	public void setRequesterEmail(String requesterEmail) {
		this.requesterEmail = requesterEmail;
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}

	public String getCustomerLocalName() {
		return customerLocalName;
	}

	public void setCustomerLocalName(String customerLocalName) {
		this.customerLocalName = customerLocalName;
	}

	public String getColtRegion() {
		return coltRegion;
	}

	public void setColtRegion(String coltRegion) {
		this.coltRegion = coltRegion;
	}

	public String getPortVlanMapping() {
		return portVlanMapping;
	}

	public void setPortVlanMapping(String portVlanMapping) {
		this.portVlanMapping = portVlanMapping;
	}

	public String getPortVlanType() {
		return portVlanType;
	}

	public void setPortVlanType(String portVlanType) {
		this.portVlanType = portVlanType;
	}

	public List<RequestVLANIdRange> getPortVlanIds() {
		return portVlanIds;
	}

	public void setPortVlanIds(List<RequestVLANIdRange> portVlanIds) {
		this.portVlanIds = portVlanIds;
	}

	@Override
	public String toString() {
		return "IpAccessRequestResponse [action=" + action + ", portVlanMapping=" + portVlanMapping + ", portVlanType="
				+ portVlanType + ", portVlanIds=" + portVlanIds + ", bandwidth=" + bandwidth + ", bcn=" + bcn
				+ ", commitmentPeriod=" + commitmentPeriod + ", connectionId=" + connectionId + ", connectionName="
				+ connectionName + ", coterminusOption=" + coterminusOption + ", currency=" + currency
				+ ", customerName=" + customerName + ", decommissioningCharge=" + decommissioningCharge
				+ ", description=" + description + ", fromPortId=" + fromPortId + ", novitasServiceId="
				+ novitasServiceId + ", requestId=" + requestId + ", installationCharge=" + installationCharge
				+ ", lastUpdated=" + lastUpdated + ", modificationCharge=" + modificationCharge + ", ocn=" + ocn
				+ ", oldBandwidth=" + oldBandwidth + ", oldRentalCharge=" + oldRentalCharge + ", oldRentalUnit="
				+ oldRentalUnit + ", penaltyCharge=" + penaltyCharge + ", portalUserId=" + portalUserId + ", priceId="
				+ priceId + ", rentalCharge=" + rentalCharge + ", rentalUnit=" + rentalUnit + ", requestedAt="
				+ requestedAt + ", requestedBy=" + requestedBy + ", requesterEmail=" + requesterEmail + ", serviceId="
				+ serviceId + ", status=" + status + ", statusCode=" + statusCode + ", customerLocalName="
				+ customerLocalName + ", coltRegion=" + coltRegion + "]";
	}

}