package net.colt.novitas.workitems.response.equinix;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@Getter
@Setter
@ToString
public class EquinixInterconnectResponse implements Serializable {

	private static final long serialVersionUID = 2536518881940008325L;

	private String name;

	private String cspReferenceId;

	private String state;

	private Integer vlan;

	private Integer bandwidth;

	private String ncTechServiceId;
	
}
