package net.colt.novitas.workitems.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import net.colt.novitas.workitems.BaseWorkItemHandler;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomerCrossConnectResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4006762844256648535L;

	@JsonProperty("cross_connect_id")
	private String crossConnectId;

	@JsonProperty("associated_port_id")
	private String associatedPortId;

	@JsonProperty("ocn")
	private String ocn;

	@JsonProperty("commitment_period")
	private Integer commitmentPeriod;

	@JsonProperty("commitment_expiry_date")
	private String commitmentExpiryDate;

	@JsonProperty("created_date")
	private String createdDate;

	@JsonProperty("customer_name")
	private String customerName;

	@JsonProperty("customer_pp_port")
	private String customerPpPort;

	@JsonProperty("customer_pp_floor")
	private String customerPpFloor;

	@JsonProperty("customer_pp_room")
	private String customerPpRoom;

	@JsonProperty("customer_pp_cabinet")
	private String customerPpCabinet;

	@JsonProperty("connector")
	private String connector;

	@JsonProperty("customer_pp_device")
	private String customerPpDevice;

	@JsonProperty("decomissoned_on")
	private String decommisonedOn;

	@JsonProperty("penalty_charge")
	private Float penaltyCharge;

	@JsonProperty("penalty_currency")
	private String penaltyCurrency;

	@JsonProperty("rental_charge")
	private Float rentalCharge;

	@JsonProperty("rental_currency")
	private String rentalCurrency;

	@JsonProperty("rental_unit")
	private String rentalUnit;

	@JsonProperty("service_id")
	private String serviceId;

	@JsonProperty("status")
	private String status;

	public String getCrossConnectId() {
		return crossConnectId;
	}

	public void setCrossConnectId(String crossConnectId) {
		this.crossConnectId = crossConnectId;
	}

	public String getAssociatedPortId() {
		return associatedPortId;
	}

	public void setAssociatedPortId(String associatedPortId) {
		this.associatedPortId = associatedPortId;
	}

	public String getOcn() {
		return ocn;
	}

	public void setOcn(String ocn) {
		this.ocn = ocn;
	}

	public Integer getCommitmentPeriod() {
		return commitmentPeriod;
	}

	public void setCommitmentPeriod(Integer commitmentPeriod) {
		this.commitmentPeriod = commitmentPeriod;
	}

	public String getCommitmentExpiryDate() {
		return commitmentExpiryDate;
	}

	public void setCommitmentExpiryDate(String commitmentExpiryDate) {
		this.commitmentExpiryDate = commitmentExpiryDate;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerPpPort() {
		return customerPpPort;
	}

	public void setCustomerPpPort(String customerPpPort) {
		this.customerPpPort = customerPpPort;
	}

	public String getCustomerPpFloor() {
		return customerPpFloor;
	}

	public void setCustomerPpFloor(String customerPpFloor) {
		this.customerPpFloor = customerPpFloor;
	}

	public String getCustomerPpRoom() {
		return customerPpRoom;
	}

	public void setCustomerPpRoom(String customerPpRoom) {
		this.customerPpRoom = customerPpRoom;
	}

	public String getCustomerPpCabinet() {
		return customerPpCabinet;
	}

	public void setCustomerPpCabinet(String customerPpCabinet) {
		this.customerPpCabinet = customerPpCabinet;
	}

	public String getCustomerPpDevice() {
		return customerPpDevice;
	}

	public void setCustomerPpDevice(String customerPpDevice) {
		this.customerPpDevice = customerPpDevice;
	}

	public String getDecommisonedOn() {
		return decommisonedOn;
	}

	public void setDecommisonedOn(String decommisonedOn) {
		this.decommisonedOn = decommisonedOn;
	}

	public Float getPenaltyCharge() {
		return penaltyCharge;
	}

	public void setPenaltyCharge(Float penaltyCharge) {
		this.penaltyCharge = penaltyCharge;
	}

	public String getPenaltyCurrency() {
		return penaltyCurrency;
	}

	public void setPenaltyCurrency(String penaltyCurrency) {
		this.penaltyCurrency = penaltyCurrency;
	}

	public Float getRentalCharge() {
		return rentalCharge;
	}

	public void setRentalCharge(Float rentalCharge) {
		this.rentalCharge = rentalCharge;
	}

	public String getRentalCurrency() {
		return rentalCurrency;
	}

	public void setRentalCurrency(String rentalCurrency) {
		this.rentalCurrency = rentalCurrency;
	}

	public String getRentalUnit() {
		return rentalUnit;
	}

	public void setRentalUnit(String rentalUnit) {
		this.rentalUnit = rentalUnit;
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getConnector() {
		return connector;
	}

	public void setConnector(String connector) {
		this.connector = connector;
	}

	@Override
	public String toString() {
		return BaseWorkItemHandler.toString(this);
	}

}
