/**
 * 
 */
package net.colt.novitas.workitems.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import net.colt.novitas.workitems.BaseWorkItemHandler;

/**
 * @author ODawelbeit
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class EventResponse implements Serializable {


	private static final long serialVersionUID = 1L;

	@JsonProperty("event_id")
	private Integer eventId;
	
	@JsonProperty("customer_id")
	private String customerId;
	
	@JsonProperty("connection_id")
	private String connectionId;
	
	@JsonProperty("new_bandwidth")
	private Integer newBandwidth;
	
	@JsonProperty("event_time")
	private String eventTime;
	
	@JsonProperty("event_day_of_week")
	private Integer eventDayOfWeek;
	
	@JsonProperty("event_date")
	private String eventDate;
	
	@JsonProperty("event_frequency")
	private String frequency;
	
	@JsonProperty("created_date")
	private String createdDate;
	
	@JsonProperty("cancelled_or_completed_date")
	private String cancelledOrCompletedDate;
	
	@JsonProperty("last_success_run")
	private String lastSuccessRunDate;
	
	@JsonProperty("last_run_date")
	private String lastRunDate;
	
	@JsonProperty("last_run_status")
	private String lastRunStatus;
	
	@JsonProperty("next_run_date")
	private String nextRunDate;
	
	@JsonProperty("status")
	private String status;
	
	@JsonProperty("new_rental_price")
	private Float newRentalPrice;
	
	@JsonProperty("rental_currency")
	private String rentalCurrency;
	
	@JsonProperty("novitas_service_id")
	private String novitasServiceId;
	
	@JsonProperty("ocn")
	private String ocn;
	
	@JsonProperty("bcn")
	private String bcn;

	public Integer getEventId() {
		return eventId;
	}

	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getConnectionId() {
		return connectionId;
	}

	public void setConnectionId(String connectionId) {
		this.connectionId = connectionId;
	}

	public Integer getNewBandwidth() {
		return newBandwidth;
	}

	public void setNewBandwidth(Integer newBandwidth) {
		this.newBandwidth = newBandwidth;
	}

	public String getEventTime() {
		return eventTime;
	}

	public void setEventTime(String eventTime) {
		this.eventTime = eventTime;
	}

	public Integer getEventDayOfWeek() {
		return eventDayOfWeek;
	}

	public void setEventDayOfWeek(Integer eventDayOfWeek) {
		this.eventDayOfWeek = eventDayOfWeek;
	}

	public String getEventDate() {
		return eventDate;
	}

	public void setEventDate(String eventDate) {
		this.eventDate = eventDate;
	}

	public String getFrequency() {
		return frequency;
	}

	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getCancelledOrCompletedDate() {
		return cancelledOrCompletedDate;
	}

	public void setCancelledOrCompletedDate(String cancelledOrCompletedDate) {
		this.cancelledOrCompletedDate = cancelledOrCompletedDate;
	}

	public String getLastSuccessRunDate() {
		return lastSuccessRunDate;
	}

	public void setLastSuccessRunDate(String lastSuccessRunDate) {
		this.lastSuccessRunDate = lastSuccessRunDate;
	}

	public String getLastRunDate() {
		return lastRunDate;
	}

	public void setLastRunDate(String lastRunDate) {
		this.lastRunDate = lastRunDate;
	}

	public String getLastRunStatus() {
		return lastRunStatus;
	}

	public void setLastRunStatus(String lastRunStatus) {
		this.lastRunStatus = lastRunStatus;
	}

	public String getNextRunDate() {
		return nextRunDate;
	}

	public void setNextRunDate(String nextRunDate) {
		this.nextRunDate = nextRunDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Float getNewRentalPrice() {
		return newRentalPrice;
	}

	public void setNewRentalPrice(Float newRentalPrice) {
		this.newRentalPrice = newRentalPrice;
	}

	public String getRentalCurrency() {
		return rentalCurrency;
	}

	public void setRentalCurrency(String rentalCurrency) {
		this.rentalCurrency = rentalCurrency;
	}

	public String getNovitasServiceId() {
		return novitasServiceId;
	}

	public void setNovitasServiceId(String novitasServiceId) {
		this.novitasServiceId = novitasServiceId;
	}

	public String getOcn() {
		return ocn;
	}

	public void setOcn(String ocn) {
		this.ocn = ocn;
	}

	public String getBcn() {
		return bcn;
	}

	public void setBcn(String bcn) {
		this.bcn = bcn;
	}

	@Override
	public String toString() {
		return BaseWorkItemHandler.toString(this);
	}
	
	
}
