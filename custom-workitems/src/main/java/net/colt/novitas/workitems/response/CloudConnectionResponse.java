package net.colt.novitas.workitems.response;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import net.colt.novitas.workitem.request.ConnectionPortPair;
import net.colt.novitas.workitems.BaseWorkItemHandler;
import net.colt.novitas.workitems.constants.ConnectionStatus;
import net.colt.novitas.workitems.constants.RentalUnit;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CloudConnectionResponse implements Serializable {

	private static final long serialVersionUID = -1705595914057459510L;

	@JsonProperty("connection_id")
	private String connectionId;

	@JsonProperty("name")
	private String name;

	@JsonProperty("bandwidth")
	private Integer bandwidth;

	@JsonProperty("created_date")
	private String createdDate;

	@JsonProperty("last_updated")
	private String lastUpdated;

	@JsonProperty("decommissioned_on")
	private String decommissionedOn;

	@JsonProperty("status")
	private ConnectionStatus status;

	@JsonProperty("rental_charge")
	private Float rentalCharge;

	@JsonProperty("rental_unit")
	private RentalUnit rentalUnit;

	@JsonProperty("rental_currency")
	private String rentalCurrency;

	@JsonProperty("service_id")
	private String serviceId;

	@JsonProperty("resource_id_1")
	private String resourceId1;

	@JsonProperty("resource_id_2")
	private String resourceId2;

	@JsonProperty("customer_name")
	private String customerName;

	@JsonProperty("ocn")
	private String ocn;

	@JsonProperty("sla_id")
	private String slaId;

	@JsonProperty("penalty_charge")
	private Float penaltyCharge;

	@JsonProperty("commitment_period")
	private Integer commitmentPeriod;

	@JsonProperty("commitment_expiry_date")
	private String commitmentExpiryDate;

	@JsonProperty("component_connections")
	private List<ConnectionPortPair> ports;

	@JsonProperty("connection_type")
	private String connectionType;

	@JsonProperty("nms")
	private String nms;

	@JsonProperty("nc_tech_service_id_1")
	private String ncTechServiceId1;

	@JsonProperty("nc_tech_service_id_2")
	private String ncTechServiceId2;

	@JsonProperty("cease_request_id")
	private String ceaseRequestId;

	@JsonProperty("base_bandwidth")
	private Integer baseBandwidth;

	@JsonProperty("base_rental")
	private Float baseRental;

	public String getConnectionId() {
		return connectionId;
	}

	public void setConnectionId(String connectionId) {
		this.connectionId = connectionId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getBandwidth() {
		return bandwidth;
	}

	public void setBandwidth(Integer bandwidth) {
		this.bandwidth = bandwidth;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(String lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public String getDecommissionedOn() {
		return decommissionedOn;
	}

	public void setDecommissionedOn(String decommissionedOn) {
		this.decommissionedOn = decommissionedOn;
	}

	public ConnectionStatus getStatus() {
		return status;
	}

	public void setStatus(ConnectionStatus status) {
		this.status = status;
	}

	public Float getRentalCharge() {
		return rentalCharge;
	}

	public void setRentalCharge(Float rentalCharge) {
		this.rentalCharge = rentalCharge;
	}

	public RentalUnit getRentalUnit() {
		return rentalUnit;
	}

	public void setRentalUnit(RentalUnit rentalUnit) {
		this.rentalUnit = rentalUnit;
	}

	public String getRentalCurrency() {
		return rentalCurrency;
	}

	public void setRentalCurrency(String rentalCurrency) {
		this.rentalCurrency = rentalCurrency;
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public String getResourceId1() {
		return resourceId1;
	}

	public void setResourceId1(String resourceId1) {
		this.resourceId1 = resourceId1;
	}

	public String getResourceId2() {
		return resourceId2;
	}

	public void setResourceId2(String resourceId2) {
		this.resourceId2 = resourceId2;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getOcn() {
		return ocn;
	}

	public void setOcn(String ocn) {
		this.ocn = ocn;
	}

	public String getSlaId() {
		return slaId;
	}

	public void setSlaId(String slaId) {
		this.slaId = slaId;
	}

	public Float getPenaltyCharge() {
		return penaltyCharge;
	}

	public void setPenaltyCharge(Float penaltyCharge) {
		this.penaltyCharge = penaltyCharge;
	}

	public Integer getCommitmentPeriod() {
		return commitmentPeriod;
	}

	public void setCommitmentPeriod(Integer commitmentPeriod) {
		this.commitmentPeriod = commitmentPeriod;
	}

	public String getCommitmentExpiryDate() {
		return commitmentExpiryDate;
	}

	public void setCommitmentExpiryDate(String commitmentExpiryDate) {
		this.commitmentExpiryDate = commitmentExpiryDate;
	}

	public List<ConnectionPortPair> getPorts() {
		return ports;
	}

	public void setPorts(List<ConnectionPortPair> ports) {
		this.ports = ports;
	}

	public String getConnectionType() {
		return connectionType;
	}

	public void setConnectionType(String connectionType) {
		this.connectionType = connectionType;
	}

	public String getNms() {
		return nms;
	}

	public void setNms(String nms) {
		this.nms = nms;
	}

	public String getNcTechServiceId1() {
		return ncTechServiceId1;
	}

	public void setNcTechServiceId1(String ncTechServiceId1) {
		this.ncTechServiceId1 = ncTechServiceId1;
	}

	public String getNcTechServiceId2() {
		return ncTechServiceId2;
	}

	public void setNcTechServiceId2(String ncTechServiceId2) {
		this.ncTechServiceId2 = ncTechServiceId2;
	}

	public String getCeaseRequestId() {
		return ceaseRequestId;
	}

	public void setCeaseRequestId(String ceaseRequestId) {
		this.ceaseRequestId = ceaseRequestId;
	}

	public Integer getBaseBandwidth() {
		return baseBandwidth;
	}

	public void setBaseBandwidth(Integer baseBandwidth) {
		this.baseBandwidth = baseBandwidth;
	}

	public Float getBaseRental() {
		return baseRental;
	}

	public void setBaseRental(Float baseRental) {
		this.baseRental = baseRental;
	}

	@Override
	public String toString() {
		return BaseWorkItemHandler.toString(this);
	}

}
