package net.colt.novitas.workitems.response;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import net.colt.novitas.workitem.request.RequestVLANIdRange;
import net.colt.novitas.workitems.BaseWorkItemHandler;
import net.colt.novitas.workitems.constants.ConnectionStatus;
import net.colt.novitas.workitems.constants.LatLong;
import net.colt.novitas.workitems.constants.RentalUnit;
import net.colt.novitas.workitems.constants.VlanMapping;
import net.colt.novitas.workitems.constants.VlanType;
import net.colt.novitas.workitems.response.bandwidth.boost.BandwidthBoostPricesResponseDto;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ConnectionResponse implements Serializable{

	private static final long serialVersionUID = 1L;

	private String connectionId;
	private String name;
	private String fromPortName;
	private String toPortName;
	private Integer bandwidth;
	private String createdDate;
	private String lastUpdated;
	private String decommissionedOn;
	private ConnectionStatus status;
	private Float rentalCharge;
	private RentalUnit rentalUnit;
	private String rentalCurrency;
	private String fromPortId;
	private String toPortId;
	private String serviceId;
	private String resourceId;
	private LatLong fromLatLong;
	private LatLong toLatLong;
	private VlanMapping fromVlanMapping;
	private VlanMapping toVlanMapping;
	private VlanType fromVlanType;
	private VlanType toVlanType;
	private List<RequestVLANIdRange> fromPortVLANIdRange;
	private List<RequestVLANIdRange> toPortVLANIdRange;
	private String customerName;
	private String ocn;
	private String slaId;
	private Float penaltyCharge;
	private Integer commitmentPeriod;
	private String commitmentExpiryDate;
	private String connectionType;

	@JsonProperty("nms")
	private String nms;

	@JsonProperty("nc_tech_service_id")
	private String ncTechServiceId;

	@JsonProperty("cease_request_id")
	private String ceaseRequestId;

	@JsonProperty("bandwidth_boost_prices")
	private List<BandwidthBoostPricesResponseDto> bandwidthBoostPricesRequestDto;

	@JsonProperty("base_bandwidth_mb")
	private Integer baseBandwidthMb;

	@JsonProperty("base_rental")
	private Float baseRental;

	@JsonProperty("connection_id")
	public String getConnectionId() {
		return connectionId;
	}

	public void setConnectionId(String connectionId) {
		this.connectionId = connectionId;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("from_port_name")
	public String getFromPortName() {
		return fromPortName;
	}

	public void setFromPortName(String fromPortName) {
		this.fromPortName = fromPortName;
	}

	@JsonProperty("to_port_name")
	public String getToPortName() {
		return toPortName;
	}

	public void setToPortName(String toPortName) {
		this.toPortName = toPortName;
	}

	@JsonProperty("bandwidth")
	public Integer getBandwidth() {
		return bandwidth;
	}

	public void setBandwidth(Integer bandwidth) {
		this.bandwidth = bandwidth;
	}

	@JsonProperty("created_date")
	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	@JsonProperty("last_updated")
	public String getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(String lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	@JsonProperty("decommissioned_on")
	public String getDecommissionedOn() {
		return decommissionedOn;
	}

	public void setDecommissionedOn(String decommissionedOn) {
		this.decommissionedOn = decommissionedOn;
	}

	@JsonProperty("status")
	public ConnectionStatus getStatus() {
		return status;
	}

	public void setStatus(ConnectionStatus status) {
		this.status = status;
	}

	@JsonProperty("rental_charge")
	public Float getRentalCharge() {
		return rentalCharge;
	}

	public void setRentalCharge(Float rentalCharge) {
		this.rentalCharge = rentalCharge;
	}

	@JsonProperty("rental_unit")
	public RentalUnit getRentalUnit() {
		return rentalUnit;
	}

	public void setRentalUnit(RentalUnit rentalUnit) {
		this.rentalUnit = rentalUnit;
	}

	@JsonProperty("rental_currency")
	public String getRentalCurrency() {
		return rentalCurrency;
	}

	public void setRentalCurrency(String rentalCurrency) {
		this.rentalCurrency = rentalCurrency;
	}

	@JsonProperty("from_port_id")
	public String getFromPortId() {
		return fromPortId;
	}

	public void setFromPortId(String fromPortId) {
		this.fromPortId = fromPortId;
	}

	@JsonProperty("to_port_id")
	public String getToPortId() {
		return toPortId;
	}

	public void setToPortId(String toPortId) {
		this.toPortId = toPortId;
	}

	@JsonProperty("service_id")
	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	@JsonProperty("resource_id")
	public String getResourceId() {
		return resourceId;
	}

	public void setResourceId(String resourceId) {
		this.resourceId = resourceId;
	}

	@JsonProperty("from_latlong")
	public LatLong getFromLatLong() {
		return fromLatLong;
	}

	public void setFromLatLong(LatLong fromLatLong) {
		this.fromLatLong = fromLatLong;
	}

	@JsonProperty("to_latlong")
	public LatLong getToLatLong() {
		return toLatLong;
	}

	public void setToLatLong(LatLong toLatLong) {
		this.toLatLong = toLatLong;
	}

	@JsonProperty("a_end_vlan_mapping")
	public VlanMapping getFromVlanMapping() {
		return fromVlanMapping;
	}

	public void setFromVlanMapping(VlanMapping fromVlanMapping) {
		this.fromVlanMapping = fromVlanMapping;
	}

	@JsonProperty("b_end_vlan_mapping")
	public VlanMapping getToVlanMapping() {
		return toVlanMapping;
	}

	public void setToVlanMapping(VlanMapping toVlanMapping) {
		this.toVlanMapping = toVlanMapping;
	}

	@JsonProperty("a_end_vlan_type")
	public VlanType getFromVlanType() {
		return fromVlanType;
	}

	public void setFromVlanType(VlanType fromVlanType) {
		this.fromVlanType = fromVlanType;
	}

	@JsonProperty("b_end_vlan_type")
	public VlanType getToVlanType() {
		return toVlanType;
	}

	public void setToVlanType(VlanType toVlanType) {
		this.toVlanType = toVlanType;
	}

	@JsonProperty("a_end_vlan_ids")
	public List<RequestVLANIdRange> getFromPortVLANIdRange() {
		return fromPortVLANIdRange;
	}

	public void setFromPortVLANIdRange(List<RequestVLANIdRange> fromPortVLANIdRange) {
		this.fromPortVLANIdRange = fromPortVLANIdRange;
	}

	@JsonProperty("b_end_vlan_ids")
	public List<RequestVLANIdRange> getToPortVLANIdRange() {
		return toPortVLANIdRange;
	}

	public void setToPortVLANIdRange(List<RequestVLANIdRange> toPortVLANIdRange) {
		this.toPortVLANIdRange = toPortVLANIdRange;
	}

	@JsonProperty("customer_name")
	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	@JsonProperty("ocn")
	public String getOcn() {
		return ocn;
	}

	public void setOcn(String ocn) {
		this.ocn = ocn;
	}

	@JsonProperty("sla_id")
	public String getSlaId() {
		return slaId;
	}

	public void setSlaId(String slaId) {
		this.slaId = slaId;
	}

	@JsonProperty("penalty_charge")
	public Float getPenaltyCharge() {
		return penaltyCharge;
	}

	public void setPenaltyCharge(Float penaltyCharge) {
		this.penaltyCharge = penaltyCharge;
	}

	@JsonProperty("commitment_period")
	public Integer getCommitmentPeriod() {
		return commitmentPeriod;
	}

	public void setCommitmentPeriod(Integer commitmentPeriod) {
		this.commitmentPeriod = commitmentPeriod;
	}

	@JsonProperty("commitment_expiry_date")
	public String getCommitmentExpiryDate() {
		return commitmentExpiryDate;
	}

	public void setCommitmentExpiryDate(String commitmentExpiryDate) {
		this.commitmentExpiryDate = commitmentExpiryDate;
	}

	@JsonProperty("connection_type")
	public String getConnectionType() {
		return connectionType;
	}

	public void setConnectionType(String connectionType) {
		this.connectionType = connectionType;
	}

	public String getNms() {
		return nms;
	}

	public void setNms(String nms) {
		this.nms = nms;
	}

	public String getNcTechServiceId() {
		return ncTechServiceId;
	}

	public void setNcTechServiceId(String ncTechServiceId) {
		this.ncTechServiceId = ncTechServiceId;
	}


	public String getCeaseRequestId() {
		return ceaseRequestId;
	}

	public void setCeaseRequestId(String ceaseRequestId) {
		this.ceaseRequestId = ceaseRequestId;
	}

	public List<BandwidthBoostPricesResponseDto> getBandwidthBoostPricesRequestDto() {
		return bandwidthBoostPricesRequestDto;
	}

	public void setBandwidthBoostPricesRequestDto(List<BandwidthBoostPricesResponseDto> bandwidthBoostPricesRequestDto) {
		this.bandwidthBoostPricesRequestDto = bandwidthBoostPricesRequestDto;
	}

	public Float getBaseRental() {
		return baseRental;
	}

	public void setBaseRental(Float baseRental) {
		this.baseRental = baseRental;
	}

	public Integer getBaseBandwidthMb() {
		return baseBandwidthMb;
	}

	public void setBaseBandwidthMb(Integer baseBandwidthMb) {
		this.baseBandwidthMb = baseBandwidthMb;
	}

	@Override
	public String toString() {
		return BaseWorkItemHandler.toString(this);
	}






}