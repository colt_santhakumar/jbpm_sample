package net.colt.novitas.workitems.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SitePortContact implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonProperty("site")
	private SiteContact siteContact;

	@JsonProperty("technical")
	private TechnicalContact techniContact;

	@JsonProperty("user")
	private SiteContact userContact;

	public SiteContact getSiteContact() {
		return siteContact;
	}

	public void setSiteContact(SiteContact siteContact) {
		this.siteContact = siteContact;
	}

	public TechnicalContact getTechniContact() {
		return techniContact;
	}

	public void setTechniContact(TechnicalContact techniContact) {
		this.techniContact = techniContact;
	}

	public SiteContact getUserContact() {
		return userContact;
	}

	public void setUserContact(SiteContact userContact) {
		this.userContact = userContact;
	}

	@Override
	public String toString() {
		return "SitePortContact [siteContact=" + siteContact + ", techniContact=" + techniContact + ", userContact="
				+ userContact + "]";
	}

}
