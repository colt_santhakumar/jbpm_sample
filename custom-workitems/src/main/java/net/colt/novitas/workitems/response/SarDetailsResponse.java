package net.colt.novitas.workitems.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import net.colt.novitas.workitems.BaseWorkItemHandler;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SarDetailsResponse implements Serializable {

	private static final long serialVersionUID = 1888627819803815001L;

	@JsonProperty("from_port_site_type")
	private String fromPortSiteType;

	@JsonProperty("sar_details")
	private SarDetails sarDetailsType;

	public SarDetailsResponse() {
		super();
	}

	public SarDetailsResponse(String fromPortSiteType, SarDetails sarDetailsType) {
		super();
		this.fromPortSiteType = fromPortSiteType;
		this.sarDetailsType = sarDetailsType;
	}

	public String getFromPortSiteType() {
		return fromPortSiteType;
	}

	public void setFromPortSiteType(String fromPortSiteType) {
		this.fromPortSiteType = fromPortSiteType;
	}

	public SarDetails getSarDetailsType() {
		return sarDetailsType;
	}

	public void setSarDetailsType(SarDetails sarDetailsType) {
		this.sarDetailsType = sarDetailsType;
	}

	@Override
	public String toString() {
		return BaseWorkItemHandler.toString(this);
	}

}