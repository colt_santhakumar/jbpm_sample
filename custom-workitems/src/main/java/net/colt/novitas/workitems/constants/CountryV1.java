package net.colt.novitas.workitems.constants;

public enum CountryV1 {
	AUSTRALIA,
    AUSTRIA,
    BELGIUM,
    BULGARIA,
    CANADA,
    CROATIA,
    CZECH_REPUBLIC,
    DENMARK,
    FINLAND,
    FRANCE,
    GERMANY,
    HONG_KONG,
    HUNGARY,
    IRELAND,
    ITALY,
    JAPAN,
    LUXEMBOURG,
    NETHERLANDS,
    POLAND,
    PORTUGAL,
    ROMANIA,
    SERBIA,
    SINGAPORE,
    SLOVAKIA,
    SPAIN,
    SWEDEN,
    SWITZERLAND,
    UNITED_KINGDOM,
    UNITED_STATES;

}
