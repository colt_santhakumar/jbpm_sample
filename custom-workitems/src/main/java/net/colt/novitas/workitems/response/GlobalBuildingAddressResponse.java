package net.colt.novitas.workitems.response;



import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import net.colt.novitas.workitems.BaseWorkItemHandler;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GlobalBuildingAddressResponse implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty("building_id")
	private String buildingId;

	@JsonProperty("building_name")
	private String buildingName;

	@JsonProperty("address")
	private String address;

	@JsonProperty("city")
	private String city;

	@JsonProperty("country")
	private String country;
	
	@JsonProperty("building_type")
	private String buildingType;
	
	@JsonProperty("local_city")
	private String localCity;
	
	@JsonProperty("local_street_name")
	private String localStreetName;
	
	@JsonProperty("new_site_allowed")
	private Boolean newSiteAllowed;
	
	@JsonProperty("postcode")
	private String postcode;
	
	@JsonProperty("streetType")
	private String streetType;
	
	@JsonProperty("streetNr")
	private String streetNr;
	

	public String getBuildingId() {
		return buildingId;
	}

	public void setBuildingId(String buildingId) {
		this.buildingId = buildingId;
	}

	public String getBuildingName() {
		return buildingName;
	}

	public void setBuildingName(String buildingName) {
		this.buildingName = buildingName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getBuildingType() {
		return buildingType;
	}

	public void setBuildingType(String buildingType) {
		this.buildingType = buildingType;
	}
	
	public String getLocalCity() {
		return localCity;
	}

	public void setLocalCity(String localCity) {
		this.localCity = localCity;
	}

	public String getLocalStreetName() {
		return localStreetName;
	}

	public void setLocalStreetName(String localStreetName) {
		this.localStreetName = localStreetName;
	}
	
	public Boolean getNewSiteAllowed() {
		return newSiteAllowed;
	}

	public void setNewSiteAllowed(Boolean newSiteAllowed) {
		this.newSiteAllowed = newSiteAllowed;
	}

	public String getPostcode() {
		return postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public String getStreetType() {
		return streetType;
	}

	public void setStreetType(String streetType) {
		this.streetType = streetType;
	}

	public String getStreetNr() {
		return streetNr;
	}

	public void setStreetNr(String streetNr) {
		this.streetNr = streetNr;
	}

	@Override
	public String toString() {
		return BaseWorkItemHandler.toString(this);
	}

	

}
