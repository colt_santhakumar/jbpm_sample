package net.colt.novitas.workitems;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Pattern;

import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.services.directconnect.model.AllocateHostedConnectionRequest;
import com.amazonaws.services.directconnect.model.AllocateHostedConnectionResult;
import com.amazonaws.services.directconnect.model.ConnectionState;
import com.colt.novitas.EnvironmentProperties;
import com.colt.novitas.HandlerUtils;
import com.colt.novitas.exception.HandlerErrorCode;
import com.colt.novitas.exception.NovitasHandlerException;

public class AWSHCreateConnection extends BaseWorkItemHandler{

	private static Logger logger = LoggerFactory.getLogger(AWSHCreateConnection.class);
	private static final String regex = "[A-Za-z0-9- _,/]*";
	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		manager.abortWorkItem(workItem.getId());
	}

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
		EnvironmentProperties prop = HandlerUtils.getEnvironmentProperties(workItem);
		logger = getLogger(workItem, prop.getLogUniqueId());
		try {
			String name = replaceWithDash(String.valueOf(workItem.getParameter("port_name")));
			String bandwidth = String.valueOf(workItem.getParameter("bandwidth"))+"Mbps";
			String awsInterConnectionId = getAwsInterConnectionId(workItem);
			String strVlan = String.valueOf(workItem.getParameter("vlan"));
			Integer vlan = null;
			if(null != strVlan) {
				vlan = Integer.parseInt(strVlan);
			} else {
				logger.error("VLAN is empty, cannot create AWS Connection without VLAN");
			}
			String awsAccountNo = getAwsAccountNo(workItem);
			String region = getAWSRegion(workItem); 
			AllocateHostedConnectionRequest req = new AllocateHostedConnectionRequest();
			req.setBandwidth(bandwidth);
			req.setConnectionName(name);
			req.setOwnerAccount(awsAccountNo);
			req.setConnectionId(awsInterConnectionId);
			req.setVlan(vlan);
			AllocateHostedConnectionResult res =null;
			if(!prop.isAWSMocked()){
				logger.info("Creating AWS Hosted Connection|"+req);
				 res = getAmazonDirectConnectClient(region, prop,getColtRegion(workItem)).allocateHostedConnection(req);
			}else{
				logger.info("Mock AWS Hosted Connection ");
				res = new AllocateHostedConnectionResult();
				res.setBandwidth(bandwidth);
				res.setConnectionId(UUID.randomUUID().toString());
				res.setConnectionName(name);
				res.setConnectionState(ConnectionState.Ordering);
				res.setLocation("");
				res.setOwnerAccount(awsAccountNo);
				res.setVlan(vlan);
			}
			logger.info("AWS Hosted create connection response"+res);
			Map<String, Object> retval = new HashMap<String, Object>();
					
			retval.put("Result", res);
			retval.put("status",res.getConnectionState());
			retval.put("awsh_connection_id", res.getConnectionId());
			manager.completeWorkItem(workItem.getId(), retval);

		} catch (Exception e) {
			logger.error("AWSHCreateConnection -> Failed", e);
				HandlerUtils.throwHandlerExcetpion(new NovitasHandlerException(
						HandlerErrorCode.CP_AWSH_CREATED_PORT));
			
		}
	}
    
	private String  replaceWithDash(String connectionName) {
		StringBuilder sb = new StringBuilder();
		 char phraseArray[] = connectionName.toCharArray(); 
		 for (char c : phraseArray) {
			  if(!Pattern.matches(regex, String.valueOf(c))){
				  c = '-';
			  }
			  sb.append(c);
		  }
		return sb.toString();
	}
	
	
}
