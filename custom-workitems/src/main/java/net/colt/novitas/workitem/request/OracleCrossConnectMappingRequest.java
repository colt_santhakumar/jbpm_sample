package net.colt.novitas.workitem.request;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
@ToString
public class OracleCrossConnectMappingRequest implements Serializable {

	private static final long serialVersionUID = 2636145997584291152L;

	private String crossConnectOrCrossConnectGroupId;

	private Integer vlanId;

	public String getCrossConnectOrCrossConnectGroupId() {
		return crossConnectOrCrossConnectGroupId;
	}

	public void setCrossConnectOrCrossConnectGroupId(String crossConnectOrCrossConnectGroupId) {
		this.crossConnectOrCrossConnectGroupId = crossConnectOrCrossConnectGroupId;
	}

	public Integer getVlanId() {
		return vlanId;
	}

	public void setVlanId(Integer vlanId) {
		this.vlanId = vlanId;
	}

}
