package net.colt.novitas.workitem.request;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import net.colt.novitas.workitems.BaseWorkItemHandler;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CreateCloudConnRequest implements Serializable {

    private static final long serialVersionUID = 2766815226223017067L;
    
    @JsonProperty("name")
    private String name;
    
    @JsonProperty("resource_id_1")
    private String resourceId1;
    
    @JsonProperty("resource_id_2")
    private String resourceId2;
   
    @JsonProperty("bandwidth")
    private Integer bandwidth;
    
    @JsonProperty("rental_charge")
    private Float rentalCharge;
    
    @JsonProperty("rental_unit")
    private String rentalUnit;
    
    @JsonProperty("rental_currency")
    private String rentalCurrency;
    
    @JsonProperty("connection_type")
    private String connectionType;
    
    @JsonProperty("service_id")
    private String serviceId;

    @JsonProperty("customer_name")
    private String customerName;
    
    @JsonProperty("ocn")
    private String ocn;
    
    @JsonProperty("commitment_period")
    private Integer commitmentPeriod;
    
    @JsonProperty("commitment_expiry_date")
    private String commitmentExpiryDate;
    
    @JsonProperty("nms")
    private String nms;

    @JsonProperty("component_connections")
    private List<ConnectionPortPair> ports;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getBandwidth() {
        return bandwidth;
    }

    public void setBandwidth(Integer bandwidth) {
        this.bandwidth = bandwidth;
    }

    public Float getRentalCharge() {
        return rentalCharge;
    }

    public void setRentalCharge(Float rentalCharge) {
        this.rentalCharge = rentalCharge;
    }

    public String getRentalUnit() {
        return rentalUnit;
    }

    public void setRentalUnit(String rentalUnit) {
        this.rentalUnit = rentalUnit;
    }

    public String getRentalCurrency() {
        return rentalCurrency;
    }

    public void setRentalCurrency(String rentalCurrency) {
        this.rentalCurrency = rentalCurrency;
    }

    public String getConnectionType() {
        return connectionType;
    }

    public void setConnectionType(String connectionType) {
        this.connectionType = connectionType;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getOcn() {
        return ocn;
    }

    public void setOcn(String ocn) {
        this.ocn = ocn;
    }

    public Integer getCommitmentPeriod() {
        return commitmentPeriod;
    }

    public void setCommitmentPeriod(Integer commitmentPeriod) {
        this.commitmentPeriod = commitmentPeriod;
    }

    public String getCommitmentExpiryDate() {
        return commitmentExpiryDate;
    }

    public void setCommitmentExpiryDate(String commitmentExpiryDate) {
        this.commitmentExpiryDate = commitmentExpiryDate;
    }

    /**
     * @return the resourceId1
     */
    public String getResourceId1() {
        return resourceId1;
    }

    /**
     * @param resourceId1 the resourceId1 to set
     */
    public void setResourceId1(String resourceId1) {
        this.resourceId1 = resourceId1;
    }

    /**
     * @return the resourceId2
     */
    public String getResourceId2() {
        return resourceId2;
    }

    /**
     * @param resourceId2 the resourceId2 to set
     */
    public void setResourceId2(String resourceId2) {
        this.resourceId2 = resourceId2;
    }

    /**
     * @return the ports
     */
    public List<ConnectionPortPair> getPorts() {
        return ports;
    }

    /**
     * @param ports the ports to set
     */
    public void setPorts(List<ConnectionPortPair> ports) {
        this.ports = ports;
    }

	public String getNms() {
		return nms;
	}

	public void setNms(String nms) {
		this.nms = nms;
	}

	@Override
	public String toString() {
		return BaseWorkItemHandler.toString(this);
	}   
	
    

}
