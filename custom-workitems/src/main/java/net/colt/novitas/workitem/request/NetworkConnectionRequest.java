package net.colt.novitas.workitem.request;

import java.io.Serializable;
import java.net.URI;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import net.colt.novitas.workitems.BaseWorkItemHandler;

@JsonIgnoreProperties(ignoreUnknown = true)
public class NetworkConnectionRequest implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty("callback_url")
	private URI callbackUrl;
	
	@JsonProperty("deployment_id")
	private String deploymentId;
	
	@JsonProperty("signal_name")
	private String signalName;;
	
	@JsonProperty("process_instance_id")
	private Long processInstanceId;
	
	@JsonProperty("evc_id")
	private String evc_id; 		 // the ID of the EVC, populated by Create and Modify calls
	
	@JsonProperty("from_port_name")  
	private String fromPortName;	
	
	@JsonProperty("to_port_name") 
	private String toPortName;
	
	@JsonProperty("connection_name") 
	private String connectionName;
	
	@JsonProperty("if_speed") 
	private Integer if_speed; 			// speed of the physical interface in kbps

	@JsonProperty("cir")
	private Integer cir; 					// speed we want to configure in kbps 
	
	@JsonProperty("cbs")
	private Integer cbs;					// burst size in kbps
	
	@JsonProperty("eir")
	private Integer eir; 					// speed we want to configure in kbps 
	
	@JsonProperty("eir_max")
	private Integer eirMax;
	
	@JsonProperty("from_port_vlans")
	private String fromPortVLANs;	// e.g. "1-35, 43, 2000-2000"
	
	@JsonProperty("to_port_vlans")
	private String toPortVLANs; 	// e.g. "1-35, 43, 2000-2000"
	
	@JsonProperty("from_port_vlan_mapping")
	private String fromPortVLANMapping; // P, F or X
	
	@JsonProperty("to_port_vlan_mapping")
	private String toPortVLANMapping; 	// P, F or X
	
	@JsonProperty("from_port_vlan_type")
	private String fromPortVLANType;	// C or S 
	
	@JsonProperty("to_port_vlan_type")
	private String toPortVLANType;		// C or S
	
	@JsonProperty("from_uni")
	private String fromUNI;				// populated by Create and Modify calls
	
	@JsonProperty("to_uni")
	private String toUNI;				// as above
	
	@JsonProperty("customer")
	private String customer;
	
	@JsonProperty("target")
	private String target;
	
	@JsonProperty("service_profile")
	private String serviceProfile;
	
	

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public URI getCallbackUrl() {
		return callbackUrl;
	}

	public void setCallbackUrl(URI callbackUrl) {
		this.callbackUrl = callbackUrl;
	}

	public String getDeploymentId() {
		return deploymentId;
	}

	public void setDeploymentId(String deploymentId) {
		this.deploymentId = deploymentId;
	}

	public String getSignalName() {
		return signalName;
	}

	public void setSignalName(String signalName) {
		this.signalName = signalName;
	}

	public Long getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(Long processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getEvc_id() {
		return evc_id;
	}

	public void setEvc_id(String evc_id) {
		this.evc_id = evc_id;
	}

	public String getFromPortName() {
		return fromPortName;
	}

	public void setFromPortName(String fromPortName) {
		this.fromPortName = fromPortName;
	}

	public String getToPortName() {
		return toPortName;
	}

	public void setToPortName(String toPortName) {
		this.toPortName = toPortName;
	}

	public String getConnectionName() {
		return connectionName;
	}

	public void setConnectionName(String connectionName) {
		this.connectionName = connectionName;
	}

	public Integer getIf_speed() {
		return if_speed;
	}

	public void setIf_speed(Integer if_speed) {
		this.if_speed = if_speed;
	}

	public Integer getCir() {
		return cir;
	}

	public void setCir(Integer cir) {
		this.cir = cir;
	}

	public Integer getCbs() {
		return cbs;
	}

	public void setCbs(Integer cbs) {
		this.cbs = cbs;
	}

	public String getFromPortVLANs() {
		return fromPortVLANs;
	}

	public void setFromPortVLANs(String fromPortVLANs) {
		this.fromPortVLANs = fromPortVLANs;
	}

	public String getToPortVLANs() {
		return toPortVLANs;
	}

	public void setToPortVLANs(String toPortVLANs) {
		this.toPortVLANs = toPortVLANs;
	}

	public String getFromPortVLANMapping() {
		return fromPortVLANMapping;
	}

	public void setFromPortVLANMapping(String fromPortVLANMapping) {
		this.fromPortVLANMapping = fromPortVLANMapping;
	}

	public String getToPortVLANMapping() {
		return toPortVLANMapping;
	}

	public void setToPortVLANMapping(String toPortVLANMapping) {
		this.toPortVLANMapping = toPortVLANMapping;
	}

	public String getFromPortVLANType() {
		return fromPortVLANType;
	}

	public void setFromPortVLANType(String fromPortVLANType) {
		this.fromPortVLANType = fromPortVLANType;
	}

	public String getToPortVLANType() {
		return toPortVLANType;
	}

	public void setToPortVLANType(String toPortVLANType) {
		this.toPortVLANType = toPortVLANType;
	}

	public String getFromUNI() {
		return fromUNI;
	}

	public void setFromUNI(String fromUNI) {
		this.fromUNI = fromUNI;
	}

	public String getToUNI() {
		return toUNI;
	}

	public void setToUNI(String toUNI) {
		this.toUNI = toUNI;
	}

	public String getCustomer() {
		return customer;
	}

	public void setCustomer(String customer) {
		this.customer = customer;
	}

	public String getServiceProfile() {
		return serviceProfile;
	}

	public void setServiceProfile(String serviceProfile) {
		this.serviceProfile = serviceProfile;
	}

	public Integer getEir() {
		return eir;
	}

	public void setEir(Integer eir) {
		this.eir = eir;
	}

	public Integer getEirMax() {
		return eirMax;
	}

	public void setEirMax(Integer eirMax) {
		this.eirMax = eirMax;
	}

	@Override
	public String toString() {
		return BaseWorkItemHandler.toString(this);
	}

	


}
