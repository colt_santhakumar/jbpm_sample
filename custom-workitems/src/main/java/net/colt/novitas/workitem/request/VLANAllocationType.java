
package net.colt.novitas.workitem.request;

public enum VLANAllocationType {

    VCPE("VCPE"),
    NON_VCPE("NonVCPE"),
    VROUTER("vRouter");

    private final String value;

    VLANAllocationType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static VLANAllocationType fromValue(String v) {
        for (VLANAllocationType c: VLANAllocationType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
