package net.colt.novitas.workitem.request;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CSPSignalDetails implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty("request_id")
	private Integer requestId;

	@JsonProperty("process_instance_id")
	private long processInstanceId;

	@JsonProperty("deployment_id")
	private String deploymentId;

	@JsonProperty("cloud_provider")
	private String cloudProvider;

	@JsonProperty("cloud_region")
	private String cloudRegion;

	@JsonProperty("csp_port_status")
	private String cspPortStatus;

	@JsonProperty("csp_port_ref")
	private String cspPortRef;
	
	@JsonProperty("host_address")
	private String hostAddress;
	
	@JsonProperty("colt_region")
	private String coltRegion;
	
	@JsonProperty("aws_ref")
	private String awsRef;

	public Integer getRequestId() {
		return requestId;
	}

	public void setRequestId(Integer requestId) {
		this.requestId = requestId;
	}

	public long getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(long processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getDeploymentId() {
		return deploymentId;
	}

	public void setDeploymentId(String deploymentId) {
		this.deploymentId = deploymentId;
	}

	public String getCloudProvider() {
		return cloudProvider;
	}

	public void setCloudProvider(String cloudProvider) {
		this.cloudProvider = cloudProvider;
	}

	public String getCloudRegion() {
		return cloudRegion;
	}

	public void setCloudRegion(String cloudRegion) {
		this.cloudRegion = cloudRegion;
	}

	public String getCspPortStatus() {
		return cspPortStatus;
	}

	public void setCspPortStatus(String cspPortStatus) {
		this.cspPortStatus = cspPortStatus;
	}

	public String getCspPortRef() {
		return cspPortRef;
	}

	public void setCspPortRef(String cspPortRef) {
		this.cspPortRef = cspPortRef;
	}
	
	public String getHostAddress() {
		return hostAddress;
	}

	public void setHostAddress(String hostAddress) {
		this.hostAddress = hostAddress;
	}

	public String getColtRegion() {
		return coltRegion;
	}

	public void setColtRegion(String coltRegion) {
		this.coltRegion = coltRegion;
	}

	public String getAwsRef() {
		return awsRef;
	}

	public void setAwsRef(String awsRef) {
		this.awsRef = awsRef;
	}

	@Override
	public String toString() {
		return "CSPSignalDetails [requestId=" + requestId + ", processInstanceId=" + processInstanceId
				+ ", deploymentId=" + deploymentId + ", cloudProvider=" + cloudProvider + ", cloudRegion=" + cloudRegion
				+ ", cspPortStatus=" + cspPortStatus + ", cspPortRef=" + cspPortRef + ", hostAddress=" + hostAddress
				+ ", coltRegion=" + coltRegion + ", awsRef=" + awsRef + "]";
	}


}
