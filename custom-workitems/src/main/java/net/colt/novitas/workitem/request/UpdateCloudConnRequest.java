package net.colt.novitas.workitem.request;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import net.colt.novitas.workitems.BaseWorkItemHandler;

public class UpdateCloudConnRequest implements Serializable {

	private static final long serialVersionUID = -5886828712798157081L;

	@JsonProperty("status")
	private String status;
	
    @JsonProperty("resource_id_1")
    private String resourceId1;
    
    @JsonProperty("resource_id_2")
    private String resourceId2;
    	
    @JsonProperty("bandwidth")
    private Integer bandwidth;
    
    @JsonProperty("rental_charge")
    private Float rentalCharge;
    
    @JsonProperty("rental_unit")
    private String rentalUnit;
    
    @JsonProperty("rental_currency")
    private String rentalCurrency;

    @JsonProperty("commitment_period")
	private Integer commitmentPeriod;
    
    @JsonProperty("commitment_expiry_date")
	private String commitmentExpiryDate;
	
    @JsonProperty("component_connections")
    private List<ConnectionPortPair> ports;
    
    @JsonProperty("olo_service_id")
	private String oloServiceId;
    
    @JsonProperty("nc_tech_service_id_1")
    private String ncTechServiceId1;
	
	@JsonProperty("nc_tech_service_id_2")
    private String ncTechServiceId2;
	
	@JsonProperty("cease_request_id")
    private String ceaseReqId;
	
	@JsonProperty("base_bandwidth")
    private Integer baseBandwidthMb;

	@JsonProperty("base_rental")
    private Float baseRental;

	public UpdateCloudConnRequest() {
		super();
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getBandwidth() {
		return bandwidth;
	}

	public void setBandwidth(Integer bandwidth) {
		this.bandwidth = bandwidth;
	}

	public Float getRentalCharge() {
		return rentalCharge;
	}

	public void setRentalCharge(Float rentalCharge) {
		this.rentalCharge = rentalCharge;
	}

	public String getRentalCurrency() {
		return rentalCurrency;
	}

	public void setRentalCurrency(String rentalCurrency) {
		this.rentalCurrency = rentalCurrency;
	}

	public String getRentalUnit() {
		return rentalUnit;
	}

	public void setRentalUnit(String rentalUnit) {
		this.rentalUnit = rentalUnit;
	}


	public Integer getCommitmentPeriod() {
		return commitmentPeriod;
	}

	public void setCommitmentPeriod(Integer commitmentPeriod) {
		this.commitmentPeriod = commitmentPeriod;
	}

	public String getCommitmentExpiryDate() {
		return commitmentExpiryDate;
	}

	public void setCommitmentExpiryDate(String commitmentExpiryDate) {
		this.commitmentExpiryDate = commitmentExpiryDate;
	}

    /**
     * @return the resourceId1
     */
    public String getResourceId1() {
        return resourceId1;
    }

    /**
     * @param resourceId1 the resourceId1 to set
     */
    public void setResourceId1(String resourceId1) {
        this.resourceId1 = resourceId1;
    }

    /**
     * @return the resourceId2
     */
    public String getResourceId2() {
        return resourceId2;
    }

    /**
     * @param resourceId2 the resourceId2 to set
     */
    public void setResourceId2(String resourceId2) {
        this.resourceId2 = resourceId2;
    }

    /**
     * @return the ports
     */
    public List<ConnectionPortPair> getPorts() {
        return ports;
    }

    /**
     * @param ports the ports to set
     */
    public void setPorts(List<ConnectionPortPair> ports) {
        this.ports = ports;
    }
    

	public String getOloServiceId() {
		return oloServiceId;
	}

	public void setOloServiceId(String oloServiceId) {
		this.oloServiceId = oloServiceId;
	}

	public String getNcTechServiceId1() {
		return ncTechServiceId1;
	}

	public void setNcTechServiceId1(String ncTechServiceId1) {
		this.ncTechServiceId1 = ncTechServiceId1;
	}

	public String getNcTechServiceId2() {
		return ncTechServiceId2;
	}

	public void setNcTechServiceId2(String ncTechServiceId2) {
		this.ncTechServiceId2 = ncTechServiceId2;
	}

	public String getCeaseReqId() {
		return ceaseReqId;
	}

	public void setCeaseReqId(String ceaseReqId) {
		this.ceaseReqId = ceaseReqId;
	}
	
	public Integer getBaseBandwidthMb() {
		return baseBandwidthMb;
	}

	public void setBaseBandwidthMb(Integer baseBandwidthMb) {
		this.baseBandwidthMb = baseBandwidthMb;
	}

	public Float getBaseRental() {
		return baseRental;
	}

	public void setBaseRental(Float baseRental) {
		this.baseRental = baseRental;
	}

	@Override
	public String toString() {
		return BaseWorkItemHandler.toString(this);
	}
	

	
}