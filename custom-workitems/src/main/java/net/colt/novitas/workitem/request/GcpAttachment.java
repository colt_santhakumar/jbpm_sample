package net.colt.novitas.workitem.request;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import net.colt.novitas.workitems.BaseWorkItemHandler;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GcpAttachment implements Serializable {

	/**
	* 
	*/
	private static final long serialVersionUID = 1L;

	@JsonProperty("cloud_region")
	private String region; // Needed for create|get|delete

	@JsonProperty("bandwidth")
	private String bandwidth; // Needed for create|get

	@JsonProperty("description")
	private String description; // Needed for get

	@JsonProperty("inteconnect_attachment_name")
	private String name; // Needed for get|delete

	@JsonProperty("csp_key")
	private String pairingKey; // Needed for create|get

	@JsonProperty("state")
	private String state; // Needed for get

	@JsonProperty("vlan")
	private Integer vlanTag8021q; // Needed for create|get

	@JsonProperty("interconnect_ref")
	private String interConnectRef;

	@JsonProperty("nc_tech_service_id")
	private String ncTechServiceId;

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getBandwidth() {
		return bandwidth;
	}

	public void setBandwidth(String bandwidth) {
		this.bandwidth = bandwidth;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPairingKey() {
		return pairingKey;
	}

	public void setPairingKey(String pairingKey) {
		this.pairingKey = pairingKey;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Integer getVlanTag8021q() {
		return vlanTag8021q;
	}

	public void setVlanTag8021q(Integer vlanTag8021q) {
		this.vlanTag8021q = vlanTag8021q;
	}
	
	public String getInterConnectRef() {
		return interConnectRef;
	}

	public void setInterConnectRef(String interConnectRef) {
		this.interConnectRef = interConnectRef;
	}

	public String getNcTechServiceId() {
		return ncTechServiceId;
	}

	public void setNcTechServiceId(String ncTechServiceId) {
		this.ncTechServiceId = ncTechServiceId;
	}

	@Override
	public String toString() {
		return BaseWorkItemHandler.toString(this);
	}

}
