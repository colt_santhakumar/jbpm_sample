package net.colt.novitas.workitem.request;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import net.colt.novitas.workitems.BaseWorkItemHandler;

public class ModifyRecurringChargeRequest implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@JsonProperty("request_id")
	private Integer requestId;
	@JsonProperty("currency")
	private String currency;
	@JsonProperty("amount")
	private Float amount;
	@JsonProperty("frequency")
	private String frequency;
	@JsonProperty("commitment_type")
	private String commitmentType;
	@JsonProperty("commitment_expiry_date")
	private String commitmentExpiryDate;
	
	public Integer getRequestId() {
		return requestId;
	}
	public void setRequestId(Integer requestId) {
		this.requestId = requestId;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public Float getAmount() {
		return amount;
	}
	public void setAmount(Float amount) {
		this.amount = amount;
	}
	public String getFrequency() {
		return frequency;
	}
	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}
	public String getCommitmentType() {
		return commitmentType;
	}
	public void setCommitmentType(String commitmentType) {
		this.commitmentType = commitmentType;
	}
	public String getCommitmentExpiryDate() {
		return commitmentExpiryDate;
	}
	public void setCommitmentExpiryDate(String commitmentExpiryDate) {
		this.commitmentExpiryDate = commitmentExpiryDate;
	}
	
	@Override
	public String toString() {
		return BaseWorkItemHandler.toString(this);
	}

    
}
