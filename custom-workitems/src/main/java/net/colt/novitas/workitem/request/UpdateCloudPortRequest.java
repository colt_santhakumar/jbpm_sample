package net.colt.novitas.workitem.request;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import net.colt.novitas.workitems.BaseWorkItemHandler;

public class UpdateCloudPortRequest implements Serializable {

	private static final long serialVersionUID = -1772326569297485367L;

	@JsonProperty("location")
	private String location;

	@JsonProperty("bandwidth")
	private Integer bandwidth;

	@JsonProperty("address")
	private String address;

	@JsonProperty("site_type")
	private String siteType;

	@JsonProperty("site_floor")
	private String siteFloor;

	@JsonProperty("site_room_name")
	private String siteRoomName;

	@JsonProperty("location_premises_number")
	private String locationPremisesNumber;

	@JsonProperty("location_building_name")
	private String locationBuildingName;

	@JsonProperty("location_street_name")
	private String locationStreetName;

	@JsonProperty("location_city")
	private String locationCity;

	@JsonProperty("location_state")
	private String locationState;

	@JsonProperty("location_country")
	private String locationCountry;

	@JsonProperty("postal_zip_code")
	private String postalZipCode;

	@JsonProperty("latitude")
	private Float latitude;

	@JsonProperty("longitude")
	private Float longitude;

	@JsonProperty("location_id")
	private String locationId;

	@JsonProperty("port_1")
	private String port1;

	@JsonProperty("port_2")
	private String port2;

	@JsonProperty("vlan")
	private Integer vlan;

	@JsonProperty("status")
	private String status;

	@JsonProperty("resource_id_1")
	private String resourceId1;

	@JsonProperty("resource_id_2")
	private String resourceId2;

	@JsonProperty("aws_connection_id")
	private String awsConnId;

	@JsonProperty("nni_circuit_id")
	private String nniCircuitId;

	@JsonProperty("aws_ref")
	private String awsRef;

	@JsonProperty("connector_type")
	private String connectorType;

	@JsonProperty("technology_type")
	private String technologyType;

	@JsonProperty("presentation_label")
	private String presentationLabel;

	@JsonProperty("nms")
	private String nms;

	@JsonProperty("local_building_name")
	private String localBuildingName;

	@JsonProperty("location_city_code")
	private String locationCityCode;

	@JsonProperty("location_country_code")
	private String locationCountryCode;

	@JsonProperty("resource_port_name_1")
	private String resourcePortName1;

	@JsonProperty("resource_port_name_2")
	private String resourcePortName2;

	@JsonProperty("nc_tech_service_id_1")
	private String ncTechServiceId1;

	@JsonProperty("nc_tech_service_id_2")
	private String ncTechServiceId2;

	@JsonProperty("nc_tech_service_id")
	private String ncTechServiceId;

	@JsonProperty("novitas_site_type")
	private String novitasSiteType;

	@JsonProperty("interconnect_attachment_name")
	private String interconnectAttachmentName;

	@JsonProperty("interconnect_name")
	private String interconnectName;

	@JsonProperty("interconnect")
	private String interconnect;
	

	public String getNcTechServiceId() {
		return ncTechServiceId;
	}

	public void setNcTechServiceId(String ncTechServiceId) {
		this.ncTechServiceId = ncTechServiceId;
	}

	public String getNovitasSiteType() {
		return novitasSiteType;
	}

	public void setNovitasSiteType(String novitasSiteType) {
		this.novitasSiteType = novitasSiteType;
	}

	public String getInterconnectAttachmentName() {
		return interconnectAttachmentName;
	}

	public void setInterconnectAttachmentName(String interconnectAttachmentName) {
		this.interconnectAttachmentName = interconnectAttachmentName;
	}

	public String getInterconnectName() {
		return interconnectName;
	}

	public void setInterconnectName(String interconnectName) {
		this.interconnectName = interconnectName;
	}

	public String getInterconnect() {
		return interconnect;
	}

	public void setInterconnect(String interconnect) {
		this.interconnect = interconnect;
	}

	public UpdateCloudPortRequest() {
		super();
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the resourceId1
	 */
	public String getResourceId1() {
		return resourceId1;
	}

	/**
	 * @param resourceId1
	 *            the resourceId1 to set
	 */
	public void setResourceId1(String resourceId1) {
		this.resourceId1 = resourceId1;
	}

	/**
	 * @return the resourceId2
	 */
	public String getResourceId2() {
		return resourceId2;
	}

	/**
	 * @param resourceId2
	 *            the resourceId2 to set
	 */
	public void setResourceId2(String resourceId2) {
		this.resourceId2 = resourceId2;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public Integer getBandwidth() {
		return bandwidth;
	}

	public void setBandwidth(Integer bandwidth) {
		this.bandwidth = bandwidth;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getSiteType() {
		return siteType;
	}

	public void setSiteType(String siteType) {
		this.siteType = siteType;
	}

	public String getSiteFloor() {
		return siteFloor;
	}

	public void setSiteFloor(String siteFloor) {
		this.siteFloor = siteFloor;
	}

	public String getSiteRoomName() {
		return siteRoomName;
	}

	public void setSiteRoomName(String siteRoomName) {
		this.siteRoomName = siteRoomName;
	}

	public String getLocationPremisesNumber() {
		return locationPremisesNumber;
	}

	public void setLocationPremisesNumber(String locationPremisesNumber) {
		this.locationPremisesNumber = locationPremisesNumber;
	}

	public String getLocationBuildingName() {
		return locationBuildingName;
	}

	public void setLocationBuildingName(String locationBuildingName) {
		this.locationBuildingName = locationBuildingName;
	}

	public String getLocationStreetName() {
		return locationStreetName;
	}

	public void setLocationStreetName(String locationStreetName) {
		this.locationStreetName = locationStreetName;
	}

	public String getLocationCity() {
		return locationCity;
	}

	public void setLocationCity(String locationCity) {
		this.locationCity = locationCity;
	}

	public String getLocationState() {
		return locationState;
	}

	public void setLocationState(String locationState) {
		this.locationState = locationState;
	}

	public String getLocationCountry() {
		return locationCountry;
	}

	public void setLocationCountry(String locationCountry) {
		this.locationCountry = locationCountry;
	}

	public String getPostalZipCode() {
		return postalZipCode;
	}

	public void setPostalZipCode(String postalZipCode) {
		this.postalZipCode = postalZipCode;
	}

	public Float getLatitude() {
		return latitude;
	}

	public void setLatitude(Float latitude) {
		this.latitude = latitude;
	}

	public Float getLongitude() {
		return longitude;
	}

	public void setLongitude(Float longitude) {
		this.longitude = longitude;
	}

	public String getLocationId() {
		return locationId;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	public String getPort1() {
		return port1;
	}

	public void setPort1(String port1) {
		this.port1 = port1;
	}

	public String getPort2() {
		return port2;
	}

	public void setPort2(String port2) {
		this.port2 = port2;
	}

	public Integer getVlan() {
		return vlan;
	}

	public void setVlan(Integer vlan) {
		this.vlan = vlan;
	}

	public String getAwsConnId() {
		return awsConnId;
	}

	public void setAwsConnId(String awsConnId) {
		this.awsConnId = awsConnId;
	}

	public String getNniCircuitId() {
		return nniCircuitId;
	}

	public void setNniCircuitId(String nniCircuitId) {
		this.nniCircuitId = nniCircuitId;
	}

	public String getAwsRef() {
		return awsRef;
	}

	public void setAwsRef(String awsRef) {
		this.awsRef = awsRef;
	}

	public String getConnectorType() {
		return connectorType;
	}

	public void setConnectorType(String connectorType) {
		this.connectorType = connectorType;
	}

	public String getTechnologyType() {
		return technologyType;
	}

	public void setTechnologyType(String technologyType) {
		this.technologyType = technologyType;
	}

	public String getPresentationLabel() {
		return presentationLabel;
	}

	public void setPresentationLabel(String presentationLabel) {
		this.presentationLabel = presentationLabel;
	}

	public String getNms() {
		return nms;
	}

	public void setNms(String nms) {
		this.nms = nms;
	}

	public String getLocalBuildingName() {
		return localBuildingName;
	}

	public void setLocalBuildingName(String localBuildingName) {
		this.localBuildingName = localBuildingName;
	}

	public String getLocationCityCode() {
		return locationCityCode;
	}

	public void setLocationCityCode(String locationCityCode) {
		this.locationCityCode = locationCityCode;
	}

	public String getLocationCountryCode() {
		return locationCountryCode;
	}

	public void setLocationCountryCode(String locationCountryCode) {
		this.locationCountryCode = locationCountryCode;
	}

	public String getResourcePortName1() {
		return resourcePortName1;
	}

	public void setResourcePortName1(String resourcePortName1) {
		this.resourcePortName1 = resourcePortName1;
	}

	public String getResourcePortName2() {
		return resourcePortName2;
	}

	public void setResourcePortName2(String resourcePortName2) {
		this.resourcePortName2 = resourcePortName2;
	}

	public String getNcTechServiceId1() {
		return ncTechServiceId1;
	}

	public void setNcTechServiceId1(String ncTechServiceId1) {
		this.ncTechServiceId1 = ncTechServiceId1;
	}

	public String getNcTechServiceId2() {
		return ncTechServiceId2;
	}

	public void setNcTechServiceId2(String ncTechServiceId2) {
		this.ncTechServiceId2 = ncTechServiceId2;
	}
	
	

	@Override
	public String toString() {
		return BaseWorkItemHandler.toString(this);
	}

}