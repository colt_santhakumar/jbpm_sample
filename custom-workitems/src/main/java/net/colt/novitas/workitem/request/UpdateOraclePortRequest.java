package net.colt.novitas.workitem.request;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@Getter
@Setter
@ToString
public class UpdateOraclePortRequest implements Serializable {

	private static final long serialVersionUID = -2763065351024929462L;
	
	private String portName;
	private String bcn;
	private String ocn;
	private String portId;
	private String siteId;
	private String status;
	private Integer statusCode;
	private String processInstanceId;
	private String hostAddress;
	private Integer customerId;
	private String siteName;
	private String pmSiteType;
	private String siteFloor;
	private String siteRoomName;
	private String address;
	private String serviceId;
	private Integer bandwidth;
    private String buildingId;
    private Float latitude;
    private Float longitude;
    private String postalZipCode;
    private String localBuildingName; //localBuildingName
    private String locationBuildingName; //buildingName
    private String locationStreetName; //streetName
    private String locationCity;  //city
    private String locationState;  //state
    private String locationCountry; //country
    private String locationPremisesNumber; //premisesNumber
}
