package net.colt.novitas.workitem.request;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RequestVLANIdRange implements Comparable<RequestVLANIdRange>, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5611516292958685337L;
	
	@JsonProperty("from_id_range")
	Integer fromIdRange;
	
	@JsonProperty("to_id_range")
	Integer toIdRange;
	
	/*@JsonProperty("cloud")*/
	boolean isCloud;

	public RequestVLANIdRange() {
		super();
	}

	
	public Integer getFromIdRange() {
		return fromIdRange;
	}

	public void setFromIdRange(Integer fromIdRange) {
		this.fromIdRange = fromIdRange;
	}

	public Integer getToIdRange() {
		return toIdRange;
	}

	public void setToIdRange(Integer toIdRange) {
		this.toIdRange = toIdRange;
	}

	

	public boolean isCloud() {
		return isCloud;
	}


	public void setCloud(boolean isCloud) {
		this.isCloud = isCloud;
	}


	public int compareTo(RequestVLANIdRange o) {
		return this.getFromIdRange().compareTo(o.getFromIdRange());
	}


	
	
}
