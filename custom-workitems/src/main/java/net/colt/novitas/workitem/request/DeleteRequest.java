package net.colt.novitas.workitem.request;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import net.colt.novitas.workitems.BaseWorkItemHandler;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DeleteRequest implements Serializable {

	private static final long serialVersionUID = -1171868509051005851L;

	private Integer portalUserId;
	private String bcn;

	@JsonProperty("cron_execution")
	private String cronExecution;

	@JsonProperty("customer_id")
	private Integer customerId;

	@JsonProperty("portal_user_id")
	public Integer getPortalUserId() {
		return portalUserId;
	}

	public void setPortalUserId(Integer portalUserId) {
		this.portalUserId = portalUserId;
	}

	@JsonProperty("bcn")
	public String getBcn() {
		return bcn;
	}

	public void setBcn(String bcn) {
		this.bcn = bcn;
	}

	public String getCronExecution() {
		return cronExecution;
	}

	public void setCronExecution(String cronExecution) {
		this.cronExecution = cronExecution;
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	@Override
	public String toString() {
		return BaseWorkItemHandler.toString(this);
	}

}
