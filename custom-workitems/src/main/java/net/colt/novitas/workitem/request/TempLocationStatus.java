package net.colt.novitas.workitem.request;

public enum TempLocationStatus {

	ACTIVE, UPDATED, CANCELLED, FAILED;
}
