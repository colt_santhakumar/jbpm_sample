package net.colt.novitas.workitem.request;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AllocateIPInternalRequest implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonProperty("ocn")
	private String ocn;

	@JsonProperty("country")
	private String countryCode;

	@JsonProperty("metro")
	String cityCode;

	@JsonProperty("customer_name")
	String custName;

	@JsonProperty("service_id")
	String serviceId;

	@JsonProperty("cct_name")
	String circuitName;

	@JsonProperty("network_type")
	String networkType;

	@JsonProperty("ip_address")
	String ipAddress;

	@JsonProperty("dev_name")
	String devName;

	@JsonProperty("intf_name")
	String intfName;

	@JsonProperty("ip_version")
	String ipVersion;

	public String getOcn() {
		return ocn;
	}

	public void setOcn(String ocn) {
		this.ocn = ocn;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getCityCode() {
		return cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}

	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public String getCircuitName() {
		return circuitName;
	}

	public void setCircuitName(String circuitName) {
		this.circuitName = circuitName;
	}

	public String getNetworkType() {
		return networkType;
	}

	public void setNetworkType(String networkType) {
		this.networkType = networkType;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getDevName() {
		return devName;
	}

	public void setDevName(String devName) {
		this.devName = devName;
	}

	public String getIntfName() {
		return intfName;
	}

	public void setIntfName(String intfName) {
		this.intfName = intfName;
	}

	public String getIpVersion() {
		return ipVersion;
	}

	public void setIpVersion(String ipVersion) {
		this.ipVersion = ipVersion;
	}

}
