package net.colt.novitas.workitem.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import net.colt.novitas.workitems.BaseWorkItemHandler;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeInfo(include = JsonTypeInfo.As.WRAPPER_OBJECT, use = JsonTypeInfo.Id.NAME)
@JsonTypeName(value = "current_config")
public class IbmAttachment implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty("name")
    private String connectionName;

    @JsonProperty("status")
    private String status;

    @JsonProperty("provider")
    private String provider;

    @JsonProperty("type")
    private String type;

    @JsonProperty("location")
    private String location;

    @JsonProperty("speed-mbps")
    private Integer bandwidth;

    @JsonProperty("global")
    private Boolean isGlobal;

    @JsonProperty("port")
    private String port;

    @JsonProperty("bgp-asn")
    private Integer bgpAsn;

    @JsonProperty("service_key")
    private String serviceKey;

    @JsonProperty("bgp-ibm-ip")
    private String bgpIbmIp;

    @JsonProperty("bgp-cer-ip")
    private String bgpCerIp;

    @JsonProperty("vlan-id")
    private String vlanId;

    public String getConnectionName() {
        return connectionName;
    }

    public void setConnectionName(String connectionName) {
        this.connectionName = connectionName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Integer getBandwidth() {
        return bandwidth;
    }

    public void setBandwidth(Integer bandwidth) {
        this.bandwidth = bandwidth;
    }

    public Boolean getGlobal() {
        return isGlobal;
    }

    public void setGlobal(Boolean global) {
        isGlobal = global;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public Integer getBgpAsn() {
        return bgpAsn;
    }

    public void setBgpAsn(Integer bgpAsn) {
        this.bgpAsn = bgpAsn;
    }

    public String getServiceKey() {
        return serviceKey;
    }

    public void setServiceKey(String serviceKey) {
        this.serviceKey = serviceKey;
    }

    public String getBgpIbmIp() {
        return bgpIbmIp;
    }

    public void setBgpIbmIp(String bgpIbmIp) {
        this.bgpIbmIp = bgpIbmIp;
    }

    public String getBgpCerIp() {
        return bgpCerIp;
    }

    public void setBgpCerIp(String bgpCerIp) {
        this.bgpCerIp = bgpCerIp;
    }

    public String getVlanId() {
        return vlanId;
    }

    public void setVlanId(String vlanId) {
        this.vlanId = vlanId;
    }

    @Override
    public String toString() {
        return BaseWorkItemHandler.toString(this);
    }
}
