package net.colt.novitas.workitem.request;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import net.colt.novitas.workitems.BaseWorkItemHandler;

@JsonIgnoreProperties(ignoreUnknown = true)
public class IpAccessConfig implements Serializable {

	private static final long serialVersionUID = 1298779098823075914L;

	@JsonProperty("bandwidth")
	private String bandwidth;

	@JsonProperty("burst_buffer")
	private Integer burstBuffer;

	@JsonProperty("cpe_ip_address")
	private String cpeIpAddress;

	@JsonProperty("customer_ip_range")
	private String customerIpRange;

	@JsonProperty("interface_port")
	private String interfacePort;

	@JsonProperty("interface_type")
	private String interfaceType;

	@JsonProperty("unit_id")
	private Integer unitId;

	@JsonProperty("vlan_descr")
	private String vlanDescription;

	@JsonProperty("vlan_id")
	private Integer vlanId;

	@JsonProperty("wanip")
	private String wanIp;

	@JsonProperty("wanprefix")
	private String wanPrefix;

	public IpAccessConfig() {
		super();
	}

	public IpAccessConfig(String bandwidth, Integer burstBuffer, String cpeIpAddress, String customerIpRange,
			String interfacePort, String interfaceType, Integer unitId, String vlanDescription, Integer vlanId,
			String wanIp, String wanPrefix) {
		super();
		this.bandwidth = bandwidth;
		this.burstBuffer = burstBuffer;
		this.cpeIpAddress = cpeIpAddress;
		this.customerIpRange = customerIpRange;
		this.interfacePort = interfacePort;
		this.interfaceType = interfaceType;
		this.unitId = unitId;
		this.vlanDescription = vlanDescription;
		this.vlanId = vlanId;
		this.wanIp = wanIp;
		this.wanPrefix = wanPrefix;
	}

	public String getBandwidth() {
		return bandwidth;
	}

	public void setBandwidth(String bandwidth) {
		this.bandwidth = bandwidth;
	}

	public Integer getBurstBuffer() {
		return burstBuffer;
	}

	public void setBurstBuffer(Integer burstBuffer) {
		this.burstBuffer = burstBuffer;
	}

	public String getCpeIpAddress() {
		return cpeIpAddress;
	}

	public void setCpeIpAddress(String cpeIpAddress) {
		this.cpeIpAddress = cpeIpAddress;
	}

	public String getCustomerIpRange() {
		return customerIpRange;
	}

	public void setCustomerIpRange(String customerIpRange) {
		this.customerIpRange = customerIpRange;
	}

	public String getInterfacePort() {
		return interfacePort;
	}

	public void setInterfacePort(String interfacePort) {
		this.interfacePort = interfacePort;
	}

	public String getInterfaceType() {
		return interfaceType;
	}

	public void setInterfaceType(String interfaceType) {
		this.interfaceType = interfaceType;
	}

	public Integer getUnitId() {
		return unitId;
	}

	public void setUnitId(Integer unitId) {
		this.unitId = unitId;
	}

	public String getVlanDescription() {
		return vlanDescription;
	}

	public void setVlanDescription(String vlanDescription) {
		this.vlanDescription = vlanDescription;
	}

	public Integer getVlanId() {
		return vlanId;
	}

	public void setVlanId(Integer vlanId) {
		this.vlanId = vlanId;
	}

	public String getWanIp() {
		return wanIp;
	}

	public void setWanIp(String wanIp) {
		this.wanIp = wanIp;
	}

	public String getWanPrefix() {
		return wanPrefix;
	}

	public void setWanPrefix(String wanPrefix) {
		this.wanPrefix = wanPrefix;
	}

	@Override
	public String toString() {
		return BaseWorkItemHandler.toString(this);
	}

}