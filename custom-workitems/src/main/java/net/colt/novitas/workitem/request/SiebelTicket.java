package net.colt.novitas.workitem.request;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import net.colt.novitas.workitems.constants.CountryCodeV1;

@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class SiebelTicket implements Serializable {

	private static final long serialVersionUID = -7502900488353078593L;

	private String requestId;

	private String ocn;

	private CountryCodeV1 customerCountryCode;

	private String umbrellaServiceId;

	private String serviceInstanceId;

	private String legalCustomerName;

	private String currentTaskName;

	private PortalUser portalUser;
	
}
