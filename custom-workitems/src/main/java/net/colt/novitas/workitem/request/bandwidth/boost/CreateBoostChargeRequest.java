package net.colt.novitas.workitem.request.bandwidth.boost;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@Getter
@Setter
@ToString
public class CreateBoostChargeRequest implements Serializable {

	private static final long serialVersionUID = -1889118505745741616L;

	private Integer requestId;

	private Integer serviceId;

	private Integer serviceInstanceId;

	private String serviceInstanceType;

	private String serviceInstanceName;

	private Integer bcn;

	private String description;

	private String currency;

	private Float amount;

	private String frequency;

	private String commitmentExpiryDate;

}
