package net.colt.novitas.workitem.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class VlanIdRangeRequest implements Comparable<VlanIdRangeRequest> {

	@JsonProperty("from_id_range")
	private Integer fromIdRange;

	@JsonProperty("to_id_range")
	private Integer toIdRange;

	private boolean cloud = false;

	public VlanIdRangeRequest() {
		super();
	}

	public VlanIdRangeRequest(Integer fromIdRange, Integer toIdRange, boolean cloud) {
		super();
		this.fromIdRange = fromIdRange;
		this.toIdRange = toIdRange;
		this.cloud = cloud;
	}

	public Integer getFromIdRange() {
		return fromIdRange;
	}

	public void setFromIdRange(Integer fromIdRange) {
		this.fromIdRange = fromIdRange;
	}

	public Integer getToIdRange() {
		return toIdRange;
	}

	public void setToIdRange(Integer toIdRange) {
		this.toIdRange = toIdRange;
	}

	public boolean isCloud() {
		return cloud;
	}

	public void setCloud(boolean cloud) {
		this.cloud = cloud;
	}

	@Override
	public int compareTo(VlanIdRangeRequest o) {
		return this.getFromIdRange().compareTo(o.getFromIdRange());
	}

}
