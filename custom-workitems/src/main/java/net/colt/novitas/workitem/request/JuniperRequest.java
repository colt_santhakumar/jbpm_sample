package net.colt.novitas.workitem.request;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import net.colt.novitas.workitems.BaseWorkItemHandler;

@JsonIgnoreProperties(ignoreUnknown = true)
public class JuniperRequest implements Serializable{

	/**
	 *
	 */
	private static final long serialVersionUID = -895325296700309697L;

	@JsonProperty("host_ip")
	private String hostIp;

	@JsonProperty("credentials")
	private String credentials;

	@JsonProperty("ip_access_config")
	private IpAccessConfig ipAccessConfig;

	public JuniperRequest() {
		super();
	}

	public JuniperRequest(String hostIp, String credentials, IpAccessConfig ipAccessConfig) {
		super();
		this.hostIp = hostIp;
		this.credentials = credentials;
		this.ipAccessConfig = ipAccessConfig;
	}

	public String getHostIp() {
		return hostIp;
	}

	public void setHostIp(String hostIp) {
		this.hostIp = hostIp;
	}

	public String getCredentials() {
		return credentials;
	}

	public void setCredentials(String credentials) {
		this.credentials = credentials;
	}

	public IpAccessConfig getIpAccessConfig() {
		return ipAccessConfig;
	}

	public void setIpAccessConfig(IpAccessConfig ipAccessConfig) {
		this.ipAccessConfig = ipAccessConfig;
	}

	@Override
	public String toString() {
		return BaseWorkItemHandler.toString(this);
	}

}