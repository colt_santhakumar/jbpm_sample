package net.colt.novitas.workitem.request.equinix;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@Getter
@Setter
public class UpdateEquinixPortService implements Serializable {

	private static final long serialVersionUID = 1860164814895157105L;

	private Float latitude;

	private Float longitude;

	private String country;

	private String countryCode;

	private String state;

	private String city;

	private String cityCode;

	private String streetName;

	private String postalZipCode;

	private String buildingId;

	private String buildingName;

	private String premisesNumber;

	private String siteName;

	private String novitasSiteType;

	private Float rentalCharge;

	private String rentalUnit;

	private String currency;

	private Float installationCharge;

	private Float decommissioningCharge;

	private String commitmentExpiry;

	private Float penaltyCharge;

	private String id;

	private String name;

	private String status;

	@JsonProperty("circuit_name_1")
	private String circuitName1;

	private String decommissionedOn;

	private String createdDate;

	private String customerName;

	private String ocn;

	private String bcn;

	private String lastUpdated;

	private String portalUserId;

	private String novitasServiceId;

	private Integer customerId;

	private String presentationLabel;

	private String ncTechServiceId;

	private Integer bandwidth;

	private Integer usedBandwidth;

	private Integer availableBandwidth;

	private Integer noOfConnections;

	private Integer maxAllowedConnections;

	private Integer noOfConnectionsRequested;

	private String cloudProvider;

	private String cspKey;

	private String cloudRegion;

	private String interconnectName;

	private Integer vlan;

}
