package net.colt.novitas.workitem.request;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import net.colt.novitas.workitems.BaseWorkItemHandler;

@JsonIgnoreProperties(ignoreUnknown = true)
public class NetCrackerConnectionRequest implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty("ocn")
	private String ocn; //party id
	
	@JsonProperty("customer_name")
	private String customerName; //legal name
	
	@JsonProperty("service_id")
	private String serviceId;   // novitas port service id
	
	@JsonProperty("service_order_reference")
	private String serviceOrderReference;  // novitas request id
	
	@JsonProperty("bandwidth")
	private Integer bandwidth; 
	
	@JsonProperty("technical_service_id")
	private String technicalServiceId;   //connection technical service id
	
	@JsonProperty("a_technical_service_id")
	private String aTechnicalServiceId;
	
	@JsonProperty("b_technical_service_id")
	private String bTechnicalServiceId;
	
	@JsonProperty("a_end_vlan_mapping")
	private String aEndVlanMapping;
	
	@JsonProperty("b_end_vlan_mapping")
	private String bEndVlanMapping;
	
	@JsonProperty("a_end_vlan_type")
	private String aEndVlanType;
	
	@JsonProperty("b_end_vlan_type")
	private String bEndVlanType;
	
	@JsonProperty("a_end_vlans")
	private String aEndVlans;
	
	@JsonProperty("b_end_vlans")
	private String bEndVlans;
	
	@JsonProperty("callback_url")
	private String callBackUrl;

	public String getOcn() {
		return ocn;
	}

	public void setOcn(String ocn) {
		this.ocn = ocn;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public String getServiceOrderReference() {
		return serviceOrderReference;
	}

	public void setServiceOrderReference(String serviceOrderReference) {
		this.serviceOrderReference = serviceOrderReference;
	}

	public Integer getBandwidth() {
		return bandwidth;
	}

	public void setBandwidth(Integer bandwidth) {
		this.bandwidth = bandwidth;
	}

	public String getaTechnicalServiceId() {
		return aTechnicalServiceId;
	}

	public void setaTechnicalServiceId(String aTechnicalServiceId) {
		this.aTechnicalServiceId = aTechnicalServiceId;
	}

	public String getbTechnicalServiceId() {
		return bTechnicalServiceId;
	}

	public void setbTechnicalServiceId(String bTechnicalServiceId) {
		this.bTechnicalServiceId = bTechnicalServiceId;
	}

	public String getaEndVlanMapping() {
		return aEndVlanMapping;
	}

	public void setaEndVlanMapping(String aEndVlanMapping) {
		this.aEndVlanMapping = aEndVlanMapping;
	}

	public String getbEndVlanMapping() {
		return bEndVlanMapping;
	}

	public void setbEndVlanMapping(String bEndVlanMapping) {
		this.bEndVlanMapping = bEndVlanMapping;
	}

	public String getaEndVlanType() {
		return aEndVlanType;
	}

	public void setaEndVlanType(String aEndVlanType) {
		this.aEndVlanType = aEndVlanType;
	}

	public String getbEndVlanType() {
		return bEndVlanType;
	}

	public void setbEndVlanType(String bEndVlanType) {
		this.bEndVlanType = bEndVlanType;
	}

	public String getaEndVlans() {
		return aEndVlans;
	}

	public void setaEndVlans(String aEndVlans) {
		this.aEndVlans = aEndVlans;
	}

	public String getbEndVlans() {
		return bEndVlans;
	}

	public void setbEndVlans(String bEndVlans) {
		this.bEndVlans = bEndVlans;
	}

	public String getCallBackUrl() {
		return callBackUrl;
	}

	public void setCallBackUrl(String callBackUrl) {
		this.callBackUrl = callBackUrl;
	}

	public String getTechnicalServiceId() {
		return technicalServiceId;
	}

	public void setTechnicalServiceId(String technicalServiceId) {
		this.technicalServiceId = technicalServiceId;
	}

	@Override
	public String toString() {
		return BaseWorkItemHandler.toString(this);
	}

	
	

}
