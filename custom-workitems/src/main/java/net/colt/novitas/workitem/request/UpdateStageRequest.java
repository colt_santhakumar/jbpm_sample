package net.colt.novitas.workitem.request;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class UpdateStageRequest implements Serializable {

	private static final long serialVersionUID = 3255750633736015308L;

	@JsonProperty("status")
	private String status;

}
