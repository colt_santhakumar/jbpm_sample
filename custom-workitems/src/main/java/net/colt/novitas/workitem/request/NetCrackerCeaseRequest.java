package net.colt.novitas.workitem.request;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import net.colt.novitas.workitems.BaseWorkItemHandler;

@JsonIgnoreProperties(ignoreUnknown = true)
public class NetCrackerCeaseRequest implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty("ocn")
	private String ocn; //party id
	
	@JsonProperty("customer_name")
	private String customerName; //legal name
	
	@JsonProperty("service_id")
	private String serviceId;   // novitas port service id
	
	@JsonProperty("service_order_reference")
	private String serviceOrderReference;  // novitas request id
	
	@JsonProperty("technical_service_id")
	private String technicalServiceId;
	
	@JsonProperty("callback_url")
	private String callBackUrl;

	public String getOcn() {
		return ocn;
	}

	public void setOcn(String ocn) {
		this.ocn = ocn;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public String getServiceOrderReference() {
		return serviceOrderReference;
	}

	public void setServiceOrderReference(String serviceOrderReference) {
		this.serviceOrderReference = serviceOrderReference;
	}

	public String getTechnicalServiceId() {
		return technicalServiceId;
	}

	public void setTechnicalServiceId(String technicalServiceId) {
		this.technicalServiceId = technicalServiceId;
	}

	public String getCallBackUrl() {
		return callBackUrl;
	}

	public void setCallBackUrl(String callBackUrl) {
		this.callBackUrl = callBackUrl;
	}

	@Override
	public String toString() {
		return BaseWorkItemHandler.toString(this);
	}
	

}
