package net.colt.novitas.workitem.request;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.ToString;

@JsonIgnoreProperties(ignoreUnknown = true)
@ToString
public class AllocateManagedIPInternalRequest implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonProperty("device_name")
	private String deviceName;

	@JsonProperty("country_code")
	private String countryCode;

	@JsonProperty("city_code")
	private String cityCode;

	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getCityCode() {
		return cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}
}
