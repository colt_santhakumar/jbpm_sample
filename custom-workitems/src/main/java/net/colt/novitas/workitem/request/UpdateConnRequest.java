package net.colt.novitas.workitem.request;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import net.colt.novitas.workitems.BaseWorkItemHandler;
import net.colt.novitas.workitems.constants.RentalUnit;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UpdateConnRequest implements Serializable {

	private static final long serialVersionUID = 1L;

	private String name;
	private String status;
	private String resourceId;
	private Integer bandwidth;
	private Float rentalCharge;
	private String rentalCurrency;
	private RentalUnit rentalUnit;
	private String fromVlanMapping;
	private String toVlanMapping;
	private String fromVlanType;
	private String toVlanType;
	private List<RequestVLANIdRange> fromPortVLANIdRange;
	private List<RequestVLANIdRange> toPortVLANIdRange;
	private Integer commitmentPeriod;
	private String commitmentExpiryDate;

	@JsonProperty("olo_service_id")
	private String oloServiceId;

	@JsonProperty("nc_tech_service_id")
	private String ncTechServiceId;

	@JsonProperty("cease_request_id")
	private String ceaseReqId;

	@JsonProperty("base_bandwidth")
	private Integer baseBandwidth;

	@JsonProperty("base_rental")
	private Float baseRental;

	public UpdateConnRequest() {
		super();
	}

	public String getCeaseReqId() {
		return ceaseReqId;
	}

	public void setCeaseReqId(String ceaseReqId) {
		this.ceaseReqId = ceaseReqId;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("status")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@JsonProperty("resource_id")
	public String getResourceId() {
		return resourceId;
	}

	public void setResourceId(String resourceId) {
		this.resourceId = resourceId;
	}

	@JsonProperty("bandwidth")
	public Integer getBandwidth() {
		return bandwidth;
	}

	public void setBandwidth(Integer bandwidth) {
		this.bandwidth = bandwidth;
	}

	@JsonProperty("rental_charge")
	public Float getRentalCharge() {
		return rentalCharge;
	}

	public void setRentalCharge(Float rentalCharge) {
		this.rentalCharge = rentalCharge;
	}

	@JsonProperty("rental_currency")
	public String getRentalCurrency() {
		return rentalCurrency;
	}

	public void setRentalCurrency(String rentalCurrency) {
		this.rentalCurrency = rentalCurrency;
	}

	@JsonProperty("rental_unit")
	public RentalUnit getRentalUnit() {
		return rentalUnit;
	}

	public void setRentalUnit(RentalUnit rentalUnit) {
		this.rentalUnit = rentalUnit;
	}

	@JsonProperty("a_end_vlan_mapping")
	public String getFromVlanMapping() {
		return fromVlanMapping;
	}

	public void setFromVlanMapping(String fromVlanMapping) {
		this.fromVlanMapping = fromVlanMapping;
	}

	@JsonProperty("b_end_vlan_mapping")
	public String getToVlanMapping() {
		return toVlanMapping;
	}

	public void setToVlanMapping(String toVlanMapping) {
		this.toVlanMapping = toVlanMapping;
	}

	@JsonProperty("a_end_vlan_type")
	public String getFromVlanType() {
		return fromVlanType;
	}

	public void setFromVlanType(String fromVlanType) {
		this.fromVlanType = fromVlanType;
	}

	@JsonProperty("b_end_vlan_type")
	public String getToVlanType() {
		return toVlanType;
	}

	public void setToVlanType(String toVlanType) {
		this.toVlanType = toVlanType;
	}

	@JsonProperty("a_end_vlan_ids")
	public List<RequestVLANIdRange> getFromPortVLANIdRange() {
		return fromPortVLANIdRange;
	}

	public void setFromPortVLANIdRange(List<RequestVLANIdRange> fromPortVLANIdRange) {
		this.fromPortVLANIdRange = fromPortVLANIdRange;
	}

	@JsonProperty("b_end_vlan_ids")
	public List<RequestVLANIdRange> getToPortVLANIdRange() {
		return toPortVLANIdRange;
	}

	public void setToPortVLANIdRange(List<RequestVLANIdRange> toPortVLANIdRange) {
		this.toPortVLANIdRange = toPortVLANIdRange;
	}

	@JsonProperty("commitment_period")
	public Integer getCommitmentPeriod() {
		return commitmentPeriod;
	}

	public void setCommitmentPeriod(Integer commitmentPeriod) {
		this.commitmentPeriod = commitmentPeriod;
	}

	@JsonProperty("commitment_expiry_date")
	public String getCommitmentExpiryDate() {
		return commitmentExpiryDate;
	}

	public void setCommitmentExpiryDate(String commitmentExpiryDate) {
		this.commitmentExpiryDate = commitmentExpiryDate;
	}

	public String getOloServiceId() {
		return oloServiceId;
	}

	public void setOloServiceId(String oloServiceId) {
		this.oloServiceId = oloServiceId;
	}

	public String getNcTechServiceId() {
		return ncTechServiceId;
	}

	public void setNcTechServiceId(String ncTechServiceId) {
		this.ncTechServiceId = ncTechServiceId;
	}

	public Integer getBaseBandwidth() {
		return baseBandwidth;
	}

	public Float getBaseRental() {
		return baseRental;
	}

	public void setBaseRental(Float baseRental) {
		this.baseRental = baseRental;
	}

	public void setBaseBandwidth(Integer baseBandwidth) {
		this.baseBandwidth = baseBandwidth;
	}

	@Override
	public String toString() {
		return BaseWorkItemHandler.toString(this);
	}

}