package net.colt.novitas.workitem.request;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import net.colt.novitas.workitems.BaseWorkItemHandler;

public class UpdateRequest implements Serializable {

	private static final long serialVersionUID = 3255750633736015308L;

	@JsonProperty("status")
	private String status;

	@JsonProperty("id")
	private Integer id;

	@JsonProperty("status_code")
	private Integer statusCode;

	@JsonProperty("service_id")
	private String serviceId;

	@JsonProperty("penalty_charge")
	private Float penaltyCharge;

	@JsonProperty("process_instance_id")
	private String processInstanceId;

	@JsonProperty("host_address")
	private String hostAddress;

	@JsonProperty("bandwidth")
	private Integer bandwidth;

	@JsonProperty("technology_type")
	private String technologyType;

	@JsonProperty("connector_type")
	private String connectorType;

	@JsonProperty("loa_validated")
	private Boolean loaValidated;

	@JsonProperty("aws_ref")
	private String awsRef;

	@JsonProperty("order_id")
	private String orderId;

	@JsonProperty("order_status")
	private String orderStatus;

	@JsonProperty("site_id")
	private String siteId;
	
	@JsonProperty("street_name")
	private String locationStreetName;

	@JsonProperty("site_name")
	private String siteName;

	@JsonProperty("pm_site_type")
	private String pmSiteType;

	@JsonProperty("site_floor")
	private String siteFloor;

	@JsonProperty("site_room_name")
	private String siteRoomName;

	@JsonProperty("address")
	private String address;
	
	
	
	public String getLocationStreetName() {
		return locationStreetName;
	}

	public void setLocationStreetName(String locationStreetName) {
		this.locationStreetName = locationStreetName;
	}

	public String getSiteName() {
		return siteName;
	}

	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}

	public String getPmSiteType() {
		return pmSiteType;
	}

	public void setPmSiteType(String pmSiteType) {
		this.pmSiteType = pmSiteType;
	}

	public String getSiteFloor() {
		return siteFloor;
	}

	public void setSiteFloor(String siteFloor) {
		this.siteFloor = siteFloor;
	}

	public String getSiteRoomName() {
		return siteRoomName;
	}

	public void setSiteRoomName(String siteRoomName) {
		this.siteRoomName = siteRoomName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public Float getPenaltyCharge() {
		return penaltyCharge;
	}

	public void setPenaltyCharge(Float penaltyCharge) {
		this.penaltyCharge = penaltyCharge;
	}

	/**
	 * @return the processInstanceId
	 */
	public String getProcessInstanceId() {
		return processInstanceId;
	}

	/**
	 * @param processInstanceId
	 *            the processInstanceId to set
	 */
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	/**
	 * @return the hostAddress
	 */
	public String getHostAddress() {
		return hostAddress;
	}

	/**
	 * @param hostAddress
	 *            the hostAddress to set
	 */
	public void setHostAddress(String hostAddress) {
		this.hostAddress = hostAddress;
	}

	public Integer getBandwidth() {
		return bandwidth;
	}

	public void setBandwidth(Integer bandwidth) {
		this.bandwidth = bandwidth;
	}

	public String getTechnologyType() {
		return technologyType;
	}

	public void setTechnologyType(String technologyType) {
		this.technologyType = technologyType;
	}

	public String getConnectorType() {
		return connectorType;
	}

	public void setConnectorType(String connectorType) {
		this.connectorType = connectorType;
	}

	public Boolean getLoaValidated() {
		return loaValidated;
	}

	public void setLoaValidated(Boolean loaValidated) {
		this.loaValidated = loaValidated;
	}

	public String getAwsRef() {
		return awsRef;
	}

	public void setAwsRef(String awsRef) {
		this.awsRef = awsRef;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public String getSiteId() {
		return siteId;
	}

	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return BaseWorkItemHandler.toString(this);
	}

}
