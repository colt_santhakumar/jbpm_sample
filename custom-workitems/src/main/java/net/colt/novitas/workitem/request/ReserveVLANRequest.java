package net.colt.novitas.workitem.request;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ReserveVLANRequest implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -3331958589960663869L;

	@JsonProperty("circuit_id")
	private String circuitId;

	@JsonProperty("service_id")
	private String serviceID;

	@JsonProperty("bandwidth")
	private String bandwidth;

	@JsonProperty("customer_name")
	private String customerName;

	@JsonProperty("vlan_allocation_type")
	private VLANAllocationType vlanAllocationType;

	@JsonProperty("csp_name")
	private String cspName;

	@JsonProperty("building_id")
	private String buildingId;

	public ReserveVLANRequest() {

	}

	public ReserveVLANRequest(String circuitId, String serviceID, String bandWidth, String customerName,
			VLANAllocationType vlanAllocationType, String cspName, String buildingId) {
		super();
		this.circuitId = circuitId;
		this.serviceID = serviceID;
		this.bandwidth = bandWidth;
		this.customerName = customerName;
		this.vlanAllocationType = vlanAllocationType;
		this.cspName = cspName;
		this.buildingId = buildingId;
	}

	public String getCircuitId() {
		return circuitId;
	}

	public void setCircuitId(String circuitId) {
		this.circuitId = circuitId;
	}

	public String getServiceID() {
		return serviceID;
	}

	public void setServiceID(String serviceID) {
		this.serviceID = serviceID;
	}

	public String getBandWidth() {
		return bandwidth;
	}

	public void setBandWidth(String bandWidth) {
		this.bandwidth = bandWidth;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public VLANAllocationType getVlanAllocationType() {
		return vlanAllocationType;
	}

	public void setVlanAllocationType(VLANAllocationType vlanAllocationType) {
		this.vlanAllocationType = vlanAllocationType;
	}

	public String getCspName() {
		return cspName;
	}

	public void setCspName(String cspName) {
		this.cspName = cspName;
	}

	public String getBuildingId() {
		return buildingId;
	}

	public void setBuildingId(String buildingId) {
		this.buildingId = buildingId;
	}

	@Override
	public String toString() {
		return "ReserveVLANRequest [circuitId=" + circuitId + ", serviceID=" + serviceID + ", bandWidth=" + bandwidth
				+ ", customerName=" + customerName + ", vlanAllocationType=" + vlanAllocationType + ", cspName="
				+ cspName + ", buildingId=" + buildingId + "]";
	}

}
