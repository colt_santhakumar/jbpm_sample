package net.colt.novitas.workitem.request;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.ToString;
import net.colt.novitas.workitems.response.bandwidth.boost.BandwidthBoostPricesResponseDto;

@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class IpAccessServiceRequest implements Serializable {

	private static final long serialVersionUID = -3719774214294851771L;

	@JsonProperty("bandwidth")
	private Integer bandwidth;

	@JsonProperty("cease_request_id")
	private String ceaseRequestId;

	@JsonProperty("commitment_expiry_date")
	private String commitmentExpiryDate;

	@JsonProperty("commitment_period")
	private Integer commitmentPeriod;

	@JsonProperty("connection_type")
	private String connectionType;

	@JsonProperty("customer_name")
	private String customerName;

	@JsonProperty("decommissioned_on")
	private String decommissionedOn;

	@JsonProperty("from_port_id")
	private String fromPortId;

	@JsonProperty("port_vlan_mapping")
	private String portVlanMapping;

	@JsonProperty("port_vlan_type")
	private String portVlanType;

	@JsonProperty("port_vlan_ids")
	private List<RequestVLANIdRange> portVlanIds;

	@JsonProperty("connection_name")
	private String connectionName;

	@JsonProperty("nc_tech_service_id")
	private String ncTechServiceId;

	@JsonProperty("nni_nc_tech_service_id")
	private String nniNcTechServiceId;

	@JsonProperty("nms")
	private String nms;

	@JsonProperty("xng_circuit_id")
	private String xngCircuitId;

	@JsonProperty("nni_type")
	private String nniType;

	@JsonProperty("nni_vlan_id")
	private Integer nniVlanId;

	@JsonProperty("nni_vlan_mapping")
	private String nniVlanMapping;

	@JsonProperty("nni_vlan_type")
	private String nniVlanType;

	@JsonProperty("novitas_service_id")
	private String novitasServiceId;

	@JsonProperty("ocn")
	private String ocn;

	@JsonProperty("rental_charge")
	private Float rentalCharge;

	@JsonProperty("rental_unit")
	private String rentalUnit;

	@JsonProperty("resource_id")
	private String resourceId;

	@JsonProperty("status")
	private String status;

	@JsonProperty("currency")
	private String currency;

	@JsonProperty("cpe_public_ip")
	private String cpePublicIp;

	@JsonProperty("cpe_private_ip")
	private String cpePrivateIp;

	@JsonProperty("private_ip_range")
	private String privateIpRange;

	@JsonProperty("sar_vlan_ip")
	private String sarVlanIp;

	@JsonProperty("customer_ip_range")
	private String customerIpRange;

	@JsonProperty("lan_ip_list")
	private List<String> lanIpList;

	@JsonProperty("wan_ip_list")
	private List<String> wanIpList;

	@JsonProperty("sar_name")
	private String sarName;

	@JsonProperty("sar_port")
	private String sarPort;

	@JsonProperty("sar_management_ip")
	private String sarManagementIp;

	@JsonProperty("base_bandwidth")
	private Integer baseBandwidth;

	@JsonProperty("base_rental")
	private Float baseRental;

    @JsonProperty("bandwidth_boost")
    private List<BandwidthBoostPricesResponseDto> bandwidthBoostPricesResponseDtos;

    @JsonProperty("iqnet_nni_circuit")
    private String iqnetNniCircuit;

	public Float getBaseRental() {
		return baseRental;
	}

	public void setBaseRental(Float baseRental) {
		this.baseRental = baseRental;
	}

	public Integer getBaseBandwidth() {
		return baseBandwidth;
	}

	public void setBaseBandwidth(Integer baseBandwidth) {
		this.baseBandwidth = baseBandwidth;
	}

	public String getSarName() {
		return sarName;
	}

	public String getSarPort() {
		return sarPort;
	}

	public String getSarManagementIp() {
		return sarManagementIp;
	}

	public void setSarName(String sarName) {
		this.sarName = sarName;
	}

	public void setSarPort(String sarPort) {
		this.sarPort = sarPort;
	}

	public void setSarManagementIp(String sarManagementIp) {
		this.sarManagementIp = sarManagementIp;
	}

	public Integer getBandwidth() {
		return bandwidth;
	}

	public void setBandwidth(Integer bandwidth) {
		this.bandwidth = bandwidth;
	}

	public String getCeaseRequestId() {
		return ceaseRequestId;
	}

	public void setCeaseRequestId(String ceaseRequestId) {
		this.ceaseRequestId = ceaseRequestId;
	}

	public String getCommitmentExpiryDate() {
		return commitmentExpiryDate;
	}

	public void setCommitmentExpiryDate(String commitmentExpiryDate) {
		this.commitmentExpiryDate = commitmentExpiryDate;
	}

	public Integer getCommitmentPeriod() {
		return commitmentPeriod;
	}

	public void setCommitmentPeriod(Integer commitmentPeriod) {
		this.commitmentPeriod = commitmentPeriod;
	}

	public String getConnectionType() {
		return connectionType;
	}

	public void setConnectionType(String connectionType) {
		this.connectionType = connectionType;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getDecommissionedOn() {
		return decommissionedOn;
	}

	public void setDecommissionedOn(String decommissionedOn) {
		this.decommissionedOn = decommissionedOn;
	}

	public String getFromPortId() {
		return fromPortId;
	}

	public void setFromPortId(String fromPortId) {
		this.fromPortId = fromPortId;
	}

	public String getConnectionName() {
		return connectionName;
	}

	public void setConnectionName(String connectionName) {
		this.connectionName = connectionName;
	}

	public String getNcTechServiceId() {
		return ncTechServiceId;
	}

	public void setNcTechServiceId(String ncTechServiceId) {
		this.ncTechServiceId = ncTechServiceId;
	}

	public String getNms() {
		return nms;
	}

	public void setNms(String nms) {
		this.nms = nms;
	}

	public String getXngCircuitId() {
		return xngCircuitId;
	}

	public void setXngCircuitId(String xngCircuitId) {
		this.xngCircuitId = xngCircuitId;
	}

	public String getNniType() {
		return nniType;
	}

	public void setNniType(String nniType) {
		this.nniType = nniType;
	}

	public Integer getNniVlanId() {
		return nniVlanId;
	}

	public void setNniVlanId(Integer nniVlanId) {
		this.nniVlanId = nniVlanId;
	}

	public String getNniVlanMapping() {
		return nniVlanMapping;
	}

	public void setNniVlanMapping(String nniVlanMapping) {
		this.nniVlanMapping = nniVlanMapping;
	}

	public String getNniVlanType() {
		return nniVlanType;
	}

	public void setNniVlanType(String nniVlanType) {
		this.nniVlanType = nniVlanType;
	}

	public String getNovitasServiceId() {
		return novitasServiceId;
	}

	public void setNovitasServiceId(String novitasServiceId) {
		this.novitasServiceId = novitasServiceId;
	}

	public String getOcn() {
		return ocn;
	}

	public void setOcn(String ocn) {
		this.ocn = ocn;
	}

	public Float getRentalCharge() {
		return rentalCharge;
	}

	public void setRentalCharge(Float rentalCharge) {
		this.rentalCharge = rentalCharge;
	}

	public String getRentalUnit() {
		return rentalUnit;
	}

	public void setRentalUnit(String rentalUnit) {
		this.rentalUnit = rentalUnit;
	}

	public String getResourceId() {
		return resourceId;
	}

	public void setResourceId(String resourceId) {
		this.resourceId = resourceId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getCpePublicIp() {
		return cpePublicIp;
	}

	public void setCpePublicIp(String cpePublicIp) {
		this.cpePublicIp = cpePublicIp;
	}

	public String getCpePrivateIp() {
		return cpePrivateIp;
	}

	public void setCpePrivateIp(String cpePrivateIp) {
		this.cpePrivateIp = cpePrivateIp;
	}

	public String getPrivateIpRange() {
		return privateIpRange;
	}

	public void setPrivateIpRange(String privateIpRange) {
		this.privateIpRange = privateIpRange;
	}

	public String getSarVlanIp() {
		return sarVlanIp;
	}

	public void setSarVlanIp(String sarVlanIp) {
		this.sarVlanIp = sarVlanIp;
	}

	public String getCustomerIpRange() {
		return customerIpRange;
	}

	public void setCustomerIpRange(String customerIpRange) {
		this.customerIpRange = customerIpRange;
	}

	public List<String> getLanIpList() {
		return lanIpList;
	}

	public void setLanIpList(List<String> lanIpList) {
		this.lanIpList = lanIpList;
	}

	public List<String> getWanIpList() {
		return wanIpList;
	}

	public void setWanIpList(List<String> wanIpList) {
		this.wanIpList = wanIpList;
	}

	public String getPortVlanMapping() {
		return portVlanMapping;
	}

	public void setPortVlanMapping(String portVlanMapping) {
		this.portVlanMapping = portVlanMapping;
	}

	public String getPortVlanType() {
		return portVlanType;
	}

	public void setPortVlanType(String portVlanType) {
		this.portVlanType = portVlanType;
	}

	public List<RequestVLANIdRange> getPortVlanIds() {
		return portVlanIds;
	}

	public void setPortVlanIds(List<RequestVLANIdRange> portVlanIds) {
		this.portVlanIds = portVlanIds;
	}

	public String getNniNcTechServiceId() {
		return nniNcTechServiceId;
	}

	public void setNniNcTechServiceId(String nniNcTechServiceId) {
		this.nniNcTechServiceId = nniNcTechServiceId;
	}

	public List<BandwidthBoostPricesResponseDto> getBandwidthBoostPricesResponseDtos() {
		return bandwidthBoostPricesResponseDtos;
	}

	public void setBandwidthBoostPricesResponseDtos(
			List<BandwidthBoostPricesResponseDto> bandwidthBoostPricesResponseDtos) {
		this.bandwidthBoostPricesResponseDtos = bandwidthBoostPricesResponseDtos;
	}

	public String getIqnetNniCircuit() {
		return iqnetNniCircuit;
	}

	public void setIqnetNniCircuit(String iqnetNniCircuit) {
		this.iqnetNniCircuit = iqnetNniCircuit;
	}

}