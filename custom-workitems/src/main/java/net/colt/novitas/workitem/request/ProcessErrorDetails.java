package net.colt.novitas.workitem.request;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import net.colt.novitas.workitems.BaseWorkItemHandler;

public class ProcessErrorDetails implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8610752631955149236L;

	@JsonProperty("request_id")
	private Integer requestId;

	@JsonProperty("deployment_id")
	private String deploymentId;

	@JsonProperty("error_stack")
	private byte[] errorFullStack;

	@JsonProperty("task_name")
	private String taskName;

	@JsonProperty("process_instance_id")
	private Long processInstanceId;

	@JsonProperty("host_address")
	private String hostAddress;

	public Integer getRequestId() {
		return requestId;
	}

	public void setRequestId(Integer requestId) {
		this.requestId = requestId;
	}

	public String getDeploymentId() {
		return deploymentId;
	}

	public void setDeploymentId(String deploymentId) {
		this.deploymentId = deploymentId;
	}

	public byte[] getErrorFullStack() {
		return errorFullStack;
	}

	public void setErrorFullStack(byte[] errorFullStack) {
		this.errorFullStack = errorFullStack;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public Long getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(Long processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getHostAddress() {
		return hostAddress;
	}

	public void setHostAddress(String hostAddress) {
		this.hostAddress = hostAddress;
	}

	@Override
	public String toString() {
		return BaseWorkItemHandler.toString(this);
	}

}
