package net.colt.novitas.workitem.request;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import net.colt.novitas.workitems.BaseWorkItemHandler;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UpdateCrossConnectRuquest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty("status")
	private String status;

	@JsonProperty("status_code")
	private Integer statusCode;

	@JsonProperty("port_id")
	private String portId;

	@JsonProperty("cross_connect_id")
	private String crossConnectId;

	@JsonProperty("host_address")
	private String hostAddress;

	@JsonProperty("process_instance_id")
	private Long processInstanceId;

	@JsonProperty("penalty_charge")
	private Float penaltyCharge;

	@JsonProperty("penalty_currency")
	private String penaltyCurrency;

	@JsonProperty("loa_validated")
	private Boolean loaValidated;

	@JsonProperty("customer_pp_port")
	private String customerPpPort;

	@JsonProperty("customer_pp_floor")
	private String customerPpFloor;

	@JsonProperty("customer_pp_room")
	private String customerPpRoom;

	@JsonProperty("customer_pp_cabinet")
	private String customerPpCabinet;

	@JsonProperty("customer_pp_device")
	private String customerPpDevice;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}

	public String getPortId() {
		return portId;
	}

	public void setPortId(String portId) {
		this.portId = portId;
	}

	public String getCrossConnectId() {
		return crossConnectId;
	}

	public void setCrossConnectId(String crossConnectId) {
		this.crossConnectId = crossConnectId;
	}

	public String getHostAddress() {
		return hostAddress;
	}

	public void setHostAddress(String hostAddress) {
		this.hostAddress = hostAddress;
	}

	public Long getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(Long processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public Float getPenaltyCharge() {
		return penaltyCharge;
	}

	public void setPenaltyCharge(Float penaltyCharge) {
		this.penaltyCharge = penaltyCharge;
	}

	public String getPenaltyCurrency() {
		return penaltyCurrency;
	}

	public void setPenaltyCurrency(String penaltyCurrency) {
		this.penaltyCurrency = penaltyCurrency;
	}

	public Boolean getLoaValidated() {
		return loaValidated;
	}

	public void setLoaValidated(Boolean loaValidated) {
		this.loaValidated = loaValidated;
	}

	public String getCustomerPpPort() {
		return customerPpPort;
	}

	public void setCustomerPpPort(String customerPpPort) {
		this.customerPpPort = customerPpPort;
	}

	public String getCustomerPpFloor() {
		return customerPpFloor;
	}

	public void setCustomerPpFloor(String customerPpFloor) {
		this.customerPpFloor = customerPpFloor;
	}

	public String getCustomerPpRoom() {
		return customerPpRoom;
	}

	public void setCustomerPpRoom(String customerPpRoom) {
		this.customerPpRoom = customerPpRoom;
	}

	public String getCustomerPpCabinet() {
		return customerPpCabinet;
	}

	public void setCustomerPpCabinet(String customerPpCabinet) {
		this.customerPpCabinet = customerPpCabinet;
	}

	public String getCustomerPpDevice() {
		return customerPpDevice;
	}

	public void setCustomerPpDevice(String customerPpDevice) {
		this.customerPpDevice = customerPpDevice;
	}

	@Override
	public String toString() {
		return BaseWorkItemHandler.toString(this);
	}
	

}
