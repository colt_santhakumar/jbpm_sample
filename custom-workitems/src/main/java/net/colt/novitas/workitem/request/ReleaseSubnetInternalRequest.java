package net.colt.novitas.workitem.request;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ReleaseSubnetInternalRequest implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonProperty("subnet_address")
	private String subnetAddress;

	@JsonProperty("ip_version")
	private String ipVersion;

	public String getSubnetAddress() {
		return subnetAddress;
	}

	public void setSubnetAddress(String subnetAddress) {
		this.subnetAddress = subnetAddress;
	}

	public String getIpVersion() {
		return ipVersion;
	}

	public void setIpVersion(String ipVersion) {
		this.ipVersion = ipVersion;
	}

	

}
