package net.colt.novitas.workitem.request.equinix;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@Getter
@Setter
@ToString
public class EquinixInterconnectRequest implements Serializable {

	private static final long serialVersionUID = -6027449931700345585L;

	private String cspReferenceId;

	private Integer vlan;

	private String ncTechServiceId;

	private String cspKey;
}
