package net.colt.novitas.workitem.request;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;


public class CallBackDetails implements Serializable{
    
	private static final long serialVersionUID = 8837573856265216816L;
	
	@JsonProperty("transaction_id")
	private String transactionId;
	@JsonProperty("process_instance_id")
	private long processInstanceId;
	@JsonProperty("status")
	private String status;
	@JsonProperty("status_description")
	private String statusDescription;
	@JsonProperty("signal_name")
	private String signalName;
	@JsonProperty("deployment_id")
	private String deploymentId;
	@JsonProperty("jbpm_url")
	private String jbpmUrl;

	/**
	 * @return the transactionId
	 */
	public String getTransactionId() {
		return transactionId;
	}

	/**
	 * @param transactionId
	 *            the transactionId to set
	 */
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	/**
	 * @return the processInstanceId
	 */
	public long getProcessInstanceId() {
		return processInstanceId;
	}

	/**
	 * @param processInstanceId
	 *            the processInstanceId to set
	 */
	public void setProcessInstanceId(long processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the signalName
	 */
	public String getSignalName() {
		return signalName;
	}

	/**
	 * @param signalName
	 *            the signalName to set
	 */
	public void setSignalName(String signalName) {
		this.signalName = signalName;
	}

	public String getDeploymentId() {
		return deploymentId;
	}

	public void setDeploymentId(String deploymentId) {
		this.deploymentId = deploymentId;
	}

	/**
	 * @return the statusDescription
	 */
	public String getStatusDescription() {
		return statusDescription;
	}

	/**
	 * @param statusDescription the statusDescription to set
	 */
	public void setStatusDescription(String statusDescription) {
		this.statusDescription = statusDescription;
	}

	public String getJbpmUrl() {
		return jbpmUrl;
	}

	public void setJbpmUrl(String jbpmUrl) {
		this.jbpmUrl = jbpmUrl;
	}

	@Override
	public String toString() {
		return "CallBackDetails [transactionId=" + transactionId
				+ ", processInstanceId=" + processInstanceId + ", status="
				+ status + ", statusDescription=" + statusDescription
				+ ", signalName=" + signalName + ", deploymentId="
				+ deploymentId + ", jbpmUrl=" + jbpmUrl + "]";
	}

	
	

}
