package net.colt.novitas.workitem.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import net.colt.novitas.workitems.BaseWorkItemHandler;

import java.io.Serializable;
import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CreatePortRequest implements Serializable {

	private static final long serialVersionUID = 2104343630728333762L;

	@JsonProperty("name")
	private String name;

	@JsonProperty("location")
	private String location;

	@JsonProperty("bandwidth")
	private Integer bandwidth;

	@JsonProperty("connector")
	private String connector;

	@JsonProperty("technology")
	private String technology;
	
	@JsonProperty("port_type")
	private String portType;
	
	@JsonProperty("cross_connect_allowed")
	private Boolean crossConnectAllowed;
	
	@JsonProperty("presentation_label")
	private String presentationLabel;

	@JsonProperty("rental_charge")
	private Float rentalCharge;

	@JsonProperty("rental_unit")
	private String rentalUnit;

	@JsonProperty("rental_currency")
	private String rentalCurrency;

	@JsonProperty("address")
	private String address;

	@JsonProperty("site_type")
	private String siteType;

	@JsonProperty("commitment_expiry")
	private Date commitmentExpiry;

	@JsonProperty("service_id")
	private String serviceId;

	@JsonProperty("installation_charge")
	protected Float installationCharge;

	@JsonProperty("installation_currency")
	protected String installationCurrency;

	@JsonProperty("decommissioning_charge")
	private Float decommissioningCharge;

	@JsonProperty("decommissioning_currency")
	private String decommissioningCurrency;

	@JsonProperty("customer_name")
	private String customerName;

	@JsonProperty("ocn")
	private String ocn;

	@JsonProperty("site_floor")
	private String siteFloor;

	@JsonProperty("site_room_name")
	private String siteRoomName;

	@JsonProperty("location_premises_number")
	private String locationPremisesNumber;

	@JsonProperty("location_building_name")
	private String locationBuildingName;

	@JsonProperty("location_street_name")
	private String locationStreetName;

	@JsonProperty("location_city")
	private String locationCity;

	@JsonProperty("location_state")
	private String locationState;

	@JsonProperty("location_country")
	private String locationCountry;

	@JsonProperty("postal_zip_code")
	private String postalZipCode;

	@JsonProperty("latitude")
	private Float latitude;

	@JsonProperty("longitude")
	private Float longitude;

	@JsonProperty("location_id")
	private String locationId;

	@JsonProperty("resource_id")
	private String resourceId1;

	@JsonProperty("expiration_period")
	private Integer portExpiryPeriod;

	@JsonProperty("expires_on")
	private String portExpiresOn;

	@JsonProperty("in_use")
	private Boolean isPortInUse;

	@JsonProperty("local_building_name")
	private String localBuildingName;

	@JsonProperty("location_city_code")
	private String locationCityCode;

	@JsonProperty("location_country_code")
	private String locationCountryCode;

	@JsonProperty("commitment_period")
	private Integer commitmentPeriod;
	
	@JsonProperty("is_hybrid")
	private boolean isHybrid;
	
	@JsonProperty("customer_port_id")
	private String customerPortId;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public Integer getBandwidth() {
		return bandwidth;
	}

	public void setBandwidth(Integer bandwidth) {
		this.bandwidth = bandwidth;
	}

	public Float getRentalCharge() {
		return rentalCharge;
	}

	public void setRentalCharge(Float rentalCharge) {
		this.rentalCharge = rentalCharge;
	}

	public String getRentalUnit() {
		return rentalUnit;
	}

	public void setRentalUnit(String rentalUnit) {
		this.rentalUnit = rentalUnit;
	}

	public String getRentalCurrency() {
		return rentalCurrency;
	}

	public void setRentalCurrency(String rentalCurrency) {
		this.rentalCurrency = rentalCurrency;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getSiteType() {
		return siteType;
	}

	public void setSiteType(String siteType) {
		this.siteType = siteType;
	}

	public Date getCommitmentExpiry() {
		return commitmentExpiry;
	}

	public void setCommitmentExpiry(Date commitmentExpiry) {
		this.commitmentExpiry = commitmentExpiry;
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public Float getDecommissioningCharge() {
		return decommissioningCharge;
	}

	public void setDecommissioningCharge(Float decommissioningCharge) {
		this.decommissioningCharge = decommissioningCharge;
	}

	public String getDecommissioningCurrency() {
		return decommissioningCurrency;
	}

	public void setDecommissioningCurrency(String decommissioningCurrency) {
		this.decommissioningCurrency = decommissioningCurrency;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getOcn() {
		return ocn;
	}

	public void setOcn(String ocn) {
		this.ocn = ocn;
	}

	public String getSiteFloor() {
		return siteFloor;
	}

	public void setSiteFloor(String siteFloor) {
		this.siteFloor = siteFloor;
	}

	public String getSiteRoomName() {
		return siteRoomName;
	}

	public void setSiteRoomName(String siteRoomName) {
		this.siteRoomName = siteRoomName;
	}

	public String getLocationPremisesNumber() {
		return locationPremisesNumber;
	}

	public void setLocationPremisesNumber(String locationPremisesNumber) {
		this.locationPremisesNumber = locationPremisesNumber;
	}

	public String getLocationBuildingName() {
		return locationBuildingName;
	}

	public void setLocationBuildingName(String locationBuildingName) {
		this.locationBuildingName = locationBuildingName;
	}

	public String getLocationStreetName() {
		return locationStreetName;
	}

	public void setLocationStreetName(String locationStreetName) {
		this.locationStreetName = locationStreetName;
	}

	public String getLocationCity() {
		return locationCity;
	}

	public void setLocationCity(String locationCity) {
		this.locationCity = locationCity;
	}

	public String getLocationState() {
		return locationState;
	}

	public void setLocationState(String locationState) {
		this.locationState = locationState;
	}

	public String getLocationCountry() {
		return locationCountry;
	}

	public void setLocationCountry(String locationCountry) {
		this.locationCountry = locationCountry;
	}

	public String getPostalZipCode() {
		return postalZipCode;
	}

	public void setPostalZipCode(String postalZipCode) {
		this.postalZipCode = postalZipCode;
	}

	public Float getLatitude() {
		return latitude;
	}

	public void setLatitude(Float latitude) {
		this.latitude = latitude;
	}

	public Float getLongitude() {
		return longitude;
	}

	public void setLongitude(Float longitude) {
		this.longitude = longitude;
	}

	public String getLocationId() {
		return locationId;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	public String getResourceId1() {
		return resourceId1;
	}

	public void setResourceId1(String resourceId1) {
		this.resourceId1 = resourceId1;
	}

	public Float getInstallationCharge() {
		return installationCharge;
	}

	public void setInstallationCharge(Float installationCharge) {
		this.installationCharge = installationCharge;
	}

	public String getInstallationCurrency() {
		return installationCurrency;
	}

	public void setInstallationCurrency(String installationCurrency) {
		this.installationCurrency = installationCurrency;
	}

	public Integer getPortExpiryPeriod() {
		return portExpiryPeriod;
	}

	public void setPortExpiryPeriod(Integer portExpiryPeriod) {
		this.portExpiryPeriod = portExpiryPeriod;
	}

	public String getPortExpiresOn() {
		return portExpiresOn;
	}

	public void setPortExpiresOn(String portExpiresOn) {
		this.portExpiresOn = portExpiresOn;
	}

	public Boolean getIsPortInUse() {
		return isPortInUse;
	}

	public void setIsPortInUse(Boolean isPortInUse) {
		this.isPortInUse = isPortInUse;
	}

	public String getLocalBuildingName() {
		return localBuildingName;
	}

	public void setLocalBuildingName(String localBuildingName) {
		this.localBuildingName = localBuildingName;
	}

	public String getLocationCityCode() {
		return locationCityCode;
	}

	public void setLocationCityCode(String locationCityCode) {
		this.locationCityCode = locationCityCode;
	}

	public String getLocationCountryCode() {
		return locationCountryCode;
	}

	public void setLocationCountryCode(String locationCountryCode) {
		this.locationCountryCode = locationCountryCode;
	}

	public String getConnector() {
		return connector;
	}

	public void setConnector(String connector) {
		this.connector = connector;
	}

	public String getTechnology() {
		return technology;
	}

	public void setTechnology(String technology) {
		this.technology = technology;
	}

	public Integer getCommitmentPeriod() {
		return commitmentPeriod;
	}

	public void setCommitmentPeriod(Integer commitmentPeriod) {
		this.commitmentPeriod = commitmentPeriod;
	}
	
	public boolean isHybrid() {
		return isHybrid;
	}
	
	public void setIsHybrid(boolean isHybrid) {
		this.isHybrid = isHybrid;
	}
	
	public String getCustomerPortId() {
		return customerPortId;
	}

	public void setCustomerPortId(String customerPortId) {
		this.customerPortId = customerPortId;
	}

	
	public String getPortType() {
		return portType;
	}

	public void setPortType(String portType) {
		this.portType = portType;
	}

	public String getPresentationLabel() {
		return presentationLabel;
	}

	public void setPresentationLabel(String presentationLabel) {
		this.presentationLabel = presentationLabel;
	}
	
	public Boolean isCrossConnectAllowed() {
		return crossConnectAllowed;
	}

	public void setCrossConnectAllowed(Boolean crossConnectAllowed) {
		this.crossConnectAllowed = crossConnectAllowed;
	}

	@Override
	public String toString() {
		return BaseWorkItemHandler.toString(this);
	}

}
