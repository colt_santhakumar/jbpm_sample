package net.colt.novitas.workitem.request;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import net.colt.novitas.workitems.BaseWorkItemHandler;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UpdateCustomerCrossConnectRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty("status")
	private String status;

	@JsonProperty("penalty_charge")
	private Float penaltyCharge;

	@JsonProperty("penalty_currency")
	private String penaltyCurrency;

	@JsonProperty("associated_port_id")
	private String associatedPortId;

	@JsonProperty("customer_pp_port")
	private String customerPpPort;

	@JsonProperty("customer_pp_floor")
	private String customerPpFloor;

	@JsonProperty("customer_pp_room")
	private String customerPpRoom;

	@JsonProperty("customer_pp_cabinet")
	private String customerPpCabinet;

	@JsonProperty("customer_pp_device")
	private String customerPpDevice;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Float getPenaltyCharge() {
		return penaltyCharge;
	}

	public void setPenaltyCharge(Float penaltyCharge) {
		this.penaltyCharge = penaltyCharge;
	}

	public String getPenaltyCurrency() {
		return penaltyCurrency;
	}

	public void setPenaltyCurrency(String penaltyCurrency) {
		this.penaltyCurrency = penaltyCurrency;
	}

	public String getAssociatedPortId() {
		return associatedPortId;
	}

	public void setAssociatedPortId(String associatedPortId) {
		this.associatedPortId = associatedPortId;
	}

	public String getCustomerPpPort() {
		return customerPpPort;
	}

	public void setCustomerPpPort(String customerPpPort) {
		this.customerPpPort = customerPpPort;
	}

	public String getCustomerPpFloor() {
		return customerPpFloor;
	}

	public void setCustomerPpFloor(String customerPpFloor) {
		this.customerPpFloor = customerPpFloor;
	}

	public String getCustomerPpRoom() {
		return customerPpRoom;
	}

	public void setCustomerPpRoom(String customerPpRoom) {
		this.customerPpRoom = customerPpRoom;
	}

	public String getCustomerPpCabinet() {
		return customerPpCabinet;
	}

	public void setCustomerPpCabinet(String customerPpCabinet) {
		this.customerPpCabinet = customerPpCabinet;
	}

	public String getCustomerPpDevice() {
		return customerPpDevice;
	}

	public void setCustomerPpDevice(String customerPpDevice) {
		this.customerPpDevice = customerPpDevice;
	}

	@Override
	public String toString() {
		return BaseWorkItemHandler.toString(this);
	}

}
