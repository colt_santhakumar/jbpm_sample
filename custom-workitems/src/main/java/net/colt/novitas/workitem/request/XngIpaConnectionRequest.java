package net.colt.novitas.workitem.request;

import java.io.Serializable;
import java.math.BigInteger;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.ToString;

@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class XngIpaConnectionRequest implements Serializable {

	private static final long serialVersionUID = 736133692827470294L;

	@JsonProperty("service_id")
	private String serviceId;

	@JsonProperty("circuit_type")
	private String circuitType;

	@JsonProperty("a_logical_port_id")
	private String aLogicalPortId;

	@JsonProperty("bandwidth")
	private Integer bandwidth;

	@JsonProperty("legal_customer_party")
	private String legalCustomerParty;

	@JsonProperty("a_end_service_party")
	private String aEndServiceParty;

	@JsonProperty("b_end_service_party")
	private String bEndServiceParty;

	@JsonProperty("number_of_circuits")
	private BigInteger numberOfCircuits;

	@JsonProperty("sla_id")
	private Integer slaId;

	@JsonProperty("url_callback")
	private String urlCallback;

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public String getCircuitType() {
		return circuitType;
	}

	public void setCircuitType(String circuitType) {
		this.circuitType = circuitType;
	}

	public String getaLogicalPortId() {
		return aLogicalPortId;
	}

	public void setaLogicalPortId(String aLogicalPortId) {
		this.aLogicalPortId = aLogicalPortId;
	}

	public Integer getBandwidth() {
		return bandwidth;
	}

	public void setBandwidth(Integer bandwidth) {
		this.bandwidth = bandwidth;
	}

	public String getLegalCustomerParty() {
		return legalCustomerParty;
	}

	public void setLegalCustomerParty(String legalCustomerParty) {
		this.legalCustomerParty = legalCustomerParty;
	}

	public String getaEndServiceParty() {
		return aEndServiceParty;
	}

	public void setaEndServiceParty(String aEndServiceParty) {
		this.aEndServiceParty = aEndServiceParty;
	}

	public String getbEndServiceParty() {
		return bEndServiceParty;
	}

	public void setbEndServiceParty(String bEndServiceParty) {
		this.bEndServiceParty = bEndServiceParty;
	}

	public BigInteger getNumberOfCircuits() {
		return numberOfCircuits;
	}

	public void setNumberOfCircuits(BigInteger numberOfCircuits) {
		this.numberOfCircuits = numberOfCircuits;
	}

	public Integer getSlaId() {
		return slaId;
	}

	public void setSlaId(Integer slaId) {
		this.slaId = slaId;
	}

	public String getUrlCallback() {
		return urlCallback;
	}

	public void setUrlCallback(String urlCallback) {
		this.urlCallback = urlCallback;
	}

}
