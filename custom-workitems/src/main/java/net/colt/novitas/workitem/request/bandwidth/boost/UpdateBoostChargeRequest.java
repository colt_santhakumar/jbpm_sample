package net.colt.novitas.workitem.request.bandwidth.boost;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@Getter
@Setter
@ToString
public class UpdateBoostChargeRequest implements Serializable {

	private static final long serialVersionUID = -1889118505745741616L;

	private Integer requestId;

	private String currency;

	private Float amount;

	private String frequency;

	private String commitmentType;

}
