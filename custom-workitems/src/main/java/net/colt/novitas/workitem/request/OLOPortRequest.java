package net.colt.novitas.workitem.request;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class OLOPortRequest implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty("request_id")
	private String requestId; 

	@JsonProperty("service_id")
	private String serviceId;
	
	
	@JsonProperty("location_id") 
	private String locationId;
	
	@JsonProperty("bandwidth") 
	private int bandwidth;
		
		
	@JsonProperty("port_type")
	private String portType;

	@JsonProperty("connector")
	private String connector;

	
	
	

	public OLOPortRequest() {
		super();
	}

	public OLOPortRequest(String requestId, String serviceId, String locationId, int bandwidth, String portType,
			String connector) {
		super();
		this.requestId = requestId;
		this.serviceId = serviceId;
		this.locationId = locationId;
		this.bandwidth = bandwidth;
		this.portType = portType;
		this.connector = connector;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public String getLocationId() {
		return locationId;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public int getBandwidth() {
		return bandwidth;
	}

	public void setBandwidth(int bandwidth) {
		this.bandwidth = bandwidth;
	}

	public String getPortType() {
		return portType;
	}

	public void setPortType(String portType) {
		this.portType = portType;
	}

	public String getConnector() {
		return connector;
	}

	public void setConnector(String connector) {
		this.connector = connector;
	}

	@JsonIgnore
	public boolean isAnyFieldNull(){
		return connector==null || portType==null ||  bandwidth==0
				|| serviceId==null || requestId==null || locationId==null;
		
		
	}
	
	
	
}
