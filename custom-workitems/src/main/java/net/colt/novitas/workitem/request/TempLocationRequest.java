package net.colt.novitas.workitem.request;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;
import net.colt.novitas.workitem.request.TempLocationStatus;

/**
 * @author Drao
 *
 */
public class TempLocationRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty("ocn")
	private String ocn;

	@JsonProperty("building_id")
	private String buildingId;
	
	@JsonProperty("building_name")
	private String buildingName;

	@JsonProperty("location_name")
	private String locationName;

	@JsonProperty("address")
	private String address;

	@JsonProperty("site_floor")
	private String siteFloor;

	@JsonProperty("site_room_name")
	private String siteRoomName;

	@JsonProperty("site_type")
	private String siteType;

	@JsonProperty("private_site")
	private Boolean privateSite;

	@JsonProperty("validated")
	private Boolean validated;
	
	@JsonProperty("country")
	private String country;

	@JsonProperty("status")
	private TempLocationStatus status;
	
	public TempLocationRequest() {
		super();
	}
	
	public String getOcn() {
		return ocn;
	}

	public void setOcn(String ocn) {
		this.ocn = ocn;
	}

	public String getBuildingId() {
		return buildingId;
	}

	public void setBuildingId(String buildingId) {
		this.buildingId = buildingId;
	}

	public String getBuildingName() {
		return buildingName;
	}

	public void setBuildingName(String buildingName) {
		this.buildingName = buildingName;
	}

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getSiteFloor() {
		return siteFloor;
	}

	public void setSiteFloor(String siteFloor) {
		this.siteFloor = siteFloor;
	}

	public String getSiteRoomName() {
		return siteRoomName;
	}

	public void setSiteRoomName(String siteRoomName) {
		this.siteRoomName = siteRoomName;
	}

	public String getSiteType() {
		return siteType;
	}

	public void setSiteType(String siteType) {
		this.siteType = siteType;
	}

	public Boolean getPrivateSite() {
		return privateSite;
	}

	public void setPrivateSite(Boolean privateSite) {
		this.privateSite = privateSite;
	}

	public Boolean getValidated() {
		return validated;
	}

	public void setValidated(Boolean validated) {
		this.validated = validated;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public TempLocationStatus getStatus() {
		return status;
	}

	public void setStatus(TempLocationStatus status) {
		this.status = status;
	}

}
