package net.colt.novitas.workitem.request;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ReleaseVLANRequest  implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 566011463831522784L;

	@JsonProperty("circuit_id")
	private String circuitId;

	@JsonProperty("vlan_id")
	private String vlanId;

	@JsonProperty("logical_port_id")
	private String logicalPortID;

	@JsonProperty("vcpe_name")
	private String vcpeName;

	@JsonProperty("release_vlan_type")
	private VLANAllocationType releaseVlanType;

	@JsonProperty("sar_name")
	private String sarName;

	@JsonProperty("building_id")
	private String buildingId;

	public ReleaseVLANRequest() {

	}

	public String getCircuitId() {
		return circuitId;
	}

	public void setCircuitId(String circuitId) {
		this.circuitId = circuitId;
	}

	public String getVlanId() {
		return vlanId;
	}

	public void setVlanId(String vlanId) {
		this.vlanId = vlanId;
	}

	public String getLogicalPortID() {
		return logicalPortID;
	}

	public void setLogicalPortID(String logicalPortID) {
		this.logicalPortID = logicalPortID;
	}

	public String getVcpeName() {
		return vcpeName;
	}

	public void setVcpeName(String vcpe_name) {
		this.vcpeName = vcpe_name;
	}

	public VLANAllocationType getReleaseVlanType() {
		return releaseVlanType;
	}

	public void setReleaseVlanType(VLANAllocationType releaseVlanType) {
		this.releaseVlanType = releaseVlanType;
	}

	public String getSarName() {
		return sarName;
	}

	public void setSarName(String sarName) {
		this.sarName = sarName;
	}

	public String getBuildingId() {
		return buildingId;
	}

	public void setBuildingId(String buildingId) {
		this.buildingId = buildingId;
	}

	public ReleaseVLANRequest(String circuitId, String vlanId, String logicalPortID, String vcpe_name, String bandwidth,
			VLANAllocationType releaseVlanType, String sarName, String buildingId) {
		super();
		this.circuitId = circuitId;
		this.vlanId = vlanId;
		this.logicalPortID = logicalPortID;
		this.vcpeName = vcpe_name;
		this.releaseVlanType = releaseVlanType;
		this.sarName = sarName;
		this.buildingId = buildingId;
	}

	@Override
	public String toString() {
		return "ReleaseVLANRequest [circuitId=" + circuitId + ", vlanId=" + vlanId + ", logicalPortID=" + logicalPortID
				+ ", vcpe_name=" + vcpeName + ", releaseVlanType=" + releaseVlanType
				+ ", sarName=" + sarName + ", buildingId=" + buildingId + "]";
	}

}
