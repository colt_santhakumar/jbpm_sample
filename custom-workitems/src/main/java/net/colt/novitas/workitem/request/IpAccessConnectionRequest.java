package net.colt.novitas.workitem.request;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import net.colt.novitas.workitems.response.PriceConnectionResponse;

@JsonIgnoreProperties(ignoreUnknown = true)
public class IpAccessConnectionRequest implements Serializable {

	private static final long serialVersionUID = -3719774214294851771L;

	@JsonProperty("ocn")
	private String ocn;

	@JsonProperty("portal_user_id")
	private Integer portalUserId;

	@JsonProperty("action")
	private String action;

	@JsonProperty("connection_id")
	private String connectionId;

	@JsonProperty("connection_name")
	private String connectionName;

	@JsonProperty("price_id")
	private Integer priceId;

	@JsonProperty("from_port_id")
	private String fromPortId;

	@JsonProperty("port_vlan_mapping")
	private String portVlanMapping;

	@JsonProperty("port_vlan_type")
	private String portVlanType;

	@JsonProperty("port_vlan_ids")
	private List<VlanIdRangeRequest> portVlanIds;

	// price details when connection is scheduled
	@JsonProperty("coterminus_option")
	private Boolean coterminusOption;

	@JsonProperty("commitment_period")
	private Integer commitmentPeriod;

	@JsonProperty("rental_currency")
	private String currency;

	@JsonProperty("rental_unit")
	private String rentalUnit;

	@JsonProperty("modification_charge")
	private Float modificationCharge;

	@JsonProperty("rental_charge")
	private Float rentalCharge;

	@JsonProperty("bandwidth")
	private Integer bandwidth;

	@JsonIgnore
	public boolean hasPrice() {
		return priceId != null || (bandwidth != null && commitmentPeriod != null);
	}

	@JsonIgnore
	public PriceConnectionResponse getPrice() {
		PriceConnectionResponse price = null;
		if (bandwidth != null && commitmentPeriod != null) {
			price = new PriceConnectionResponse();
			price.setCommitmentPeriod(commitmentPeriod);
			price.setRentalCurrency(currency);
			price.setRentalUnit(rentalUnit);
			price.setInstallationCurrency(currency);
			price.setModificationCharge(modificationCharge);
			price.setModificationCurrency(currency);
			price.setRentalCharge(rentalCharge);

			// price.setProduct(new Product());
			price.setBandwidth(bandwidth);
		}
		return price;
	}

	public String getOcn() {
		return ocn;
	}

	public void setOcn(String ocn) {
		this.ocn = ocn;
	}

	public Integer getPortalUserId() {
		return portalUserId;
	}

	public void setPortalUserId(Integer portalUserId) {
		this.portalUserId = portalUserId;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getConnectionId() {
		return connectionId;
	}

	public void setConnectionId(String connectionId) {
		this.connectionId = connectionId;
	}

	public String getConnectionName() {
		return connectionName;
	}

	public void setConnectionName(String connectionName) {
		this.connectionName = connectionName;
	}

	public Integer getPriceId() {
		return priceId;
	}

	public void setPriceId(Integer priceId) {
		this.priceId = priceId;
	}

	public String getFromPortId() {
		return fromPortId;
	}

	public void setFromPortId(String fromPortId) {
		this.fromPortId = fromPortId;
	}

	public String getPortVlanMapping() {
		return portVlanMapping;
	}

	public void setPortVlanMapping(String portVlanMapping) {
		this.portVlanMapping = portVlanMapping;
	}

	public String getPortVlanType() {
		return portVlanType;
	}

	public void setPortVlanType(String portVlanType) {
		this.portVlanType = portVlanType;
	}

	public List<VlanIdRangeRequest> getPortVlanIds() {
		return portVlanIds;
	}

	public void setPortVlanIds(List<VlanIdRangeRequest> portVlanIds) {
		this.portVlanIds = portVlanIds;
	}

	public Boolean getCoterminusOption() {
		return coterminusOption;
	}

	public void setCoterminusOption(Boolean coterminusOption) {
		this.coterminusOption = coterminusOption;
	}

	public Integer getCommitmentPeriod() {
		return commitmentPeriod;
	}

	public void setCommitmentPeriod(Integer commitmentPeriod) {
		this.commitmentPeriod = commitmentPeriod;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getRentalUnit() {
		return rentalUnit;
	}

	public void setRentalUnit(String rentalUnit) {
		this.rentalUnit = rentalUnit;
	}

	public Float getModificationCharge() {
		return modificationCharge;
	}

	public void setModificationCharge(Float modificationCharge) {
		this.modificationCharge = modificationCharge;
	}

	public Float getRentalCharge() {
		return rentalCharge;
	}

	public void setRentalCharge(Float rentalCharge) {
		this.rentalCharge = rentalCharge;
	}

	public Integer getBandwidth() {
		return bandwidth;
	}

	public void setBandwidth(Integer bandwidth) {
		this.bandwidth = bandwidth;
	}

	@Override
	public String toString() {
		return "IpAccessConnectionRequest [ocn=" + ocn + ", portalUserId=" + portalUserId + ", action=" + action
				+ ", connectionId=" + connectionId + ", connectionName=" + connectionName + ", priceId=" + priceId
				+ ", fromPortId=" + fromPortId + ", portVlanMapping=" + portVlanMapping + ", portVlanType="
				+ portVlanType + ", portVlanIds=" + portVlanIds + ", coterminusOption=" + coterminusOption
				+ ", commitmentPeriod=" + commitmentPeriod + ", currency=" + currency + ", rentalUnit=" + rentalUnit
				+ ", modificationCharge=" + modificationCharge + ", rentalCharge=" + rentalCharge + ", bandwidth="
				+ bandwidth + "]";
	}

}