package net.colt.novitas.workitem.request;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * {
      "from_port_id": "80000239",
      "to_port_id": "80000299",
      "cloud_provider_from_port": "AWS",
      "cloud_provider_to_port": "AZURE_EXPRESS"
    }
 * @author omerio
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ConnectionPortPair implements Serializable {
    
    /**
	 * 
	 */
	private static final long serialVersionUID = 3965544935266737734L;

	// used only for create
    @JsonProperty("from_port_id")
    private String fromPortId;
    
    @JsonProperty("to_port_id")
    private String toPortId;
    
    // response only (readonly)
    @JsonProperty("from_port_name")
    private String fromPortName;
    
    @JsonProperty("to_port_name")
    private String toPortName;
    
    @JsonProperty("cloud_provider_from_port")
    private String cloudProviderFromPort;
    
    @JsonProperty("cloud_provider_to_port")
    private String cloudProviderToPort;
    
    @JsonProperty("a_end_vlan_mapping")
    private String aEndVlanMapping;
    
    @JsonProperty("b_end_vlan_mapping")
    private String bEndVlanMapping;
    
    @JsonProperty("a_end_vlan_type")
    private String aEndVlanType;
    
    @JsonProperty("b_end_vlan_type")
    private String bEndVlanType;
    
    @JsonProperty("a_end_vlan_ids")
    private List<RequestVLANIdRange> aEndVlanIds;
    
    @JsonProperty("b_end_vlan_ids")
    private List<RequestVLANIdRange> bEndVlanIds;
    
    public String getFromPortId() {
        return fromPortId;
    }

    public void setFromPortId(String fromPortId) {
        this.fromPortId = fromPortId;
    }

    public String getToPortId() {
        return toPortId;
    }

    public void setToPortId(String toPortId) {
        this.toPortId = toPortId;
    }
    
    public String getaEndVlanMapping() {
        return aEndVlanMapping;
    }

    public void setaEndVlanMapping(String aEndVlanMapping) {
        this.aEndVlanMapping = aEndVlanMapping;
    }

    public String getbEndVlanMapping() {
        return bEndVlanMapping;
    }

    public void setbEndVlanMapping(String bEndVlanMapping) {
        this.bEndVlanMapping = bEndVlanMapping;
    }

    public String getaEndVlanType() {
        return aEndVlanType;
    }

    public void setaEndVlanType(String aEndVlanType) {
        this.aEndVlanType = aEndVlanType;
    }

    public String getbEndVlanType() {
        return bEndVlanType;
    }

    public void setbEndVlanType(String bEndVlanType) {
        this.bEndVlanType = bEndVlanType;
    }

    public List<RequestVLANIdRange> getaEndVlanIds() {
        return aEndVlanIds;
    }

    public void setaEndVlanIds(List<RequestVLANIdRange> aEndVlanIds) {
        this.aEndVlanIds = aEndVlanIds;
    }

    public List<RequestVLANIdRange> getbEndVlanIds() {
        return bEndVlanIds;
    }

    public void setbEndVlanIds(List<RequestVLANIdRange> bEndVlanIds) {
        this.bEndVlanIds = bEndVlanIds;
    }

    public String getCloudProviderFromPort() {
        return cloudProviderFromPort;
    }

    public void setCloudProviderFromPort(String cloudProviderFromPort) {
        this.cloudProviderFromPort = cloudProviderFromPort;
    }

    public String getCloudProviderToPort() {
        return cloudProviderToPort;
    }

    public void setCloudProviderToPort(String cloudProviderToPort) {
        this.cloudProviderToPort = cloudProviderToPort;
    }
    
    public String getFromPortName() {
        return fromPortName;
    }

    public void setFromPortName(String fromPortName) {
        this.fromPortName = fromPortName;
    }


    public String getToPortName() {
        return toPortName;
    }

    public void setToPortName(String toPortName) {
        this.toPortName = toPortName;
        
    }

    
    

}
