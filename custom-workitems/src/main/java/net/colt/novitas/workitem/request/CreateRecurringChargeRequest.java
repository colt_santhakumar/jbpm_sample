package net.colt.novitas.workitem.request;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import net.colt.novitas.workitems.BaseWorkItemHandler;

public class CreateRecurringChargeRequest implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@JsonProperty("request_id")
	private Integer requestId;
	@JsonProperty("service_id")
	private String serviceId;
	@JsonProperty("service_instance_id")
	private String serviceInstanceId;
	@JsonProperty("service_instance_type")
	private String serviceInstanceType;
	@JsonProperty("service_instance_name")
	private String serviceInstanceName;
	@JsonProperty("bcn")
	private String bcn;
	@JsonProperty("charge_type")
	private String chargeType;
	@JsonProperty("description")
	private String description;
	@JsonProperty("currency")
	private String currency;
	@JsonProperty("amount")
	private Float amount;
	@JsonProperty("frequency")
	private String frequency;
	@JsonProperty("commitment_type")
	private String commitmentType;
	@JsonProperty("commitment_expiry_date")
	private String commitmentExpiryDate;
	
	public Integer getRequestId() {
		return requestId;
	}
	public void setRequestId(Integer requestId) {
		this.requestId = requestId;
	}
	public String getServiceId() {
		return serviceId;
	}
	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}
	public String getServiceInstanceId() {
		return serviceInstanceId;
	}
	public void setServiceInstanceId(String serviceInstanceId) {
		this.serviceInstanceId = serviceInstanceId;
	}
	public String getServiceInstanceType() {
		return serviceInstanceType;
	}
	public void setServiceInstanceType(String serviceInstanceType) {
		this.serviceInstanceType = serviceInstanceType;
	}
	public String getServiceInstanceName() {
		return serviceInstanceName;
	}
	public void setServiceInstanceName(String serviceInstanceName) {
		this.serviceInstanceName = serviceInstanceName;
	}
	public String getBcn() {
		return bcn;
	}
	public void setBcn(String bcn) {
		this.bcn = bcn;
	}
	public String getChargeType() {
		return chargeType;
	}
	public void setChargeType(String chargeType) {
		this.chargeType = chargeType;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public Float getAmount() {
		return amount;
	}
	public void setAmount(Float amount) {
		this.amount = amount;
	}
	public String getFrequency() {
		return frequency;
	}
	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}
	public String getCommitmentType() {
		return commitmentType;
	}
	public void setCommitmentType(String commitmentType) {
		this.commitmentType = commitmentType;
	}
	public String getCommitmentExpiryDate() {
		return commitmentExpiryDate;
	}
	public void setCommitmentExpiryDate(String commitmentExpiryDate) {
		this.commitmentExpiryDate = commitmentExpiryDate;
	}
	
	@Override
	public String toString() {
		return BaseWorkItemHandler.toString(this);
	}
    
	
	

}
