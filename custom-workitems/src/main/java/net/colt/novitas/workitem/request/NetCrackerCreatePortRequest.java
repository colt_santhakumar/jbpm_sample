package net.colt.novitas.workitem.request;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import net.colt.novitas.workitems.BaseWorkItemHandler;

@JsonIgnoreProperties(ignoreUnknown = true)
public class NetCrackerCreatePortRequest  implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

    @JsonProperty("ocn")
	private String ocn;  //party id
    
	@JsonProperty("customer_name")
	private String customerName; // party name
	
	@JsonProperty("service_id")
	private String serviceId;   // novitas port service id
	
	@JsonProperty("service_order_reference")
	private String serviceOrderReference;  // novitas request id
	
	@JsonProperty("csp_interconnect_ref")
	private String cspInterConnRef; //CSP Interconnect Reference
	
	@JsonProperty("aws_account_no")
	private String awsAccountNo; //Customer AWS account no in case of Hosted   CSP Customer ID
	
	@JsonProperty("bandwidth")
	private Integer bandwidth;
	
	@JsonProperty("azure_vlan")
	private String vlanTagId;

	@JsonProperty("cap_region")
	private String cspRegion; // Amazon Region
	
	private String buildingId; // required only for AWS Hosted
	
	private String siteId; // required only for AWS Dedicated
	
	@JsonProperty("callback_url")
	private String callBackUrl;  
	
	@JsonProperty("connector")
	private String connector;   // Connector Type
	
	@JsonProperty("technology")
	private String technology;   //Presentation Interface
	
	@JsonProperty("xng_migration_reference")
	private String xngMigrationReference;

	@JsonProperty("novitas_migration_reference")
	private String novitasMigrationReference;
	
	@JsonProperty("csp_key")
	private String cspKey;
	
    
	public String getCspKey() {
		return cspKey;
	}

	public void setCspKey(String cspKey) {
		this.cspKey = cspKey;
	}

	public String getOcn() {
		return ocn;
	}

	public void setOcn(String ocn) {
		this.ocn = ocn;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public String getServiceOrderReference() {
		return serviceOrderReference;
	}

	public void setServiceOrderReference(String serviceOrderReference) {
		this.serviceOrderReference = serviceOrderReference;
	}

	public String getCspInterConnRef() {
		return cspInterConnRef;
	}

	public void setCspInterConnRef(String cspInterConnRef) {
		this.cspInterConnRef = cspInterConnRef;
	}

	public String getAwsAccountNo() {
		return awsAccountNo;
	}

	public void setAwsAccountNo(String awsAccountNo) {
		this.awsAccountNo = awsAccountNo;
	}

	public Integer getBandwidth() {
		return bandwidth;
	}

	public void setBandwidth(Integer bandwidth) {
		this.bandwidth = bandwidth;
	}

	public String getVlanTagId() {
		return vlanTagId;
	}

	public void setVlanTagId(String vlanTagId) {
		this.vlanTagId = vlanTagId;
	}

	public String getCspRegion() {
		return cspRegion;
	}

	public void setCspRegion(String cspRegion) {
		this.cspRegion = cspRegion;
	}

	public String getBuildingId() {
		return buildingId;
	}

	public void setBuildingId(String buildingId) {
		this.buildingId = buildingId;
	}

	public String getSiteId() {
		return siteId;
	}

	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}

	public String getCallBackUrl() {
		return callBackUrl;
	}

	public void setCallBackUrl(String callBackUrl) {
		this.callBackUrl = callBackUrl;
	}

	public String getConnector() {
		return connector;
	}

	public void setConnector(String connector) {
		this.connector = connector;
	}

	public String getTechnology() {
		return technology;
	}

	public void setTechnology(String technology) {
		this.technology = technology;
	}

	public String getXngMigrationReference() {
		return xngMigrationReference;
	}

	public void setXngMigrationReference(String xngMigrationReference) {
		this.xngMigrationReference = xngMigrationReference;
	}

	public String getNovitasMigrationReference() {
		return novitasMigrationReference;
	}

	public void setNovitasMigrationReference(String novitasMigrationReference) {
		this.novitasMigrationReference = novitasMigrationReference;
	}

	@Override
	public String toString() {
		return BaseWorkItemHandler.toString(this);
	}

	
	

	
}
