package net.colt.novitas.workitem.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UpdateNNIRequest {

	@JsonProperty("old_nni_bandwidth")
	private Integer oldNNIBandwidth;

	@JsonProperty("new_nni_bandwidth")
	private Integer newNNIBandwidth;

	public UpdateNNIRequest() {
		super();
	}

	public Integer getOldNNIBandwidth() {
		return oldNNIBandwidth;
	}

	public void setOldNNIBandwidth(Integer oldNNIBandwidth) {
		this.oldNNIBandwidth = oldNNIBandwidth;
	}

	public Integer getNewNNIBandwidth() {
		return newNNIBandwidth;
	}

	public void setNewNNIBandwidth(Integer newNNIBandwidth) {
		this.newNNIBandwidth = newNNIBandwidth;
	}

}
