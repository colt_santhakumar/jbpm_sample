package net.colt.novitas.workitem.request;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import net.colt.novitas.workitems.BaseWorkItemHandler;

@JsonIgnoreProperties(ignoreUnknown=true)
public class NotificationSignalDetailsRequest implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty("id")
	private Integer id;
	
	@JsonProperty("request_id")
	private Integer requestId;
	
	@JsonProperty("process_instance_id")
	private long processInstanceId;
	
	@JsonProperty("status")
	private String status;
	
	@JsonProperty("status_description")
	private String statusDescription;
	
	@JsonProperty("signal_name")
	private String signalName;
	
	@JsonProperty("deployment_id")
	private String deploymentId;
	
	@JsonProperty("jbpm_url")
	private String jbpmUrl;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getRequestId() {
		return requestId;
	}

	public void setRequestId(Integer requestId) {
		this.requestId = requestId;
	}

	public long getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(long processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatusDescription() {
		return statusDescription;
	}

	public void setStatusDescription(String statusDescription) {
		this.statusDescription = statusDescription;
	}

	public String getSignalName() {
		return signalName;
	}

	public void setSignalName(String signalName) {
		this.signalName = signalName;
	}

	public String getDeploymentId() {
		return deploymentId;
	}

	public void setDeploymentId(String deploymentId) {
		this.deploymentId = deploymentId;
	}

	public String getJbpmUrl() {
		return jbpmUrl;
	}

	public void setJbpmUrl(String jbpmUrl) {
		this.jbpmUrl = jbpmUrl;
	}

	@Override
	public String toString() {
		return BaseWorkItemHandler.toString(this);
	}
	
	

}
