package net.colt.novitas.workitem.request;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@Getter
@Setter
@ToString
public class CreatePortService implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3506455576007937237L;

	@JsonProperty("name")
	private String portName;
	private Integer bandwidth;
	@JsonProperty("service_id")
	private String novitasServiceId;
	@JsonProperty("csp_key")
	private String pairingKey;
	private String ocn;
	private String customerName;
	private String currency;
	private String rentalUnit;
	private Float installationCharge;
	private Float decommissioningCharge;
	private Date commitmentExpiry;
	private String cloudRegion;

	public String getPortName() {
		return portName;
	}

	public void setPortName(String portName) {
		this.portName = portName;
	}

	public Integer getBandwidth() {
		return bandwidth;
	}

	public void setBandwidth(Integer bandwidth) {
		this.bandwidth = bandwidth;
	}

	public String getNovitasServiceId() {
		return novitasServiceId;
	}

	public void setNovitasServiceId(String novitasServiceId) {
		this.novitasServiceId = novitasServiceId;
	}

	public String getPairingKey() {
		return pairingKey;
	}

	public void setPairingKey(String pairingKey) {
		this.pairingKey = pairingKey;
	}

	public String getOcn() {
		return ocn;
	}

	public void setOcn(String ocn) {
		this.ocn = ocn;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getRentalUnit() {
		return rentalUnit;
	}

	public void setRentalUnit(String rentalUnit) {
		this.rentalUnit = rentalUnit;
	}

	public Float getInstallationCharge() {
		return installationCharge;
	}

	public void setInstallationCharge(Float installationCharge) {
		this.installationCharge = installationCharge;
	}

	public Float getDecommissioningCharge() {
		return decommissioningCharge;
	}

	public void setDecommissioningCharge(Float decommissioningCharge) {
		this.decommissioningCharge = decommissioningCharge;
	}

	public Date getCommitmentExpiry() {
		return commitmentExpiry;
	}

	public void setCommitmentExpiry(Date commitmentExpiry) {
		this.commitmentExpiry = commitmentExpiry;
	}

	public String getCloudRegion() {
		return cloudRegion;
	}

	public void setCloudRegion(String cloudRegion) {
		this.cloudRegion = cloudRegion;
	}

}
