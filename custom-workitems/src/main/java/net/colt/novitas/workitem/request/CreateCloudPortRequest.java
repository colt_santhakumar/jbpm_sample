package net.colt.novitas.workitem.request;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

import net.colt.novitas.workitems.BaseWorkItemHandler;

public class CreateCloudPortRequest implements Serializable {

	private static final long serialVersionUID = 2104343630728333762L;

	@JsonProperty("name")
	private String name;

	@JsonProperty("location")
	private String location;

	@JsonProperty("bandwidth")
	private Integer bandwidth;

	@JsonProperty("rental_charge")
	private Float rentalCharge;

	@JsonProperty("rental_unit")
	private String rentalUnit;

	@JsonProperty("rental_currency")
	private String rentalCurrency;
	
	@JsonProperty("currency")
	private String currency;

	@JsonProperty("address")
	private String address;

	@JsonProperty("site_type")
	private String siteType;
	
	@JsonProperty("csp_key")
	private String pairingKey;

	@JsonProperty("commitment_expiry")
	private Date commitmentExpiry;

	@JsonProperty("service_id")
	private String serviceId;

	@JsonProperty("installation_charge")
	protected Float installationCharge;

	@JsonProperty("installation_currency")
	protected String installationCurrency;

	@JsonProperty("decommissioning_charge")
	private Float decommissioningCharge;

	@JsonProperty("decommissioning_currency")
	private String decommissioningCurrency;

	@JsonProperty("customer_name")
	private String customerName;

	@JsonProperty("ocn")
	private String ocn;

	@JsonProperty("site_floor")
	private String siteFloor;

	@JsonProperty("site_room_name")
	private String siteRoomName;

	@JsonProperty("location_premises_number")
	private String locationPremisesNumber;

	@JsonProperty("location_building_name")
	private String locationBuildingName;

	@JsonProperty("location_street_name")
	private String locationStreetName;

	@JsonProperty("location_city")
	private String locationCity;

	@JsonProperty("location_state")
	private String locationState;

	@JsonProperty("location_country")
	private String locationCountry;

	@JsonProperty("postal_zip_code")
	private String postalZipCode;

	@JsonProperty("latitude")
	private Float latitude;

	@JsonProperty("longitude")
	private Float longitude;

	@JsonProperty("location_id")
	private String locationId;

	@JsonProperty("cloud_provider")
	private String cloudProvider;

	@JsonProperty("azure_service_key")
	private String serviceKey;

	@JsonProperty("port_1")
	private String port1;

	@JsonProperty("port_2")
	private String port2;

	@JsonProperty("vlan")
	private Integer vlan;

	@JsonProperty("resource_id_1")
	private String resourceId1;

	@JsonProperty("resource_id_2")
	private String resourceId2;

	@JsonProperty("aws_account_no")
	private String awsAccountno;

	@JsonProperty("aws_ref")
	private String awsRef;

	@JsonProperty("aws_region")
	private String region;
    
	@JsonProperty("cloud_region")
	private String cloudRegion;
	
	@JsonProperty("expiration_period")
	private Integer portExpiryPeriod;

	@JsonProperty("expires_on")
	private String portExpiresOn;

	@JsonProperty("in_use")
	private Boolean isPortInUse;

	@JsonProperty("local_building_name")
	private String localBuildingName;

	@JsonProperty("location_city_code")
	private String locationCityCode;

	@JsonProperty("location_country_code")
	private String locationCountryCode;
	
	@JsonProperty("resource_port_name_1")
	private String resourcePortName1;
    
    @JsonProperty("resource_port_name_2")
   	private String resourcePortName2;
    
    @JsonProperty("building_id")
	private String buildingId;
    
	public String getBuildingId() {
		return buildingId;
	}

	public void setBuildingId(String buildingId) {
		this.buildingId = buildingId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public Integer getBandwidth() {
		return bandwidth;
	}

	public void setBandwidth(Integer bandwidth) {
		this.bandwidth = bandwidth;
	}

	public Float getRentalCharge() {
		return rentalCharge;
	}

	public void setRentalCharge(Float rentalCharge) {
		this.rentalCharge = rentalCharge;
	}

	public String getRentalUnit() {
		return rentalUnit;
	}

	public void setRentalUnit(String rentalUnit) {
		this.rentalUnit = rentalUnit;
	}

	public String getRentalCurrency() {
		return rentalCurrency;
	}

	public void setRentalCurrency(String rentalCurrency) {
		this.rentalCurrency = rentalCurrency;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getSiteType() {
		return siteType;
	}

	public void setSiteType(String siteType) {
		this.siteType = siteType;
	}

	public Date getCommitmentExpiry() {
		return commitmentExpiry;
	}

	public void setCommitmentExpiry(Date commitmentExpiry) {
		this.commitmentExpiry = commitmentExpiry;
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public Float getDecommissioningCharge() {
		return decommissioningCharge;
	}

	public void setDecommissioningCharge(Float decommissioningCharge) {
		this.decommissioningCharge = decommissioningCharge;
	}

	public String getDecommissioningCurrency() {
		return decommissioningCurrency;
	}

	public void setDecommissioningCurrency(String decommissioningCurrency) {
		this.decommissioningCurrency = decommissioningCurrency;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getOcn() {
		return ocn;
	}

	public void setOcn(String ocn) {
		this.ocn = ocn;
	}

	public String getSiteFloor() {
		return siteFloor;
	}

	public void setSiteFloor(String siteFloor) {
		this.siteFloor = siteFloor;
	}

	public String getSiteRoomName() {
		return siteRoomName;
	}

	public void setSiteRoomName(String siteRoomName) {
		this.siteRoomName = siteRoomName;
	}

	public String getLocationPremisesNumber() {
		return locationPremisesNumber;
	}

	public void setLocationPremisesNumber(String locationPremisesNumber) {
		this.locationPremisesNumber = locationPremisesNumber;
	}

	public String getLocationBuildingName() {
		return locationBuildingName;
	}

	public void setLocationBuildingName(String locationBuildingName) {
		this.locationBuildingName = locationBuildingName;
	}

	public String getLocationStreetName() {
		return locationStreetName;
	}

	public void setLocationStreetName(String locationStreetName) {
		this.locationStreetName = locationStreetName;
	}

	public String getLocationCity() {
		return locationCity;
	}

	public void setLocationCity(String locationCity) {
		this.locationCity = locationCity;
	}

	public String getLocationState() {
		return locationState;
	}

	public void setLocationState(String locationState) {
		this.locationState = locationState;
	}

	public String getLocationCountry() {
		return locationCountry;
	}

	public void setLocationCountry(String locationCountry) {
		this.locationCountry = locationCountry;
	}

	public String getPostalZipCode() {
		return postalZipCode;
	}

	public void setPostalZipCode(String postalZipCode) {
		this.postalZipCode = postalZipCode;
	}

	public Float getLatitude() {
		return latitude;
	}

	public void setLatitude(Float latitude) {
		this.latitude = latitude;
	}

	public Float getLongitude() {
		return longitude;
	}

	public void setLongitude(Float longitude) {
		this.longitude = longitude;
	}

	public String getLocationId() {
		return locationId;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	public String getCloudProvider() {
		return cloudProvider;
	}

	public void setCloudProvider(String cloudProvider) {
		this.cloudProvider = cloudProvider;
	}

	public String getServiceKey() {
		return serviceKey;
	}

	public void setServiceKey(String serviceKey) {
		this.serviceKey = serviceKey;
	}

	public String getPort1() {
		return port1;
	}

	public void setPort1(String port1) {
		this.port1 = port1;
	}

	public String getPort2() {
		return port2;
	}

	public void setPort2(String port2) {
		this.port2 = port2;
	}

	public Integer getVlan() {
		return vlan;
	}

	public void setVlan(Integer vlan) {
		this.vlan = vlan;
	}

	public String getResourceId1() {
		return resourceId1;
	}

	public void setResourceId1(String resourceId1) {
		this.resourceId1 = resourceId1;
	}

	public String getResourceId2() {
		return resourceId2;
	}

	public void setResourceId2(String resourceId2) {
		this.resourceId2 = resourceId2;
	}

	public String getAwsAccountno() {
		return awsAccountno;
	}

	public void setAwsAccountno(String awsAccountno) {
		this.awsAccountno = awsAccountno;
	}

	public Float getInstallationCharge() {
		return installationCharge;
	}

	public void setInstallationCharge(Float installationCharge) {
		this.installationCharge = installationCharge;
	}

	public String getInstallationCurrency() {
		return installationCurrency;
	}

	public void setInstallationCurrency(String installationCurrency) {
		this.installationCurrency = installationCurrency;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getAwsRef() {
		return awsRef;
	}

	public void setAwsRef(String awsRef) {
		this.awsRef = awsRef;
	}

	public Integer getPortExpiryPeriod() {
		return portExpiryPeriod;
	}

	public void setPortExpiryPeriod(Integer portExpiryPeriod) {
		this.portExpiryPeriod = portExpiryPeriod;
	}

	public String getPortExpiresOn() {
		return portExpiresOn;
	}

	public void setPortExpiresOn(String portExpiresOn) {
		this.portExpiresOn = portExpiresOn;
	}

	public Boolean getIsPortInUse() {
		return isPortInUse;
	}

	public void setIsPortInUse(Boolean isPortInUse) {
		this.isPortInUse = isPortInUse;
	}

	public String getLocalBuildingName() {
		return localBuildingName;
	}

	public void setLocalBuildingName(String localBuildingName) {
		this.localBuildingName = localBuildingName;
	}

	public String getLocationCityCode() {
		return locationCityCode;
	}

	public void setLocationCityCode(String locationCityCode) {
		this.locationCityCode = locationCityCode;
	}

	public String getLocationCountryCode() {
		return locationCountryCode;
	}

	public void setLocationCountryCode(String locationCountryCode) {
		this.locationCountryCode = locationCountryCode;
	}
	
	

	public String getResourcePortName1() {
		return resourcePortName1;
	}

	public void setResourcePortName1(String resourcePortName1) {
		this.resourcePortName1 = resourcePortName1;
	}

	public String getResourcePortName2() {
		return resourcePortName2;
	}

	public void setResourcePortName2(String resourcePortName2) {
		this.resourcePortName2 = resourcePortName2;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getPairingKey() {
		return pairingKey;
	}

	public void setPairingKey(String pairingKey) {
		this.pairingKey = pairingKey;
	}
	
	public String getCloudRegion() {
		return cloudRegion;
	}

	public void setCloudRegion(String cloudRegion) {
		this.cloudRegion = cloudRegion;
	}

	@Override
	public String toString() {
		return BaseWorkItemHandler.toString(this);
	}
    


}
