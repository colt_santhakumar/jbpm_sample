package net.colt.novitas.workitem.request;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AllocateSubnetInternalRequest implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonProperty("ocn")
	private String ocn;

	@JsonProperty("country")
	private String countryCode;

	@JsonProperty("metro")
	String cityCode;

	@JsonProperty("customer_name")
	String custName;

	@JsonProperty("service_id")
	String serviceId;

	@JsonProperty("cct_name")
	String circuitName;

	@JsonProperty("network_type")
	String networkType;
	
	@JsonProperty("site_type")
	String siteType;
	
	@JsonProperty("bandwidth")
	String bandwidth;

	public String getOcn() {
		return ocn;
	}

	public void setOcn(String ocn) {
		this.ocn = ocn;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getCityCode() {
		return cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}

	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public String getCircuitName() {
		return circuitName;
	}

	public void setCircuitName(String circuitName) {
		this.circuitName = circuitName;
	}

	public String getNetworkType() {
		return networkType;
	}

	public void setNetworkType(String networkType) {
		this.networkType = networkType;
	}

	public String getSiteType() {
		return siteType;
	}

	public void setSiteType(String siteType) {
		this.siteType = siteType;
	}

	public String getBandwidth() {
		return bandwidth;
	}

	public void setBandwidth(String bandwidth) {
		this.bandwidth = bandwidth;
	}
	
	

}
