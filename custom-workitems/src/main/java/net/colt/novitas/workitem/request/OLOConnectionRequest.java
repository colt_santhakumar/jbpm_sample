package net.colt.novitas.workitem.request;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class OLOConnectionRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty("request_id")
	private String requestId;

	@JsonProperty("port_id")
	private String portId;

	@JsonProperty("enni_id")
	private String enniId;

	@JsonProperty("port_vlans")
	private List<Integer> portVlans;

	@JsonProperty("port_vlan_mapping")
	private String portVlanMapping;

	@JsonProperty("port_vlan_type")
	private String portVlanType;

	@JsonProperty("enni_vlans")
	private int enniVlans;

	@JsonProperty("enni_vlan_mapping")
	private String enniVlanMapping;

	@JsonProperty("enni_vlan_type")
	private String enniVlanType;

	@JsonProperty("cir")
	private int cir;

	public OLOConnectionRequest() {
		super();

	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public String getPortId() {
		return portId;
	}

	public void setPortId(String portId) {
		this.portId = portId;
	}

	public String getEnniId() {
		return enniId;
	}

	public void setEnniId(String enniId) {
		this.enniId = enniId;
	}

	public List<Integer> getPortVlans() {
		return portVlans;
	}

	public void setPortVlans(List<Integer> portVlans) {
		this.portVlans = portVlans;
	}

	public String getPortVlanMapping() {
		return portVlanMapping;
	}

	public void setPortVlanMapping(String portVlanMapping) {
		this.portVlanMapping = portVlanMapping;
	}

	public String getPortVlanType() {
		return portVlanType;
	}

	public void setPortVlanType(String portVlanType) {
		this.portVlanType = portVlanType;
	}

	public int getEnniVlans() {
		return enniVlans;
	}

	public void setEnniVlans(int enniVlans) {
		this.enniVlans = enniVlans;
	}

	public String getEnniVlanMapping() {
		return enniVlanMapping;
	}

	public void setEnniVlanMapping(String enniVlanMapping) {
		this.enniVlanMapping = enniVlanMapping;
	}

	public String getEnniVlanType() {
		return enniVlanType;
	}

	public void setEnniVlanType(String enniVlanType) {
		this.enniVlanType = enniVlanType;
	}

	public int getCir() {
		return cir;
	}

	public void setCir(int cir) {
		this.cir = cir;
	}

	@Override
	public String toString() {
		return "ConnectionRequest [requestId=" + requestId + ", portId="
				+ portId + ", enniId=" + enniId + ", portVlans=" + portVlans
				+ ", portVlanMapping=" + portVlanMapping + ", portVlanType="
				+ portVlanType + ", enniVlans=" + enniVlans
				+ ", enniVlanMapping=" + enniVlanMapping + ", enniVlanType="
				+ enniVlanType + ", cir=" + cir + "]";
	}

}
