package net.colt.novitas.workitem.request;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import net.colt.novitas.workitems.BaseWorkItemHandler;

@JsonIgnoreProperties(ignoreUnknown = true)
public class NotificationUpdateRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	

	@JsonProperty("request_id")
	private Integer requestId;
	
	@JsonProperty("order_number")
	private String orderNumber;
	
	@JsonProperty("order_status")
	private String orderStatus;
	
	@JsonProperty("cpd")
	private String cpd;
	
	@JsonProperty("request_satus")
	private String requestSatus;
	
	@JsonProperty("circuit_id")
	private String circuitId;
	
	@JsonProperty("nc_tech_service_id")
	private String ncTechServiceId;

	public Integer getRequestId() {
		return requestId;
	}

	public void setRequestId(Integer requestId) {
		this.requestId = requestId;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public String getCpd() {
		return cpd;
	}

	public void setCpd(String cpd) {
		this.cpd = cpd;
	}

	public String getRequestSatus() {
		return requestSatus;
	}

	public void setRequestSatus(String requestSatus) {
		this.requestSatus = requestSatus;
	}

	public String getCircuitId() {
		return circuitId;
	}

	public void setCircuitId(String circuitId) {
		this.circuitId = circuitId;
	}

	public String getNcTechServiceId() {
		return ncTechServiceId;
	}

	public void setNcTechServiceId(String ncTechServiceId) {
		this.ncTechServiceId = ncTechServiceId;
	}

	@Override
	public String toString() {
		return BaseWorkItemHandler.toString(this);
	}

}
