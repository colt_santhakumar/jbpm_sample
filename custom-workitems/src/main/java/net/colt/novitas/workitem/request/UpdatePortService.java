package net.colt.novitas.workitem.request;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@Getter
@Setter
@ToString
public class UpdatePortService implements Serializable {

	private static final long serialVersionUID = 2381759816434385064L;

	private String status;
	private String serviceId;
	private String ocn;
	private String locationId;
	@JsonProperty("site_type")
	private String novitasSiteType;
	private Integer vlan;
	private String interconnect;
	private String interconnectAttachmentName;
	@JsonProperty("location_city_code")
	private String cityCode;
	@JsonProperty("location_country_code")
	private String countryCode;
	@JsonProperty("resoure_id_1")
	private String circuitReference;
	private String ncTechServiceId;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public String getOcn() {
		return ocn;
	}

	public void setOcn(String ocn) {
		this.ocn = ocn;
	}

	public String getLocationId() {
		return locationId;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	public String getNovitasSiteType() {
		return novitasSiteType;
	}

	public void setNovitasSiteType(String novitasSiteType) {
		this.novitasSiteType = novitasSiteType;
	}

	public Integer getVlan() {
		return vlan;
	}

	public void setVlan(Integer vlan) {
		this.vlan = vlan;
	}

	public String getInterconnect() {
		return interconnect;
	}

	public void setInterconnect(String interconnect) {
		this.interconnect = interconnect;
	}

	public String getInterconnectAttachmentName() {
		return interconnectAttachmentName;
	}

	public void setInterconnectAttachmentName(String interconnectAttachmentName) {
		this.interconnectAttachmentName = interconnectAttachmentName;
	}

	public String getCityCode() {
		return cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getCircuitReference() {
		return circuitReference;
	}

	public void setCircuitReference(String circuitReference) {
		this.circuitReference = circuitReference;
	}

	public String getNcTechServiceId() {
		return ncTechServiceId;
	}

	public void setNcTechServiceId(String ncTechServiceId) {
		this.ncTechServiceId = ncTechServiceId;
	}

}
