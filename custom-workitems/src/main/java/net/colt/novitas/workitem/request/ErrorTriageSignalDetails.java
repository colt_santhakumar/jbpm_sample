 package net.colt.novitas.workitem.request;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ErrorTriageSignalDetails implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;


	private String action;

	private String ncTechnicalServiceId1;

	private String ncTechnicalServiceId2;

	private String ppDetails;

	private String siteId;

	private String vlan;

	private String awsRef;

	private String circuitId;

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getNcTechnicalServiceId1() {
		return ncTechnicalServiceId1;
	}

	public void setNcTechnicalServiceId1(String ncTechnicalServiceId1) {
		this.ncTechnicalServiceId1 = ncTechnicalServiceId1;
	}

	public String getNcTechnicalServiceId2() {
		return ncTechnicalServiceId2;
	}

	public void setNcTechnicalServiceId2(String ncTechnicalServiceId2) {
		this.ncTechnicalServiceId2 = ncTechnicalServiceId2;
	}

	public String getPpDetails() {
		return ppDetails;
	}

	public void setPpDetails(String ppDetails) {
		this.ppDetails = ppDetails;
	}

	public String getSiteId() {
		return siteId;
	}

	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}

	public String getVlan() {
		return vlan;
	}

	public void setVlan(String vlan) {
		this.vlan = vlan;
	}

	public String getAwsRef() {
		return awsRef;
	}

	public void setAwsRef(String awsRef) {
		this.awsRef = awsRef;
	}

	public String getCircuitId() {
		return circuitId;
	}

	public void setCircuitId(String circuitId) {
		this.circuitId = circuitId;
	}

	@Override
	public String toString() {
		return "ErrorTriageSignalDetails [action=" + action + ", ncTechnicalServiceId1=" + ncTechnicalServiceId1
				+ ", ncTechnicalServiceId2=" + ncTechnicalServiceId2 + ", ppDetails=" + ppDetails + ", siteId=" + siteId
				+ ", vlan=" + vlan + ", awsRef=" + awsRef + ", circuitId=" + circuitId + "]";
	}


}
