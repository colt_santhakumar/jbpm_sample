package net.colt.novitas.workitem.request;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import net.colt.novitas.workitems.BaseWorkItemHandler;


public class SiteTypeRequest implements Serializable {

	private static final long serialVersionUID = 1077361349055646395L;
	private String pmCompositeSiteType;

	public SiteTypeRequest() {
		super();
	}
	
	@JsonProperty("pm_composite_site_type")
	public String getPmCompositeSiteType() {
		return pmCompositeSiteType;
	}

	public void setPmCompositeSiteType(String pmCompositeSiteType) {
		this.pmCompositeSiteType = pmCompositeSiteType;
	}

	@Override
	public String toString() {
		return BaseWorkItemHandler.toString(this);
	}

}
