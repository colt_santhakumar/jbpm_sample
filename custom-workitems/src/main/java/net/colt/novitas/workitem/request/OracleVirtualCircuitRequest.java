package net.colt.novitas.workitem.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
@ToString
public class OracleVirtualCircuitRequest implements Serializable {

	private static final long serialVersionUID = -6027449931700345585L;

	private String id;

	private List<OracleCrossConnectMappingRequest> crossConnectMappings;

	private String providerState;

	private String bandwidthShapeName;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<OracleCrossConnectMappingRequest> getCrossConnectMappings() {
		return crossConnectMappings;
	}

	public void setCrossConnectMappings(List<OracleCrossConnectMappingRequest> crossConnectMappings) {
		this.crossConnectMappings = crossConnectMappings;
	}

	public String getProviderState() {
		return providerState;
	}

	public void setProviderState(String providerState) {
		this.providerState = providerState;
	}

	public String getBandwidthShapeName() {
		return bandwidthShapeName;
	}

	public void setBandwidthShapeName(String bandwidthShapeName) {
		this.bandwidthShapeName = bandwidthShapeName;
	}

}
